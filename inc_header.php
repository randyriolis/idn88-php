<?php 
	$qu = mysqli_query($conn, "SELECT * FROM _users WHERE kduser = $iduser ");
	$res = mysqli_fetch_array($qu);	
	
	$access = $res['link_assigned'];
	$access = explode(",",$access);	
	
	if($res['isactive'] == 0) {
		$string = '<script type="text/javascript">';
		$string .= 'window.location = "dologout.php"';
		$string .= '</script>';

		echo $string;
		exit();
	}		
	
?>

<script src="js/jquery.js"></script>


<script>
	var currentNumber = [];
	
	function updateNumber(dataIndex, newTotal){
		// the difference between NEW and OLD+rather NEW
		var indexSound = dataIndex + "_sound";
		var myAudio = $("#"+indexSound);
		
		var curDelay = myAudio.attr("data-delay");
		curDelay --;
		
		if(newTotal > 0){
			$("#" + dataIndex).find("span").html(newTotal);
			
			// console.log(dataIndex+" = "+newTotal);
			
			if(curDelay <= 0) {
				myAudio[0].play();
				
				curDelay = 5;
			}
		}
		else 
			$("#" + dataIndex).find("span").html("");
			
			
		myAudio.attr("data-delay", curDelay);
	}
	
	function addTimeout(){
			
		// Send the data using post
		var posting = $.get( "doheader_query.php" );
					 
		// Put the results in a div
		posting.done(function( data ) {									
			var splt = data.split(",");
			
			updateNumber("total_member", splt[0]);
			updateNumber("total_deposit", splt[1]);
			updateNumber("total_withdraw", splt[2]);		

			console.log( "member:" + splt[0] + ", deposit:" + splt[1] + ", withdraw:" + splt[2] );

			setTimeout(function(){
				addTimeout();
			}, 1000);
		});		
	}
	
	$(document).ready(function(){
		addTimeout();
	} );	
</script>

<header class="main-header">

    <a class="logo" href="site.php"><span class="logo-mini">APP</span><span class="logo-lg">Admin Panel</span></a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
		
		<div class="" style="float: left; ">
			<ul class="nav navbar-nav">
			
				<li class="dropdown user user-menu" >
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" > 
					<i class="fa fa-map-o fa-lg"></i> 
						<?php 							
							
							$templink = $_SESSION['alpha_admb'];							
							if(count($access) <= 2 && $templink <= 0 && $access[0] != null){
								$templink = $access[0];
							}	
							
							$aa = mysqli_query($conn, "SELECT * FROM link WHERE idlink=$templink");
							$bla = mysqli_fetch_array($aa);
																
							if($templink == 0) {
								echo 'All Link';
							}
							else {															
								echo $bla['namalink'];
							}							
						?>
					</a>
					
					<?php 
						if($res['superadmin'] == 1 || $res['superadmin'] == 11) {
					?>
					<ul class="dropdown-menu" style="width:150px;">
						<!-- User image -->						
						<li class="user-body">
							<div class="pull-right">
							<p><a class="dropdown-toggle" href="dolink_switch.php?admb=0&back=<?php echo $act ?> "><button style="width:120px;" type="button" class="btn btn-primary">All Link</button></a></p>
							
							<?php
								$aa = mysqli_query($conn, "SELECT * FROM link where status = 1");
								while($row = mysqli_fetch_array($aa)) {
									echo '<p><a class="dropdown-toggle" href="dolink_switch.php?admb='.$row['idlink'].'&back='.$act.' "><button style="width:120px;" type="button" class="btn btn-primary">'.$row['namalink'].'</button></a></p>';									
								}
							?>
							</div>
						</li>
						
						<!-- Menu Body -->

						<!-- Menu Footer-->
						
					</ul>
					
					<?php } else  { ?>
						<ul class="dropdown-menu" style="width:150px;">
							<!-- User image -->
							<li class="user-body">
								<div class="pull-right">
									
									<?php if($res['superadmin'] != 5 && count($access) > 2) { ?>
										<p><a class="dropdown-toggle" href="dolink_switch.php?admb=0&back=<?php echo $act ?> "><button style="width:120px;" type="button" class="btn btn-primary">All Link</button></a></p>
									<?php } ?>
								
									<?php
										$qq = "SELECT * FROM link where status = 1";
										$aa = mysqli_query($conn, $qq);
																	
										while($row = mysqli_fetch_array($aa)) {
																				
											foreach ($access as $value) {									
												if($value == $row['idlink']) {
													echo '<p><a class="dropdown-toggle" href="dolink_switch.php?admb='.$value.'&back='.$act.' "><button style="width:120px;" type="button" class="btn btn-primary">'.$row['namalink'].'</button></a></p>';																					
												}
											}
										}
									?>
									
								</div>
							</li>
							
							<!-- Menu Body -->

							<!-- Menu Footer-->
							
						</ul>
					<?php } ?>
					
				</li>
			</ul>
		</div>
		
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <li>
                    <a href="member.php" target="_parent">
						<i class="fa fa-user-circle fa-lg"></i> Member
						<div id="total_member" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
							<span class="label label-success"></span>
						</div>                    
					</a>
                </li>
				
                <li class="notifications-menu">
                    <a href="deposit.php">
					<i class="fa fa-money fa-lg"></i> Deposit 
					<div id="total_deposit" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
						<span class="label label-warning"></span>                        
					</div>                    
					</a>
                </li>
                
				<!-- Tasks: style can be found in dropdown.less -->
                <li class="tasks-menu">
                    <a href="withdraw.php">
					<i class="fa fa-share-square-o fa-lg"></i> Withdraw
					<div id="total_withdraw" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
						<span class="label label-danger"></span>
					</div>
					</a>
                </li>
				
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">                     
                        <span class="hidden-xs"><?php echo $alpha_username; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
                            <p>
                                <?php echo $alpha_username; ?>
								<small></small>
                            </p>
                        </li>
						
                        <!-- Menu Body -->

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                
                            </div>
                            <div class="pull-right">								
								<a class="btn btn-default btn-flat" href="dologout.php" data-toggle="modal" data-target="#modalHeader" data-title="Detail Data">
									Sign out
								</a>
							</div>
                        </li>
                    </ul>
					
                </li>
            </ul>

        </div>
    </nav>
</header>

<?php
	$siteActive = $act == "site"? "active" : "";
	$accountActive = $act == "account"? "active" : "";
	$memberActive = $act == "member"? "active" : "";
	$depositActive = $act == "deposit"? "active" : "";
	$withdrawActive = $act == "withdraw"? "active" : "";
	$laporanActive = $act == "laporan"? "active" : "";
		$memberWinLoseActive = $subact == "memberWinLose"? "active": "";
		$harianActive = $subact == "laporanHarian"? "active": "";
		
	$promoActive = $act == "promo"? "active" : "";
	$beritaActive = $act == "berita"? "active" : "";
	$gameActive = $act == "game"? "active" : "";
	$bankListActive = $act == "bankList"? "active" : "";
	$bankOnoffActive = $act == "bankOnoff"? "active" : "";
	$passwordActive = $act == "user"? "active" : "";
	$nonohpActive = $act == "nonohp"? "active" : "";
	$togelActive = $act == "togel"? "active" : "";
	$popupActive = $act == "popup"? "active" : "";
	$announcementActive = $act == "announcement"? "active" : "";
	$contactusActive = $act == "contactus"? "active" : "";
	$infoActive = $act == "info"? "active" : "";
	$userAdminActive = $act == "userAdmin"? "active" : "";
	$adminLogActive = $act == "adminLog"? "active" : "";
	$adminAnnouncementActive = $act == "adminAnnouncement"? "active" : "";
	$linkActive = $act == "link"? "active" : "";
	$followUpActive = $act == "followup"? "active" : "";
		$followUpAllActive = $subact == "followup_all"? "active" : "";
		$followUpNonDepoActive = $subact == "followup_non"? "active" : "";
		$followUpDepoActive = $subact == "followup_depo"? "active" : "";
		$followUpLossMemberActive = $subact == "loss_member"? "active" : "";
	
?>
<aside class="main-sidebar">
    <section class="sidebar">
		<ul class="sidebar-menu">
			<li class="<?php echo $accountActive ?>"><a href="account.php"><i class="fa fa-group"></i>  <span>Account</span></a></li>
			<li class="<?php echo $memberActive ?>"><a href="member.php"><i class="fa fa-user-circle"></i>  <span>Member</span></a></li>
			<li class="<?php echo $depositActive ?>"><a href="deposit.php"><i class="fa fa-money"></i>  <span>Deposit</span></a></li>
			<li class="<?php echo $withdrawActive ?>"><a href="withdraw.php"><i class="fa fa-share-square-o"></i>  <span>Withdraw</span></a></li>
			
			
			<?php
				
				if($res['superadmin'] == 1){
			?>				
			<?php
					echo '<li class="'.$nonohpActive.'"><a href="nonohp.php" style="color:pink"><i class="glyphicon glyphicon-cloud"></i>  <span>Hapus No.HP</span></a></li>';
					echo '<li class="'.$adminAnnouncementActive.'"><a href="adminAnnouncement.php" style="color:pink"><i class="glyphicon glyphicon-star"></i>  <span>Admin Announcement</span></a></li>';
					echo '<li class="'.$gameActive.'"><a href="game.php" style="color:pink"><i class="fa fa-gamepad"></i>  <span>Game</span></a></li>';
					echo '<li class="'.$linkActive.'"><a href="link.php" style="color:pink"><i class="fa  fa-map-o"></i>  <span>Link</span></a></li>';
					echo '<li class="'.$bankListActive.'"><a href="bankList.php" style="color:pink"><i class="fa  fa-bank"></i>  <span>Bank List</span></a></li>';
					echo '<li class="'.$userAdminActive.'"><a href="userAdmin.php" style="color:pink"><i class="fa fa-gamepad"></i>  <span>Admin</span></a></li>';
					echo '<li><a href="deposit-import.php" target="_blank" style="color:pink"><i class="fa fa-money"></i>  <span>Import Deposit</span></a></li>';
					echo '<li><a href="withdraw-import.php" target="_blank" style="color:pink"><i class="fa fa-share-square-o"></i>  <span>Import Withdraw</span></a></li>';
				}
			?>

		</ul>
    </section>

</aside>

<div id="boxSound" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
	<audio id="total_member_sound" data-delay="0">
		<source src="sound/solemn.ogg" type="audio/ogg">
		<source src="sound/solemn.m4r" type="audio/m4r">
		<source src="sound/solemn.mp3" type="audio/mpeg">
	</audio>
		
	<audio id="total_withdraw_sound" data-delay="0">
		<source src="sound/oh-really.ogg" type="audio/ogg">
		<source src="sound/oh-really.m4r" type="audio/m4r">
		<source src="sound/oh-really.mp3" type="audio/mpeg">
	</audio>
	
	<audio id="total_deposit_sound" data-delay="0">
		<source src="sound/coins.ogg" type="audio/ogg">
		<source src="sound/coins.m4r" type="audio/m4r">
		<source src="sound/coins.mp3" type="audio/mpeg">
	</audio>
	
</div>

<div id="modalHeader" class="fade modal" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4><b>Laporan Work Session</b></h4>
</div>
<div class="modal-body">

</div>

</div>
</div>
</div>
	
<script>
	jQuery(document).ready(function () 
	{
		jQuery('#modalHeader').modal({"show":false});
		
		$('#modalHeader').on('show.bs.modal', function (event) {		
			var button = $(event.relatedTarget);
			var modal = $(this);
			var title = button.data('title');
			var href = button.attr('href');
			modal.find('.modal-title').html(title);
			modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>');
						
			$.post(href)
			.done(function( data ) {
				modal.find('.modal-body').html(data);
			});
		});
	});
</script>
	