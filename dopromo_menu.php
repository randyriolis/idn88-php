<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$kdpromo = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post")
	{
		$err = "";
		$textSuccess = "";
		
		// PROSES FORM
		$judul = $_REQUEST['judul'];
		$kodeinpromo = $_REQUEST['kodeinpromo'];
		$deposit = $_REQUEST['deposit'];
		$withdraw = $_REQUEST['withdraw'];
		$isactive = $_REQUEST['isactive'];
		$isi = $_REQUEST['isi'];
				
		$target_file = "";
		
		if($judul == "")		$err .= "<br/>Nama Promo harus diisi";
		if($kodeinpromo == "")		$err .= "<br/>Kode Promo harus diisi";
				 
		if(isset($_FILES['imagepromo']) && $err == "" && $_FILES['imagepromo']['error'] <= 0)
		{			
			// UPLOAD IMAGE PROMO
			$target_dir = "uploads/";
			
			$uploadOk = 1;
			$imageFileType = pathinfo(basename($_FILES["imagepromo"]["name"]), PATHINFO_EXTENSION);
						
			$removeWhite = str_replace(" ","", $kodeinpromo);					
			$string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $removeWhite);
			$newname = substr($string, 0, 20)."_".$idlink.".".$imageFileType;
			$target_file = $target_dir.$newname;
			
			// Check if image file is a actual image or fake image
			$check = getimagesize($_FILES["imagepromo"]["tmp_name"]);
			if($check !== false) {
				$uploadOk = 1;
			} else {
				$err .= "<br/>Kok keknya bukan image";
				$uploadOk = 0;
			}
			
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) 
			{
				$err .= "<br/>Image cuma boleh format JPG, JPEG, PNG & GIF ";
				$uploadOk = 0;
			}
			
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {				
				$err .= "<br/>Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
			} 
			else 
			{
				if (move_uploaded_file($_FILES["imagepromo"]["tmp_name"], $target_file)) {
					// echo "The file ". basename( $_FILES["imagepromo"]["name"]). " has been uploaded.";
					$data = file_get_contents($target_file);
					$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);				
				} else {
					$err .= "<br/>Sorry, there was an error uploading your file.";
				}
			}
		}
		
		
		if($err=="" && $menu == "edit"){
			$query_targetfile = $base64==""? "" : ", imagelink='$base64' ";
			
			$query = "UPDATE promos SET judul='$judul', kodeinpromo='$kodeinpromo', isactive=$isactive, isi='$isi' ".$query_targetfile." WHERE kdpromo=$kdpromo ";
			$textSuccess = "Promo berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		else if($err=="" && $menu == "create"){
			$query_targetfile_1 = $base64==""? "" : "imagelink, ";
			$query_targetfile_2 = $base64==""? "" : ", '$base64' ";
			
			$query = "INSERT INTO promos (judul, kodeinpromo, isactive, isi, ".$query_targetfile_1." idlink) VALUES ('$judul', '$kodeinpromo', $isactive, '$isi' ".$query_targetfile_2." , $idlink) ";
			$textSuccess = "Promo berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
				
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';
		
		mysqli_close($conn);
		exit();
	}
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM promos WHERE kdpromo = $kdpromo ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Promo: ".$kdpromo;
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Promo";
	}
?>

<link rel="stylesheet" href="richText/richtext.min.css">
<script src="richText/jquery.richtext.js"></script>

<div class="promo-update">
	<h1><?php echo $title ?></h1>
	<form id="form_promo" class="form-vertical" action="dopromo_menu.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $kdpromo ?>" >
		<input type="hidden" name="post" value="post" >
		
		<fieldset id="w2">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group field-promo-judul required">
						<label class="control-label" for="promo-judul">Nama Promo</label>
						<input type="text" id="promo-judul" class="form-control" name="judul" value="<?php echo $row['judul'] ?>" placeholder="Nama Promo...">
						<div class="help-block"></div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group field-promo-kodeinpromo required">
						<label class="control-label" for="promo-kodeinpromo">Kode Promo</label>
						<input type="text" id="promo-kodeinpromo" class="form-control" name="kodeinpromo" value="<?php echo $row['kodeinpromo'] ?>" placeholder="Kode Promo...">
						<div class="help-block"></div>
					</div>
				</div>
			</div>
		</fieldset>
		
		<fieldset id="w3">
			<div class="row">
				<div class="col-sm-4">		
					<div class="form-group field-promo-isactive required">
						<label class="control-label">Status</label>
						<input type="hidden" name="isactive" value="">
						
						<div id="promo-isactive">
							<label class="radio-inline"><input type="radio" name="isactive" value="1" <?php if($menu=="create" || $row['isactive']==1) echo 'checked'; ?>> Active</label>
							<label class="radio-inline"><input type="radio" name="isactive" value="0" <?php if($menu=="edit" && $row['isactive']==0) echo 'checked'; ?>> Inactive</label>
						</div>

						<div class="help-block"></div>
					</div>
				</div>

			</div>

		</fieldset>
		
		<fieldset id="w4">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group field-promo-isi required">
						<label class="control-label" for="promo-isi">Keterangan</label>
						<textarea rows="10" id="promo-isi" class="form-control" name="isi" placeholder="Keterangan bila perlu..."><?php echo $row['isi'] ?></textarea>

						<div class="help-block"></div>
					</div>

				</div>
			</div>
		</fieldset>
		
		
		<fieldset id="w5">
			<div class="row">
				<div class="col-sm-9">
					<div class="form-group field-promo-imagepromo required">
						<label class="control-label" for="promo-imagepromo">Image</label>
						
						<input type="file" name="imagepromo" id="imagepromo" />
						<?php 
							if($row['imagelink'] != NULL && $row['imagelink'] != ""){
								echo '<br/>';
								echo '<image src="'.$row['imagelink'].'" style="max-width: 100%; max-height: 100%;" />';
							}
						?>

						<div class="help-block"></div>
					</div>									

				</div>
								
				<div class="col-sm-3">
					<div style="text-align: right; margin-top: 20px">
						<div id="feedback_edit"></div>
						<img class="thisLoadingGif" src="img/loading.gif" />
						<button type="reset" class="btn btn-default">Reset</button> 
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
				
			</div>			
		</fieldset>
		
	</form>

</div>

<script>
	$(document).ready(function() {
		$('#promo-isi').richText();
	});
</script>

<?php 
	$formName = '"#form_promo"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
?>
