<?php 
	if($action == ""){
		$action = "exportExcel_followup.php";
	}
?>

<form id="formexport" method="GET" action="<?php echo $action ?>">	
	<input type="hidden" id="process" name="process" value="OKE" />
	
	<div class="btn-group" style="margin-left:10px;">
		<select class="" name="filter_bulan" id="filter_bulan"  style=" height: 30px; padding-left: 5px">
			<?php 
				$arrbulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

				for($a=1; $a<=12; $a++)
				{
					$print = $arrbulan[$a-1];
					$bulan = date("n");
					if($bulan == $a)	$sel = "selected";
					else $sel = "";
					echo '<option value="'.$a.'" '.$sel.'>'.$print."</option>";
				}
			
			?>
		</select>
		<select class="" name="filter_tahun" id="filter_tahun"  style=" height: 30px; padding-left: 5px">
			<?php
				$a = "2016";
				
				for($a=date("Y"); $a>=2016; $a--) {
					$print = "Tahun ". $a;
					echo '<option value="'.$a.'">'.$print."</option>";
				}
				
			?>
		</select>
	</div>	
	
	<div class="btn-group" style="margin-left:10px;">
		<a class="btn btn-warning" href="#" title="Reset Grid" data-pjax="0" onclick="submitExport(); return false;">
			Export Excel
		</a>
	</div>
 
</form>


<script>
	function submitExport()
	{
				
		document.getElementById("formexport").submit();
	}
</script>