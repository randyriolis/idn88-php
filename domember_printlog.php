<?php 
	$id = $_REQUEST['id'];
	include "config.php";	
	
?>
<div class="member-log">

<table class="table table-condensed">
    <thead class="thead-inverse">
      <tr >
        <th>User</th>
        <th>Date Create</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
		<?php			
			$query = "SELECT zlm.*, us.username FROM zlog_member zlm ".
						   "LEFT JOIN user us ON zlm.iduser=us.id ".
						   "WHERE idmember = $id ORDER BY idlogmb DESC ";						   
			$result = mysqli_query($conn, $query);	
			
			while($row = mysqli_fetch_assoc($result))
			{
				?>
				<tr>
					<td><?php echo $row['username'] ?></td>
					<td><?php echo $row['datecreate'] ?></td>
					<td><?php echo $row['keterangan'] ?></td>
				  </tr>
				<?php
			}
		
		?>	
    </tbody>
  </table>
</div>

<?php mysqli_close($conn); ?>