
<script>
	
	$(<?php echo $formName ?>).submit(function(event)
	{
		event.preventDefault();
				
		// Get some values from elements on the page:
		var $form = $(this);
		var sent = $($form).serialize();
		var url = $form.attr("action");
		
		// Send the data using post
		// var posting = $.post( url, sent );		
		$(".thisLoadingGif").css("display","");
			  
		$.ajax({
			url: url, // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				$(".thisLoadingGif").css("display","none");			
				$(<?php echo $feedback ?>).css("opacity", "1");
				$(<?php echo $feedback ?>).css("display", "block");
				
				if(data.indexOf("Error") == -1){
					// success.. Close
					$(<?php echo $feedback ?>).html(data);
					setTimeout(function(){ $(<?php echo $feedback ?>).fadeTo("slow", 0, function() {
						// Animation complete.
						$(<?php echo $feedback ?>).css("display", "none");
						$('.modal').modal('hide');
						
						refreshContent();
						
						<?php echo $add_reload; ?>
					  }); }, 2000);
				}
				else {
					// error
					$(<?php echo $feedback ?>).html(data);
					setTimeout(function(){ $(<?php echo $feedback ?>).fadeTo("slow", 0, function() {
						// Animation complete.
						$(<?php echo $feedback ?>).css("display", "none");
					  }); }, 4000);
				}
			}
		});

		/*
		// Put the results in a div
		posting.done(function( data ) {				  					
			
		});
		*/
		
	});
	
	$(".thisLoadingGif").css("display","none");
</script>
