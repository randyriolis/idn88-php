<?php 
	include_once "inc_login.php";
	include "config.php";
	include "function.php";
		
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	
	$wherequery = " AND ln.status=1 ";
	
	// all request
	$page = $_REQUEST['page'];
	$form_search_form = $_REQUEST['form_search_form'];
	
	
	$form_date_start = empty($_REQUEST['form_date_start']) ? "" : date("Y-m-d", strtotime($_REQUEST['form_date_start']));;
	$form_date_end = empty($_REQUEST['form_date_end']) ? "" : date("Y-m-d", strtotime($_REQUEST['form_date_end']));;
	
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];	
	
	if($form_search_form != ""){
		$wherequery .= " AND ((mem.username LIKE '%$form_search_form%' OR acc.pass LIKE '%$form_search_form%') 
									OR (gm.nama like '%$form_search_form%' ) 
									OR (gm.kodeagen like '%$form_search_form%' ) 
									OR (mem.nama like '%$form_search_form%') 
									OR (mem.norek like '%$form_search_form%') 
									OR (namalink like '%$form_search_form%') 
									) ";
		
	}	
	
	$orderquery = "ORDER BY mem.kdmember DESC ";
	if($sort != "")	
	{
		if($sort == "game_asc")		$orderquery = "ORDER BY mem.kdproduct ASC ";
		else if($sort == "game_desc") $orderquery = "ORDER BY mem.kdproduct DESC ";
		
		else if($sort == "namalink_asc")		$orderquery = "ORDER BY ln.namalink ASC ";
		else if($sort == "namalink_desc") $orderquery = "ORDER BY ln.namalink DESC ";
		
		else if($sort == "account_asc")		$orderquery = "ORDER BY mem.username ASC ";
		else if($sort == "account_desc") $orderquery = "ORDER BY mem.username DESC ";
		
		else if($sort == "bank_asc")		$orderquery = "ORDER BY mem.kdbank ASC ";
		else if($sort == "bank_desc") $orderquery = "ORDER BY mem.kdbank DESC ";
		
		else if($sort == "nama_asc")		$orderquery = "ORDER BY mem.nama ASC ";
		else if($sort == "nama_desc") $orderquery = "ORDER BY mem.nama DESC ";
		
		else if($sort == "no_rekening_asc")		$orderquery = "ORDER BY mem.norek ASC ";
		else if($sort == "no_rekening_desc") $orderquery = "ORDER BY mem.norek DESC ";
		
		else if($sort == "email_asc")		$orderquery = "ORDER BY mem.email ASC ";
		else if($sort == "email_desc") $orderquery = "ORDER BY mem.email DESC ";
		
		else if($sort == "totalDeposit_asc")		$orderquery = "ORDER BY totalDeposit ASC ";
		else if($sort == "totalDeposit_desc") $orderquery = "ORDER BY totalDeposit DESC ";
		
		else if($sort == "totalWithdraw_asc")		$orderquery = "ORDER BY totalWithdraw ASC ";
		else if($sort == "totalWithdraw_desc") $orderquery = "ORDER BY totalWithdraw DESC ";
		
		else if($sort == "totalJumlah_asc")		$orderquery = "ORDER BY totalJumlah ASC ";
		else if($sort == "totalJumlah_desc") $orderquery = "ORDER BY totalJumlah DESC ";
		
		else if($sort == "totalBonus_asc")		$orderquery = "ORDER BY totalBonus ASC ";
		else if($sort == "totalBonus_desc") $orderquery = "ORDER BY totalBonus DESC ";
		
		else if($sort == "credit_asc")		$orderquery = "ORDER BY credit ASC ";
		else if($sort == "credit_desc") $orderquery = "ORDER BY credit DESC ";
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = "  ";
	if($maxrow >= 9999)	$limitquery = "";
	
	
	$form_filter_date = $_REQUEST['form_filter_date'];
	
	// DATE-selected
	if($form_date_start != "" && $form_date_end != "")	{
		// $datequery .= " AND ( date(CONVERT_TZ(datecreate,".$curtimezone.")) between date('$form_date_start') AND date('$form_date_end') ) ";
		$datequery .= " AND ( CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00') between date('$form_date_start') AND date('$form_date_end') ) ";
	}
	
	// YESTERDAY
	else if($form_filter_date == 2){
		$datequery .= " AND ( date(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d') , @@session.time_zone, '+7:00')) = SUBDATE(CURDATE(), 1)  ) ";
		// $datequery .= " AND ( CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00') between date('$form_date_start') AND date('$form_date_end') ) ";
	}
	
	// seminggu
	else if($form_filter_date == 3){
		$datequery .= " AND ( date(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d') , @@session.time_zone, '+7:00')) between SUBDATE(CURDATE(), 7) AND CURDATE() ) ";
	}
	
	// sebulan	
	else {
		$datequery .= " AND ( date(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d') , @@session.time_zone, '+7:00')) between SUBDATE(CURDATE(), 30) AND CURDATE() ) ";		
	}
	
	/*
	// TODAY
	else {
		// $datequery .= " AND ( date(CONVERT_TZ(dp.cretime,".$curtimezone.")) = CURDATE() ) ";
		// $datequery .= " AND ( date(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d') , @@session.time_zone, '+7:00')) = CURDATE() ) ";
		$datequery .= " AND ( date(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d') , @@session.time_zone, '+7:00')) = SUBDATE(CURDATE(), 0)  ) ";
	}
	*/
	
	$queryDeposit = "SELECT kdmember, SUM(IF(dp.isclear=1, dp.jumlah, 0)) as totalDeposit, COUNT(dp.jumlah) as banyakDeposit ".
				   "FROM deposits dp ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
				   "WHERE 1=1 ".
				   $datequery.
				   " GROUP BY kdmember ".
				   " ORDER BY kdmember ASC ";
	
	$queryWithdraw = "SELECT kdmember, SUM(IF(dp.isclear=1, dp.jumlah, 0)) as totalWithdraw, COUNT(dp.jumlah) as banyakWithdraw ".
				   "FROM withdraws dp ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
				   "WHERE 1=1 ".
				   $datequery.
				   " GROUP BY kdmember ".
				   " ORDER BY kdmember ASC ";
	
	
	$query = "SELECT mem.kdmember, mem.username, ln.namalink, mem.nama, mem.norek, bk.inisialbank, 
					gm.kodeagen AS hex_account, gm.nama AS nama_game, 
					totalDeposit, banyakDeposit, totalWithdraw, banyakWithdraw, (COALESCE(totalDeposit, 0) - COALESCE(totalWithdraw, 0)) AS totalJumlah
					
					FROM members mem  
					LEFT JOIN ($queryDeposit) as tempDeposit ON tempDeposit.kdmember=mem.kdmember 
					LEFT JOIN ($queryWithdraw) as tempWithdraw ON tempWithdraw.kdmember=mem.kdmember 
					LEFT JOIN link ln ON mem.idlink = ln.idlink 
					LEFT JOIN products gm ON gm.kdproduct=mem.kdproduct 
					LEFT JOIN banks bk ON bk.kdbank=mem.kdbank  
					".					
					"WHERE 1=1 AND (totalDeposit > 0 OR totalWithdraw > 0) ".
					$wherequery.
					" GROUP BY mem.kdmember ".					
					$orderquery.
					$limitquery;	
					
	// GROUP BY YEAR(new_datecreate), MONTH(new_datecreate), DAY(new_datecreate) 
	// , SUM(IF(wd.status=1, wd.jumlah, 0)) as totaWithdraw, COUNT(wd.jumlah) as banyakWithdraw 
					
	/*
	$query = "SELECT dp.*, mem.kdmember, mem.nama, mem.no_hp, mem.email, acc.idaccount, mem.username, gm.hex_account, gm.nama_game, us.username AS nama_admin, ln.namalink ".
				   ",CONVERT_TZ(dp.dateassign,".$curtimezone.") as new_dateassign ".
				   ",CONVERT_TZ(dp.datecreate,".$curtimezone.") as new_datecreate ".
				   "FROM deposit dp ".
				   "LEFT JOIN member mem ON dp.kdmember=mem.kdmember ".
				   "LEFT JOIN account acc ON mem.idaccount=acc.idaccount ".
				   "LEFT JOIN game gm ON gm.idgame=dp.idbank ".				   
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".			// Update terakhir, jadi semua link cm pake game dari gilabet || tapi data lama kan kagak..
				   "LEFT JOIN user us ON dp.iduser=us.id ".
				   "WHERE 1=1 ".
				   $wherequery.
				   $orderquery.
				   $limitquery;	
   */
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	$jumlah_semua_deposit = 0;
	$jumlah_semua_withdraw = 0;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	
		
		$bgcolor = $cnt %2 ==0? "odd" : "even";
		
		$jumlah_semua_deposit += $row['totalDeposit'];
		$jumlah_semua_withdraw += $row['totalWithdraw'];
?>
	
	<tr data-key="<?php echo $row['kddeposit'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:140px;" data-col-seq="1">
			<?php echo $row['username'] ?>			
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:180px;" data-col-seq="2">
			<?php
				echo $row['namalink'];			
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="3" >
			<?php echo $row['nama'] ?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="4">			
			<?php echo $row['norek'] ?> <br />
			<?php echo $row['inisialbank'] ?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:220px;" data-col-seq="5">
			<?php echo explode(",", $row['hex_account'])[0] ?> <br/>
			<?php echo $row['nama_game'] ?>
		</td>
		<td class="kartik-sheet-style kv-align-right kv-align-middle <?php echo $bgcolor ?>" style="width:220px;" data-col-seq="6">
			<?php echo number_format($row['totalDeposit'] , 0); ?>
		</td>
		<td class="kartik-sheet-style kv-align-right kv-align-middle <?php echo $bgcolor ?>">
			<?php echo number_format($row['totalWithdraw'] , 0); ?>
		</td>
		<td class="kartik-sheet-style kv-align-right kv-align-middle <?php echo $bgcolor ?>">
			<?php 
				$jml = $row['totalJumlah'];
				if($jml > 0)	echo '<span class=" bg-success">'. number_format($jml , 0);
				else echo '<span class=" bg-danger">'. number_format($jml , 0);
				
			?>
			</span>
		</td>
		<td class="kartik-sheet-style kv-align-right kv-align-middle <?php echo $bgcolor ?>">
			<!-- TOTAL BONUS -->
		</td>
		<td class="kartik-sheet-style kv-align-right kv-align-middle <?php echo $bgcolor ?>">
			<!-- CREDIT GAME -->
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >
			<?php
				// Last deposit
				$kdmember = $row['kdmember'];
				
				$queryLastDeposit = "SELECT jumlah , ".
									"CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00') as dateLastDeposit ".
									"FROM deposits dp ".
									"WHERE kdmember = $kdmember ".
									"ORDER BY dp.cretime DESC ".
									"LIMIT 0, 1";
									
				$resLastDeposit = mysqli_query($conn, $queryLastDeposit);
				while($rowLastDepost=mysqli_fetch_array($resLastDeposit) ) {
					echo number_format($rowLastDepost['jumlah'], 0).'<br/>';
					echo date( "d-m-Y H:i:s" , strtotime($rowLastDepost['dateLastDeposit']));
				}
			?>
		</td>
			
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >
			<?php
				// Last deposit
				$kdmember = $row['kdmember'];
				
				$queryLastWithdraw = "SELECT jumlah , ".									
									"CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00') as dateLastWd ".
									"FROM withdraws dp ".
									"WHERE kdmember = $kdmember ".
									"ORDER BY dp.cretime DESC ".
									"LIMIT 0, 1";
				$resLastWithdraw = mysqli_query($conn, $queryLastWithdraw);
				while($rowLastWd=mysqli_fetch_array($resLastWithdraw) ) {
					echo number_format($rowLastWd['jumlah'], 0).'<br/>';
					echo date( "d-m-Y H:i:s" , strtotime($rowLastWd['dateLastWd']));
				}
			?>
		</td>
		
	</tr>


<?php } ?>

	<!-- paging -->
	<tr>
		<td colspan="99">
			<br/>
			<div class="laporan_1">Total Deposit: </div>
			<div class="laporan_2"><?php echo number_format($jumlah_semua_deposit, 0) ?> </div>
			<br/>
			
			<div class="laporan_1">Total Withdraw: </div>
			<div class="laporan_2"><?php echo number_format($jumlah_semua_withdraw, 0) ?> </div>
			<br/>
			
			<div class="laporan_1">Total Jumlah: </div>
			<div class="laporan_2">
				<?php 
					$jumlah_semua_difference = $jumlah_semua_deposit - $jumlah_semua_withdraw;
					echo number_format($jumlah_semua_difference, 0);									
				?> 
			</div>
						
			<?php 
				if($jumlah_semua_difference > 0)	echo '<div style="float:left; " class="label-success">(MENANG)</div>';
				else echo '<div style="float:left; " class="label-danger">(KALAH)</div>';
			?>
		
			<br/>		
			<br/>
			<div class="laporan_1">Total Bonus Pemain: </div>
			<div class="laporan_2">
				0
			</div>
		</td>
		
	</tr>
	

<script>
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>