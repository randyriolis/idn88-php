<?php
	include_once "inc_login.php";
	include_once "config.php";
	
	$page = $_REQUEST['page'];
	if(empty($page)) $page = 1;
	
	// check access
	$queryuser = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM user WHERE id=$iduser "));
	
	if($queryuser['superadmin'] == 0){
		header("location:site.php");
		exit();
	}
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Admins</title>    	
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "adminLog";
		include_once "inc_header.php";
	?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1></h1>			
			<ul class="breadcrumb">
			<li><a href="site.php">Home</a></li>
			<li class="active">admin</li>
			</ul>    
		</section>
		
		<section class="content">
			<div class="user-index">							
				<div class="account-form">
					<p>
					<form id="form_newuser" class="form-inline" action="docreate_user.php" method="post">
						<div class="form-group">
							<a class="btn btn-success modalButton" type="button" id="modalButton" value="douser_menu.php?menu=create" href="#" onClick="return false;">
								Create Admin
							</a>
						</div>

					</form>    
					</p>
				</div>
				
				<div id="w0" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">    
				<div id="user-id-pjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
					<div id="user-id" class="grid-view hide-resize" data-krajee-grid="kvGridInit_7e6ea3f8">
						<div class="panel panel-primary">
							<div class="panel-heading">    
								<div class="pull-right">
									<div id="page_number" class="summary">
									</div>
								</div>
								<h3 class="panel-title">
									Admin
								</h3>
								<div class="clearfix"></div>
							</div>
							
							<div class="kv-panel-before">    
								<div class="pull-right">
									<div class="btn-toolbar kv-grid-toolbar" role="toolbar">
										<div class="btn-group">
											<a class="btn btn-default" href="member.php" title="Reset Grid" data-pjax="0" onClick="refreshContent(); return false;">
												<i class="glyphicon glyphicon-repeat"></i>
											</a>
										</div>

										<div class="btn-group">
											<a id="showall" class="btn btn-default" href="#" title="Show all data" onClick="tryShowAll(); return false;">
												<i class='glyphicon glyphicon-resize-full'></i> All								
											</a>
										</div>
									</div>    
								</div>
						
								<div class="clearfix"></div>
							</div>
							
							<div id="user-id-container" class="table-responsive kv-grid-container">
							<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
								<colgroup>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
								</colgroup>
								<thead>

									
									
									<tr id="user-id-filters" class="filters skip-export" style="display:none">
										<form id="form_search" class="form-inline" action="doprint_adminlog.php" method="post">
										
										<input type="hidden" id="page" name="page" value="<?php echo $page ?>" />
										<input type="hidden" id="sort" name="sort" value="" />
										<input type="hidden" id="totalrow" name="totalrow" value="" />
										<input type="hidden" id="maxrow" name="maxrow" value="20" />
										<input type="hidden" id="delete" name="delete" value="" />
										
										<td><input type="text" class="form-control" name="username"></td>
										<td><input type="text" class="form-control" name="password"></td>
										<td>
											<select class="form-control" name="durasi_approve" onChange="refreshContent();">
												<option value=""></option>
												<option value="1">Active</option>
												<option value="0">Inactive</option>
											</select>
										</td>										
										<td>											
											<select class="form-control" name="superadmin" onChange="refreshContent();">
												<option value=""></option>
												<option value="1">A</option>
												<option value="2">B</option>
												<option value="0">F</option>
											</select>											
											<input type="submit" style="display:none" />
										</td>
										<td>
											<select class="form-control" name="idlink" onChange="refreshContent();">
												<option value=""></option>
												<?php 
													$qu = mysqli_query($conn, "SELECT * FROM link WHERE durasi_approve =1 ");
													while($dt=mysqli_fetch_array($qu)){
														echo '<option value="'.$dt['idlink'].'">'.$dt['namalink'].'</option>';
													}
												?>
											</select>											
											<input type="submit" style="display:none" />
										</td>
										</form>
									</tr>

								</thead>
								
								<tbody id="tbody_content">
									
								</tbody>
							</table>
						</div>
						
						</div>
					</div>
				</div>
				</div>
			</div>

			<div id="modalcreate" class="fade modal" role="dialog" tabindex="-1">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4>Admin Log</h4>
						</div>
						<div class="modal-body">
							<div id="modalContent"></div>
						</div>
					</div>
				</div>
			</div>


			</div>
		</section>
		
	</div>

<?php 
	include_once "inc_script.php";
?>
<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		refreshContent();
		
		jQuery('#modalcreate').modal({"show":false});
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});	
	
	
</script>
<?php 
	// close
	mysqli_close($conn);
?>

</body>

</html>