<?php
	include_once "inc_login.php";
	include_once "config.php";
	
	$page = $_REQUEST['page'];
	if(empty($page)) $page = 1;
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	
	// QUERY
	$sqlgame = "SELECT gm.* FROM products gm WHERE gm.isactive = 1 $tm ORDER BY kdproduct ASC  ";		
	$result = mysqli_query($conn, $sqlgame);
		
	$arrgame = array();
	while($row = mysqli_fetch_assoc($result)) {
		array_push($arrgame, $row);
	}
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>member</title>    	
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "member";
		include_once "inc_header.php";
	?>
	<div class="alert alert-warning" id="copy_clipboard" style="position: fixed; left: 50%;  transform: translate(-50%, -50%); top: 70px; display:none; z-index:100000 ">
		abc
	</div>

	<div class="content-wrapper">
		<section class="content-header">
			<h1></h1>			
			<ul class="breadcrumb">
			<li><a href="site.php">Home</a></li>
			<li class="active">Members</li>
			</ul>    
		</section>

		<section class="content">
			<div class="member-index">
				<p>
					<button type="button" id="modalButton" class="btn btn-success" value="domember_create.php">Tambah Pemain</button>
					<button type="button" id="ajaxpaste" class="btn btn-danger" onclick="pasteMember(this)">Paste Pemain</button>
				</p>
				
				    
				<div id="modalcreate" class="fade modal" role="dialog" tabindex="-1">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4>Tambah Pemain</h4>
							</div>
							<div class="modal-body">
								<div id="modalContent"></div>
							</div>
						</div>
					</div>
				</div>

	<div id="w0" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">    
		<div id="w1-pjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
		<div id="w1" class="grid-view hide-resize" data-krajee-grid="kvGridInit_6958ba83">
		<div class="panel panel-primary">
			<div class="panel-heading">    
				<div class="pull-right">
					<div id="page_number" class="summary">
					</div>
				</div>
				<h3 class="panel-title">
					Daftar Member Terdaftar
				</h3>
				<div class="clearfix"></div>
			</div>
		
			<div class="kv-panel-before">
				<div class="pull-right">
					<div class="btn-toolbar kv-grid-toolbar" role="toolbar">
						<div id="loadingGif" class="btn-group" style="margin-right:20px">
							<img src="img/loading.gif" />
						</div>
						
						<div class="btn-group">
							<a class="btn btn-default" href="member.php" title="Reset Grid" data-pjax="0" onClick="refreshContent(); return false;">
								<i class="glyphicon glyphicon-repeat"></i>
							</a>
						</div>

						<div class="btn-group">
							<a id="showall" class="btn btn-default" href="#" title="Show all data" onClick="tryShowAll(); return false;">
								<i class='glyphicon glyphicon-resize-full'></i> All								
							</a>
						</div>
						
						<?php if($levelAdmin == 1 && $arrUser['isheadadmin'] == 1) { ?>
						<div class="btn-group">
							<?php include "tombolExportExcel_Member.php" ?>
						</div>
						<?php } ?>
						
					</div>
				</div>
		
				<div class="clearfix"></div>
			</div>
			
			<div id="w1-container" class="table-responsive kv-grid-container">
			
			<form id="form_search" class="form-inline" action="doprint_member.php" method="post">
				
			<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
			<colgroup>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col class="skip-export">
			<col class="skip-export">
			<col class="skip-export">
			<col class="skip-export">
			<col class="skip-export">
			</colgroup>	
			
			<thead>
			
			<tr>
				<th class="kartik-sheet-style kv-align-center kv-align-middle kv-merged-header" style="width:36px;" rowspan="2" data-col-seq="0">&nbsp;</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="1">
					<a href="#" onClick="return trysort('account');" id="account" class="sorter">Username</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="2">
					<a href="#" onClick="return trysort('namalink');" id="namalink" class="sorter">Website</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="3" style="width:150px">
					<a href="#" onClick="return trysort('nama');" id="nama" class="sorter">Nama Pemain</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:100px;" data-col-seq="4">				
					<a href="#" onClick="return trysort('norek');" id="norek" class="sorter">No.Rekening</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="5">
					<a href="#" onClick="return trysort('email');" id="email" class="sorter">Kontak Pemain</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="5">
					<a href="#" onClick="return trysort('game');" id="game" class="sorter">Kode Game</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="5">
					<a href="#" onClick="return trysort('idsumber');" id="idsumber" class="sorter">Sumber</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="5">
					<a href="#" onClick="return trysort('url');" id="url" class="sorter">Url</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle"  data-col-seq="7">
					<a href="#" onClick="return trysort('dateassign');" id="dateassign" class="sorter">Tanggal Assign</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle"  data-col-seq="8">
					<a href="#" onClick="return trysort('ip');" id="ip" class="sorter">IP Pendaftar</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="9">
					<a href="#" onClick="return trysort('status');" id="status" class="sorter">Status</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="10">SMS</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="11">SMS Rek</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="12">Assign</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="13">Copy</th>
					
				<?php 
					// Access Admin
					if($levelAdmin == 1) {
				?>
					<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="13">Edit</th>
					<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="14">Delete</th>
					<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="14">Active</th>
				<?php } else { ?>
					<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="14">Edit</th>
				<?php } ?>
			</tr>
						
			<tr id="w1-filters" class="filters skip-export">
											
				<td>
					<input type="hidden" id="page" name="page" value="<?php echo $page ?>" />
					<input type="hidden" id="delete" name="delete" value="" />			
					<input type="hidden" id="sort" name="sort" value="" />
					<input type="hidden" id="totalrow" name="totalrow" value="" />
					<input type="hidden" id="maxrow" name="maxrow" value="20" />
					<input type="hidden" id="temp_id" name="temp_id" value="" />
					<input type="hidden" id="temp_function" name="temp_function" value="" />
					<input type="hidden" id="bb" name="bb" value="<?php echo $_REQUEST["bb"]; ?>" />
					
					<input type="text" class="form-control" name="account" style="width:80px">
				</td>
				<td style="min-width: 110px">
					<select id="accountsearch-namalink" class="form-control" name="namalink" onchange="refreshContent();">
					<option value=""></option>
					<?php
						$rs = mysqli_query($conn, "SELECT * FROM link WHERE status=1 ");
						
						while($rw=mysqli_fetch_array($rs)) {
							echo '<option value="'.$rw['namalink'].'" >'.$rw['namalink'].'</option>';
						}
						
						for($i=0; $i<count($arrgame); $i++) {
								
							}
					?>
					</select>																			
				</td>
				
				<td>
					<input type="text" class="form-control" name="nama" style="width:100%">
				</td>
				<td>
					<input type="text" class="form-control" name="norek" style="width:100%">
				</td>
				<td>
					<input type="text" class="form-control" name="email" style="width:100%">
				</td>
				<td>
					<select id="accountsearch-idgame" class="form-control js-example-basic-single" name="kdproduct" onchange="refreshContent();" style="width:110px;">
						<option value="">Any Game</option>
						<?php 
							for($i=0; $i<count($arrgame); $i++) {
								echo '<option value="'.$arrgame[$i]['kdproduct'].'" data-nama="'.$arrgame[$i]['kodeagen'].'" data-pass="'.$arrgame[$i]['defpass'].'" >
									'.$arrgame[$i]['nama'].'
								</option>';
							}
						?>						
					</select>
				</td>
				<td style="min-width: 110px">
					<select id="accountsearch-idsumber" class="form-control" name="idsumber" onchange="refreshContent();">			<option value="">All Sumber</option>
						<?php
							$sumbers = array(
								"1" => "Google", 
								"2" => "Facebook", 
								"3" => "Instagram", 
								"4" => "Twitter", 
								"5" => "WhatsApp",
								"6" => "Refferal",
								"7" => "Lainnya"
							);

							foreach($sumbers as $code => $item){
								$sel = ($code == $row['idsumber'])? 'selected' : '';
								echo '<option value="'.$code.'">'.$item.'</option>';
							}
						?>
					</select>
					<input type="text" class="form-control" name="findsumber" style="width:100%;margin-top:5px;display:none;text-transform:uppercase">
				</td>
				<td>
					<input type="text" class="form-control" name="url" style="width:100%">
				</td>
				<td>
					<input type="text" class="form-control" name="dateassign" style="width:100%">
				</td>
				<td>
					<input type="text" class="form-control" name="ip" style="max-width:90px">
				</td>
				<td>
					<select class="form-control" name="status" onchange="refreshContent();" style="width:60px;">
						<option value=""></option>
						<option value="1">Active</option>
						<option value="0">Inactive</option>
					</select>
					
					<input type="submit" style="display:none" />
				</td>
				
				
				
				
			</tr>

			</thead>
			
			<tbody id="tbody_content">
				
			</tbody>
			
			</table>
			</form>
	
			</div>
	
			
			
		</div>	
	</div>
		</div>
	</div>
		</div>
	
	
	
	
	</section>
	</div>
	

	<?php 
		include_once "inc_script.php";
	?>
	
	
	<div id="myModal" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4>SMS Account Info</h4>
	</div>
	<div class="modal-body">

	</div>

	</div>
	</div>
	</div>
	
	<div id="myModallog" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog ">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Log Member</h4>
	</div>
	<div class="modal-body">

	</div>

	</div>
	</div>
	</div>
	
	<div id="myModalsms" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog ">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>SMS Bank Info</h4>
	</div>
	<div class="modal-body">

	</div>

	</div>
	</div>
	</div>
	
	<div id="myModalassign" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog ">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Assign Account</h4>
	</div>
	<div class="modal-body">

	</div>

	</div>
	</div>
	</div>


	<div id="myModalreassign" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog ">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Re-Assign Account</h4>
	</div>
	<div class="modal-body">

	</div>

	</div>
	</div>
	</div>

	<div id="myModalupdate" class="fade modal" role="dialog" tabindex="-1">
	<div class="modal-dialog modal-lg">
	<div class="modal-content">
	<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>Update Member</h4>
	</div>
	<div class="modal-body">

	</div>

	</div>
	</div>
	</div>
	
	<script type="text/javascript">
	jQuery(document).ready(function () 
	{
		jQuery('#modalcreate').modal({"show":false});
		jQuery('#myModal').modal({"show":false});
		jQuery('#myModallog').modal({"show":false});
		jQuery('#myModalsms').modal({"show":false});
		jQuery('#myModalassign').modal({"show":false});
		jQuery('#myModalreassign').modal({"show":false});
		jQuery('#myModalupdate').modal({"show":false});
			

		$('#myModal').on('show.bs.modal', function (event) {
			
			var button = $(event.relatedTarget);
			var modal = $(this);
			var title = button.data('title');
			var href = button.attr('href');
			modal.find('.modal-title').html(title);
			modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>');
						
			$.post(href)
				.done(function( data ) {
					modal.find('.modal-body').html(data);
				});
			})


		$('#myModalsms').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget)
			var modal = $(this)
			var title = button.data('title') 
			var href = button.attr('href') 
			modal.find('.modal-title').html(title)
			modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
			$.post(href)
				.done(function( data ) {
					modal.find('.modal-body').html(data)
				});
			})


		$('#myModallog').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget)
			var modal = $(this)
			var title = button.data('title') 
			var href = button.attr('href') 
			modal.find('.modal-title').html(title)
			modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
			$.post(href)
				.done(function( data ) {
					modal.find('.modal-body').html(data)
				});
			})


		$('#myModalassign').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget);
			var modal = $(this);
			var title = button.data('title');
			var href = button.attr('href');			
			modal.find('.modal-title').html(title);
			modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>');
			$.post(href)
				.done(function( data ) {
					modal.find('.modal-body').html(data);
				});
			})


		$('#myModalreassign').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget)
			var modal = $(this)
			var title = button.data('title') 
			var href = button.attr('href') 
			modal.find('.modal-title').html(title)
			modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
			$.post(href)
				.done(function( data ) {
					modal.find('.modal-body').html(data)
				});
			})


		$('#myModalupdate').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget)
			var modal = $(this)
			var title = button.data('title') 
			var href = button.attr('href') 
			modal.find('.modal-title').html(title)
			modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
			$.post(href)
				.done(function( data ) {
					modal.find('.modal-body').html(data)
				});
			})
	 		
	});
</script>

<?php 
	// close
	mysqli_close($conn);
?>
	
</body>

<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		$(".js-example-basic-single").select2();
		refreshContent();
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});
	
	$(function(){
		$('#modalButton').click(function (){
			$('#modalcreate').modal('show')
				.find('#modalContent')
				.load($(this).attr('value'));
		});

		$('#accountsearch-idsumber').click(function() {
			$('input[name=findsumber]').val('');
		});

		$('#accountsearch-idsumber').change(function() {
			if ($.inArray($(this).val(), ['6','7']) > -1) {
		        $('input[name=findsumber]').css('display', 'block');
		    } else {
		    	$('input[name=findsumber]').css('display', 'none');
		    }
		});

		/*$('#ajaxpaste').click(function(){
			$.ajax({
			  	type: "POST",
			    url: "domember_ajax.php",
			    cache: false,
			  	data: {

			  	},
			    dataType: "json",
			  	success:function(data){
			  		console.log('sukses')
			  	}
			});
		}); */
	});
</script>

</html>