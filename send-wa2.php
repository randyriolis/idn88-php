<?php

require_once __DIR__.'/vendor/autoload.php';

$messageBird = new MessageBird\Client('HSiWwHeG2zyltYIYiDgpcdDDf');

//$messageBird = new MessageBird\Client('HSiWwHeG2zyltYIYiDgpcdDDf', null, [MessageBird\Client::ENABLE_CONVERSATIONSAPI_WHATSAPP_SANDBOX]);

// Enable the whatsapp sandbox feature
//$messageBird = new MessageBirdClient(
//    Rs9Nx7c7AhtCRlUqmJaa7X8mj',
//    null,
//    [MessageBirdClient::ENABLE_CONVERSATIONSAPI_WHATSAPP_SANDBOX]
//);

$hsmParam1 = new \MessageBird\Objects\Conversation\HSM\Params();
$hsmParam1->default = 'ALKBSAB561';

$hsmParam2 = new \MessageBird\Objects\Conversation\HSM\Params();
$hsmParam2->default = 'Kwe1j';

$hsmParam3 = new \MessageBird\Objects\Conversation\HSM\Params();
$hsmParam3->default = ' ';

$hsmLanguage = new \MessageBird\Objects\Conversation\HSM\Language();
$hsmLanguage->policy = \MessageBird\Objects\Conversation\HSM\Language::DETERMINISTIC_POLICY;
//$hsmLanguage->policy = \MessageBird\Objects\Conversation\HSM\Language::FALLBACK_POLICY;
$hsmLanguage->code = 'id';

$hsm = new \MessageBird\Objects\Conversation\HSM\Message();
$hsm->templateName = 'send_message_member';
$hsm->namespace = '0d70a159_95d6_4402_b33c_6cb7405b1869';
$hsm->params = [$hsmParam1,$hsmParam2,$hsmParam3];
$hsm->language = $hsmLanguage;

$content = new \MessageBird\Objects\Conversation\Content();
$content->hsm = $hsm;

$message = new \MessageBird\Objects\Conversation\Message();
$message->channelId = '4b5f3a89-c7ee-4ccb-a5ef-4051b0afc3e6';
$message->content = $content;
$message->to = '628983869801';
$message->type = 'hsm';

try {
    $conversation = $messageBird->conversations->start($message);

    var_dump($conversation);
} catch (\Exception $e) {
    echo sprintf("%s: %s", get_class($e), $e->getMessage());
}