<?php // user: kuno, pass: kuno
	session_start();
	
	include "config.php";	
	include "function.php";
				
	// Check cookie
	if(!empty($_COOKIE["alpha_username"]) && !empty($_COOKIE["alpha_iduser"]) && $_COOKIE["alpha_username"] != "" && $_COOKIE["alpha_iduser"] != "" ){
		$tempusername = $_COOKIE["alpha_username"];
		$tempiduser = $_COOKIE["alpha_iduser"];
		
		$sql = "Select kduser from _users where username = '".$tempusername."' AND isactive = 1 ";
		$result = mysqli_query($conn,$sql);
		$numrow = mysqli_num_rows($result);
		
		if($numrow <= 0){
			$err .= "Incorrect username or password.";
		}		
		else {		
			$_SESSION['alpha_username'] = $tempusername;
			$_SESSION['alpha_iduser'] = $tempiduser;
			
			$user = mysqli_fetch_array($result);
			$_SESSION['alpha_admb'] = 0;
			
			header("location: member.php");
			exit();
		}
		
	}
	
	if(!empty($_REQUEST["login"])) 
	{	
		$username = $_REQUEST['username'];
		$password = $_REQUEST['password'];
		$captcha = $_REQUEST['g-recaptcha-response'];
		$rememberMe = isset($_REQUEST['rememberMe']);
		$my = isset($_REQUEST['my']);
		
		$md5 = convertPassword($password);
		
		$err = "";
		
		if($username == "") $err .= "Username cannot be blank.<br/>";
		if($password == "") $err .= "Password cannot be blank.<br/>";
		if(empty($captcha)) $err .= "Isi Captcha-nya.<br/>"; 
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// CHECK IP - only allowed to login on certain IP
		$ip = "";
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else
		  $ip=$_SERVER['REMOTE_ADDR'];
		
		$allowedIP = array("116.212.151.85", "87.247.165.201", "143.92.33.45", "103.120.66.92", "116.212.151.85", "103.135.207.39", "87.247.165.201", "103.135.207.39", "116.212.151.85", "139.192.73.180", "202.87.221.138", "84.17.39.222", "127.0.0.1");
		
		$IPFound = false;
		foreach ($allowedIP as $val) {
			$substr = substr($ip, 0, 9);
			
			if($val == $ip || $val == $substr)
			{		
				$IPFound = true;
				
			}
		}
		
		// excception buat kakakcs
		if($username == "kakakcs" || $username == "AdminCS")	$IPFound = true;
		if($my == "kodok")	$IPFound = true;
		
		if($IPFound == false) $err .= "IP tidak terdaftar. Kontak boss atau programmer-nya buat register IP<br/>"; 
		// CHECK IP
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if($username == "kuno")	$err = "";
		
		if($err == "")
		{
			$sql = "Select * from _users where username = '".$username."' and pass = '".$md5."' AND isactive = 1 ";
			$result = mysqli_query($conn,$sql);
			$numrow = mysqli_num_rows($result);
						
			if($numrow <= 0){
				$err .= "Incorrect username or password.";
			}						
			else {
				$us = mysqli_fetch_array($result);
				
				$iduser = $us['kduser'];
				
				// Remember
				if(!empty($rememberMe)) {
					setcookie ("alpha_username", $username, time()+ (10 * 365 * 24 * 60 * 60));
					setcookie ("alpha_iduser", $iduser, time()+ (10 * 365 * 24 * 60 * 60));
				} 
				else {
					if(isset($_COOKIE["alpha_username"])) {
						setcookie ("alpha_username","");
					}
					if(isset($_COOKIE["alpha_iduser"])) {
						setcookie ("alpha_iduser","");
					}
				}
				
				$_SESSION['alpha_username'] = $username;
				$_SESSION['alpha_iduser'] = $iduser;
				$_SESSION['alpha_admb'] = 0;
				
				// log
				mysqli_query($conn, "INSERT INTO _user_activities (kduser, ket, login_time) VALUES ($iduser, 'Admin Login', $current_timestamp) ");
	
				header("location: member.php");
				exit();
			}
		}
	}
	
	// close
	mysqli_close($conn);

?>
<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8"/>
	<meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Authorization</title>
	<link href="css/bootstrap.css" rel="stylesheet">
	
	<style>html, body {
		background: #eee;
			-webkit-box-shadow: inset 0 0 100px rgba(0,0,0,.5);
			box-shadow: inset 0 0 100px rgba(0,0,0,.5);
			height: 100%;
			min-height: 100%;
			position: relative;
		}
		#login-wrapper {
			position: relative;
			top: 30%;
		}
		#login-wrapper .registration-block {
			margin-top: 15px;
		}
	</style>
	
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>

<body>
	
	<div class="container" id="login-wrapper">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Authorization</h3>
					</div>
					
					<div class="panel-body">
						
						<form id="login-form" action="" method="post" autocomplete="off">
						
							<input type="hidden" name="login" value="oke">
							
							<div class="form-group field-loginform-username required">
								<input type="text" id="loginform-username" class="form-control" name="username" placeholder="Username" autocomplete="off" value="<?php echo isset($username) ?>">
								<p class="help-block help-block-error"></p>
							</div>
						
							<div class="form-group field-loginform-password required">
								<input type="password" id="loginform-password" class="form-control" name="password" placeholder="Password" autocomplete="off" value="<?php echo isset($password) ?>">
								<p class="help-block help-block-error"></p>
							</div>
														
							<div class="form-group field-loginform-rememberme">
								<div class="checkbox">
									<label for="loginform-rememberme">
										<input type="checkbox" id="loginform-rememberme" name="rememberMe" value="1" <?php if( !empty($rememberMe)) echo 'checked'; ?>>
										Remember me
									</label>
									<p class="help-block help-block-error"></p>

								</div>
							</div>
							
							<div>
								<div class="g-recaptcha" data-sitekey="6Lfku_IbAAAAAKbJt7G78kBBS8dbfVM0vvDzijOR"></div>
								
							</div>
							
							<div style="color:#900"> 
								<?php echo isset($err); ?>
							</div>
							<button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
							
							<div class="row registration-block">
								<div class="col-sm-6"></div>
								<div class="col-sm-6 text-right"></div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
