DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `before_deposits_insert`$$

CREATE
    TRIGGER `before_deposits_insert` BEFORE INSERT ON `deposits` 
    FOR EACH ROW BEGIN
	    -- variable declare
	    DECLARE m_kdmember INT;
	    DECLARE m_norek VARCHAR(100);
	    DECLARE m_namarek VARCHAR(100);
	    DECLARE m_kdproduct INT;
	    DECLARE m_kdbank INT;
	    
	    -- select member
	    SELECT kdmember, norek, namarek, kdproduct, kdbank INTO m_kdmember, m_norek, m_namarek, m_kdproduct, m_kdbank 
	    FROM members WHERE username = new.username;
	    
	    SET NEW.`kdmember` = m_kdmember, new.norek = m_norek, new.namarek = m_namarek, new.kdproduct = m_kdproduct, new.kdbank = m_kdbank;

    END;
$$

DELIMITER ;