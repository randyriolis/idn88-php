<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$idcontact_us = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post")
	{
		$err = "";
		$textSuccess = "";
		
		// PROSES FORM
		$nama_contact_us = $_REQUEST['nama_contact_us'];
		$nomor_contact_us = $_REQUEST['nomor_contact_us'];
		$idlink = $_REQUEST['idlink'];
				
		$target_file = "";
		
		if($nama_contact_us == "")		$err .= "<br/>Nama Contact harus diisi";
		if($nomor_contact_us == "")		$err .= "<br/>Nomor / ID Contact harus diisi";
				 
		if($err=="" && $menu == "edit"){			
			$query = "UPDATE contacts SET info='$nomor_contact_us', kdcontact_cat='$nama_contact_us', idlink='$idlink' WHERE kdcontact=$idcontact_us ";
			$textSuccess = "Contact berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		else if($err=="" && $menu == "create"){			
			$query = "INSERT INTO contacts (info, kdcontact_cat, isactive, idlink) VALUES ('$nomor_contact_us', '$nama_contact_us', 1, '$idlink') ";
			$textSuccess = "Contact berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
				
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';
		
		mysqli_close($conn);
		exit();
	}
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT cont.info AS nomor_contact_us, cats.nama AS nama_contact_us, kdcontact, cont.kdcontact_cat, cont.idlink
					FROM contacts cont INNER JOIN contact_cats cats ON cont.kdcontact_cat=cats.kdcontact_cat WHERE kdcontact = $idcontact_us ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Contact: ".$idcontact_us;
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Contact";
	}
?>


<div class="contact_us-update">
	<h1><?php echo $title ?></h1>
	<form id="form_contact_us" class="form-vertical" action="docontactus_menu.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $idcontact_us ?>" >
		<input type="hidden" name="post" value="post" >
		
		<fieldset id="w2">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group field-contact_us-nama_contact_us required">
						<label class="control-label" for="contact_us-nama_contact_us">Nama Contact</label>
						<select name="nama_contact_us" class="form-control">
							<?php 
								$qw = mysqli_query($conn, "SELECT * FROM contact_cats");
								while($aa=mysqli_fetch_array($qw)) {
									$sel = $aa['kdcontact_cat']==$row['kdcontact_cat']? "selected" : "";
									
									echo '<option '.$sel.' value="'.$aa['kdcontact_cat'].'">'.$aa['nama'].'</option>';
								}
							?>
						</select>
						<div class="help-block"></div>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="form-group field-contact_us-nomor_contact_us required">
						<label class="control-label" for="contact_us-nomor_contact_us">Nomor / ID Contact</label>
						<input type="text" id="contact_us-nomor_contact_us" class="form-control" name="nomor_contact_us" value='<?php echo $row['nomor_contact_us'] ?>' placeholder="Nomor / ID Contact...">
						<div class="help-block"></div>
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="form-group field-contact_us-nama_contact_us required">
						<label class="control-label" for="namalink">Website</label>
						<select id="accountsearch-namalink" class="form-control" name="idlink">
						<?php
							$rs = mysqli_query($conn, "SELECT * FROM link WHERE status=1 ");
							
							while($rw=mysqli_fetch_array($rs)) {
								$sel = $rw['idlink']==$row['idlink']? "selected" : "";
								
								echo '<option '.$sel.' value="'.$rw['idlink'].'" >'.$rw['namalink'].'</option>';
							}
							
							for($i=0; $i<count($arrgame); $i++) {
									
								}
						?>
						</select>
						<div class="help-block"></div>
					</div>
				</div>
			</div>
		</fieldset>
		
		<fieldset id="w5">
			<div class="row">				
			
				<div class="col-sm-3">
					<div style="text-align: left; margin-top: 20px">
						<div id="feedback_edit"></div>
						<img class="thisLoadingGif" src="img/loading.gif" />
						<button type="reset" class="btn btn-default">Reset</button> 
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
				
			</div>			
		</fieldset>
		
	</form>

</div>

<?php 
	$formName = '"#form_contact_us"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
