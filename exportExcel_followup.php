<?php
	include_once 'PHPExcel/Classes/PHPExcel.php';
	include_once "inc_login.php";
	include_once "config.php";
	
	function cellColor($objPHPExcel, $cells,$color){
		$objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => $color
			)
		));
	}
	
	function getTanggalFormat($dates) {
		$strtotime = strtotime($dates);
		$arrbulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
		
		$txt = date("d", $strtotime)." ".$arrbulan[date("n")-1]." ".date("Y", $strtotime);
		
		return $txt;
	}

	
	$process = $_REQUEST['process'];
	
	if(empty($process) )
	{
		mysqli_close($conn);
		exit;
	}
	
	$filter_bulan = $_REQUEST['filter_bulan'];
	$filter_tahun = $_REQUEST['filter_tahun'];
	
	$sort = $_REQUEST['sort'];
	
	
	// $wherequery = " AND MONTH(mem.dateassign) = $filter_bulan AND YEAR(mem.dateassign) = $filter_tahun ";
	$wherequery = " AND (from_unixtime(acc.modtime,'%m') = '$filter_bulan' OR from_unixtime(acc.modtime,'%m') = '0$filter_bulan') AND (from_unixtime(acc.modtime,'%Y') = '$filter_tahun')  ";
	
	
	$query = "SELECT fol.*, mem.username, mem.followup_status  
			 FROM followup fol 
			 INNER JOIN members mem ON fol.kdmember = mem.kdmember 
			 INNER JOIN akuns acc ON fol.kdmember = acc.kdmember 
			 WHERE 1=1 
			 AND acc.isassigned = 1 AND mem.isactive = 1 
			 $wherequery 
			 ORDER BY mem.kdmember DESC, fol.idfollowup ASC 
			 ";
			 // echo $query;exit;
			 
	$objPHPExcel = new PHPExcel();
	PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
		
				
	$exec1 = mysqli_query($conn, $query) or die ("Error in Query1".mysql_error());
	$serialnumber=0;
	
	$sheet = array();
	
	// Header
	$tmparray = array("", "Berikan warna pada kolom username berdasarkan niat untuk deposit");
	array_push($sheet,$tmparray);
	
	$tmparray = array("", "", "SUDAH DEPOSIT");
	array_push($sheet,$tmparray);
	
	$tmparray = array("", "", "KEMUNGKINAN KECIL (No salah, no ga aktif, wa tidak dibaca)");
	array_push($sheet,$tmparray);
	
	$tmparray = array("", "", "KEMUNGKINAN SEDANG (WA dibaca, WA delivered)");
	array_push($sheet,$tmparray);
	
	$tmparray = array("", "", "KEMUNGKINAN BAGUS (WA dibalas, minta norek, Nunggu pertandingan)");
	array_push($sheet,$tmparray);
	
	
	// Title
	$tmparray = array("No", "");
	array_push($sheet,$tmparray);
	
	//Set header with temp array
	$tmparray = array("", "Username", "Status", "Contact Person", "Follow Up 1", "Tanggal", "Via", "Follow Up 2", "Tanggal", "Via", "Follow Up 3", "Tanggal", "Via", "Follow Up 4", "Tanggal", "Via", "Follow Up 5", "Tanggal", "Via", "Follow Up 6", "Tanggal", "Via", "Follow Up 7", "Tanggal", "Via", "Follow Up 8", "Tanggal", "Via", "Follow Up 9", "Tanggal", "Via", );
	//take new main array and set header array in it.
	array_push($sheet,$tmparray);
	
	
	$jumlahBaris = 0;
	$prevkdmember = -1;
	$tmparray = array();
	
	while ($res = mysqli_fetch_array($exec1))
	{
		// Different ID
		if($prevkdmember != $res["kdmember"])
		{
			if(empty($tmparray) == false) 
			{
				array_push($sheet, $tmparray);
			}
			
			$prevkdmember = $res["kdmember"];
			$jumlahBaris ++;
			$tmparray = array();
		
			array_push($tmparray, $jumlahBaris);
			array_push($tmparray, strtoupper($res["username"]));
			array_push($tmparray, "");
			array_push($tmparray, "");
			array_push($tmparray, $res["followup_respon"]);
			
			array_push($tmparray, getTanggalFormat($res["followup_date"]) );
			array_push($tmparray, strtoupper($res["followup_via"]));
			
			// Status background color
			$colname = "B".($jumlahBaris + 7);
			if($res["followup_status"] == 0)	cellColor($objPHPExcel, $colname, '00ff00');
			else if($res["followup_status"] == 1)	cellColor($objPHPExcel, $colname, 'ffff00');
			else if($res["followup_status"] == 2)	cellColor($objPHPExcel, $colname, 'ff4b4b');
			else if($res["followup_status"] == 5)	cellColor($objPHPExcel, $colname, '4a86e8');
		}
		
		// Same ID 
		else 
		{
			array_push($tmparray, $res["followup_respon"]);
			
			array_push($tmparray, getTanggalFormat($res["followup_date"]) );
			array_push($tmparray, strtoupper($res["followup_via"]));
		}		
	}
	
	// Push last array
	array_push($sheet, $tmparray);
	
	
	$worksheet = $objPHPExcel->getActiveSheet();
	foreach($sheet as $row => $columns) {
		foreach($columns as $column => $data) {
			// echo $column. ' '.$row.' = ' . $data.'<br/>';			
			$worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
			
		}
	}
	
	$arrbulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$namabulan = $arrbulan[$filter_bulan - 1];
	
	$namafile = "followup_member_".$namabulan."_". $filter_tahun  .".xlsx";
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="'.$namafile.'"');
	
	//make first & second row bold
	$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A6:AE6")->getFont()->setBold(true);
	$objPHPExcel->getActiveSheet()->getStyle("A7:AE7")->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0);
	
	
	$jumlahBaris += 5;
	
	// Number Formatting
	// $objPHPExcel->getActiveSheet()->getStyle('B3:B'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	// $objPHPExcel->getActiveSheet()->getStyle('D3:D'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	
	$objPHPExcel->getActiveSheet()->getStyle('G3:G'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	
	// Merge Title
	$objPHPExcel->getActiveSheet()->mergeCells('B1:H1');
	$objPHPExcel->getActiveSheet()->mergeCells('C2:H2');
	$objPHPExcel->getActiveSheet()->mergeCells('C3:H3');
	$objPHPExcel->getActiveSheet()->mergeCells('C4:H4');
	$objPHPExcel->getActiveSheet()->mergeCells('C5:H5');
	
	$objPHPExcel->getActiveSheet()->mergeCells('A6:A7');
	$objPHPExcel->getActiveSheet()->mergeCells('B6:AE6');	

	// Center Text
	$objPHPExcel->getActiveSheet()->getStyle('B6')->getAlignment()->applyFromArray(
		array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);
	$objPHPExcel->getActiveSheet()->getStyle('E6')->getAlignment()->applyFromArray(
		array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,)
	);

	// Header Color
	cellColor($objPHPExcel, 'B2', '4a86e8');
	cellColor($objPHPExcel, 'B3', 'ff4b4b');
	cellColor($objPHPExcel, 'B4', 'ffff00');
	cellColor($objPHPExcel, 'B5', '00ff00');
	
	// auto size
	// foreach(range('A','Z') as $columnID) {
		// $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
	// }
	
	// Set Width
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(4);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(18);
	
	$nCols = 31; //set the number of columns
    foreach (range(3, $nCols) as $col) {
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
    }
	
	// BORDER	
	$objPHPExcel->getDefaultStyle()
		->getBorders()
		->getTop()
			->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
	$objPHPExcel->getDefaultStyle()
		->getBorders()
		->getBottom()
			->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
	$objPHPExcel->getDefaultStyle()
		->getBorders()
		->getLeft()
			->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
	$objPHPExcel->getDefaultStyle()
		->getBorders()
		->getRight()
			->setBorderStyle(PHPExcel_Style_Border::BORDER_THICK);
	
	/*
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
	*/
	
	 // Save Excel file
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	
  
	mysqli_close($conn);
?>