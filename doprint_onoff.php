<?php 
	session_start();
	include "config.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
		
	$changeID = $_REQUEST['changeID'];
	$changemode = $_REQUEST['changemode'];
	if($changeID != ""){
		
		// edit Toggle Bank
		$query = "UPDATE banks  ".
					   "set isactive = $changemode ".
					   "WHERE kdbank = $changeID";
		$result = mysqli_query($conn, $query);
	}

	
	// all request
	$page = $_REQUEST['page'];
	$nama_bank = $_REQUEST['inisialbank'];	
	$status = $_REQUEST['isactive'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = " AND time != '' ";
	
	if($nama_bank != "")	$wherequery .= " AND inisialbank LIKE '%$nama_bank%' ";
	if($status != "")	$wherequery .= " AND isactive = $status ";
		
	$orderquery = "ORDER BY inisialbank ASC, day ASC ";
	if($sort != "")	
	{
		if($sort == "nama_bank_asc")		$orderquery = "ORDER BY inisialbank ASC ";
		else if($sort == "nama_bank_desc") $orderquery = "ORDER BY inisialbank DESC ";
					
		else if($sort == "status_asc")		$orderquery = "ORDER BY isactive ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY isactive DESC ";		
		
	}
	
	$start = ($page-1) * 20;
	
	// $limitquery = " LIMIT $start, 20 ";
	// if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT * FROM banks WHERE 1=1 ".$wherequery.$orderquery.$limitquery;
	
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	
		
		$bankColor = "";
		
		if($row["inisialbank"] == "BCA") 	$bankColor = "BCA_tr";
		else if($row["inisialbank"] == "MANDIRI") 	$bankColor = "MANDIRI_tr";
		else if($row["inisialbank"] == "BNI") 	$bankColor = "BNI_tr";
		else if($row["inisialbank"] == "CIMB") 	$bankColor = "CIMB_tr";
		else $bankColor = "BRI_tr";
		
?>
					
	<tr class="hoverable_tr <?php echo $bankColor ?>" href="dobank_gantijam.php?id=<?php echo $row['kdbank'] ?>" data-toggle="modal" data-target="#myModal" data-title="Detail Data">	
		<td class="" style="width:45px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="" data-col-seq="1" style="width: 500px;">
			<div style="float:left;">
				<b><?php echo $row['inisialbank'] ?></b>
			</div>
			
			
			<?php 
					if($row["isautomatic"] == 0)	
					{
			?>
			<div style="float:left; margin-left: 60px; border-left: 1px solid #888; width:1px">&nbsp;</div>
			
			
			<a href="#" onClick="manualChange('<?php echo $row["kdbank"] ?>', 1)">			
			<div style="float:left; margin-left: 10px" class="hoverable_on">
				<div style="float:left;">
					<?php 
						if($row["isactive"] == 1)	echo '<img src="img/button on.png" style="height: 20px; vertical-align: middle" />';
						else echo '<img src="img/button normal.png" style="height: 20px; vertical-align: middle" />';
					?>
					
				</div>
				<div style="float:left; margin-left: 3px; padding-top:2px; font-weight: bold; color: black">
					ON
				</div>
			</div>
			</a>
			
			<a href="#" onClick="manualChange('<?php echo $row["kdbank"] ?>', 0)">
			<div style="float:left; margin-left: 20px" class="hoverable_off">
				<div style="float:left;">
					
					<?php 
						if($row["isactive"] == 0)	echo '<img src="img/button off.png" style="height: 20px; vertical-align: middle" />';
						else echo '<img src="img/button normal.png" style="height: 20px; vertical-align: middle" />';
					?>	
					
				</div>
				<div style="float:left; margin-left: 3px; padding-top:2px; font-weight: bold; color: black">
					OFF
				</div>
			</div>
			</a>
			
			<?php } ?>
			
			<div style="float:right; font-weight: bold; margin-right: 10px;">
				<?php 
					if($row["isautomatic"] == 1)	echo 'OTOMATIS';					
					else 
					{
						if($row["isactive"] == 1)		echo 'MANUAL : ON';
						else 									echo 'MANUAL : OFF';
					}
				?>
			</div>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="6">							
			<?php 
				$namahari = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
				
				echo $namahari[$row["day"] ];				
			?>	
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" data-col-seq="7">			
			<?php 
				$jam = explode("-",$row["time"]);
				echo $jam[0];
			?>
		</td>		
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" data-col-seq="7">			
			<?php 
				echo $jam[1];
			?>
		</td>		
	</tr>
		


<?php } ?>

<?php
	$query = "SELECT count(kdbank) as num_rows FROM banks WHERE kdbank > 0 AND isactive = 1  ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{
		// include_once "dohitung_page.php";
	
?>

<?php } ?>

<script>	
	function manualChange(id, mode)
	{
		console.log("ID = "+id+"  || mode = "+mode);
		$("#changeID").val(id);
		$("#changemode").val(mode);
		refreshContent();
		
		event.preventDefault();
		event.stopPropagation();
		
		// black everything		
		//$(".BCA_tr").addClass("blackish");
		//$(".BNI_tr").addClass("blackish");
		//$(".BRI_tr").addClass("blackish");
		//$(".MANDIRI_tr").addClass("blackish");
		
	}
	
	function toggleBank(idaccount)
	{		
		$("#edit").val(idaccount);
		refreshContent();
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>

<style>
	.hoverable_off {
		height: 22px;		
		width: 60px;
		padding-left:5px;
	}
	.hoverable_on {
		height: 22px;		
		width: 55px;
		padding-left:5px;
	}
	.hoverable_off:hover {
		background-color: #ff9999;
	}
	.hoverable_on:hover {
		background-color: #99ff99;
	}
	.blackish, .blackish:hover {
		background-color: #666 !important;
		color: #000 !important;
	}
	
</style>