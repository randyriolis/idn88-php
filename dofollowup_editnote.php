<?php 
	include_once "inc_login.php";
	include_once "config.php";
	include_once "function.php";
	
	$id = $_REQUEST['id'];	
	
	$query = "SELECT * FROM followup WHERE idfollowup = $id ";
	$res = mysqli_query($conn, $query);
	$rowfol = mysqli_fetch_array($res);
	
	$p= $_REQUEST['p'];
	if($p == 'post')
	{			
		$temp_id = $id;
		$followup_via = $_REQUEST["followup_via"];
		$followup_respon = $_REQUEST["followup_respon"];
		
		$err = "";
		
		if($followup_respon == "")		$err = "Isi respon dari member";
		
		if($err == "") 
		{
			$query = "UPDATE followup SET followup_via = '$followup_via', followup_respon = '$followup_respon' WHERE idfollowup = $id";
			$res = mysqli_query($conn, $query);
							
			if($res)		echo '<span style="color:green"> Sukses.<br/>'.$sukses.'</span><br/>';
			else 			echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
		}
		
		else 
		{
			echo '<span style="color:red"> Error<br/>'.$err.'</span><br/>';	
		}
		
		exit();
	}
	
	
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>
		<span style="background-color: #333333; color: white; padding: 5px 20px 5px 20px ;">Laporan Follow Up</span>		
	</h4>
</div>

<form id="form_confirmdeposit" class="form-vertical" action="dofollowup_editnote.php?p=post" method="post">
	<fieldset id="w2">
		<div class="row">
			<div class="col-sm-12">	
				<div class="form-group field-dpconfirm_username required">
					<input type="hidden" name="id" id="id" value="<?php echo $id ?>" />			
										
					<br/>
															
					<div class="col-sm-4">
						<div class="form-group field-member-nama required">
							<label class="control-label" for="member-asal">Follow Up Ke</label>
							
							<input type="text" name="followup_number" id="followup_number" class="form-control"  value="<?php echo $rowfol['followup_number'] ?>" readonly>
						</div>
					</div>			
					
					<div class="col-sm-4">
						<div class="form-group field-member-nama required">
							<label class="control-label" for="member-asal">Tanggal FU</label>							
							<?php 
								$skrg = date("d-m-Y", strtotime($rowfol["followup_date"]));								
							?>
							<input type="text" name="followup_date" id="followup_date" class="form-control"  value="<?php echo $skrg ?>" readonly>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group field-member-nama required">
							<label class="control-label" for="member-asal">VIA</label>							
							
							<?php 
								$followup_via = strtolower($rowfol["followup_via"]);
							?>
							
							<select class="form-control" name="followup_via">
								<option value="wa" <?php if($followup_via == "wa") echo "selected" ?>>WA</option>
								<option value="telp" <?php if($followup_via == "telp") echo "selected" ?>>Telp</option>
								<option value="line" <?php if($followup_via == "line") echo "selected" ?>>Line</option>
								<option value="facebook" <?php if($followup_via == "facebook") echo "selected" ?>>Facebook</option>
								<option value="sms" <?php if($followup_via == "sms") echo "selected" ?>>SMS</option>
								<option value="email" <?php if($followup_via == "email") echo "selected" ?>>Email</option>
							</select>
							
						</div>
					</div>
					
					<div class="col-sm-12">
						<div class="form-group field-member-nama required">
							<label class="control-label" for="member-asal">Respon</label>							
							<textarea name="followup_respon" id="followup_respon" class="form-control" placeholder="Respon Member" rows="6"><?php echo $rowfol["followup_respon"]; ?></textarea>
												
						</div>
					</div>
					
					<div class="clearfix"></div>
					<br/>
					
					<div class="help-block"></div>
					
					<div class="col-sm-12">
						<div style="text-align: right; margin-top: 20px">
							<div id="feedback_create"></div>
							<img class="thisLoadingGif" src="img/loading.gif" />
							<button type="reset" class="btn btn-default">Reset</button> 
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
					
				</div>

			</div>		
			
		</div>
		
	</fieldset>
	

	<fieldset id="w5">
		<div class="row">					
			<div class="col-sm-8 pull-right">
				<div style="text-align: right; ">
					<div id="feedback_confirm"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<?php 
						if($menu == "approve"){
							echo '<button type="submit" class="btn btn-success">Approve</button>';
						}
						else if($menu == "reject"){
							echo '<button type="submit" class="btn btn-danger">Reject</button>';
						}
					?>
					
				</div>
			</div>
			
		</div>
	</fieldset>
	
</form>


<style>	

.radio_status {
	height: 30px;
	line-height: 30px;
	border: 1px solid white;
}
.radio_status_selected {
	background-color: lightgreen;
	border: 1px solid green;
}
.normal_label {
	font-weight: normal;
}
</style>

<script type="text/javascript" src="js/select2.full.js"></script>

<script type="text/javascript">
	
	$(document).ready(function()
	{
		// $(".js-example-basic-single").select2();
		$(".thisLoadingGif").hide();
		
		$(".normal_label").click(function(){
			// Remove other styling
			$(".radio_status_selected").removeClass("radio_status_selected");
			
			$(this).parent().addClass("radio_status_selected");
		});
	});
	
	
</script>
<?php 
	$formName = '"#form_confirmdeposit"';
	$feedback = '"#feedback_confirm"';
	
	include "inc_doscript.php";
	
	mysqli_close($conn);
?>
