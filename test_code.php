<?php

function formatTelp($telp)
{
	$format1 = array('08','09');
	$format2 = array('+628','+8559');
	$format3 = array('62','855');
	$format3 = array('+62','+855');
	$code = substr($telp, 0, 2);
	$code = ($code=='08'||'09') ? str_replace($format1, $format2, $code) : str_replace($format3, $format4, $code);

	$telp = substr($telp, 2, strlen($telp)-2);
	$telp = $code . $telp;

	return $telp;
}

$no_telp = '095725844';

$format_telp = formatTelp($no_telp);

echo str_replace("+", "", $format_telp);
