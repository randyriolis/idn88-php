<?php
	include_once "config.php";
	include_once "function.php";
	include_once "inc_login.php";

	$err = array();
	$arrstyle = array();	
	$arrstyle2 = array();
	
	$post = $_REQUEST['post'];
	if(!empty($post))
	{
		$idtogel = $_REQUEST['idtogel'];
		$tanggal = $_REQUEST['tgl_togel'];
		$hasil = $_REQUEST['nomor_togel'];
		
		$query_ins = "UPDATE togel SET hasil='$hasil', tanggal='$tanggal' WHERE idtogel = $idtogel ";
		if(mysqli_query($conn, $query_ins))		$msg = "<span style='color:green'>Nomor Togel berhasil diganti</span>";
		
	}
	
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Change SGP</title>    	
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "togel";
		include_once "inc_header.php";
		
		$querytogel = "SELECT * FROM togel ORDER BY idtogel DESC LIMIT 0,1";
		$sqltogel = mysqli_query($conn, $querytogel);
		$restogel = mysqli_fetch_array($sqltogel);
		
		$idtogel = $restogel["idtogel"];
		$nomor_togel = $restogel["hasil"];
		$tgl_togel = $restogel["tanggal"];
	?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1></h1>        
			<ul class="breadcrumb"><li><a href="site.php">Home</a></li>
				<li class="active">Change SGP</li>
			</ul>    
		</section>

		<section class="content">
			<div class="change-own-password">
				<h2 class="lte-hide-title">Change SGP</h2>
				<div class="panel panel-default">
					<div class="panel-body">
		
						<div class="user-form">
							<form id="user" class="form-horizontal" action="" method="post" role="form">
								<input type="hidden" name="post" value="post">
								<input type="hidden" name="idtogel" value="<?php echo $idtogel ?>">
								
								<?php 
									if(empty($msg) == false)
									{
										echo '<div style="text-align: center">'.$msg.'</div><br/>';
									}
								?>
								
								<div class="form-group field-changeownpasswordform-password required">
									<label class="control-label col-sm-3" for="changeownpasswordform-password" <?php echo $arrstyle2[1] ?>  >Tanggal</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="tgl_togel" maxlength="255" autocomplete="off" value="<?php echo $tgl_togel ?>">
										<?php echo $err[1] ?>
										<div class="help-block help-block-error "></div>
									</div>
								</div>
								
								<div class="form-group field-changeownpasswordform-current_password required">
									<label class="control-label col-sm-3" for="changeownpasswordform-current_password" <?php echo $arrstyle2[0] ?> >Nomor Togel</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" name="nomor_togel" maxlength="255" autocomplete="off" value="<?php echo $nomor_togel ?>">
										<?php echo $err[0] ?>
										<div class="help-block help-block-error "></div>
									</div>
								</div>			
																

								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<?php echo $err[3] ?>
										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Save</button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>

			</div>
		</section>
	</div>

<?php 
	include_once "inc_script.php";
?>
<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		refreshContent();
		
		jQuery('#modalcreate').modal({"show":false});
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});	
	
	
</script>
<?php 
	// close
	mysqli_close($conn);
?>

</body>

</html>