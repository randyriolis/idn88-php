<?php
	include_once "inc_login.php";
	include_once "config.php";
	
	$page = $_REQUEST['page'];
	if(empty($page)) $page = 1;
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['kdusercat'];
	
	// QUERY
	$sqlgame = "SELECT gm.* FROM products gm WHERE gm.isactive = 1 $tm ORDER BY kdproduct ASC  ";		
	$result = mysqli_query($conn, $sqlgame);
	
	$arrgame = array();
	while($row = mysqli_fetch_assoc($result)) {
		array_push($arrgame, $row);
	}
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Account</title>
    	
	<script src="js/jquery.js"></script>
	<script src="js/new_adminjs.js"></script>
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "account";
		include_once "inc_header.php";
	?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1></h1>			
			<ul class="breadcrumb">
			<li><a href="site.php">Home</a></li>
			<li class="active">Accounts</li>
			</ul>    
		</section>

		<section class="content">
			<div class="account-index">
			<div class="account-form">
			<p>
			<div id="error_create_new_account" style="color:red; font-size:16px;"></div>
			<form id="form_newaccount" class="form-inline" action="docreate_account.php" method="post">
				
				<div class="form-group field-fakeidgame required">
					<label class="control-label sr-only" for="deposit-idmember">Idmember</label>
					
					<select id="new_fakeidgame" class="form-control js-example-basic-single" name="fakeidgame" onChange="pilihGameChange('new_fakeidgame');">
						<option value="0">-- Pilih Game --</option>
						<?php 
							for($i=0; $i<count($arrgame); $i++) 
							{
								$kodeagen = $arrgame[$i]['kodeagen'];								
								$ledak = explode(",",$kodeagen);
								
								$cnt = 0;
								foreach ($ledak as $tempkode) 
								{
									$cnt ++;
									$kdproduct = $arrgame[$i]['kdproduct'];
									$nama = $arrgame[$i]['nama'];
									if( count($ledak) > 1 ) 
									{
										$substr = substr($tempkode, -2);
										$nama .= (" ".$substr);
									}
									
									echo '<option value="'.$kdproduct.'" data-nama="'.$tempkode.'" data-pass="'.$arrgame[$i]['defpass'].'" >
										'.$nama.'
									</option>';
								}
								
							}
						?>						
					</select>
				</div>
				
				<div class="form-group field-account-username required">
					<label class="control-label sr-only" for="account-username">Username</label>
					<input type="text" id="new_username" class="form-control" name="username" maxlength="150" placeholder="Username">
				</div>
				
				<div class="form-group field-account-password required">
					<label class="control-label sr-only" for="account-password">Password</label>
					<input type="text" id="new_password" class="form-control" name="password" maxlength="150" placeholder="Password">
				</div>
				
				<div class="form-group">
					<button type="submit" class="btn btn-success">Create Account</button>
				</div>

			</form>    
			</p>
			</div>


	<div id="w0" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">    
		<div id="w1-pjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000"><div id="w1" class="grid-view hide-resize" data-krajee-grid="kvGridInit_6958ba83"><div class="panel panel-primary">
			<div class="panel-heading">    
				<div class="pull-right">
					<div id="page_number" class="summary">
					</div>
				</div>
				<h3 class="panel-title">
					Account
				</h3>
				<div class="clearfix"></div>
			</div>
		
			<div class="kv-panel-before">    
				<div class="pull-right">
					<div class="btn-toolbar kv-grid-toolbar" role="toolbar">
						<div id="loadingGif" class="btn-group" style="margin-right:20px">
							<img src="img/loading.gif" />
						</div>
						
						<div class="btn-group">
							<a class="btn btn-default" href="account.php" title="Reset Grid" data-pjax="0" onClick="refreshContent(); return false;">
								<i class="glyphicon glyphicon-repeat"></i>
							</a>
						</div>

						<div class="btn-group">
							<a id="showall" class="btn btn-default" href="#" title="Show all data" onClick="tryShowAll(); return false;">
								<i class='glyphicon glyphicon-resize-full'></i> All								
							</a>
						</div>
					</div>
				</div>
		
				<div class="clearfix"></div>
			</div>
		
			<div id="w1-container" class="table-responsive kv-grid-container">
			
			<form id="form_search" class="form-inline" action="doprint_account.php" method="post">
			
			<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap"><colgroup>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col>
			<col class="skip-export"></colgroup>
			<thead>

			<tr>
				<th class="kartik-sheet-style kv-align-center kv-align-middle kv-merged-header" style="width:36px;" rowspan="2" data-col-seq="0">&nbsp;</th>
				<th class="kartik-sheet-style kv-align-middle" style="width:180px;" data-col-seq="1">						
					<a href="#" onClick="return trysort('game');" id="game" class="sorter">Game</a>					
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="2">
					<a href="#" onClick="return trysort('username')" id="username" class="sorter">Username | Password</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="3">
					<a href="#" onClick="return trysort('member')" id="member" class="sorter">Member</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="4">
					<a href="#" onClick="return trysort('status')" id="status" class="sorter">Status</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="5">
					<a href="#" onClick="return trysort('assign')" id="assign" class="sorter">Assign</a>
				</th>
				<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="6">
					Actions
				</th>
			</tr>
			
			<tr id="w1-filters" class="filters skip-export">							
				
				<td>
				
					<input type="hidden" id="page" name="page" value="<?php echo $page ?>" />
					<input type="hidden" id="delete" name="delete" value="" />			
					<input type="hidden" id="sort" name="sort" value="" />
					<input type="hidden" id="totalrow" name="totalrow" value="" />
					<input type="hidden" id="maxrow" name="maxrow" value="20" />
					
					<select id="accountsearch-idgame" class="form-control js-example-basic-single" name="idgame" onchange="refreshContent();">
						<option value="">Any Game</option>
						<?php 
							for($i=0; $i<count($arrgame); $i++) {
								echo '<option value="'.$arrgame[$i]['nama'].'" data-nama="'.$arrgame[$i]['kodeagen'].'" data-pass="'.$arrgame[$i]['defpass'].'" >
									'.$arrgame[$i]['nama'].'
								</option>';
							}
						?>						
					</select>					
				</td>
				<td>
					<input type="text" class="form-control" name="username" style="width:100%">
				</td>
				<td>
					<input type="text" class="form-control" name="member" style="width:100%">
				</td>
				
				<td>
					<select class="form-control" name="status" onchange="refreshContent();">
						<option value=""></option>
						<option value="1">Active</option>
						<option value="0">Inactive</option>
					</select>
				</td>
				<td>
					<select class="form-control" name="assign" onchange="refreshContent();">
						<option value=""></option>
						<option value="1">Assign</option>
						<option value="0">Not Assign</option>
					</select>
					<input type="submit" style="display:none" />
				</td>
				
			</tr>
			
			
			
			

			</thead>
			
			<tbody id="tbody_content">
				
			</tbody>
			
			</table>
			</form>
	
			</div>
	
			
			
		</div>	
	</div>
		</div>
	</div>
		</div>

	</section>
	</div>
	

	<?php 
		include_once "inc_script.php";
	?>
	
	<?php 
		// close
		mysqli_close($conn);
	?>
</body>

<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	
	function changeNewAccountPassword(what)
	{
		var option = $('#'+what).find('option:selected');
		
		var pass = $(option).data("pass");		
		if(pass == "" || pass == 0 || pass == "undefined")			return;
		
		var nm = $("#new_username").val();
		
		var last = nm.substr(-1);	
		pass += last;
		
		if(last % 2 == 0)		pass += "n";
		else pass += "j";
		
		$("#new_password").val(pass);
		
	}

	$(document).ready(function()
	{
		$(".js-example-basic-single").select2();
		refreshContent();
		
		$("#new_username").keyup(function() {			
			changeNewAccountPassword('new_fakeidgame');			
		});
		
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});
	
	// Attach a submit handler to the form
	$("#form_newaccount").submit(function( event ) {
		event.preventDefault();
		
		var cont = true;
				
		if(cont == true){			
			// Get some values from elements on the page:
			var $form = $("#form_newaccount");
			var sent = $($form).serialize();
			var url = $form.attr("action");
			
			// Send the data using post
			var posting = $.post( url, sent );
			
			$("#loadingGif").css("display","");
			
			// Put the results in a div
			posting.done(function( data ) {				
				console.log(data);
				
				$("#loadingGif").css("display","none");				
				if(data == "sukses"){
					refreshContent();
					
					// reset delete variables
					pilihGameChange('new_fakeidgame');
					// $("#new_username").val("");
					// $("#new_password").val("");
					
					$("#new_username").css("border", "1px solid lightgray");
					$("#new_password").css("border", "1px solid lightgray");
				}
				else{
					$("#new_username").css("border", "1px solid red");
					$("#new_password").css("border", "1px solid red");
					
					$("#error_create_new_account").html(data);
					$('#error_create_new_account').show(0).delay(3000).hide(0);
				}
			});
			
			refreshContent();
		}
	});
	
	
</script>	
</html>