<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM _users WHERE kduser = $delete ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$username = $_REQUEST['username'];
	$pass = $_REQUEST['pass'];	
	$isactive = $_REQUEST['isactive'];
	$kdusercat = $_REQUEST['kdusercat'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = "";
	
	if($username != "")	$wherequery .= " AND username LIKE '%$username%' ";
	if($pass != "")	$wherequery .= " AND pass LIKE '%$pass%' ";
	if($hex_password != "")	$wherequery .= " AND hex_password like '%$hex_password%' ";
	if($kdusercat != "")	$wherequery .= " AND kdusercat = $kdusercat ";
	if($isactive != "")	$wherequery .= " AND isactive = $isactive ";	
	
	
	$orderquery = "ORDER BY us.superadmin DESC, us.kduser ASC ";
	if($sort != "")	
	{
		if($sort == "username_asc")		$orderquery = "ORDER BY username ASC ";
		else if($sort == "username_desc") $orderquery = "ORDER BY username DESC ";
		
		else if($sort == "pass_asc")		$orderquery = "ORDER BY pass ASC ";
		else if($sort == "pass_desc") $orderquery = "ORDER BY pass DESC ";
		
		else if($sort == "kdusercat_asc")		$orderquery = "ORDER BY kdusercat ASC ";
		else if($sort == "kdusercat_desc") $orderquery = "ORDER BY kdusercat DESC ";
				
		else if($sort == "isactive_asc")		$orderquery = "ORDER BY isactive ASC ";
		else if($sort == "isactive_desc") $orderquery = "ORDER BY isactive DESC ";
		
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT us.* FROM _users us ".				  
				   "WHERE 1=1 AND kduser > 3  AND kdusercat <= 90  
				   ".$wherequery.$orderquery.$limitquery;	
				   echo $query;
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	

?>
	<tr data-key="<?php echo $row['kduser'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1">
			<?php
				$class = "";
				
				if($row['kdusercat'] == 0)	{
					$class = "default";
				}
				else if($row['kdusercat'] == 2)	{
					$class = "warning";
				}
				else if($row['kdusercat'] == 1)	{
					$class = "danger";
				}
				
				echo '<a class="btn btn-'.$class.' btn-sm modalButton" type="button" id="modalButton" value="doadmin_log.php?iduser='.$row['kduser'].'&rd='.rand(10,100).'" href="#" onClick="return false;" style="min-width:50px;">
								'.$row['username'].'
							</a>';
							
			?>
			
		</td>
		<td class="kv-align-center kv-align-middle" data-col-seq="2">-</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="5">
			<?php 
				if($row['isactive'] == 1) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>
		</td>		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="6">
			<?php 
				if($row['isheadadmin'] == 1)	echo 'S';
				else if($row['superadmin'] == 1)		echo 'A';
				else if($row['kdusercat'] == 0)		echo 'F';
				else echo 'B';
			?>	
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="douser_menu.php?menu=edit&id=<?php echo $row['kduser'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="8">
			<?php 			
				if($iduser != $row['kduser'] && $row["superadmin"] != 1){ 
			?>
				<a class="btn btn-danger btn-xs" href="#" onClick="deleteAdmin(<?php echo $row['kduser'] ?>); return false;">
				<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
			<?php } ?>
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(id) as num_rows FROM user WHERE id > 0  ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deleteAdmin(idaccount)
	{
		var str = "Apakah Kamu yakin delete Admin ini ?? ";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>