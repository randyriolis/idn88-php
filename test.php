<?php
	include_once "inc_login.php";
	include_once "config.php";
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	
	$tgl = $_REQUEST["tgl"];
	
	// UPDATE level B dan udah milih link || Consider it is level 0
	if($alpha_admb == 0 && $levelAdmin==2)		$levelAdmin = 0;
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Panel Admin</title>
    	
	<script src="js/jquery.js"></script>
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "site";
		include_once "inc_header.php";
		
		// $default_today = " AND DATE($modtime) = $current_date ";
		
		// $cretime = "from_unixtime(cretime,'%Y-%m-%d') ";
		// $cretime = "CONVERT_TZ(from_unixtime(cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00')";
		$cretime = "CONVERT_TZ(from_unixtime(cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00')";
		
		if(empty($tgl) == false) 
		{
			$sekarang = date("Y-m-d H:i:s");
			$current_date = "DATE(CONVERT_TZ('$sekarang', @@session.time_zone, '+7:00'))";
		}
		
		date_default_timezone_set('Asia/Jakarta');
		$sekarang = date("Y-m-d H:i:s");		
		
		$default_today = " AND DATEDIFF('$sekarang', DATE($cretime)) = 0 ";
		$default_yesterday = " AND DATEDIFF('$sekarang', DATE($cretime)) = 1 ";
		
	?>

	
<div class="content-wrapper">
    <section class="content-header">
		<h1></h1>
    </section>

    <section class="content">
		<div class="site-index">
			<?php 
				$wherequery = " AND nf.status = 1 ";
				
				if($levelAdmin == 1) 
				{
					// Kepalacs bisa liat yang AjukanLossMember
					$wherequery .= " AND (type = 'AjukanLossMember' OR type = 'RevertLossMember' OR nf.idadmin = $iduser) ";
				}
				else 
				{
					$wherequery .= " AND type != 'AjukanLossMember' AND nf.idadmin = $iduser AND NOW() <= nf.dateend  ";
				}
				$sqlnotif = "SELECT nf.*, 
							 acc.username, usr.username as nama_admin, fol.followup_respon 
							 FROM notification nf 
							 LEFT JOIN followup fol ON nf.idfollowup = fol.idfollowup 
							 LEFT JOIN akuns acc ON nf.idmember = acc.kdmember 
							 LEFT JOIN _users usr ON nf.idadmin = usr.kduser 
							 WHERE 1=1 $wherequery 
							 ORDER BY idnotification DESC 
							 ";
							 
				$resnotif = mysqli_query($conn, $sqlnotif);				
				$jmlnotif = mysqli_num_rows($resnotif);
				
				if($jmlnotif > 0) {
			?>
			
			<div class="panel note" style="padding-top:8px; padding-left:10px; border-left:5px solid red; font-size:15px; margin-bottom: 10px; background: #ffcccc">
				<p>NOTIFIKASI</p>					
			</div>
			
			<?php 
				while($tempnotif = mysqli_fetch_array($resnotif) ) 
				{
					$str = "";
					$type = $tempnotif["type"];
					
					if($type == "TolakLossMember") {
						$str = "<b>".$tempnotif["username"]."</b> tidak disetujui menjadi LOSS MEMBER";
					}
					
					else if($type == "SetujuLossMember") {
						$str = "<b>".$tempnotif["username"]."</b> telah disetujui menjadi LOSS MEMBER";
					}
					
					else if($type == "TolakRevertMember") {
						$str = "<b>".$tempnotif["username"]."</b> tidak disetujui menjadi ACTIVE MEMBER";
					}
					
					else if($type == "SetujuRevertMember") {
						$str = "<b>".$tempnotif["username"]."</b> telah disetujui menjadi ACTIVE MEMBER";
					}
					
					else if($type == "AjukanLossMember") {
						$str = "<b>".$tempnotif["username"]."</b> diajukan LOSS MEMBER oleh tele <b>".$tempnotif["nama_admin"]."</b>(".$tempnotif["followup_respon"].")";					
					}
					
					else if($type == "RevertLossMember") {
						$newstat = "";
						$followup_status = $tempnotif["new_followup_status"];
						if($followup_status == 0)		$newstat = 'Kemungkinan Bagus';
						else if($followup_status == 1)	$newstat = 'Kemungkinan Sedang';
						else if($followup_status == 2)	$newstat = 'Kemungkinan Kecil';
						else if($followup_status == 5)	$newstat = 'Sudah Depo';
								
						$newstat = "<u>".$newstat."</u>";
						
						$str = "<b>".$tempnotif["username"]."</b> dikembalikan jadi ".$newstat." oleh tele <b>".$tempnotif["nama_admin"]."</b>(".$tempnotif["followup_respon"].")";					
					}
			?>
			
				<div class="notif_normal " style="background:#d6ddfa; border:1px solid #bfc4fb; min-height: 30px; line-height: 30px; padding-left: 6px; margin-bottom: 4px;">
					<?php echo $str ?>
					
					
					<?php if($type == "AjukanLossMember" || $type == "RevertLossMember") { ?>
					<a href="followup.php?d=0&id=<?php echo $tempnotif["idmember"] ?>&popup=1&ntf=<?php echo $tempnotif["idnotification"] ?>">
						<button style="margin-left: 15px; height: 20px; line-height: 15px; width: 30px; padding: 0; font-weight: bold; color: #333">Go</button>
					</a>
					<?php } ?>
					
				</div>
				
			<?php 
				}
			?>
				
			<?php } ?>
			
			<div class="panel note" style="padding-top:8px; padding-left:10px; border-left:5px solid yellow; font-size:15px; margin-bottom: 10px;">
				<p>PENGUMUMAN <i style="padding-left:6px" class="fa fa-map-o fa-lg"></i> 
					<?php 
						if($_SESSION['alpha_admb'] == 0) {
							echo 'ALL LINK / WEB';
						}
						else {
														
							echo $bla['namalink'];
						}	
					?>
				</p> 
			</div>
			
			
			
			<?php 
				$wherequery = "";
				
				// Update, see only selected Link
				if($alpha_admb != 0)		$wherequery .= " AND (mem.idlink=$alpha_admb OR mem.idlink = 0) ";
				
				// Not Kepalacs
				else if($levelAdmin != 1) {
					// Can only see assigned link
					$access = $arrUser['link_assigned'];
					$access = explode(",",$access);
					$wherequery .= " AND ( ";
					
					$cnt = 0;
					foreach ($access as $value) {
						if(empty($value) == false) {
							if($cnt > 0) $wherequery .= ' OR ';
							$wherequery .= " mem.idlink=$value ";
							
							$cnt++;
						}
					}
					
					$wherequery .= " OR mem.idlink = 0 )";		
				}
					
				$queryann = "SELECT * FROM backend_announcement mem 
							 WHERE 1=1 
							 AND CURDATE() >= tanggal_mulai AND CURDATE() <= tanggal_akhir 
							 $wherequery";
				 
					 
				$res_ann = mysqli_query($conn, $queryann);
				
				while($tmpan = mysqli_fetch_array($res_ann)) 
				{
					if($tmpan["kepentingan"] == 1) 		$colorannouncement = "announcement_yellow";
					else if($tmpan["kepentingan"] == 2) $colorannouncement = "announcement_blue";
					else if($tmpan["kepentingan"] == 3) $colorannouncement = "announcement_red";
										
				?>
					
					<div class="announcement_normal <?php echo $colorannouncement ?>">
						Tanggal: <?php echo date("d-m-Y", strtotime($tmpan["tanggal_mulai"])); ?> <br/>
						Subject: <b><?php echo $tmpan["judul"]; ?></b> <br/>
						<div style="padding-top: 5px;">
							<?php 
								$isi = $tmpan["isi"];
								$isi = str_replace("\n", "<br/>", $isi);
								echo $isi;
							?>
						</div>
					</div>
					
				<?php
				}
			?>
			
			
			<div class="panel note" style="padding-top:8px; padding-left:10px; border-left:5px solid yellow; font-size:15px; margin-bottom: 10px; margin-top: 10px;">
				<p>REMINDER <i style="padding-left:6px" class="fa fa-map-o fa-lg"></i> 
					<?php 
						if($_SESSION['alpha_admb'] == 0) {
							echo 'ALL LINK / WEB';
						}
						else {
														
							echo $bla['namalink'];
						}	
					?>
				</p> 
			</div>
			
			<div id="holder_reminder" style="margin-bottom: 12px;">
				<?php 							
					$query_reminder = " SELECT mem.*
										FROM members mem 
																				
										WHERE mem.reminder IS NOT NULL AND mem.reminder <= NOW()  
										AND mem.followup_status != 10 
										$wherequery
										";										
					$res_reminder = mysqli_query($conn, $query_reminder);
					
					while($tmprem = mysqli_fetch_array($res_reminder) ) 
					{						
						$colorreminder = "reminder_yellow";
						$img = "img/warning_yellow.png";					
						
				?>
					<div class="reminder_normal <?php echo $colorreminder ?>">
						<div style="width: 22%; border-right: 1px solid #e4e1d0; float: left; font-weight: bold">
							<img src="<?php echo $img ?>" style="width: 16px; margin-right: 6px;" />
							REMINDER FOLLOW UP
						</div>
						
						<div style="width: 8%; border-right: 1px solid #e4e1d0; float: left; padding-left: 10px;">
							<?php 
								$tgl = date("d-m-Y", strtotime($tmprem["reminder"]) );
								echo $tgl;
							?>
						</div>
						
						<div style="border-right: 1px solid #e4e1d0; float: left; padding-left: 10px; font-weight: bold">
							<?php 
								echo $tmprem["username"]. ' ('.$tmprem["nama"].')';
							?>
						</div>
						
						<a href="followup.php?d=0&id=<?php echo $tmprem["kdmember"] ?>">
							<button style="margin-left: 15px; height: 20px; line-height: 15px; width: 30px; padding: 0; font-weight: bold; color: #333">Go</button>
						</a>
					</div>
				<?php
					}
				?>
				
				<?php 							
					$query_latedp = " SELECT mem.*, 
										IFNULL(latest_dp.jumlah, 0) as jumlah, 
										latest_dp.MaxDate AS datedeposit 
										FROM members mem 
										
										LEFT JOIN (
											select t.kdmember, tm.MaxDate, t.jumlah 
											from deposits t 
											inner join ( 
												select kdmember, max(modtime) as MaxDate 
												from deposits aaa
												group by kdmember 
											) tm on t.kdmember = tm.kdmember AND t.modtime = tm.MaxDate 				
											WHERE t.isactive = 1 AND t.isclear = 1 AND t.ispending = 0 
										) latest_dp ON mem.kdmember = latest_dp.kdmember 
										
										WHERE 1=1 AND mem.reminder IS NULL 
										AND latest_dp.MaxDate is not NULL
										AND mem.followup_status != 10 
										AND from_unixtime(latest_dp.MaxDate,'%Y-%m-%d %H:%i:%s') >= NOW()-interval 3 month 
										$wherequery
										ORDER BY latest_dp.MaxDate DESC
										
										";									
					$res_latedp = mysqli_query($conn, $query_latedp);
					
					while($tmplatedp = mysqli_fetch_array($res_latedp) ) 
					{
						$datedeposit = $tmplatedp["datedeposit"];
						
						$now = time(); // or your date as well						
						$datediff = $now - $datedeposit;
						$latestdp = round($datediff / (60 * 60 * 24));

						if($latestdp >= 14 && $latestdp <= 28)	
						{
							$colorreminder = "reminder_red";
							$img = "img/warning_red.png";
				?>
					<div class="reminder_normal <?php echo $colorreminder ?>">
						<div style="width: 22%; border-right: 1px solid #e4e1d0; float: left; font-weight: bold">
							<img src="<?php echo $img ?>" style="width: 16px; margin-right: 6px;" />
							WARNING 2 MINGGU TIDAK DEPO 
						</div>
						
						<div style="width: 8%; border-right: 1px solid #e4e1d0; float: left; padding-left: 10px;">
							<?php 
								$tgl = date("d-m-Y", $datedeposit );
								echo $tgl;
							?>
						</div>
						
						<div style="border-right: 1px solid #e4e1d0; float: left; padding-left: 10px; font-weight: bold">
							<?php 
								echo $tmplatedp["username"]. ' ('.$tmplatedp["nama"].')';
							?>
						</div>
						
						<a href="followup.php?d=0&id=<?php echo $tmplatedp["kdmember"] ?>">
							<button style="margin-left: 15px; height: 20px; line-height: 15px; width: 30px; padding: 0; font-weight: bold; color: #333">Go</button>
						</a>
					</div>
				<?php
						}
					}
				?>
			</div>
		
		
			<div class="panel note" style="padding-top:8px;padding-bottom:4px; padding-left:10px; border-left:5px solid yellow; font-size:15px;">
				<p>Total Data on <i style="padding-left:6px" class="fa fa-map-o fa-lg"></i> 
					<?php 
						if($_SESSION['alpha_admb'] == 0) {
							echo 'ALL LINK / WEB';
						}
						else {
														
							echo $bla['namalink'];
						}	
					?>
				</p> 
			</div>

			<?php 
				$mx = 1;
				
				for($i=0; $i<$mx; $i++) {
					
					$query_today = $default_today;
					$query_yesterday = $default_yesterday;
			
			?>
			<div class="body-content">
				<div class="row">
					<div class="col-lg-2">
						<div class="alert alert-warning" role="alert">
							<p class="text-center">
								<strong><i class="fa fa-user-o" aria-hidden="true"></i><br />
									Today Member : 
									<?php
										$sql = "SELECT mem.* FROM members mem												
													WHERE mem.isactive = 1 ".$query_today;
											
										$result = mysqli_query($conn, $sql);
										$num_rows = mysqli_num_rows($result);
										echo $num_rows;

										
									?>
								</strong>
							</p>
						</div>
					</div>
				
				
					<div class="col-lg-2">
						<div class="alert alert-success" role="alert">
							<p class="text-center">
							<strong><i class="fa fa-money" aria-hidden="true"></i><br />
								Today Deposit<br />
								RP 
								<?php
									$sql = "SELECT dp.* FROM deposits dp											
												WHERE dp.isactive = 1 AND dp.isclear =  1 AND dp.isrejected = 0 ".$query_today;
									$result = mysqli_query($conn, $sql);
									$today_depo = mysqli_num_rows($result);
																		
									$sum = 0;	
									while($row = mysqli_fetch_assoc($result)) {
										$sum += $row['jumlah'];
									}
									echo number_format($sum);
								?>
								
								<br />
								<?php 
									echo $today_depo;
								?> 
								transactions
							</strong>
							</p>
						</div>
					</div>
				
					<div class="col-lg-2">
						<div class="alert alert-danger" role="alert">
							<p class="text-center">
							<strong><i class="fa fa-share-square-o" aria-hidden="true"></i><br />
								Today Withdraw<br />
								RP 
								<?php
									$sql = "SELECT * FROM withdraws wd										
												WHERE wd.isactive = 1 AND wd.isclear =  1  AND wd.isrejected = 0 ".$query_today;									
									$result = mysqli_query($conn, $sql);
									$today_withdraw = mysqli_num_rows($result);
									
									$sum2 = 0;	
									while($row = mysqli_fetch_assoc($result)) {
										$sum2 += $row['jumlah'];
									}
									echo number_format($sum2);
								?>
								
								<br />
								<?php 
									echo $today_withdraw;
								?> 								
								transactions
							</strong>
							</p>
						</div>
					</div>
				
					<div class="col-lg-2">
						<div class="alert alert-info" role="alert">
							<p class="text-center">
							<strong><i class="fa fa-share-square-o" aria-hidden="true"></i><br />
								Today Total<br />
								RP
								<?php
									echo number_format($sum - $sum2);
								?>
								
								<br />
								<?php
									echo $today_withdraw + $today_depo;
								?> 
								transactions								
							</strong></p>
						</div>
					</div>
				
				</div>

				<div class="row">
					<div class="col-lg-2">
						<div class="alert alert-warning" role="alert">
							<p class="text-center">
							<strong> <i class="fa fa-user-o" aria-hidden="true"></i><br />
								Yesterday Member : 
								<?php
									$sql = "SELECT * FROM members mem											
												WHERE mem.isactive = 1 ".$query_yesterday;
									$result = mysqli_query($conn, $sql);
									$num_rows = mysqli_num_rows($result);
									echo $num_rows;										
								?>
							</strong></p>
						</div>
					</div>
				
					<div class="col-lg-2">
						<div class="alert alert-success" role="alert">
							<p class="text-center">
								<strong><i class="fa fa-money" aria-hidden="true"></i><br />
								Yesterday Deposit<br />
								RP 
								<?php
									$sql = "SELECT * FROM deposits dp											
												WHERE dp.isactive = 1 AND dp.isclear =  1 AND dp.isrejected = 0 AND dp.isrejected = 0 ".$query_yesterday;
									$result = mysqli_query($conn, $sql);
									$yest_depo = mysqli_num_rows($result);
																		
									$sum = 0;	
									while($row = mysqli_fetch_assoc($result)) {
										$sum += $row['jumlah'];
									}
									echo number_format($sum);
								?>
								
								<br />
								<?php 
									echo $yest_depo;
								?> 
								transactions
								</strong></p>
						</div>
					</div>
				
					<div class="col-lg-2">
						<div class="alert alert-danger" role="alert">
							<p class="text-center">
								<strong><i class="fa fa-share-square-o" aria-hidden="true"></i><br />
								Yesterday Withdraw<br />
								RP 
								<?php
									$sql = "SELECT * FROM withdraws wd											
												WHERE wd.isactive = 1 AND wd.isclear =  1 AND wd.isrejected = 0 ".$query_yesterday;
									$result = mysqli_query($conn, $sql);
									$yest_with = mysqli_num_rows($result);									
									
									$sum2 = 0;	
									while($row = mysqli_fetch_assoc($result)) {
										$sum2 += $row['jumlah'];
									}
									echo number_format($sum2);
								?>
								
								<br />
								<?php 
									echo $yest_with;
								?> 								
								transactions
								</strong></p>
						</div>
					</div>
				
					<div class="col-lg-2">
						<div class="alert alert-info" role="alert">
							<p class="text-center"><strong>
							<i class="fa fa-share-square-o" aria-hidden="true"></i><br />
								Yesterday Total<br />
								RP 
								<?php
									echo number_format($sum - $sum2)				
								?>
								
								<br />
								<?php
									echo $yest_with + $yest_depo;
								?> 
								transactions
							</strong></p>
						</div>
					</div>
					
				</div>

			</div>
			<?php } ?>
			
		</div>
    </section>
</div>
	

	<?php 
		include_once "inc_script.php";
	
		// close
		mysqli_close($conn);
	?>
</body>

</html>