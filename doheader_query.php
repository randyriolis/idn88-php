<?php
	//include_once "inc_login.php";
	include_once "config.php";
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	$ignore_dpwd = $arrUser["ignore_dpwd"];
	
	if($ignore_dpwd == 1) {
		$query_dpwd	= "";
	}
	else {
		$query_dpwd = "CROSS JOIN (SELECT count(kddeposit) as total_deposit FROM deposits dp
						LEFT JOIN members mem ON mem.kdmember=dp.kdmember
						WHERE dp.isactive=1 AND dp.ispending=1 ) AS temp_deposit
					
					CROSS JOIN (SELECT count(kdwithdraw) as total_withdraw FROM withdraws wd 
						LEFT JOIN members mem ON mem.kdmember=wd.kdmember
						WHERE wd.isactive=1 AND wd.ispending=1 ) AS temp_withdraw";
	}
	
	$query = "SELECT * FROM 
					(SELECT count(kdmember) as total_member 
						FROM members mem 
						INNER JOIN link ln ON mem.idlink = ln.idlink
						WHERE mem.isactive=1 AND ( (mem.username='' AND ln.ispoker = 0) OR (mem.isstatus=0 AND ln.ispoker=1) ) AND mem.isdeleted=0 AND ln.status = 1) AS temp_member 
					
					".$query_dpwd;
						
	$result = mysqli_query($conn, $query);	
	$row = mysqli_fetch_array($result);
	
	$total_member = $row['total_member'];
	$total_deposit = empty($row['total_deposit'])? 0 : $row['total_deposit'];
	$total_withdraw = empty($row['total_withdraw'])? 0 : $row['total_withdraw'];
	
	echo $total_member. ',' .$total_deposit. ',' .$total_withdraw;
	
	mysqli_close($conn);
?>