<?php 
	include_once "inc_login.php";
	include "config.php";
	include "function.php";
		
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	$namaUser = $arrUser['username'];

	$delete = $_REQUEST['delete'];
	if($delete != "") 
	{
		$queryAcc = "DELETE FROM withdraws WHERE kdwithdraw = $delete ";
		mysqli_query($conn, $queryAcc);
	}
	
	$temp_function = $_REQUEST['temp_function'];
	if($temp_function != "")
	{
		$temp_id = $_REQUEST['temp_id'];
		$iduser = $_SESSION['alpha_iduser'];
		
		// approve account
		if($temp_function == "approve"){
			$query = "UPDATE withdraws SET isactive = 1, isclear = 1, ispending = 0, isrejected = 0, cretime=$wkt, cleartime=$wkt, clearby='$namaUser' WHERE kdwithdraw = $temp_id ";
			$result = mysqli_query($conn, $query);
			
			mysqli_query($conn, "INSERT INTO zlog_withdraw (kdwithdraw, kduser, keterangan, datecreate) VALUES ($temp_id, $iduser, 'Approve Withdraw SMS & E-MAIL', $current_timestamp) ");
			
			// process to send sms
			$getmember = mysqli_fetch_array(mysqli_query($conn, "SELECT mem.tlp, dp.jumlah FROM withdraws dp INNER JOIN member mem ON dp.kdmember=mem.kdmember WHERE kdwithdraw=$temp_id "));
			$msg = "Terima Kasih, Withdraw anda telah kami terima sebesar ".$getmember['jumlah'].". Selamat Bermain";
			$msg = str_replace(" ", "%20", $msg);
			$msg = str_replace("\n", "%0A", $msg);
		
			$nohp = $getmember['tlp'];
			SendSMS($msg, $nohp);
		}
		else if($temp_function == "approveWithoutSms"){
			$query = "UPDATE withdraws SET isactive = 1, isclear = 1, ispending = 0, isrejected = 0, cretime=$wkt, cleartime=$wkt, clearby='$namaUser' WHERE kdwithdraw = $temp_id ";
			$result = mysqli_query($conn, $query);
			
			mysqli_query($conn, "INSERT INTO zlog_withdraw (kdwithdraw, kduser, keterangan, datecreate) VALUES ($temp_id, $iduser, 'Approve Withdraw Tanpa SMS', $current_timestamp) ");
		}		
		else if($temp_function == "reject") {
			$query = "UPDATE withdraws SET isactive = 0, isclear = 0, ispending = 0, isrejected = 1, rejectedtime=$wkt, rejectedby='$namaUser' WHERE kdwithdraw = $temp_id ";
			$result = mysqli_query($conn, $query);
			
			mysqli_query($conn, "INSERT INTO zlog_withdraw (kdwithdraw, kduser, keterangan, datecreate) VALUES ($temp_id, $iduser, 'Reject Withdraw', $current_timestamp) ");
		}
		else if($temp_function == "pending"){
			$query = "UPDATE withdraws SET isactive = 1, isclear = 0, ispending = 0, isrejected = 0, pendingtime=$wkt, pendingby='$namaUser' WHERE kdwithdraw = $temp_id ";
			$result = mysqli_query($conn, $query);
			
			mysqli_query($conn, "INSERT INTO zlog_withdraw (kdwithdraw, kduser, keterangan, datecreate) VALUES ($temp_id, $iduser, 'Make Withdraw Pending', $current_timestamp) ");
		}
		
	}
		
	$wherequery = " ";	
	
	/*
	if($alpha_admb != 0)		
		$wherequery .= " AND mem.idlink=$alpha_admb  ";
	
	// Not Kepalacs
	else if($levelAdmin != 1) {
		// Can only see assigned link
		$access = $arrUser['link_assigned'];
		$access = explode(",",$access);
		$wherequery .= " AND ( ";
		
		$cnt = 0;
		foreach ($access as $value) {
			if(empty($value) == false) {
				if($cnt > 0) $wherequery .= ' OR ';
				$wherequery .= " mem.idlink=$value ";
				
				$cnt++;
			}
		}
		
		$wherequery .= " )";	
	
	}
	*/
	
	// all request
	$page = $_REQUEST['page'];
	if($page < 1)	$page = 1;
	$username = $_REQUEST['account'];
	$idgame = $_REQUEST['idgame'];
	$bank = $_REQUEST['bank'];
	$nama = $_REQUEST['nama'];
	$no_rekening = $_REQUEST['no_rekening'];
	$email = $_REQUEST['email'];
	$dateassign = $_REQUEST['dateassign'];
	$ip = $_REQUEST['ip'];
	$status = $_REQUEST['status'];
	$jumlah = $_REQUEST['jumlah'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];	
	
	if($username != "")	$wherequery .= " AND (acc.username LIKE '%$username%' OR acc.pass LIKE '%$username%') ";
	if($idgame != "")	$wherequery .= " AND gm.nama like '%$idgame%' ";
	if($bank != "")	$wherequery .= " AND mem.kdbank = '$bank' ";
	if($nama != "")	$wherequery .= " AND mem.nama like '%$nama%' ";
	if($no_rekening != "")	$wherequery .= " AND mem.norek like '%$no_rekening%' ";
	if($email != "")	$wherequery .= " AND mem.email like '%$email%' ";
	if($dateassign != "")	{		
		$wherequery .= " AND DATE_FORMAT(CONVERT_TZ(from_unixtime(dp.cretime, '%Y-%m-%d  %H:%i:%s') , @@session.time_zone, '+7:00'), '%d-%m-%Y') = '$dateassign' ";
	}
	if($ip != "")	$wherequery .= " AND dp.dari_ip1 like '%$ip%' ";
	if($jumlah != "")	$wherequery .= " AND jumlah = $jumlah ";
	
	if($status != "") {
		if($status == 0)
			// pending
			$wherequery .= " AND dp.isactive = 1 AND dp.ispending = 1 ";
		else if($status == 1){
			// accepted
			$wherequery .= " AND dp.status = 1 AND dp.isclear = 1 ";
		}
		else if($status == 2){
			// rejected
			$wherequery .= " AND dp.isrejected = 1 ";
		}
	}	
	
	$orderquery = "ORDER BY kdwithdraw DESC ";
	if($sort != "")	
	{
		if($sort == "game_asc")		$orderquery = "ORDER BY acc.kdproduct ASC ";
		else if($sort == "game_desc") $orderquery = "ORDER BY acc.kdproduct DESC ";
		
		else if($sort == "account_asc")		$orderquery = "ORDER BY acc.username ASC ";
		else if($sort == "account_desc") $orderquery = "ORDER BY acc.username DESC ";
		
		else if($sort == "bank_asc")		$orderquery = "ORDER BY mem.kdbank ASC ";
		else if($sort == "bank_desc") $orderquery = "ORDER BY mem.kdbank DESC ";
		
		else if($sort == "nama_asc")		$orderquery = "ORDER BY mem.nama ASC ";
		else if($sort == "nama_desc") $orderquery = "ORDER BY mem.nama DESC ";
		
		else if($sort == "no_rekening_asc")		$orderquery = "ORDER BY mem.norek ASC ";
		else if($sort == "no_rekening_desc") $orderquery = "ORDER BY mem.norek DESC ";
		
		else if($sort == "email_asc")		$orderquery = "ORDER BY mem.email ASC ";
		else if($sort == "email_desc") $orderquery = "ORDER BY mem.email DESC ";
		
		else if($sort == "dateassign_asc")		$orderquery = "ORDER BY dp.tanggal ASC ";
		else if($sort == "dateassign_desc") $orderquery = "ORDER BY dp.tanggal DESC ";
		
		else if($sort == "ip_asc")		$orderquery = "ORDER BY mem.dari_ip1 ASC ";
		else if($sort == "ip_desc") $orderquery = "ORDER BY mem.dari_ip1 DESC ";
		
		else if($sort == "status_asc")		$orderquery = "ORDER BY isactive ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY isactive DESC ";
		
		else if($sort == "jumlah_asc")		$orderquery = "ORDER BY ABS(jumlah) ASC ";
		else if($sort == "jumlah_desc") $orderquery = "ORDER BY ABS(jumlah) DESC ";
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
	
	$sql_datecreate = "CONVERT_TZ(from_unixtime(dp.tanggal,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00')";
	$sql_dateassign = "CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00')";
	
	$query = "SELECT dp.*, mem.kdmember, mem.nama, mem.tlp, mem.email, acc.kdakun, acc.username AS akun_username, dp.clearby AS nama_admin, dp.tanggal ".
					", gm.nama AS nama_game, bk.inisialbank AS namabank, ln.namalink ".
				   ",$sql_datecreate as new_datecreate ".
					", $sql_dateassign as new_dateassign ".
				   "FROM withdraws dp ".
				   "LEFT JOIN members mem ON dp.kdmember=mem.kdmember ".
				   "LEFT JOIN akuns acc ON mem.kdmember=acc.kdmember ".
				   "LEFT JOIN products gm ON dp.kdproduct=gm.kdproduct ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".			// Update terakhir, jadi semua link cm pake game dari gilabet || tapi data lama kan kagak..
				   "LEFT JOIN banks bk ON dp.kdbank = bk.kdbank ".
				   "WHERE 1=1 ".
				   $wherequery.
				   $orderquery.
				   $limitquery;	
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	
		
		$new_dateassign = date("d-m-Y H:i:s", strtotime($row['new_dateassign']));
		$new_datecreate = date("d-m-Y H:i:s", strtotime($row['new_datecreate']));
		
		$kdproduct = $row['kdproduct'];
		$kdbank = $row['kdbank'];
		$norek = $row['norek'];		
		$nama = $row['nama'];
		$namarek = $row['namarek'];
		$jumlah = $row['jumlah'];
		$tanggal = $row['tanggal'];
		// $isnew = $row['isnew'];
		$isactive = $row['isactive'];
		$ispending = $row['ispending'];
		$isclear = $row['isclear'];
		$clearby = $row['clearby'];
		$cleartime = $row['cleartime'];
		$isrejected = $row['isrejected'];
		$rejectedby = $row['rejectedby'];
		$rejectedtime = $row['rejectedtime'];
		$creby = $row['creby'];
		$cretime = $row['cretime'];
		$modby = $row['modby'];
		$modtime = $row['modtime'];		
		$dari_ip1 = $row['dari_ip1'];
		$idlink = $row['idlink'];
		
				
		$json = "{ -=data-=:-=withdraw-=, -=kdproduct-=:-=$kdproduct-=, -=kdbank-=:-=$kdbank-=, -=norek-=:-=$norek-=, -=nama-=:-=$nama-=, -=namarek-=:-=$namarek-=, ".
				   "-=jumlah-=:-=$jumlah-=, -=tanggal-=:-=$tanggal-=, -=isactive-=:-=$isactive-=, -=ispending-=:-=$ispending-=, ".
				   "-=isclear-=:-=$isclear-=, -=clearby-=:-=$clearby-=, -=cleartime-=:-=$cleartime-=, -=isrejected-=:-=$isrejected-=, -=rejectedby-=:-=$rejectedby-=, ".
				   "-=rejectedtime-=:-=$rejectedtime-=, -=creby-=:-=$creby-=, -=cretime-=:-=$cretime-=, -=modby-=:-=$modby-=, -=modtime-=:-=$modtime-=, ".
				   "-=dari_ip1-=:-=$dari_ip1-=, -=idlink-=:-=$idlink-= }";
		
		if($row['isactive'] == 1 && $row['isclear'] == 0)		$ispending = true;
		else													$ispending = false;
?>
	
	<tr data-key="<?php echo $row['kdwithdraw'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:140px;" data-col-seq="1">
			<?php 
				if( empty($row["akun_username"]) == false )
					echo $row['akun_username']; 
				else 
					echo $row['username']; 
			?>
			<br/>
			<a class="btn btn-default btn-xs" href="#" onClick="copyToClipboard('<?php echo $row['username']; ?>'); return false;">
				<span class="glyphicon glyphicon-plus"></span>
			</a>
			<a class="btn btn-default btn-xs" href="viewmember.php?id=<?php echo $row['kdmember'] ?>" target="_blank" data-pjax="0">
				<span class="glyphicon glyphicon-link"></span>					
			</a>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:180px;" data-col-seq="2">
			<?php
				echo $row['namalink'];			
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:180px;" data-col-seq="2">
			<?php echo $row['nama_game'] ?>			
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:220px;" data-col-seq="3">
			<?php echo $row['namarek'] ?><br />
			<?php echo $row['norek'] ?> <br />
			(<?php echo $row['namabank'] ?>)
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:180px;" data-col-seq="4">
			<a class="btn btn-default btn-xs" href="dowithdraw_editdateassign.php?id=<?php echo $row['kdwithdraw'] ?>" data-toggle="modal" data-target="#myModallog" data-title="Detail Data">
				<?php echo $new_dateassign ?>
			</a><br />
				<?php 
					$namaadmin = $row['nama_admin']=="kuno"? "Boss" : $row['nama_admin'];
					
					$styleAdmin = "";
					$to_time = strtotime($row['new_dateassign']);
					$from_time = strtotime($new_datecreate);
					$diff = round(abs($to_time - $from_time) / 60,2);

					if($diff < 3)  $styleAdmin = 'class="label label-success" ';
					else if($diff > 5)  $styleAdmin = 'class="label label-danger" ';
					else $styleAdmin = 'class="label label-warning" ';
					
					echo "<span $styleAdmin style='font-size:11px;'>".$nama_admin.'</span>'; 
				?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:150px;" data-col-seq="5">
			<?php echo $row['dari_ip1'] ?>
			<br/>
			<a class="btn btn-default btn-xs" href="dowithdraw_log.php?id=<?php echo $row['kdwithdraw'] ?>" data-toggle="modal" data-target="#myModallog" data-title="Detail Data">
				<?php echo $new_datecreate ?>
			</a>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:200px;" data-col-seq="6">
			<?php 
				$tlp = $row['tlp'];
				$email = $row['email'];
				
				if($levelAdmin <= 1) {
					$tlp = substr($tlp, 0, -3) . 'xxx';
					
					// email replace
					$email = $row["email"];
					$mail_parts = explode("@", $email);
					$length = strlen($mail_parts[0]);
					$show = $length - 3;
					$hide = $length - $show;
					$replace = str_repeat("x", $hide);
					$email = substr_replace ( $mail_parts[0] , $replace , $show, $hide ) . "@" . substr_replace($mail_parts[1], "xxx", 0, 3);
				}
				
				
				echo $tlp.'<br/>'.$email;
			?>
		</td>
		<td class="kartik-sheet-style kv-align-right kv-align-middle" style="width:220px;" data-col-seq="7">
			IDR  
			<?php echo number_format($row['jumlah'] , 0, ',', '.'); ?>,00
			
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:150px;" data-col-seq="8">
			<?php 
				if($row['isrejected'] == 1)			echo '<text class="bg-danger"><strong>REJECTED</strong></text>';				
				else if($row['isclear'] == 0 && $row['isactive'] == 1)	echo '<text class="bg-warning"><strong>PENDING</strong></text>';
				else if($row['isactive'] == 1 && $row['isclear'] == 1)	echo '<text class="bg-success"><strong>APPROVED</strong></text>';				
			?>
			
		</td>
		
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:80px;" data-col-seq="14">						
			<a class="btn btn-success btn-xs" style="background-color:#008000" href="#" onClick="copyWithdraw('<?php echo $json; ?>', '<?php echo $row['nama']; ?>', '<?php echo $row['kdwithdraw']; ?>'); return false;"><i class="fa fa-files-o fa-lg" aria-hidden="true"></i><br/>COPY</a>
		</td>
			
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="14">			
			<a class="btn btn-danger btn-xs" style="background-color:#990000" href="#" onClick="deleteWithdraw(<?php echo $row['kdwithdraw'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 						
		</td>
	</tr>


<?php } ?>

<?php
	/*
	$querytot = "SELECT count(kdwithdraw) as num_rows ".
				   "FROM withdraws dp ".
				   "INNER JOIN members mem ON dp.kdmember=mem.kdmember ".
				   "INNER JOIN akuns acc ON mem.kdmember=acc.kdmember ".
				   "INNER JOIN products gm ON mem.kdproduct=gm.kdproduct ".
				   "LEFT JOIN banks bk ON mem.kdbank = bk.kdbank ".
				   "LEFT JOIN _users us ON mem.modby = us.kduser ".
				   "WHERE 1=1 ".
				   $wherequery.
				   $orderquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $querytot));
	*/
	$rowTotal = 9999;	
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);	
	
	//paging
	if($maxrow <= 20)
	{				
		include_once "dohitung_page.php";	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function copyWithdraw(txt, nama, iddeposit) 
	{
		txt = txt.split("-=").join('"');
		
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(txt).select();
		document.execCommand("copy");
		$temp.remove();
		
		$("#copy_clipboard").css("display", "block");
		$("#copy_clipboard").html("'<b>"+nama + "</b>' is copied");
		
		$("#copy_clipboard").fadeTo(1000, 500).slideUp(500, function(){
			$("#copy_clipboard").slideUp(500);
		});  
		
		$("#temp_function").val("approveWithoutSms");
		$("#temp_id").val(iddeposit);		
		refreshContent();
	}
	
	function copyToClipboard(txt) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(txt).select();
		document.execCommand("copy");
		$temp.remove();
		
		$("#copy_clipboard").css("display", "block");
		$("#copy_clipboard").html("'<b>"+txt + "</b>' is copied");
		
		$("#copy_clipboard").fadeTo(1000, 500).slideUp(500, function(){
			$("#copy_clipboard").slideUp(500);
		});  
	}
	
	function deleteWithdraw(kdwithdraw)
	{
		var str = "Apakah Kamu yakin delete Withdraw ini ?? Kehapus bener nih withdrawnya!!";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(kdwithdraw);
			refreshContent();
		}
	}
	
	function approveWithdraw(kdwithdraw, withSms)
	{
		var str = "Apakah Kamu yakin approve Withdraw ini ??";
		var aa = confirm(str);
		if(aa)
		{
			$("#temp_id").val(kdwithdraw);
			
			if(withSms == true)
				$("#temp_function").val("approve");
			else 
				$("#temp_function").val("approveWithoutSms");				
			refreshContent();
		}
	}
	
	function pendingWithdraw(kdwithdraw)
	{
		var str = "Apakah Kamu yakin membuat Withdraw ini menjadi pending??";
		var aa = confirm(str);
		if(aa)
		{
			$("#temp_id").val(kdwithdraw);			
			$("#temp_function").val("pending");
			
			refreshContent();
		}
	}
	
	function rejectWithdraw(kdwithdraw)
	{
		var str = "Apakah Kamu yakin reject Withdraw ini ??";
		var aa = confirm(str);
		if(aa)
		{
			$("#temp_id").val(kdwithdraw);			
			$("#temp_function").val("reject");
			
			refreshContent();
		}
	}
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		
	?>
	
	<?php
		// Saved NEW data into OLD
		echo ' 
			if(localStorage.getItem("total_withdraw_old") == null || '.$rowTotal.' > localStorage.getItem("total_withdraw_old")){
				
				localStorage.setItem("total_withdraw_old", '.$rowTotal.');
			}
		';
	?>
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>