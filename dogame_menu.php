<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$kdproduct = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post"){
		
		$nama = $_REQUEST['nama'];
		$kodeagen = $_REQUEST['kodeagen'];
		$defpass = $_REQUEST['defpass'];
		$url = $_REQUEST['url'];
		$url_mobile = $_REQUEST['url_mobile'];
		$isactive = $_REQUEST['isactive'];
		$kodegame = strtoupper($_REQUEST['kodegame']);
		
		$err = "";
		$textSuccess = "";
		
		if($nama == "")		$err .= "<br/>Nama Game harus diisi";
		if($kodegame == "")		$err .= "<br/>Kode Game harus diisi";
		if($kodeagen == "")		$err .= "<br/>Kodeagen harus diisi";
		if($defpass == "")		$err .= "<br/>Pass harus diisi";
		if($url == "")		$err .= "<br/>Url harus diisi";
		if($url_mobile == "")		$err .= "<br/>Url Mobile harus diisi";
		
		if($err=="" && $menu == "edit"){
			$query = "UPDATE products SET nama='$nama', kodeagen='$kodeagen', defpass='$defpass', url='$url', url_mobile='$url_mobile', isactive=$isactive, kodegame='$kodegame' WHERE kdproduct=$kdproduct ";			
			$textSuccess = "Game berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		else if($err=="" && $menu == "create"){
			$query = "INSERT INTO products (nama, kodeagen, defpass, isactive, url, url_mobile, kodegame) VALUES ('$nama', '$kodeagen', '$defpass', $isactive, '$url', '$url_mobile', '$kodegame') ";
			$textSuccess = "Game berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
				
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';		
		
		mysqli_close($conn);
		exit();
	}
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM products WHERE kdproduct = $kdproduct ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Game: ".$kdproduct;
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Game";
	}
?>

<div class="game-update">
	<h1><?php echo $title ?></h1>
	<form id="form_game" class="form-vertical" action="dogame_menu.php" method="post">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $kdproduct ?>" >
		<input type="hidden" name="post" value="post" >
		<div class="row">
			
			<div class="col-sm-4">		
				<div class="form-group field-game-nama required">
					<label class="control-label" for="game-nama">Nama Game</label>
					<input type="text" id="game-nama" class="form-control" name="nama" placeholder="Nama Game..." value="<?php echo $row['nama'] ?>">

					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group field-game-kodeagen required">
					<label class="control-label" for="game-kodeagen">Hex Account</label>
					<input type="text" id="game-kodeagen" class="form-control" name="kodeagen" placeholder="Hex Account..." value="<?php echo $row['kodeagen'] ?>">
					
					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">	
				<div class="form-group field-game-defpass required">
					<label class="control-label" for="game-defpass">Hex Password</label>
					<input type="text" id="game-defpass" class="form-control" name="defpass" placeholder="Hex Password..." value="<?php echo $row['defpass'] ?>">

					<div class="help-block"></div>
				</div>
			</div>
		</div>

		<div class="row">			
			<div class="col-sm-4">		
				<div class="form-group field-game-kode required">
					<label class="control-label" for="game-kode">Kode Game</label>
					<input type="text" id="game-kode" class="form-control" name="kodegame" placeholder="Kode Game..." value="<?php echo $row['kodegame'] ?>" style="text-transform: uppercase">

					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">		
				<div class="form-group field-game-url required">
					<label class="control-label" for="game-url">URL</label>
					<textarea id="game-url" class="form-control" name="url" placeholder="Url Website..."><?php echo $row['url'] ?></textarea>

					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">		
				<div class="form-group field-game-url required">
					<label class="control-label" for="game-url">URL Mobile</label>
					<textarea id="game-url" class="form-control" name="url_mobile" placeholder="Url Website..."><?php echo $row['url_mobile'] ?></textarea>

					<div class="help-block"></div>
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-4">
				<div class="form-group field-game-isactive required">
					<label class="control-label">isactive</label>
					<input type="hidden" name="Game[isactive]" value="">
					<div id="game-isactive">
						<label class="radio-inline"><input type="radio" name="isactive" value="1" <?php if($menu=="create" || $row['isactive']==1) echo 'checked'; ?>> Active</label>
						<label class="radio-inline"><input type="radio" name="isactive" value="0" <?php if($menu=="edit" && $row['isactive']==0) echo 'checked'; ?>> Inactive</label></div>

					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-12">
				<div style="text-align: right; margin-top: 20px">
					<div id="feedback_edit"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
	
	</form>
</div>

<?php 
	$formName = '"#form_game"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
