<?php 
	include_once "inc_login.php";
	include "config.php";
	include "function.php";
		
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	
	$page = $_REQUEST['page'];
	if(empty($page)) 	$page = 1;
	
	$maxrow = $_REQUEST['maxrow'];
	$idnotification = $_REQUEST['idnotification'];	
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	// FUNCTIONS
	$temp_function = $_REQUEST['temp_function'];
	$temp_idmember = $_REQUEST['temp_idmember'];
	$temp_value = $_REQUEST['temp_value'];
	$depo = $_REQUEST['depo'];
	if(empty($depo))	$depo = 0;
	$filter_hari = $_REQUEST['filter_hari'];
	if(empty($filter_hari) == true)		$filter_hari = 0;
	
	$popup = $_REQUEST["popup"];
	if(empty($popup))	$popup = 0;
		
	if($temp_function == "ganti_asal")
	{		
		$query = "UPDATE members SET followup_asal = '$temp_value' WHERE kdmember = $temp_idmember ";
		$result = mysqli_query($conn, $query);
	}
	if($temp_function == "ganti_status")
	{		
		$query = "UPDATE members SET followup_status = '$temp_value' WHERE kdmember = $temp_idmember ";
		$result = mysqli_query($conn, $query);
	}
	if($temp_function == "set_reminder")
	{		
		if($temp_value == "NULL") {
			$query = "UPDATE members SET reminder = NULL WHERE kdmember = $temp_idmember ";
		}
		else {
			$strtotim = strtotime($temp_value);
			$besok = strtotime("tomorrow");
			$sebulan = strtotime("+30 days");
			
			if($strtotim < $besok)			$strtotim = $besok;
			else if($strtotim > $sebulan)	$strtotim = $sebulan;
			
			$format = date("Y-m-d", $strtotim);
			$query = "UPDATE members SET reminder = '$format' WHERE kdmember = $temp_idmember ";
			
		}
		
		$result = mysqli_query($conn, $query);
	}
	
	
	
	
	// Withdraw 
	$wherequery = "  ";
	
	// Filter query
	$kdmember = $_REQUEST["idmember"];
	
	if(empty($kdmember) == false) {
		$wherequery .= " AND mem.kdmember = '$kdmember' ";
	}
	
	// Update, see only selected Link
	if($alpha_admb != 0)		$wherequery .= " AND mem.idlink=$alpha_admb  ";
	
	// Not Kepalacs
	else if($levelAdmin != 1 && $levelAdmin != 11) {
		// Can only see assigned link
		$access = $arrUser['link_assigned'];
		$access = explode(",",$access);
		$wherequery .= " AND ( ";
		
		$cnt = 0;
		foreach ($access as $value) {
			if(empty($value) == false) {
				if($cnt > 0) $wherequery .= ' OR ';
				$wherequery .= " mem.idlink=$value ";
				
				$cnt++;
			}
		}
		
		$wherequery .= " )";	
	}
	
	// Non-Depo Member
	if($depo == 1)
	{
		$wherequery .= " AND (jumlah = 0) ";
	}
	// Member that has depo
	else if($depo == 2)
	{
		$wherequery .= " AND jumlah > 0 ";
	}
	// Lose Member
	else if($depo == 3)
	{
		$wherequery .= " AND mem.followup_status = 10 ";
	}
	
	// Filter Search
	$username = $_REQUEST['username'];
	$nama = $_REQUEST['nama'];
	$asal = $_REQUEST['asal'];
	$no_hp = $_REQUEST['no_hp'];
	$tanggal_join = $_REQUEST['datecreate'];
	$status = $_REQUEST['status'];
	$latestdp = $_REQUEST['latestdp'];
	$tanggal_latestdp = $_REQUEST['tgl_latestdp'];
	
	
	if($username != "")	$wherequery .= " AND mem.username LIKE '%$username%' ";
	if($nama != "")	$wherequery .= " AND mem.nama like '%$nama%' ";
	if($asal != "")	$wherequery .= " AND (mem.followup_asal = '$asal' OR ln.namalink like '%$asal%') ";	
	if($no_hp != "")	$wherequery .= " AND mem.tlp like '%$no_hp%' ";
	if($tanggal_join != "")	{
		$tanggal_join = date("Y-m-d", strtotime($tanggal_join." 00:00:00"));
		$wherequery .= " AND DATE(CONVERT_TZ(from_unixtime(acc.modtime,'%Y-%m-%d'),".$curtimezone.")) = '$tanggal_join' ";
	}
	if($status != "")	$wherequery .= " AND mem.followup_status = '$status' ";
	if($latestdp != "")	$wherequery .= " AND ABS(latest_dp.jumlah) >= '$latestdp' ";
	
	if($tanggal_latestdp != "")	{		
		$tanggal_latestdp = date("Y-m-d", strtotime($tanggal_latestdp." 00:00:00"));
		$wherequery .= " AND DATE(CONVERT_TZ(from_unixtime(latest_dp.MaxDate,'%Y-%m-%d'), ".$curtimezone.")) = '$tanggal_latestdp' ";
	}
	
	if($tanggal_latestfu != "")	{		
		$tanggal_latestfu = date("Y-m-d", strtotime($tanggal_latestfu." 00:00:00"));		
		$wherequery .= " AND latest_fu.followup_date = '$tanggal_latestfu' ";		
	}
		
		
	
	// 0 - 7 Hari latest DP
	// $new_dateassign = "from_unixtime(acc.modtime,'%Y-%m-%d %H:%i:%s')";
	$new_dateassign = "from_unixtime(latest_dp.MaxDate,'%Y-%m-%d %H:%i:%s')";
	if($filter_hari == 1){
		$wherequery .= " AND $new_dateassign >= DATE(NOW()) + INTERVAL -6 DAY AND $new_dateassign <  NOW() + INTERVAL  1 DAY ";
	}
	// 8 - 30 Hari latest DP
	else if($filter_hari == 2){
		$wherequery .= " AND $new_dateassign >= DATE(NOW()) - INTERVAL 30 DAY AND $new_dateassign <  NOW() - INTERVAL 7 DAY ";
	}
	// > 30 Hari latest DP
	else if($filter_hari == 3){
		$wherequery .= " AND $new_dateassign < DATE(NOW()) - INTERVAL 30 DAY ";
	}
	
	
	// Order by
	$orderquery = " ORDER BY kdmember DESC ";
	$sort = $_REQUEST['sort'];
	if($sort != "")	
	{
		if($sort == "account_asc")		$orderquery = "ORDER BY mem.username ASC ";
		else if($sort == "account_desc") $orderquery = "ORDER BY mem.username DESC ";
		
		else if($sort == "nama_asc")		$orderquery = "ORDER BY trim(mem.nama) ASC ";
		else if($sort == "nama_desc") $orderquery = "ORDER BY trim(mem.nama) DESC ";
		
		else if($sort == "asal_asc")		$orderquery = "ORDER BY mem.followup_asal ASC ";
		else if($sort == "asal_desc") $orderquery = "ORDER BY mem.followup_asal DESC ";
		
		else if($sort == "status_asc")		$orderquery = "ORDER BY mem.followup_status ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY mem.followup_status DESC ";
		
		else if($sort == "telepon_asc")		$orderquery = "ORDER BY mem.tlp ASC ";
		else if($sort == "telepon_desc") $orderquery = "ORDER BY mem.tlp DESC ";
		
		else if($sort == "modtime_asc")		$orderquery = "ORDER BY acc.modtime ASC ";
		else if($sort == "modtime_desc") $orderquery = "ORDER BY acc.modtime DESC ";
		
		else if($sort == "latestdp_asc")		$orderquery = "ORDER BY ABS(latest_dp.jumlah) ASC ";
		else if($sort == "latestdp_desc") $orderquery = "ORDER BY ABS(latest_dp.jumlah) DESC ";
		
		else if($sort == "latestdp_date_asc")		$orderquery = "ORDER BY datedeposit ASC ";
		else if($sort == "latestdp_date_desc") $orderquery = "ORDER BY datedeposit DESC ";
		
		else if($sort == "latestfu_asc")		$orderquery = "ORDER BY latest_fu.followup_date ASC ";
		else if($sort == "latestfu_desc") $orderquery = "ORDER BY latest_fu.followup_date DESC ";
		
	}
		
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";
	

	
	$bulan = 1;
	$tahun = 2018;
	$query = "SELECT mem.kdmember, mem.nama, mem.followup_asal, mem.tlp, mem.tanggal_daftar, mem.namarek    
				, from_unixtime(acc.modtime,'%Y-%m-%d %H:%i:%s') as new_dateassign 
				, mem.followup_status, ln.namalink, ln.ispoker  
			 , mem.username, latest_dp.jumlah, IFNULL(latest_dp.MaxDate, NULL) AS datedeposit, latest_fu.followup_date  
			 , mem.reminder 
			 FROM members mem 
			 LEFT JOIN akuns acc ON mem.kdmember = acc.kdmember 
			 INNER JOIN link ln ON mem.idlink = ln.idlink 
			 
			 LEFT JOIN (
				select t.kdmember, tm.MaxDate, t.jumlah 
				from deposits t 
				inner join ( 
					select kdmember, max(modtime) as MaxDate 
					from deposits aaa
					WHERE aaa.isactive = 1 AND aaa.isclear = 1 AND aaa.ispending = 0 
					group by kdmember 
				) tm on t.kdmember = tm.kdmember AND t.modtime = tm.MaxDate 				
				WHERE t.isactive = 1 AND t.isclear = 1 AND t.ispending = 0 
			) latest_dp ON mem.kdmember = latest_dp.kdmember 
				
			 LEFT JOIN (
				SELECT max(followup_date) as followup_date, kdmember
					FROM followup 
					GROUP BY kdmember 
				) latest_fu ON mem.kdmember = latest_fu.kdmember 
			 			
			 WHERE 1=1 
			 AND mem.isactive = 1 
			 AND ((from_unixtime(acc.modtime,'%m') >= $bulan ) AND (from_unixtime(acc.modtime,'%Y') >= $tahun) 
					OR
				   ln.ispoker = 1
				 )
			 $wherequery
			 $orderquery   
			 $limitquery";
			 
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	
		
		$bgcolor = $cnt %2 ==0? "odd" : "even";
		$accordionID = "collapse_".$cnt;
		
		$kdmember = $row["kdmember"];				
		$query_fu = "SELECT fol.* 
					 FROM followup fol
					 WHERE fol.kdmember = $kdmember  
					 ORDER BY idfollowup DESC 
					 LIMIT 0,1 ";
		$result_fu = mysqli_query($conn, $query_fu);
		$num_fu = mysqli_num_rows($result_fu);
		
		
		if( empty($idnotification) == false) 
		{
			$query_notif = "SELECT * FROM notification notif 
						 WHERE notif.idnotification = $idnotification 
						 ";
			$result_notif = mysqli_query($conn, $query_notif);
			$data_notif = mysqli_fetch_array($result_notif);
				
			if($data_notif["type"] == "AjukanLossMember" || $data_notif["type"] == "RevertLossMember")
				$idedit = ' id="buttonEdit" ';
		}
		// }
		// else $idedit = "";
		
?>
	
	<tr data-key="<?php echo $row['kdmember'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="1">
			<?php 
				echo $cnt;
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="2">
			<?php
				echo $row['username'];
			?>
			<br/>
			<a class="btn btn-default btn-xs" href="#" onClick="copyToClipboard('<?php echo $row['username']; ?>'); return false;">
				<span class="glyphicon glyphicon-plus"></span>
			</a>
			<a class="btn btn-default btn-xs" href="viewmember.php?id=<?php echo $row['kdmember'] ?>" target="_blank" data-pjax="0">
				<span class="glyphicon glyphicon-link"></span>
			</a>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="1">			
			<?php 
				if($row['ispoker'] == 1)	echo htmlspecialchars($row['namarek'], ENT_QUOTES, 'utf-8'); 					
				else						echo htmlspecialchars($row['nama'], ENT_QUOTES, 'utf-8'); 
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="3">			
			<?php 
				$val = $row['followup_asal'];
				if($val == "")	$val = "web";
			?>
			<select class="select_asal" onchange="changeAsal(<?php echo $row["kdmember"] ?>, this.value)">
				<option class="option_asal" value="web" <?php if($val == "web") echo 'selected'; ?> >Web</option>
				<option class="option_asal" value="livechat" <?php if($val == "livechat") echo 'selected'; ?> >Livechat</option>
				<option class="option_asal" value="wa" <?php if($val == "wa") echo 'selected'; ?> >WA</option>
				<option class="option_asal" value="facebook" <?php if($val == "facebook") echo 'selected'; ?> >Facebook</option>
				<option class="option_asal" value="instagram" <?php if($val == "instagram") echo 'selected'; ?> >Instagram</option>
				<option class="option_asal" value="line" <?php if($val == "line") echo 'selected'; ?> >Line</option>
				<option class="option_asal" value="referral" <?php if($val == "referral") echo 'selected'; ?> >Referral</option>
			</select>
			
			<br/>
			<?php echo $row["namalink"]; ?>
			
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="3">
			<?php 
				$tlp = $row["tlp"];
				
				// sensor nomor hp kalao uda ada follow up
				if($num_fu > 0) {
					$tlp = substr($tlp, 0, -3) . 'xxx';
				}
				else {
					$nowa = $tlp;
					if(substr($nowa, 0, 1) == "0")		$nowa = substr($tlp, 1, 99);
						
					$linkwa = "https://api.whatsapp.com/send?phone=62$nowa";
					$tlp = "<u><a href='$linkwa' target='_blank'>$tlp</a></u>";
				}
				
				echo $tlp;
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="4">		
			<?php 				
				if($row['ispoker'] == 1) {
					// $tgl = date("d-m-Y", $row["tanggal_daftar"]);
					echo date("d-m-Y", $row["tanggal_daftar"]); 
					echo '<br/><b>'.date("H:i:s", $row["tanggal_daftar"]).'</b>';
				}
				else {
					// $tgl = date("d-m-Y", strtotime($row["new_dateassign"]));
					echo date("d-m-Y", strtotime($row["new_dateassign"])); 
					echo '<br/><b>'.date("H:i:s", strtotime($row["new_dateassign"])).'</b>';
				}
			?>
		</td>
		
		<?php
			$followup_status = $row["followup_status"];
			$canedit_followup = true;
			
			if($followup_status == 10 || $followup_status == 99)
				$canedit_followup = false;
						
		?>
		
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="4">		
			<a class="followup_bullet" href="#" <?php if($canedit_followup) { ?>onclick="$('#<?php echo 'select_'.$kdmember ?>').toggle()"<?php } else { ?> onclick="return false;" <?php } ?>>
			<?php 
				$followup_status = $row["followup_status"];
				$wd = " style='width: 20px' ";
				
				if($followup_status == 0)		echo '<img src="img/followup_green.png" '.$wd.' title="Kemungkinan Bagus" />';
				else if($followup_status == 1)	echo '<img src="img/followup_yellow.png" '.$wd.' title="Kemungkinan Sedang" />';
				else if($followup_status == 2)	echo '<img src="img/followup_red.png" '.$wd.' title="Kemungkinan Kecil" />';
				else if($followup_status == 5)	echo '<img src="img/followup_blue.png" '.$wd.' title="Sudah Depo" />';
				else if($followup_status == 10)	echo '<img src="img/followup_black.png" '.$wd.' title="Loss Member" />';
				else if($followup_status == 99)	echo '<img src="img/followup_wait.png" '.$wd.' title="Waiting" />';
			?>
			</a>
			<br/>
			
			<div id="<?php echo 'select_'.$kdmember ?>" class="select_status" style="display:none" >
				<div class="option_status" onclick="changeStatus('<?php echo $row["kdmember"] ?>', 5)"><img src="img/followup_blue.png" /> Sudah Depo</div>
				<div class="option_status" onclick="changeStatus('<?php echo $row["kdmember"] ?>', 2)"><img src="img/followup_red.png" /> Kemungkinan Kecil</div>
				<div class="option_status" onclick="changeStatus('<?php echo $row["kdmember"] ?>', 1)"><img src="img/followup_yellow.png" /> Kemungkinan Sedang</div>
				<div class="option_status" onclick="changeStatus('<?php echo $row["kdmember"] ?>', 0)"><img src="img/followup_green.png" /> Kemungkinan Bagus</div>
			</div>			
			
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="4">
			<?php 				
				echo number_format($row["jumlah"]);
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="4">
			<?php
				// echo $row["datedeposit"];			
				
				if($row["datedeposit"] != NULL) {
					$tgl = date("d-m-Y", $row["datedeposit"]);
					echo $tgl;
				}
				else echo '-';
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="4">
			<?php 
				
				if($num_fu <= 0){
					echo '-';
				}
				else {					
					$tgl = date("d-m-Y", strtotime($row["followup_date"]));
					echo $tgl;
				}
			?>
		</td>
				
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="min-width: 140px !important">
			<?php 
				if( empty($row["reminder"]) ) {
					$reminder = "BELUM";
					$stylereminder = "background-color:black; color: white;";
					$showX = false;
				}
				else {
					$reminder = date("d-m-Y", strtotime($row["reminder"]));
					$stylereminder = "";
					$showX = true;
				}
				
			?>
			<input class="form_date" type="text" name="reminder" id="reminder<?php echo $row['kdmember'] ?>" value="<?php echo $reminder ?>" style="height: 30px; padding-left: 2px; <?php echo $stylereminder ?>" readonly onchange="setReminder(<?php echo $row["kdmember"] ?>, this.id)" />
			
			<?php 
				if($showX == true) {
			?>
				<a href="#" type="button" class="btn btn-xs btn-danger" onclick="setReminder(<?php echo $row["kdmember"] ?>, 'NULL'); return false;">
					X
				</a>
			<?php } ?>
		</td>
		
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="5">
			<?php 
				if($canedit_followup == true || $levelAdmin == 1) {
			?>
			<a <?php echo $idedit ?> class="btn btn-default btn-xs" href="dofollowup_edit.php?id=<?php echo $row['kdmember'] ?>&ntf=<?php echo $idnotification ?>" data-toggle="modal" data-target="#myModalUpdate" data-title="Detail Data">			
			<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>
			<br/>Edit
			</a>
			<?php } ?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="5">
			<?php 
				if($num_fu <= 0)
				{ 
			?>
				<a href="#" type="button" class="btn btn-xs btn-info disabled" data-toggle="collapse" data-parent="#accordion"  style="background-color: gray; border:1px solid #888">
					Lihat
				</a>
			<?php } else { ?>
				<a href="#<?php echo $accordionID ?>" type="button" class="btn btn-xs btn-info accordion-toggle accordion-lihat" data-toggle="collapse" data-parent="#accordion" >
					Lihat
				</a>
			<?php } ?>
			
			<a class="btn btn-purple btn-xs" href="dofollowup_addnote.php?id=<?php echo $row['kdmember'] ?>" data-toggle="modal" data-target="#myModalUpdate" data-title="Detail Data" style="width: 33px">
				+
			</a>
		</td>
	</tr>
	
	<tr>
		<td colspan="99"  style="padding-top:0; padding-bottom:0; padding-left:36px">
			<div id="<?php echo $accordionID ?>" class="panel-collapse collapse" data-kdmember="<?php echo $row["kdmember"] ?>">
				
			</div>
		</td>
	</tr>


<?php } ?>

<?php
	$query2 = "SELECT count(mem.kdmember) as num_rows  
			 FROM members mem 
			 LEFT JOIN akuns acc ON mem.kdmember = acc.kdmember 
			 INNER JOIN link ln ON mem.idlink = ln.idlink 
			 			
			 WHERE 1=1 
			 AND mem.isactive = 1 
			 AND ((from_unixtime(acc.modtime,'%m') >= $bulan ) AND (from_unixtime(acc.modtime,'%Y') >= $tahun) 
					OR
				   ln.ispoker = 1
				 )
			 $wherequery ";		
			 
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query2));
	$rowTotal = $result['num_rows'];
	
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>



<script>
	function changeAsal(kdmember, thisvalue)
	{
		// alert(kdmember+" == "+thisvalue);
		$("#temp_function").val("ganti_asal");
		$("#temp_idmember").val(kdmember);
		$("#temp_value").val(thisvalue);
		
		refreshContent();
	}
	
	function changeStatus(kdmember, thisvalue)
	{
		// alert(kdmember+" == "+thisvalue);
		$("#temp_function").val("ganti_status");
		$("#temp_idmember").val(kdmember);
		$("#temp_value").val(thisvalue);
		
		refreshContent();
		
		$(".select_status").hide();
	}
	
	function setReminder(kdmember, id)
	{
		setTimeout(function()
		{ 
			$("#temp_function").val("set_reminder");
			$("#temp_idmember").val(kdmember);
			
			if(id == "NULL")
				$("#temp_value").val("NULL");
			else
				$("#temp_value").val($("#"+id).val());
						
			refreshContent();
		}, 300);
		
		// alert(kdmember+" == "+thisvalue);
		
	}
	
	/*
	$(document).mouseup(function(){
		$(".select_status").hide();
	});
	*/
	
	function copyToClipboard(txt) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(txt).select();
		document.execCommand("copy");
		$temp.remove();
		
		$("#copy_clipboard").css("display", "block");
		$("#copy_clipboard").html("'<b>"+txt + "</b>' is copied");
		
		$("#copy_clipboard").fadeTo(1000, 500).slideUp(500, function(){
			$("#copy_clipboard").slideUp(500);
		});  
	}
	
	$(document).ready(function()
	{		
		<?php 
			if($popup == 1) {				
		?>
			$("#buttonEdit").click();
		
		<?php } ?>
		
		// date click
		$( ".form_date" ).datepicker({
			dateFormat: 'dd-mm-yy'
		});		
		
		$(".accordion-lihat").click(function(){
			
			// Active Div that used
			var tmpdiv = $($(this).attr("href"));
			console.log(tmpdiv.attr("id") );
			
			if($(tmpdiv).is(':hidden') ) {
				$(this).removeClass("btn-info");
				$(this).addClass("btn-danger");
				$(this).html("tutup");
			}
			else {
				$(this).removeClass("btn-danger");
				$(this).addClass("btn-info");
				$(this).html("lihat");
			}
			
			if( $(tmpdiv).html().isEmpty() ) 
			{
				// Show loading
				$("#loadingGif").css("display","");
				
				var kdmember = $(tmpdiv).data("kdmember");
				var url = "doprint_followup_detail.php?kdmember=" +kdmember
				
				$.get( url, function(data) {
					console.log("data = "+data);
					$(tmpdiv).html(data);
					
					// Hide Loading
					$("#loadingGif").css("display","none");
				})
			}
			
		});
	});
	
	String.prototype.isEmpty = function() {
		return (this.length === 0 || !this.trim());
	};
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>

<style>
	.select_asal {
		text-align: center;
		text-align-last:center;
		width: 120px;
		border: 0px;
		background: none;
		-moz-appearance: none;
		-webkit-appearance: none;
	}
	.select_asal:hover {
		background: white;
		border:1px solid #220099;
	}
	select::-ms-expand {
		display: none;
	}
	.option_asal {
		padding-left: 100px;
	}
	
	.select_status {
		width: 170px;
		border: 1px solid darkblue;
		background: white;
		position: absolute;
		margin-left: -30px;
	}
	.option_status {
		text-align: left;
		padding: 4px 2px;
		cursor: default;
		font-size: 12px;
	}
	.option_status:hover{
		background: darkblue;
		color: white;
	}
	.option_status img {
		width: 20px;
		padding-right: 6px;
	}
	
	.followup_bullet:hover {
		opacity: 0.7;
	}
	.btn-purple{background-color:#e90fea;color:#fff}
	.btn-purple:hover{background-color:#b80bb8;color:#fff}	
</style>