<?php 
	session_start();
	include "config.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM banks WHERE kdbank = $delete ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$nama_bank = $_REQUEST['nama_bank'];
	$isactive = $_REQUEST['isactive'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = "";
	
	if($nama_bank != "")	$wherequery .= " AND namabank LIKE '%$nama_bank%' ";
	
	if($isactive != "")	$wherequery .= " AND isactive = $isactive ";
	
	
	$orderquery = "ORDER BY kdbank DESC ";
	if($sort != "")	
	{
		if($sort == "nama_bank_asc")		$orderquery = "ORDER BY nama_bank ASC ";
		else if($sort == "nama_bank_desc") $orderquery = "ORDER BY nama_bank DESC ";
		
		else if($sort == "hex_account_asc")		$orderquery = "ORDER BY hex_account ASC ";
		else if($sort == "hex_account_desc") $orderquery = "ORDER BY hex_account DESC ";
		
		else if($sort == "hex_password_asc")		$orderquery = "ORDER BY hex_password ASC ";
		else if($sort == "hex_password_desc") $orderquery = "ORDER BY hex_password DESC ";
		
		else if($sort == "url_asc")		$orderquery = "ORDER BY url ASC ";
		else if($sort == "url_desc") $orderquery = "ORDER BY url DESC ";
				
		else if($sort == "isactive_asc")		$orderquery = "ORDER BY isactive ASC ";
		else if($sort == "isactive_desc") $orderquery = "ORDER BY isactive DESC ";		
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT * FROM banks WHERE 1=1  ".$wherequery.$orderquery.$limitquery;
	echo $query;
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	
		$atas_nama = ( ! empty( $row['atas_nama'] ) ? $row['atas_nama'] : 'NULL' );
		$nomer_rekening = ( ! empty( $row['nomer_rekening'] ) ? $row['nomer_rekening'] : 'XXX' );
		//$bank = ( ! empty( $row['namabank']) ) ? $row['namabank'] . ' (' . $row['inisialbank'] . ')' : $row['inisialbank'];
		$jenis_bank = array('TRANSFER', 'E-WALLETS', 'DIGITAL');

?>
	<tr data-key="<?php echo $row['kdbank'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1"><?php echo $row['inisialbank'] ?> (<?php echo $jenis_bank[$row['istype']]; ?>)</td>
		<td class="kv-align-center kv-align-middle" data-col-seq="2"><?php echo $atas_nama ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="3"><?php echo $nomer_rekening ?></td>
		<!--<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" data-col-seq="7">			
			<?php 
				$jam = explode("-",$row["time"]);
				echo $jam[0];
			?>
		</td>		
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" data-col-seq="7">			
			<?php 
				echo $jam[1];
			?>
		</td>-->	
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="6">
			<?php 
				if($row['isactive'] == 1) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>	
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="dobank_menu.php?menu=edit&id=<?php echo $row['kdbank'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="8">
			<a class="btn btn-danger btn-xs" href="#" onClick="deletePromo(<?php echo $row['kdbank'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(kdbank) as num_rows FROM banks WHERE kdbank > 0  ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deletePromo(idaccount)
	{
		var str = "Apakah Kamu yakin delete Promo ini ?? Ilang loh Promonya??";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>