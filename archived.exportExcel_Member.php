<?php
	include_once 'PHPExcel/Classes/PHPExcel.php';
	include_once "inc_login.php";
	include_once "config.php";
	
	
	$process = $_REQUEST['process'];
	
	if(empty($process) )
	{
		mysqli_close($conn);
		exit;
	}
	
	$username = $_REQUEST['account'];
	$namalink = $_REQUEST['namalink'];
	$idgame = $_REQUEST['idgame'];
	$kdgame = $_REQUEST['kdgame'];
	$credit = $_REQUEST['credit'];
	$bank = $_REQUEST['bank'];
	$nama = $_REQUEST['nama'];
	$no_rekening = $_REQUEST['no_rekening'];
	$email = $_REQUEST['email'];
	$dateassign = $_REQUEST['dateassign'];
	$ip = $_REQUEST['ip'];
	$status = $_REQUEST['status'];
	$filter_bulan = $_REQUEST['filter_bulan'];
	$filter_tahun = $_REQUEST['filter_tahun'];
	
	$sort = $_REQUEST['sort'];
	
	
	$wherequery = " AND ln.status = 1 AND MONTH(from_unixtime(mem.tanggal_daftar)) = '$filter_bulan' AND YEAR(from_unixtime(mem.tanggal_daftar)) = '$filter_tahun' ";
	
	if($username != "")	$wherequery .= " AND mem.username LIKE '%$username%' ";
	if($idgame > 0)	$wherequery .= " AND mem.kdproduct = $idgame ";
	if($namalink != "")	$wherequery .= " AND ln.namalink like '%$namalink%' ";	
	if($kdgame != "")	$wherequery .= " AND gm.nama like '%$kdgame%' ";	
	if($nama != "")	$wherequery .= " AND mem.nama like '%$nama%' ";
	if($no_rekening != "")	$wherequery .= " AND (mem.norek like '%$no_rekening%' OR bk.namabank like '%$no_rekening%' OR bk.inisialbank like '%$no_rekening%') ";
	if($email != "")	$wherequery .= " AND (mem.email like '%$email%' OR mem.tlp like '%$email%') ";
	if($dateassign != "")	$wherequery .= " AND DATE(CONVERT_TZ($modtime,".$curtimezone.")) like '%$dateassign%' ";
	if($ip != "")	$wherequery .= " AND mem.dari_ip1 like '%$ip%' ";
	if($status != "")	$wherequery .= " AND mem.isactive = $status ";
	
	
	$orderquery = "ORDER BY mem.kdmember DESC ";
	if($sort != "")	
	{
		if($sort == "game_asc")		$orderquery = "ORDER BY gm.nama ASC ";
		else if($sort == "game_desc") $orderquery = "ORDER BY gm.nama DESC ";
		
		else if($sort == "namalink_asc")		$orderquery = "ORDER BY ln.namalink ASC ";
		else if($sort == "namalink_desc") $orderquery = "ORDER BY ln.namalink DESC ";
		
		else if($sort == "account_asc")		$orderquery = "ORDER BY acc.username ASC ";
		else if($sort == "account_desc") $orderquery = "ORDER BY acc.username DESC ";
		
		else if($sort == "bank_asc")		$orderquery = "ORDER BY mem.kdbank ASC ";
		else if($sort == "bank_desc") $orderquery = "ORDER BY mem.kdbank DESC ";
		
		else if($sort == "nama_asc")		$orderquery = "ORDER BY mem.nama ASC ";
		else if($sort == "nama_desc") $orderquery = "ORDER BY mem.nama DESC ";
		
		else if($sort == "norek_asc")		$orderquery = "ORDER BY mem.norek ASC ";
		else if($sort == "norek_desc") $orderquery = "ORDER BY mem.norek DESC ";
		
		else if($sort == "email_asc")		$orderquery = "ORDER BY mem.email ASC ";
		else if($sort == "email_desc") $orderquery = "ORDER BY mem.email DESC ";
		
		else if($sort == "dateassign_asc")		$orderquery = "ORDER BY mem.modtime ASC ";
		else if($sort == "dateassign_desc") $orderquery = "ORDER BY mem.modtime DESC ";
		
		else if($sort == "ip_asc")		$orderquery = "ORDER BY mem.dari_ip1 ASC ";
		else if($sort == "ip_desc") $orderquery = "ORDER BY mem.dari_ip1 DESC ";
		
		else if($sort == "status_asc")		$orderquery = "ORDER BY mem.isactive ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY mem.isactive DESC ";
	}
	
	$query = "SELECT mem.kdmember, acc.isassigned, mem.isactive, mem.tlp, mem.email, mem.norek, mem.nama AS namamember, bk.inisialbank AS namabank,  acc.kdakun, acc.username, gm.nama AS nama_game, gm.kodeagen AS hex_account, acc.modby AS nama_admin, ln.namalink ".
					",from_unixtime(mem.tanggal_daftar,'%Y-%m-%d %H:%i:%s') as new_datecreate ".
					",from_unixtime(acc.modtime,'%Y-%m-%d %H:%i:%s') as new_dateassign ".
				   "FROM members mem ".
				   "LEFT JOIN akuns acc ON acc.kdmember = mem.kdmember ".
				   "LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ".
				   "LEFT JOIN banks bk ON mem.kdbank = bk.kdbank ".
				   "LEFT JOIN link ln ON mem.idlink = ln.idlink ".
				   "WHERE 1=1 ".
				   $wherequery.
				   $orderquery;
			
	$objPHPExcel = new PHPExcel();
	PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
		
				
	$exec1 = mysqli_query($conn, $query) or die ("Error in Query1".mysql_error());
	$serialnumber=0;
	
	$sheet = array();
	
	// Title
	$tmparray = array("Daftar Member");
	array_push($sheet,$tmparray);
	
	//Set header with temp array
	$tmparray = array("#", "USERNAME", "WEBSITE", "NAMA PEMAIN", "NO.REKENING", "KONTAK PEMAIN", "GAME", "TANGGAL ASSIGN", "STATUS" );
	//take new main array and set header array in it.
	array_push($sheet,$tmparray);
	
	
	$jumlahBaris = 1;
	
	while ($res = mysqli_fetch_array($exec1))
	{
		$tmparray = array();
		
		array_push($tmparray, $jumlahBaris);
		array_push($tmparray, $res["username"]);
		array_push($tmparray, $res["namalink"]);
		array_push($tmparray, $res["namamember"]);
		array_push($tmparray, $res["norek"]. " - ".$res["namabank"]);
		array_push($tmparray, $res["tlp"]. " - ".$res["email"]);
		array_push($tmparray, $res["nama_game"]);
				
		if($res['kdakun'] == 0){
			array_push($tmparray, "belum assign");
		}
		else {			
			array_push($tmparray, date("d-m-Y h:i:s", strtotime($res["new_dateassign"])));
		}		
		
		
		if($res["isactive"] == 0)	array_push($tmparray, "Inactive");
		else if($res["isactive"] == 1)	array_push($tmparray, "Active");
		
		
		array_push($sheet, $tmparray);
		$jumlahBaris ++;
	}
	
	
	$worksheet = $objPHPExcel->getActiveSheet();
	foreach($sheet as $row => $columns) {
		foreach($columns as $column => $data) {
			// echo $column. ' '.$row.' = ' . $data.'<br/>';			
			$worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
			
		}
	}

	$arrbulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$namabulan = $arrbulan[$filter_bulan - 1];
	
	$namafile = "daftar_member_".$namabulan."_". $filter_tahun  .".xlsx";
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="'.$namafile.'"');
	
	//make first & second row bold
	$objPHPExcel->getActiveSheet()->getStyle("A1:J1")->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0);
	
	$objPHPExcel->getActiveSheet()->getStyle("A2:J2")->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0);
	
	$jumlahBaris += 5;
	
	// Number Formatting
	// $objPHPExcel->getActiveSheet()->getStyle('B3:B'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	// $objPHPExcel->getActiveSheet()->getStyle('D3:D'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	
	$objPHPExcel->getActiveSheet()->getStyle('G3:G'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	
	// Merge Title
	$objPHPExcel->getActiveSheet()->mergeCells('A1:B1');
	
	// auto size
	foreach(range('A','I') as $columnID) {
		$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);			
	}
	
	/*
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
	*/
	
	 // Save Excel file
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	
  
	mysqli_close($conn);
?>