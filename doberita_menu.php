<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$idberita = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post")
	{
		$err = "";
		$textSuccess = "";
		
		// PROSES FORM
		$judul_berita = $_REQUEST['judul_berita'];
		$isi_berita = $_REQUEST['isi_berita'];
		$status = $_REQUEST['status'];
				
		$target_file = "";
		
		if($judul_berita == "")		$err .= "<br/>Judul Berita harus diisi";
		if($isi_berita == "")		$err .= "<br/>Isi Berita harus diisi";
		 
		if(isset($_FILES['imageberita']) && $err == "" && $_FILES['imageberita']['error'] <= 0)
		{			
			// UPLOAD IMAGE PROMO
			$target_dir = "uploads/";
			
			$uploadOk = 1;
			$imageFileType = pathinfo(basename($_FILES["imageberita"]["name"]), PATHINFO_EXTENSION);
						
			$removeWhite = str_replace(" ","", $judul_berita);					
			$string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $removeWhite);
			$newname = substr($string, 0, 20)."_".$idlink.".".$imageFileType;
			$target_file = $target_dir.$newname;
			
			// Check if image file is a actual image or fake image
			$check = getimagesize($_FILES["imageberita"]["tmp_name"]);
			if($check !== false) {
				$uploadOk = 1;
			} else {
				$err .= "<br/>Kok keknya bukan image";
				$uploadOk = 0;
			}
			
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) 
			{
				$err .= "<br/>Image cuma boleh format JPG, JPEG, PNG & GIF ";
				$uploadOk = 0;
			}
			
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {				
				$err .= "<br/>Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
			} 
			else 
			{
				if (move_uploaded_file($_FILES["imageberita"]["tmp_name"], $target_file)) {
					// echo "The file ". basename( $_FILES["imageberita"]["name"]). " has been uploaded.";
					$data = file_get_contents($target_file);
					$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);				
				} else {
					$err .= "<br/>Sorry, there was an error uploading your file.";
				}
			}
		}
		
		
		if($err=="" && $menu == "edit"){
			$query_targetfile = $base64==""? "" : ", imagelink='$base64' ";
			
			$query = "UPDATE berita SET judul_berita='$judul_berita', status=$status, isi_berita='$isi_berita' ".$query_targetfile." WHERE idberita=$idberita ";
			$textSuccess = "Berita berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		else if($err=="" && $menu == "create"){
			$query_targetfile_1 = $base64==""? "" : "imagelink, ";
			$query_targetfile_2 = $base64==""? "" : ", '$base64' ";
			
			$query = "INSERT INTO berita (judul_berita, status, isi_berita, ".$query_targetfile_1." idlink, datecreate) VALUES ('$judul_berita', $status, '$isi_berita' ".$query_targetfile_2." , $idlink, $current_timestamp) ";
			$textSuccess = "Berita berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
				
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';
		
		mysqli_close($conn);
		exit();
	}
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM berita WHERE idberita = $idberita ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Berita: ".$idberita;
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Berita";
	}
?>

<div class="berita-update">
	<h1><?php echo $title ?></h1>
	<form id="form_berita" class="form-vertical" action="doberita_menu.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $idberita ?>" >
		<input type="hidden" name="post" value="post" >
		
		<fieldset id="w2">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group field-berita-judul_berita required">
						<label class="control-label" for="berita-judul_berita">Nama Berita</label>
						<input type="text" id="berita-judul_berita" class="form-control" name="judul_berita" value="<?php echo $row['judul_berita'] ?>" placeholder="Nama Berita...">
						<div class="help-block"></div>
					</div>
				</div>
				
				<div class="col-sm-4">		
					<div class="form-group field-berita-status required">
						<label class="control-label">Status</label>
						<input type="hidden" name="status" value="">
						
						<div id="berita-status">
							<label class="radio-inline"><input type="radio" name="status" value="1" <?php if($menu=="create" || $row['status']==1) echo 'checked'; ?>> Active</label>
							<label class="radio-inline"><input type="radio" name="status" value="0" <?php if($menu=="edit" && $row['status']==0) echo 'checked'; ?>> Inactive</label>
						</div>

						<div class="help-block"></div>
					</div>
				</div>
				
			</div>
		</fieldset>
		
		<fieldset id="w3">
			<div class="row">
				
				

			</div>

		</fieldset>
		
		<fieldset id="w4">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group field-berita-isi_berita required">
						<label class="control-label" for="berita-isi_berita">Isi Berita</label>
						<textarea rows="10" id="isi_berita" class="form-control" name="isi_berita" placeholder="Isi Berita..."><?php echo $row['isi_berita'] ?></textarea>

						<div class="help-block"></div>
					</div>

				</div>				
				
			</div>
		</fieldset>
		
		<fieldset id="w5">
			<div class="row">
				<div class="col-sm-9">
					<div class="form-group field-berita-imageberita required">
						<label class="control-label" for="berita-imageberita">Image</label>
						
						<input type="file" name="imageberita" id="imageberita" />
						<?php 
							if($row['imagelink'] != NULL && $row['imagelink'] != ""){
								echo '<br/>';
								echo '<image src="'.$row['imagelink'].'" style="max-width: 100%; max-height: 100%;" />';
							}
						?>

						<div class="help-block"></div>
					</div>

				</div>
				
				<div class="col-sm-3">
					<div style="text-align: right; margin-top: 20px">
						<div id="feedback_edit"></div>
						<img class="thisLoadingGif" src="img/loading.gif" />
						<button type="reset" class="btn btn-default">Reset</button> 
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>			
		</fieldset>
		
	</form>

</div>

<?php 
	$formName = '"#form_berita"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
