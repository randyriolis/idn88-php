<?php
	include_once "inc_login.php";
	include_once "config.php";
	
	$page = $_REQUEST['page'];
	if(empty($page)) $page = 1;
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['kdusercat'];
	
	// QUERY
	$sqlgame = "SELECT gm.* FROM products gm WHERE gm.isactive = 1 $tm ORDER BY kdproduct ASC  ";		
	$result = mysqli_query($conn, $sqlgame);
		
	$arrgame = array();
	while($row = mysqli_fetch_assoc($result)) {
		array_push($arrgame, $row);
	}
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Withdraws</title>    	
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "withdraw";
		include_once "inc_header.php";
	?>
	
	<div class="alert alert-warning" id="copy_clipboard" style="position: fixed; left: 50%;  transform: translate(-50%, -50%); top: 70px; display:none; z-index:100000 ">
		abc
	</div>

	<div class="content-wrapper">
		<section class="content-header">
			<h1></h1>			
			<ul class="breadcrumb">
			<li><a href="site.php">Home</a></li>
			<li class="active">Withdraw</li>
			</ul>    
		</section>
		
		<section class="content">
			<div class="withdraw-index">							
				<div class="account-form">
					<p>
					<form id="form_newwithdraw" class="form-inline" action="docreate_withdraw.php" method="post">
						<div class="form-group field-withdraw-kdmember required">
							<label class="control-label sr-only" for="withdraw-kdmember">Idmember</label>
							
							<select id="withdraw-kdmember" class="form-control js-example-basic-single" name="kdmember">
								<option value="">-- Search Member  Nama / Nomer Rekening / Username --</option>
								<?php 
									$tempquery = "SELECT mem.nama, mem.kdmember, acc.username, mem.norek FROM ".
												   "members mem ".
												   "LEFT JOIN akuns acc ON acc.kdmember = mem.kdmember ".
												   "LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ".												   
												   "WHERE acc.isassigned = 1 AND mem.isactive = 1 ";
									$tempresult = mysqli_query($conn, $tempquery);
									while($row = mysqli_fetch_array($tempresult)){
										echo '<option value="'.$row['kdmember'].'">'.htmlentities($row['nama']).' | '.htmlentities($row['norek']).' | '.$row['username'].'</option>
										';
									}
								?>
								
							</select>
						</div>
							
						<div class="form-group field-withdraw-jumlah required">
							<label class="control-label sr-only" for="withdraw-jumlah">Jumlah</label>
							<input type="text" id="withdraw-jumlah" class="form-control" name="jumlah" maxlength="50" placeholder="Jumlah" />
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-success">Create Withdraw</button>    
						</div>
						<div id="loadingGif_withdraw" class="btn-group" style="margin-right:20px">
							<img src="img/loading.gif" />
						</div>

					</form>    
					</p>
				</div>
				
				<div id="w0" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">    
				<div id="withdraw-id-pjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
					<div id="withdraw-id" class="grid-view hide-resize" data-krajee-grid="kvGridInit_7e6ea3f8">
						<div class="panel panel-primary">
							<div class="panel-heading">    
								<div class="pull-right">
									<div id="page_number" class="summary">
									</div>
								</div>
								<h3 class="panel-title">
									Withdraw
								</h3>
								<div class="clearfix"></div>
							</div>
							
							<div class="kv-panel-before">    
								
								<div class="pull-left">
									<button type="button" class="btn btn-info" href="dowithdraw_create.php?menu=withdraw" data-toggle="modal" data-target="#myModalCreate" data-title="Detail Data">
										WITHDRAW MANUAL
									</button>	
									<button type="button" href="withdraw-import.php" target="_blank" class="btn btn-danger">IMPORT</button>							
								</div>
								
								<div class="pull-right">
									<div class="btn-toolbar kv-grid-toolbar" role="toolbar">
										<div class="btn-group">
											<a class="btn btn-default" href="member.php" title="Reset Grid" data-pjax="0" onClick="refreshContent(); return false;">
												<i class="glyphicon glyphicon-repeat"></i>
											</a>
										</div>

										<div class="btn-group">
											<a id="showall" class="btn btn-default" href="#" title="Show all data" onClick="tryShowAll(); return false;">
												<i class='glyphicon glyphicon-resize-full'></i> All								
											</a>
										</div>
									</div>    
								</div>
						
								<div class="clearfix"></div>
							</div>
							
							<div id="withdraw-id-container" class="table-responsive kv-grid-container">
							<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
								<colgroup>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
								</colgroup>
								<thead>

									<tr>
										<th class="kartik-sheet-style kv-align-center kv-align-middle kv-merged-header" style="width:36px;" rowspan="2" data-col-seq="0">&nbsp;</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:140px;" data-col-seq="1">											
											<a href="#" onClick="return trysort('account');" id="account" class="sorter">Account</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="2">
											<a href="#" onClick="return trysort('namalink');" id="namalink" class="sorter">Website</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:100px;" data-col-seq="2">
											<a href="#" onClick="return trysort('game');" id="game" class="sorter">Game</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:220px;" data-col-seq="3">
											<a href="#" onClick="return trysort('no_rekening');" id="no_rekening" class="sorter">No Rekening</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:180px;" data-col-seq="4">
											<a href="#" onClick="return trysort('dateassign');" id="dateassign" class="sorter">Date Assign</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:150px;" data-col-seq="5">
											<a href="#" onClick="return trysort('ip');" id="ip" class="sorter">IP</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:200px;" data-col-seq="6">											
											<a href="#" onClick="return trysort('email');" id="email" class="sorter">Member Info</a>
										</th>
										<th class="kartik-sheet-style kv-align-right kv-align-middle" style="width:220px;" data-col-seq="7">
											<a href="#" onClick="return trysort('jumlah');" id="jumlah" class="sorter">Jumlah</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:150px;" data-col-seq="8">
											<a href="#" onClick="return trysort('status');" id="status" class="sorter">Status</a>
										</th>										
										<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="13">Copy</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="14">Delete</th>
									</tr>
									
									<tr id="withdraw-id-filters" class="filters skip-export">
										<form id="form_search" class="form-inline" action="doprint_withdraw.php" method="post">
										
										<input type="hidden" id="page" name="page" value="<?php echo $page ?>" />
										<input type="hidden" id="delete" name="delete" value="" />	
										<input type="hidden" id="sort" name="sort" value="" />
										<input type="hidden" id="totalrow" name="totalrow" value="" />
										<input type="hidden" id="maxrow" name="maxrow" value="20" />
										<input type="hidden" id="temp_id" name="temp_id" value="" />
										<input type="hidden" id="temp_function" name="temp_function" value="" />
																				
										<td><input type="text" class="form-control" name="account" value="" /></td>
										<td style="min-width: 110px">
											<select id="accountsearch-namalink" class="form-control" name="namalink" onchange="refreshContent();">
											<option value=""></option>
											<?php
												$rs = mysqli_query($conn, "SELECT * FROM link WHERE status=1 ");
												
												while($rw=mysqli_fetch_array($rs)) {
													echo '<option value="'.$rw['namalink'].'" >'.$rw['namalink'].'</option>';
												}
												
												for($i=0; $i<count($arrgame); $i++) {
														
													}
											?>
											</select>																			
										</td>
										<td>
											<select id="accountsearch-idgame" class="form-control js-example-basic-single" name="idgame" onchange="refreshContent();" style="width:110px;">
												<option value="">Any Game</option>
												<?php 
													for($i=0; $i<count($arrgame); $i++) {
														echo '<option value="'.$arrgame[$i]['kdproduct'].'" data-nama="'.$arrgame[$i]['kodeagen'].'" data-pass="'.$arrgame[$i]['defpass'].'" >
															'.$arrgame[$i]['nama'].'
														</option>';
													}
												?>						
											</select>											
										</td>
										<td><input type="text" class="form-control" name="no_rekening"></td>
										<td><input type="text" class="form-control" name="dateassign"></td>
										<td><input type="text" class="form-control" name="ip"></td>
										<td><input type="text" class="form-control" name="email"></td>
										<td><input type="text" class="form-control" name="jumlah"></td>
										<td>
											<select id="" class="form-control" name="status" onchange="refreshContent();" style="width:110px;">
												<option value="" > </option>
												<option value="0" > PENDING </option>
												<option value="1" > APPROVED </option>
												<option value="2" > REJECTED </option>
											</select>
											
										<input type="submit" style="display:none" /></td>
										
										</form>
									</tr>

								</thead>
								
								<tbody id="tbody_content">
									
								</tbody>
							</table>
						</div>
						
						</div>
					</div>
				</div>
				</div>
			</div>

			<div id="myModallog" class="fade modal" role="dialog" tabindex="-1">
			<div class="modal-dialog ">
			<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>Log Withdraw</h4>
			</div>
			<div class="modal-body">

			</div>

			</div>
			</div>
			</div>
			
			
			<div id="myModaledit" class="fade modal" role="dialog" tabindex="-1">
			<div class="modal-dialog ">
			<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>Edit Date Assign Deposit</h4>
			</div>
			<div class="modal-body">

			</div>

			</div>
			</div>
			</div>
			
			<div id="myModalCreate" class="fade modal" role="dialog" tabindex="-1">
			<div class="modal-dialog modal-lg">
			<div class="modal-content">
			<div class="modal-body">

			</div>

			</div>
			</div>
			</div>



			</div>
		</section>
		
	</div>

<?php 
	include_once "inc_script.php";
?>
<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		refreshContent();
		
		$(".js-example-basic-single").select2();
		$("#loadingGif_withdraw").css("display","none");
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});	
	
	
	// Attach a submit handler to the form
	$("#form_newwithdraw").submit(function( event ) {
		event.preventDefault();
		
		var cont = true;
		
		if($("#withdraw-kdmember").val() == 0){
			$("#withdraw-kdmember").css("border", "1px solid red");
			cont = false;
		}	
		else $("#withdraw-kdmember").css("border", "1px solid lightgray");
		
		if($("#withdraw-jumlah").val() == ""){
			$("#withdraw-jumlah").css("border", "1px solid red");
			cont = false;
		}	
		else $("#withdraw-jumlah").css("border", "1px solid lightgray");		
		
		if(cont == true){			
			// Get some values from elements on the page:
			var $form = $("#form_newwithdraw");
			var sent = $($form).serialize();
			var url = $form.attr("action");
			
			// Send the data using post
			var posting = $.post( url, sent );
			
			$("#loadingGif_withdraw").css("display","");
			
			// Put the results in a div
			posting.done(function( data ) {				
				$("#loadingGif_withdraw").css("display","none");				
				console.log(data);
				if(data == "sukses"){
					refreshContent();
					
					// reset delete variables
					// $("#withdraw-kdmember").val("");
					$("#withdraw-jumlah").val("");
					
					$("#withdraw-kdmember").css("border", "1px solid lightgray");
					$("#withdraw-jumlah").css("border", "1px solid lightgray");
				}
				else{
					$("#withdraw-kdmember").css("border", "1px solid red");
					$("#withdraw-jumlah").css("border", "1px solid red");
				}
			});
			
			refreshContent();
		}
	});
	
	
	jQuery('#myModallog').modal({"show":false});
	jQuery('#myModalCreate').modal({"show":false});
	jQuery('#myModaledit').modal({"show":false});

    $('#myModallog').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title') 
        var href = button.attr('href') 
        modal.find('.modal-title').html(title)
        modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data)
            });
        })
	
	
	$('#myModalCreate').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title') 
        var href = button.attr('href') 
        modal.find('.modal-title').html(title)
        modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data);
				$('#accountsearch-username').select2({
					dropdownParent: $('#myModalCreate')
				});
            });
        })
	
	$('#myModaledit').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title') 
        var href = button.attr('href') 
        modal.find('.modal-title').html(title)
        modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data);				
            });
        })
		
	var prevvalue = "";
	$("#withdraw-jumlah").keyup(function(event)
	{		
			// skip for arrow keys
		  if(event.which >= 37 && event.which <= 40){
			  event.preventDefault();
		  }
		  var tmp = $(this);
		  var val = tmp.val();
		  console.log(val);
		  if(val == prevvalue)	return;
		  
		  val = val.replace(/\D/g,'');
		  prevvalue = val;
		  var num = val.replace(/,/gi, "").split("").reverse().join("");

		  var num2 = RemoveRougeChar(num.replace(/(.{3})/g,"$1,").split("").reverse().join(""));

		  // the following line has been simplified. Revision history contains original.
		  tmp.val(num2);
	  });
	  function RemoveRougeChar(convertString){
		if(convertString.substring(0,1) == ","){

			return convertString.substring(1, convertString.length)            

		}
		return convertString;	
	}
	
</script>
<?php 
	// close
	mysqli_close($conn);
?>

</body>

</html>