<?php 
	include_once "inc_login.php";
	include "config.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM info WHERE idinfo = $delete ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$namainfo = $_REQUEST['namainfo'];
	$contentinfo = $_REQUEST['contentinfo'];
	$keterangan = $_REQUEST['keterangan'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = "  ";
	
	if($namainfo != "")	$wherequery .= " AND namainfo LIKE '%$namainfo%' ";
	if($contentinfo != "")	$wherequery .= " AND contentinfo LIKE '%$contentinfo%' ";
	if($keterangan != "")	$wherequery .= " AND keterangan like '%$keterangan%' ";
	
	
	$orderquery = "ORDER BY idinfo DESC ";
	if($sort != "")	
	{
		if($sort == "namainfo_asc")		$orderquery = "ORDER BY namainfo ASC ";
		else if($sort == "namainfo_desc") $orderquery = "ORDER BY namainfo DESC ";
		
		else if($sort == "contentinfo_asc")		$orderquery = "ORDER BY contentinfo ASC ";
		else if($sort == "contentinfo_desc") $orderquery = "ORDER BY contentinfo DESC ";
		
		else if($sort == "keterangan_asc")		$orderquery = "ORDER BY keterangan ASC ";
		else if($sort == "keterangan_desc") $orderquery = "ORDER BY keterangan DESC ";			
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT * FROM info WHERE 1=1  ".$wherequery.$orderquery.$limitquery;
	echo $query;
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	

?>
	<tr data-key="<?php echo $row['idinfo'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $row['idinfo'] ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1"><b><?php echo $row['namainfo'] ?></b></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="2"><?php echo $row['contentinfo'] ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="3"><?php echo $row['keterangan'] ?></td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="doinfo_menu.php?menu=edit&id=<?php echo $row['idinfo'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="8">
			<a class="btn btn-danger btn-xs" href="#" onClick="deleteInfo(<?php echo $row['idinfo'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(idinfo) as num_rows FROM info WHERE idinfo > 0  ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deleteInfo(idaccount)
	{
		var str = "Apakah Kamu yakin delete Info ini ?? Tanya dlu loh programmernya kalo mao apus Info..";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>