<?php
//require __DIR__ . '/vendor/autoload.php'; // If you're using Composer (recommended)
// Comment out the above line if not using Composer
// require("<PATH TO>/sendgrid-php.php");
// If not using Composer, uncomment the above line and
// download sendgrid-php.zip from the latest release here,
// replacing <PATH TO> with the path to the sendgrid-php.php file,
// which is included in the download:
// https://github.com/sendgrid/sendgrid-php/releases

$email = new \SendGrid\Mail\Mail(); 
$email->setFrom("bandarsport.corporation@gmail.com", "Bandarsport Corp");
$email->setSubject($titleEmail);
$email->addTo($toAddressEmail, $toEmailName);
$email->addContent("text/plain", $contentEmail);
$sendgrid = new \SendGrid(SENDGRID_API_KEY);
try {
    $response = $sendgrid->send($email);
    //print $response->statusCode() . "\n";
    //print_r($response->headers());
    //print $response->body() . "\n";
    echo '<span style="color:green">Success.. email sent</span>';
} catch (Exception $e) {
    echo 'Caught exception: '. $e->getMessage() ."\n";
}
?>