<?php 
	include_once "inc_login.php";
	
	$id = $_REQUEST['id'];	
	include "config.php";
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['kdusercat'];
	$namaUser = $arrUser["username"];
	
	$query = "SELECT mem.*, acc.username, acc.pass, ln.account_assigned  ".
				   "FROM members mem ".
				   "LEFT JOIN akuns acc ON acc.kdmember = mem.kdmember ".
				   "INNER JOIN link ln ON ln.idlink = mem.idlink ".
				   "WHERE mem.kdmember = $id ";
	$result = mysqli_query($conn, $query);	
	$row = mysqli_fetch_assoc($result);
	
	$menu = $_REQUEST['menu'];
	$kdproduct = $row['kdproduct'];	
	
	$wkt = time();
			
	$p= $_REQUEST['p'];
	if($p == 'post')
	{
		// process to send sms
		$kdakun = $_REQUEST["kdakun"];
		$alasan = $_REQUEST["alasan"];
		$iduser = $_SESSION['alpha_iduser'];
				
		$qa = mysqli_fetch_array(mysqli_query($conn, "SELECT username FROM akuns WHERE kdakun=$kdakun") );
		$username = $qa["username"];
		
		if($kdakun == ""){
			echo '<span style="color:red"> Error<br/>Pilih salah satu account</span><br/>';
		}
		else {
			/*
			$oldkdakun = $row['kdakun'];
			if($oldkdakun != NULL){
				// $queryold = "UPDATE akuns SET kdmember = 0, isassigned = 0, modtime='$wkt', modby='$namaUser'  WHERE kdakun = $oldkdakun; ";
				// mysqli_query($conn, $queryold);
			}
			*/
			
			// set null previous account
			mysqli_query($conn, "UPDATE akuns SET kdmember = 0, isassigned = 0, modtime='$wkt', modby='$namaUser' WHERE kdmember = $id; ");
			
			$query = "UPDATE akuns SET kdmember = $id, isassigned = 1, isactive = 1, modtime='$wkt', modby='$namaUser' WHERE kdakun = $kdakun; ";
			$query2 = "UPDATE members SET username = '$username', isactive = 1 WHERE kdmember = $id; ";
			
			// log
			if(empty($alasan)){
				$alasan = "Melakukan Assign Account kepada Member";
			}
			else {
				$alasan = "Melakukan Re-Assign Account kepada Member dengan alasan : ".$alasan;
			}
			
			mysqli_query($conn, "INSERT INTO zlog_member (kdmember, kduser, keterangan, datecreate) VALUES ($id, $iduser, '$alasan', $current_timestamp)");
			
			if(mysqli_query($conn, $query) && mysqli_query($conn, $query2)) echo '<span style="color:green"> Sukses.. <br/>Account terhubung dengan member.</span><br/>';
			else echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
			
		}
		
		
		exit();
	}
?>

<div class="member-assign">

<div class="member-assign">

	<form id="form_assign" class="form-vertical" action="domember_assign.php?id=<?php echo $id ?>&p=post" method="post">
	
	<div class="form-group field-member-kdakun required">	
		<label class="control-label" for="member-kdakun">Account</label>
		<div class="kv-plugin-loading loading-member-kdakun">&nbsp;</div>
		
		<?php 
			$addon = "";
				
			$exception = array();

			// SBOBET ONLY
			if($kdproduct == 1) {
									
				if(empty($row["account_assigned"]) == false) {
					$explode_account = explode(",", $row["account_assigned"]);
					
					$addon = " AND (";
					$cnt = 0;
					foreach($explode_account as $tmp) {
						if($cnt > 0)	$addon .= " OR ";
						$cnt ++;
						
						$addon .= "acc.username LIKE '".$tmp."%'";
						
						array_push($exception, $tmp);
					}
					$addon .= ") ";						
				}					
				
				
				// Cannot Assign to account spesific link
				$query_link = mysqli_query($conn, "SELECT idlink, account_assigned FROM link WHERE account_assigned IS NOT NULL ");
				while($res_link = mysqli_fetch_array($query_link)) {
					
					// Different Web
					if($res_link["idlink"] != $row["idlink"] )
					{
						$explode_account = explode(",", $res_link["account_assigned"]);						
						foreach($explode_account as $tmp) {
							if (in_array($tmp, $exception) == false) {
								$addon .= " AND acc.username NOT LIKE '".$tmp."%'";								
							}
						}
					}
				}
			}
			
			$sql_acc = "SELECT * FROM akuns acc 
						WHERE (kdmember IS NULL OR kdmember = 0) 
						AND kdproduct = '$kdproduct' 
						$addon 
						ORDER BY kdakun ASC";
			
			/*
			$sql_acc = "SELECT * FROM account acc WHERE (idmember IS NULL OR idmember = 0) 
							AND acc.assign = 0 AND acc.status = 1 
							AND idgame = '$idgame' $addon 
							ORDER BY idaccount ASC";
			*/
			
		?>
		<select id="member-kdakun" class="form-control" name="kdakun">
			<option value="">-- Pilih Account --</option>
			<?php
				$query2 = mysqli_query($conn, $sql_acc);
				while($temp=mysqli_fetch_array($query2)) {
					echo '<option value="'.$temp['kdakun'].'">'.$temp['username'].'</option>';
				}
			?>			
		</select>
		
		<div class="help-block"></div>
	</div>
	
	<?php 
		if($menu == "reassign") {
	?>
	<div class="form-group field-member-alasan required">
		<label class="control-label" for="member-alasan">Alasan</label>
		<textarea id="member-alasan" class="form-control" name="alasan"></textarea>
		<div class="help-block"></div>
	</div>
	<?php } ?>
	
	<div class="form-group">
		<div id="feedback_2"></div>
		<button type="submit" class="btn btn-primary">Assign Member</button>
		<img class="thisLoadingGif" src="img/loading.gif" />
	</div>



	</form>


</div>


</div>


<?php 
	$formName = '"#form_assign"';
	$feedback = '"#feedback_2"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
