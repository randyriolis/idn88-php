<?php
	include_once 'PHPExcel/Classes/PHPExcel.php';
	include_once "inc_login.php";
	include_once "config.php";
	
	$objPHPExcel = new PHPExcel();
	PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
	
	$tgl = $_REQUEST['tgl'];
	$tanggal_nama = date("d_M_Y", strtotime($tgl));
	$tanggal = date("Y-m-d", strtotime($tgl));
		
	$query_select = "SELECT dp.jumlah, dp.tanggal, dp.kdmember, dp.cleartime AS dateassign, 'Deposit' AS nama_transaksi, dp.isactive, dp.clearby ";
	
	$queryDeposit = $query_select.
				   "FROM deposits dp ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
				   "WHERE ln.status =1 ".
				   " AND from_unixtime(dp.tanggal,'%Y-%m-%d') = date('$tanggal') AND dp.isactive = 1 AND dp.isclear = 1 ";
				   
	$query_select = str_replace("'Deposit'", "'Withdraw'", $query_select);
	$queryWithdraw = $query_select.
				   "FROM withdraws dp ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
				   "WHERE ln.status =1 ".
				   " AND from_unixtime(dp.tanggal,'%Y-%m-%d') = date('$tanggal') AND dp.isactive = 1 AND dp.isclear = 1 ";
		  
	$sql = "SELECT tr.*, mem.username, mem.norek, mem.nama, bk.inisialbank AS bank, gm.nama AS nama_game, clearby AS nama_admin    
					FROM (
						$queryDeposit 
						UNION ALL 
						$queryWithdraw 
					) AS tr 
				LEFT JOIN members mem ON tr.kdmember = mem.kdmember 
				LEFT JOIN banks bk ON mem.kdbank = bk.kdbank  				
				LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ";
			
	$exec1 = mysqli_query($conn, $sql) or die ("Error in Query1".mysql_error());
	$serialnumber=0;
	
	$sheet = array();
	
	// Title
	$tmparray = array("Laporan Harian", "", $tanggal_nama, "", "", "");
	array_push($sheet,$tmparray);
	
	//Set header with temp array
	$tmparray = array("#", "USERNAME","AKUN","NO.REKENING", "BANK", "GAME", "DP/WD", "NOMINAL", "TANGGAL DAN WAKTU", "STATUS | DIAPPROVE OLEH | TIPE" );
	//take new main array and set header array in it.
	array_push($sheet,$tmparray);
	
	
	$jumlahBaris = 1;
	
	while ($res = mysqli_fetch_array($exec1))
	{
		$tmparray = array();
		
		array_push($tmparray, $jumlahBaris);
		array_push($tmparray, $res["username"]);
		array_push($tmparray, $res["nama"]);
		array_push($tmparray, $res["norek"]);
		array_push($tmparray, $res["bank"]);
		array_push($tmparray, $res["nama_game"]);
		
		array_push($tmparray, $res["nama_transaksi"]);
				
		array_push($tmparray, $res["jumlah"]);
		
		$tgl = date("d-m-Y h:i:s", $res["tanggal"]);
		array_push($tmparray, $tgl);
		array_push($tmparray, "Diapprove | ".$res["nama_admin"]);
		
		array_push($sheet, $tmparray);
		$jumlahBaris ++;
	}
	
	
	$worksheet = $objPHPExcel->getActiveSheet();
	foreach($sheet as $row => $columns) {
		foreach($columns as $column => $data) {
			// echo $column. ' '.$row.' = ' . $data.'<br/>';			
			$worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
			
		}
	}
	
	$namafile = "laporan_harian_".$tanggal_nama.".xlsx";
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="'.$namafile.'"');
	
	//make first & second row bold
	$objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0);
	
	$objPHPExcel->getActiveSheet()->getStyle("A2:M2")->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0);
	
	$jumlahBaris += 5;
	
	// Number Formatting
	// $objPHPExcel->getActiveSheet()->getStyle('B3:B'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	// $objPHPExcel->getActiveSheet()->getStyle('D3:D'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	$objPHPExcel->getActiveSheet()->getStyle('D3:D'.$jumlahBaris)->getNumberFormat()->setFormatCode('@');
	$objPHPExcel->getActiveSheet()->getStyle('H3:H'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	$objPHPExcel->getActiveSheet()->getStyle('I3:I'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	$objPHPExcel->getActiveSheet()->getStyle('J3:J'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	$objPHPExcel->getActiveSheet()->getStyle('K3:K'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	
	// Merge Title
	$objPHPExcel->getActiveSheet()->mergeCells('A1:B1');
	
	// auto size
	foreach(range('A','J') as $columnID) {
		$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);			
	}
	
	/*
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
	*/
	
	 // Save Excel file
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	
  
	mysqli_close($conn);
?>