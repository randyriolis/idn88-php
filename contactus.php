<?php
	include_once "inc_login.php";
	include_once "config.php";
	
	$page = $_REQUEST['page'];
	if(empty($page)) $page = 1;
	
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Contacts</title>    	
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "contactus";
		include_once "inc_header.php";
	?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1></h1>			
			<ul class="breadcrumb">
			<li><a href="site.php">Home</a></li>
			<li class="active">contactus</li>
			</ul>    
		</section>
		
		<section class="content">
			<div class="contactus-index">							
				<div class="account-form">
					<p>
					<form id="form_newcontactus" class="form-inline" action="docreate_contactus.php" method="post">
						<div class="form-group">
							<a class="btn btn-success modalButton" type="button" id="modalButton" value="docontactus_menu.php?menu=create" href="#" onClick="return false;">
								Create Contact
							</a>
						</div>

					</form>    
					</p>
				</div>
				
				<div id="w0" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">    
				<div id="contactus-id-pjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
					<div id="contactus-id" class="grid-view hide-resize" data-krajee-grid="kvGridInit_7e6ea3f8">
						<div class="panel panel-primary">
							<div class="panel-heading">    
								<div class="pull-right">
									<div id="page_number" class="summary">
									</div>
								</div>
								<h3 class="panel-title">
									Contact on <i style="padding-left:10px" class="fa fa-map-o fa-lg"></i> <?php echo $bla['namalink']; ?>
								</h3>
								<div class="clearfix"></div>
							</div>
							
							<div class="kv-panel-before">    
								<div class="pull-right">
									<div class="btn-toolbar kv-grid-toolbar" role="toolbar">
										<div class="btn-group">
											<a class="btn btn-default" href="member.php" title="Reset Grid" data-pjax="0" onClick="refreshContent(); return false;">
												<i class="glyphicon glyphicon-repeat"></i>
											</a>
										</div>

										<div class="btn-group">
											<a id="showall" class="btn btn-default" href="#" title="Show all data" onClick="tryShowAll(); return false;">
												<i class='glyphicon glyphicon-resize-full'></i> All								
											</a>
										</div>
									</div>    
								</div>
						
								<div class="clearfix"></div>
							</div>
							
							<div id="contactus-id-container" class="table-responsive kv-grid-container">
							<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
								<colgroup>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
								</colgroup>
								<thead>

									<tr>
										<th class="kartik-sheet-style kv-align-center kv-align-middle kv-merged-header" style="width:36px;" rowspan="2" data-col-seq="0">&nbsp;</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="1">											
											<a href="#" onClick="return trysort('nama_contact_us');" id="nama_contact_us" class="sorter">Nama Contact</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:100px;" data-col-seq="3">
											<a href="#" onClick="return trysort('kode_contactus');" id="kode_contactus" class="sorter">Nomor / ID</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:100px;"  data-col-seq="4">
											<a href="#" onClick="return trysort('namalink');" id="namalink" class="sorter">Website</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="7">Edit</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle skip-export kv-merged-header" style="width:80px;" rowspan="2" data-col-seq="8">Delete</th>
									</tr>
									
									<tr id="contactus-id-filters" class="filters skip-export">
										<form id="form_search" class="form-inline" action="doprint_contactus.php" method="post">
										
										<input type="hidden" id="page" name="page" value="<?php echo $page ?>" />
										<input type="hidden" id="sort" name="sort" value="" />
										<input type="hidden" id="totalrow" name="totalrow" value="" />
										<input type="hidden" id="maxrow" name="maxrow" value="20" />
										<input type="hidden" id="delete" name="delete" value="" />
																				
										<td><input type="text" class="form-control" name="nama_contact_us"></td>
										<td>
											<input type="text" class="form-control" name="nomor_contact_us">
											<input type="submit" style="display:none" />
										</td>										
										<td style="width:100px;"  >
											<select id="accountsearch-namalink" class="form-control" name="namalink" onchange="refreshContent();">
											<option value=""></option>
											<?php
												$rs = mysqli_query($conn, "SELECT * FROM link WHERE status=1 ");
												
												while($rw=mysqli_fetch_array($rs)) {
													echo '<option value="'.$rw['namalink'].'" >'.$rw['namalink'].'</option>';
												}
												
												for($i=0; $i<count($arrgame); $i++) {
														
													}
											?>
											</select>
										</td>										
										
										</form>
									</tr>

								</thead>
								
								<tbody id="tbody_content">
									
								</tbody>
							</table>
						</div>
						
						</div>
					</div>
				</div>
				</div>
			</div>

			<div id="modalcreate" class="fade modal" role="dialog" tabindex="-1" data-ajax="false">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4>Menu Contact</h4>
						</div>
						<div class="modal-body">
							<div id="modalContent"></div>
						</div>
					</div>
				</div>
			</div>


			</div>
		</section>
		
	</div>

<?php 
	include_once "inc_script.php";
?>
<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		refreshContent();
		
		jQuery('#modalcreate').modal({"show":false});
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});	
	
	
</script>
<?php 
	// close
	mysqli_close($conn);
?>

</body>

</html>