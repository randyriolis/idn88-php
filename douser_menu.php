<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$id = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post"){
		
		$username = $_REQUEST['username'];
		$pass = empty($_REQUEST['pass'])? "" : md5($_REQUEST['pass']);
		$kdusercat = $_REQUEST['kdusercat'];
		$isactive = $_REQUEST['isactive'];
		$ignore_dpwd = $_REQUEST['ignore_dpwd'];
		$post_link_assigned = $_REQUEST['link_assigned'];
		
		$link_assigned = "";
		if(empty($post_link_assigned) == false) 
		{
			foreach ($post_link_assigned as $key=>$value) {
				if($value == 1)
					$link_assigned .= $key.",";
			}
		}
		
		$err = "";
		$textSuccess = "";
		
		if($username == "")						$err .= "<br/>Nama Admin harus diisi";
		if($pass == "" && $menu == "create")	$err .= "<br/>Password harus diisi";
		
		
		if($err=="" && $menu == "edit")
		{
			if(empty($pass) == false && $pass != "")		$queryPass = ", pass='$pass'";
			else 							$queryPass = "";		
		
			$query = "UPDATE _users SET username='$username' $queryPass, kdusercat=1, isactive=$isactive, link_assigned='$link_assigned', ignore_dpwd=$ignore_dpwd WHERE kduser=$id ";
			$textSuccess = "Admin berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";						
		}
		else if($err=="" && $menu == "create")
		{
			$query_id = mysqli_fetch_array(mysqli_query($conn, "SELECT kduser FROM _users ORDER BY kduser DESC limit 0, 1 "));
			$tempKdUser = $query_id["kduser"] + 1;			
			
			$query = "INSERT INTO _users (kduser, username, pass, isactive, kdusercat, link_assigned, ignore_dpwd) VALUES ('$tempKdUser', '$username', '$pass', $isactive, 1, '$link_assigned', '$ignore_dpwd') ";
			$textSuccess = "Admin berhasil di-create";			
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';		
		
		mysqli_close($conn);
		exit();
	}
	
	$isself = false;
	$kdusercat = 0;
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM _users WHERE kduser = $id ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Admin: ".$row['username'];
		
		if($row['kduser'] == $iduser) $isself = true;		
		$kdusercat = $row['kdusercat'];
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Admin";
	}
?>

<div class="user-update">
	<h1><?php echo $title ?></h1>
	<form id="form_user" class="form-vertical" action="douser_menu.php" method="post">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $id ?>" >
		<input type="hidden" name="post" value="post" >
		<div class="row">
			
			<div class="col-sm-4">
				<div class="form-group field-user-username required">
					<label class="control-label" for="user-username">Nama Admin</label>
					<input <?php if($isself) echo 'readonly="readonly"'; ?> type="text" id="user-username" class="form-control" name="username" placeholder="Nama Admin..." value="<?php echo $row['username'] ?>">

					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group field-user-pass required">
					<label class="control-label" for="user-pass">Enter New Password</label>
					<input type="text" id="user-pass" class="form-control" name="pass" placeholder="Password..." value="">
					
					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group field-user-isactive required">
					<label class="control-label">isactive</label>
					<?php if($isself) echo '<input type="hidden" name="isactive" value="'.$row['isactive'].'">'; ?>
					
					<div id="user-isactive">
						<label class="radio-inline"><input <?php if($isself) echo 'disabled'; ?> type="radio" name="isactive" value="1" <?php if($menu=="create" || $row['isactive']==1) echo 'checked'; ?>> Active</label>
						<label class="radio-inline"><input <?php if($isself) echo 'disabled'; ?> type="radio" name="isactive" value="0" <?php if($menu=="edit" && $row['isactive']==0) echo 'checked'; ?>> Inactive</label>
					</div>
					
					<?php echo '<input type="hidden" name="kdusercat" value="'.$kdusercat.'">'; ?>

					<div class="help-block"></div>
				</div>
			</div>

		</div>

			
		<?php if( empty($id) == false) { ?>
		<div class="col-sm-12">
			<!--<i>Notes: password admin di Bandarsport ini encrypted, jadi pass 'f6fdffe48c908deb0f4c3bd36c032e72' mungkin artinya adalah 'abc123'</i>-->				<i>Notes: Password cuma bisa diganti yang baru, password lama tidak bisa dilihat</i>
		</div>
		<?php } ?>
			
		<div class="clearfix"></div>
		<br/>
		
		<div class="row" style="margin-top: 10px;">
			<div class="col-sm-4">
				<label class="control-label">Ignore DP/WD</label>
					
				<div id="user-status">
					<label class="radio-inline"><input type="radio" name="ignore_dpwd" value="0" <?php if($row['ignore_dpwd'] == 0) echo 'checked'; ?>> Tidak</label>
					<label class="radio-inline"><input type="radio" name="ignore_dpwd" value="1" <?php if($row['ignore_dpwd'] == 1) echo 'checked'; ?>> Iya</label>
				</div>
				<div class="help-block"></div>
			</div>
		</div>
		
		<div class="row">
			
			
			<div class="col-sm-12" >
				<div class="form-group field-user-status required">
					<label class="control-label">Link</label>
					
					<div id="user-status">
						<?php 
							$qu = mysqli_query($conn, "SELECT * FROM link WHERE status = 1");
							$cnt = 0;
							
							$access = $row['link_assigned'];
							$access = explode(",",$access);
							
							while($dt=mysqli_fetch_array($qu)) {								
								$namaRadio = "link_assigned[".$dt['idlink']."]";																
								
								// Kepalacs
								if($isself || $row["superadmin"] == 1) {
									$dis = "disabled";
									$checkedRadio = "checked";
									$notCheckedRadio = "";
								}
								else {
									$dis = "";
									$tempFound = false;
									foreach ($access as $value) {									
										if($value == $dt['idlink']){
											$tempFound = true;										
										}
									}
									
									if($tempFound == true){
										$checkedRadio = "checked";
										$notCheckedRadio = "";								
									}							
									else {
										$checkedRadio = "";
										$notCheckedRadio = "checked";
									}
								}
								
								echo '<div class="col-sm-4" style="padding-bottom: 10px; color:black;" >';
									echo '<label class="control-label">'.$dt['namalink'].'</label>';
									
									echo '<div id="">';									
									echo '<label class="radio-inline"><input type="radio" name="'.$namaRadio.'" value="1" '.$checkedRadio.' '.$dis.'>Enable</label> ';
									
									echo '<label class="radio-inline"><input type="radio" name="'.$namaRadio.'" value="0" '.$notCheckedRadio.' '.$dis.'>Disable</label>';
									
									echo '</div>';
								echo '</div>';
								$cnt++;
							}
						?>																
					</div>
					

					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-12">
				<div style="text-align: right; margin-top: 20px">
					<div id="feedback_edit"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
			
		</div>
		
		<?php if($isself) { ?>
		<div class="row">
			<div class="col-sm-8">		
				<i style="color:gray">Note: Ini ID kamu sendiri, jadi banyak yang tidak bisa di-edit.</i>
			</div>
		</div>
		<?php } ?>
	
	</form>
</div>

<?php 
	$formName = '"#form_user"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
