function trysort(what)
{	
	var addedclass = "";
	var addedval = "";
	var curval = $("#sort").val();
	
	if(curval.indexOf(what) == -1){		
		addedval = what + "_asc";
		addedclass = "asc";
	}
	else {
		if(curval.indexOf("asc") == -1){
			addedval = what + "_asc";
			addedclass = "asc";
		}
		else {
			addedval = what + "_desc";
			addedclass = "desc";
		}
	}
	
	$("#sort").val(addedval);
	
	// remove all class 	
	$(".sorter").each(function(){
		var thisid = $(this).attr("id");
		
		$(this).removeClass("asc");
		$(this).removeClass("desc");
	
		if(thisid == what){			
			$(this).addClass(addedclass);			
		}
	});
	
	
	refreshContent();
	
	return false;
}

function refreshContent()
{
	// Get some values from elements on the page:
	var $form = $("#form_search");
	var sent = $($form).serialize();	
	var url = $form.attr("action");
	
	// Send the data using post
	var posting = $.post( url, sent );
	
	$("#loadingGif").css("display","");
		  
	// Put the results in a div
	posting.done(function( data ) {				  					
		$("#loadingGif").css("display","none");
		$("#tbody_content").html(data);
		
		// reset temporary variables
		$("#delete").val("");
		$("#edit").val("");
		$("#temp_id").val("");
		$("#temp_function").val("");
	});
}


function tryShowAll()
{
	var curmax = $("#maxrow").val();
	if(curmax == "9999"){
		$("#maxrow").val("20");
		$("#showall").html("<i class='glyphicon glyphicon-resize-full'></i> All");
		$("#showall").attr("title", "Show all data");
		
		refreshContent();
	}
	else {
		var nmb = $("#totalrow").val();
		var conf = confirm("There are "+nmb+" records. Are you sure you want to display them all?");
		
		if(conf == true){
			$("#maxrow").val("9999");
			$("#showall").html("<i class='glyphicon glyphicon-resize-small'></i> Page");
			$("#showall").attr("title", "Show only 20 data");
			
			refreshContent();
		}
	}
	
	
}

function pilihGameChange(what)
{	
	var option = $('#'+what).find('option:selected');
	
	var nama = $(option).data("nama");
	
	$("#new_username").val(nama);
	
	changeNewAccountPassword(what);
	
}

function ajaxPasteMember(data) {
    var rs = JSON.parse(data);
    $.ajax({
        type: "POST",
        url: "domember_ajax.php",
        cache: false,
        data: {
            nama: rs["nama"],
            narek: rs["namarek"],
            email: rs["email"],
            tlp: rs["tlp"],
            kdp: rs["kdproduct"],
            norek: rs["norek"],
            bron: rs["browser_name"],
            ket: rs["ket"],
            idl: rs["idlink"],
            kdb: rs["kdbank"],
            isac: rs["isactive"],
            wkt: rs["tanggal_daftar"],
            ip: rs["dari_ip1"],
            kanal: rs["kenal"],
            ids: rs["idsumber"],
            sum: rs["sumber"],
            kdg: rs["kdgame"],
        },
        //data: data,
        //dataType: 'json',
        success: function (response) {
            console.log("sukses kirim data");
            refreshContent();
        },
    });
}

async function pasteMember(input) {
    const json_data = await navigator.clipboard.readText();
    $.confirm({
        //theme: 'dark',
        title: "Proses paste data",
        content: "Pastikan sebelum klik proceed copy data dulu dari dashboard panel lama looo !!!",
        icon: "fa fa-question-circle",
        animation: "scale",
        closeAnimation: "scale",
        opacity: 0.5,
        buttons: {
            confirm: {
                text: "Proceed",
                btnClass: "btn-blue",
                action: function () {
                    $.confirm({
                        title: "Kamu yakin paste Member ini ??",
                        //content: json_data,
                        icon: "fa fa-warning",
                        animation: "scale",
                        closeAnimation: "zoom",
                        buttons: {
                            confirm: {
                                text: "Yes, sure!",
                                btnClass: "btn-orange",
                                action: function () {
                                    ajaxPasteMember2(json_data);
                                    $.alert('Sukses proses paste Member!');
                                },
                            },
                            cancel: function () {
                                $.alert("Kamu membatalkan proses ini");
                            },
                        },
                    });
                },
            },
            cancel: function () {
                $.alert("you clicked on cancel");
            },
            /*moreButtons: {
                text: "something else",
                action: function () {
                    $.alert("you clicked on something else");
                },
            },*/
        },
    });
}