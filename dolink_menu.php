<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$id = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post"){
		
		$namalink = $_REQUEST['namalink'];
		$url = $_REQUEST['url'];
		$waofficial = $_REQUEST['waofficial'];
		$status = $_REQUEST['status'];
		$kodelink = strtoupper($_REQUEST['kodelink']);
		$account_assigned = $_REQUEST['account_assigned'];
		
		$err = "";
		$textSuccess = "";
		
		if($namalink == "")		$err .= "<br/>Nama Link harus diisi";
		if($kodelink == "")		$err .= "<br/>Kode Link harus diisi";
		if($waofficial == "")	$err .= "<br/>WA Official harus diisi";
		// if($url == "")		$err .= "<br/>URL harus diisi";	
		
		if($err=="" && $menu == "edit"){			
			$query = "UPDATE link SET kodelink='$kodelink', namalink='$namalink', url='$url', waofficial='$waofficial', status=$status, account_assigned = '$account_assigned' WHERE idlink=$id ";
			$textSuccess = "Link berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";						
		}
		else if($err=="" && $menu == "create"){
			$query = "INSERT INTO link (namalink, url, status, account_assigned, kodelink, waofficial) VALUES ('$namalink', '$url', $status, '$account_assigned', '$kodelink', '$waofficial') ";
			$textSuccess = "Link berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';		
		
		mysqli_close($conn);
		exit();
	}
	
	$isself = false;
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM link WHERE idlink = $id ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Link: ".$row['namalink'];		
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Link";
	}
?>

<div class="user-update">
	<h1><?php echo $title ?></h1>
	<form id="form_link" class="form-vertical" action="dolink_menu.php" method="post">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $id ?>" >
		<input type="hidden" name="post" value="post" >
		<div class="row">
			
			<div class="col-sm-6">
				<div class="form-group field-user-kodelink required">
					<label class="control-label" for="user-kodelink">Kode Link</label>
					<input type="text" id="user-kodelink" class="form-control" name="kodelink" placeholder="Kode Link..." value="<?php echo $row['kodelink'] ?>" style="text-transform: uppercase">

					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div class="form-group field-user-namalink required">
					<label class="control-label" for="user-namalink">Nama Link</label>
					<input type="text" id="user-namalink" class="form-control" name="namalink" placeholder="Nama Link..." value="<?php echo $row['namalink'] ?>">

					<div class="help-block"></div>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-sm-6">
				<div class="form-group field-user-url required">
					<label class="control-label" for="user-url">URL</label>
					<input type="text" id="user-url" class="form-control" name="url" placeholder="URL..." value="<?php echo $row['url'] ?>">
					
					<div class="help-block"></div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group field-user-acc required">
					<label class="control-label" for="user-acc">Account Assigned</label>
					<input type="text" id="user-acc" class="form-control" name="account_assigned" placeholder="Account Assigned" value="<?php echo $row['account_assigned'] ?>">

					<div class="help-block"></div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-6">
				<div class="form-group field-user-waofficial required">
					<label class="control-label" for="user-waofficial">WA Official</label>
					<input type="text" id="user-waofficial" class="form-control" name="waofficial" placeholder="Ex: +628..." value="<?php echo $row['waofficial'] ?>">

					<div class="help-block"></div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group field-user-status required">
					<label class="control-label">Status</label>
					
					<div id="user-status">
						<label class="radio-inline"><input type="radio" name="status" value="1" <?php if($menu=="create" || $row['status']==1) echo 'checked'; ?>> Active</label>
						<label class="radio-inline"><input type="radio" name="status" value="0" <?php if($menu=="edit" && $row['status']==0) echo 'checked'; ?>> Inactive</label>
					</div>
					
					<div class="help-block"></div>
				</div>
			</div>
		</div>
		
		<div class="row">
			
			<div class="col-sm-12">
				<div style="text-align: right; margin-top: 20px">
					<div id="feedback_edit"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>

		</div>
		
		
	</form>
</div>

<?php 
	$formName = '"#form_link"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
