<?php
	include_once "config.php";
	
	$sent = $_REQUEST['iduser'];
?>
<div class="member-log">

<table class="table table-condensed">
    <thead class="thead-inverse">
      <tr >
        <th>Nama</th>
        <th>Tanggal</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
		<?php
			$query = "SELECT *  ".
						  ",CONVERT_TZ(zlog.tanggal,".$curtimezone.") as new_tanggal ".
						  "FROM zlog_admin zlog ".
						  "LEFT JOIN user us ON zlog.iduser=us.id ".
						  " WHERE zlog.iduser = $sent ORDER BY idlog DESC";
			$result = mysqli_query($conn, $query);
			
			$cnt = 0;
			while($row=mysqli_fetch_array($result))
			{
				$cnt++
		?>
			<tr>	
				<td><?php echo $row['username'] ?></td>
				<td><?php echo $row['new_tanggal'] ?></td>
				<td>
					<?php 
						$ket = $row['keterangan'];
						$ket = str_replace("\n", "<br/>", $ket);
						
						$ket = str_replace("Admin Login", "<b style='color: blue'>Admin Login</b>", $ket);
						$ket = str_replace("Admin Logout", "<b style='color: red'>Admin Logout</b>", $ket);
						
						echo $ket;
					?>
				</td>
			</tr>
		<?php } ?>
		
		<?php
			if($cnt <= 0){
				echo '<tr><td>Tidak ditemukan Log untuk Admin ini </td></tr>';
			}
		?>
    </tbody>
  </table>
</div>

<?php mysqli_close($conn); ?>