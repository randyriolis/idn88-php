<?php 
	include_once "inc_login.php";
	include "config.php";
	$id = $_REQUEST['id'];
	
	if(empty($id) )	$id = 1;
	
	$query = "SELECT * FROM banks WHERE kdbank = $id";				   
	$result = mysqli_query($conn, $query);	
	$row = mysqli_fetch_assoc($result);
	
	$p= $_REQUEST['p'];
	if($p == 'post')
	{
		// process to send sms
		
		$jambuka = $_REQUEST["jambuka"];
		$menitbuka = $_REQUEST["menitbuka"];
		$jamtutup = $_REQUEST["jamtutup"];
		$menittutup = $_REQUEST["menittutup"];
		$mode = $_REQUEST["mode"];
		
		$jambank = $jambuka.":".$menitbuka."-".$jamtutup.":".$menittutup;
		
		
		
		$sql = "UPDATE banks SET time='$jambank', isautomatic=$mode WHERE kdbank = $id ";
		$res = mysqli_query($conn, $sql);
		
		if($res)			echo '<span style="color:green"> Sukses.<br/>Jam Buka Bank berhasil diganti</span><br/>';
		else 			echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
		
		exit();
	}
	
	$jambank = explode("-", $row["time"]);
	
	$jambuka = explode(":", $jambank[0])[0];
	$menitbuka = explode(":", $jambank[0])[1];
	
	$jamtutup = explode(":", $jambank[1])[0];
	$menittutup = explode(":", $jambank[1])[1];	
	
	$mode = $row["isautomatic"];
?>


<div class="member-sms">    
	<div class="member-sms">

	<form id="form_smsaccount" class="form-vertical" action="dobank_gantijam.php?id=<?php echo $id ?>&p=post" method="post">
	
			<div class="col-sm-3">	Nama Bank : </div>
			<div class="col-sm-9">	<?php echo $row['inisialbank'] ?> </div>
			
			<div class="col-sm-3">	Hari : </div>
			<div class="col-sm-9">					
				<?php 
					$namahari = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');				
					echo $namahari[$row["day"] ];
				?>
			</div>
			 <div class="clearfix"></div>
			 
		<div class="form-group field-member-formatsms" style="margin-top: 20px">
		
			<div class="col-sm-6">
				<div style="text-align:center">
					<label class="control-label" for="member-formatsms">WAKTU BUKA</label>
				</div>
				
				<div class="col-sm-6">
				<div style="text-align: center">JAM</div>
				
				<select id="member-idgame" class="form-control" name="jambuka">
					<?php
						for($i=0; $i<24; $i++) {
							$value = $i<10? "0".$i : $i;
							$selected = $value==$jambuka? "selected" : "";
							echo '<option class="form-control" '.$selected.' value="'.$value.'">'.$value.'</option>';
						}										
					?>					
				</select>
				</div>
				
				<div class="col-sm-6">
				<div style="text-align: center">MENIT</div>
				<select id="member-idgame" class="form-control" name="menitbuka">
					<?php
						for($i=0; $i<60; $i+=5) {
							$value = $i<10? "0".$i : $i;
							$selected = $value==$menitbuka? "selected" : "";
							echo '<option class="form-control" '.$selected.' value="'.$value.'">'.$value.'</option>';
						}										
					?>					
				</select>
				
				</div>
				
			</div>
			
			<div class="col-sm-6">
				<div style="text-align:center">
					<label class="control-label" for="member-formatsms">WAKTU TUTUP</label>
				</div>
				
				<div class="col-sm-6">
				<div style="text-align: center">JAM</div>
				
				<select id="member-idgame" class="form-control" name="jamtutup">
					<?php
						for($i=1; $i<=24; $i++) {
							$value = $i<10? "0".$i : $i;
							$selected = $value==$jamtutup? "selected" : "";
							echo '<option class="form-control" '.$selected.' value="'.$value.'">'.$value.'</option>';
						}										
					?>					
				</select>
				</div>
				
				<div class="col-sm-6">
				<div style="text-align: center">MENIT</div>
				
				<select id="member-idgame" class="form-control" name="menittutup">
					<?php
						for($i=0; $i<60; $i+=5) {
							$value = $i<10? "0".$i : $i;
							$selected = $value==$menittutup? "selected" : "";
							echo '<option class="form-control" '.$selected.' value="'.$value.'">'.$value.'</option>';
						}										
					?>					
				</select>
				
				</div>
				
			</div>
			
			<div class="clearfix"></div>
			
			<div class="col-sm-12" style="margin-top:20px">
				<label class="control-label" for="radio-inline">MODE</label>
				
				<div id="user-status">
				<label class="radio-inline"><input <?php if($mode == 1) echo 'checked'; ?> type="radio" name="mode" value="1"> OTOMATIS</label>
				<label class="radio-inline"><input <?php if($mode == 0) echo 'checked'; ?> type="radio" name="mode" value="0"> MANUAL</label>
				</div>				
			</div>
			
			<div class="help-block"></div>
		</div>
		
		
		
		<div class="clearfix"></div>
		<br/>
		
		<div class="form-group">
			<div id="feedback_1"></div>
			<button type="submit" class="btn btn-primary">UPDATE</button>  
				<img class="thisLoadingGif" src="img/loading.gif" />
		</div>

	</form>
	</div>
</div>

<?php 
	$formName = '"#form_smsaccount"';
	$feedback = '"#feedback_1"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>