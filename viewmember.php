<?php
	include_once "inc_login.php";
	include_once "config.php";
	include_once "function.php";
	
	$id = $_REQUEST['id'];
	
	// QUERY
	$sql = "SELECT mem.*, acc.username, gm.nama AS nama_game,  acc.modby AS nama_admin, bk.inisialbank AS namabank ".
				",from_unixtime(mem.tanggal_daftar,'%Y-%m-%d %H:%i:%s') as new_datecreate ".
				",from_unixtime(acc.modtime,'%Y-%m-%d %H:%i:%s') as new_dateassign ".
				"FROM members mem ".
				"LEFT JOIN akuns acc ON mem.kdmember=acc.kdmember ".
				"LEFT JOIN products gm ON mem.kdproduct=gm.kdproduct ".				
				"LEFT JOIN banks bk ON mem.kdbank=bk.kdbank ".				
			   "WHERE mem.kdmember = $id";			   
	$result = mysqli_query($conn, $sql);	
	$arrmem = mysqli_fetch_array($result);

	$sql_deposit = mysqli_query($conn, "SELECT SUM(jumlah) AS deposit FROM deposits WHERE kdmember = $id");
	$arrdep = mysqli_fetch_array($sql_deposit);

	$sql_withdraw = mysqli_query($conn, "SELECT SUM(jumlah) AS withdraw FROM withdraws WHERE kdmember = $id");
	$arrwd = mysqli_fetch_array($sql_withdraw);

	$sisa_saldo = ($arrdep['deposit'] - $arrwd['withdraw']);
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Deposits</title>    	
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
	<style>
		.table-bordered {
		border: 1px solid #999;
		}
		.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
			border: 1px solid #999;
		}
	</style>
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "member";
		include_once "inc_header.php";
	?>

<div class="content-wrapper">
    <section class="content-header">
		<h1></h1>
        
        <ul class="breadcrumb">
			<li><a href="site.php">Home</a></li>
			<li><a href="member.php">Members</a></li>
			<li class="active"><?php echo htmlentities($arrmem['nama']).' ['. $id .']' ?></li>
		</ul>    
	</section>

<section class="content">                
	
<div class="member-view">

    <h1><?php echo htmlentities($arrmem['nama']).'('. $arrmem['username'] .')'; ?></h1>

	<div class="row">
		<div class="col-lg-4">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2">Data Information</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td>Nama Rekening</td>
						<td><strong><?php echo htmlentities($arrmem['nama']) ?></strong></td>
					</tr>
					<tr>
						<td>No Rekening</td>
						<td><strong><?php echo htmlentities($arrmem['norek']) ?></strong></td>
					</tr>
					<tr>
						<td>Bank</td>
						<td><strong><?php echo htmlentities($arrmem['namabank']) ?></strong></td>
					</tr>
					<tr>
						<td>Game</td>
						<td><strong><?php echo htmlentities($arrmem['nama_game']) ?></strong></td>
					</tr>
					<tr>
						<td>Date Create</td>
						<td><strong><?php echo $arrmem['new_datecreate'] ?></strong></td>
					</tr>
					<tr>
						<td>Date Assign</td>
						<td><strong>
							<?php 							
								echo $arrmem["new_dateassign"];
							?>
						</strong></td>
					</tr>
					<tr>
						<td>Register IP</td>
						<td><strong><?php echo $arrmem['dari_ip1'] ?></strong></td>
					</tr>    
					<tr>
						<td>Info Browser</td>
						<td><strong><?php echo $arrmem['browser_name'] ?></strong></td>
					</tr>
					<tr>
						<td>Assign By</td>
						<td><strong><?php echo $arrmem['nama_admin'] ?></strong></td>
					</tr>    
				</tbody>
			</table>
			<div class="clearboth"></div>			
			<table class="table table-bordered">
				<thead>
					<tr>
						<th colspan="2">Data Saldo</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td>Total Deposit</td>
						<td><strong><text class="pull-right"><?php echo htmlentities(getRupiah($arrdep['deposit'])) ?></text></strong></td>
					</tr>
					<tr>
						<td>Total Withdraw</td>
						<td><strong><text class="pull-right"><?php echo htmlentities(getRupiah($arrwd['withdraw'])) ?></text></strong></td>
					</tr>
					<tr>
						<td style="background-color: yellow"><strong>Sisa Saldo</strong></td>
						<td style="background-color: yellow"><strong><text class="pull-right"><?php echo htmlentities(getRupiah($sisa_saldo)) ?></text></strong></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-lg-4">		
		<?php 
			$querydep = "SELECT dep.*, dep.clearby AS nama_admin 
								 , from_unixtime(dep.tanggal,'%d-%m-%Y %H:%i:%s') as new_datecreate 
								 FROM deposits dep 
								 WHERE dep.kdmember = $id ORDER BY kddeposit DESC";
			$resdep = mysqli_query($conn, $querydep);			
			$totaldep = mysqli_num_rows($resdep);
			
			$totalclear = 0;
			$totalreject = 0;
			
			$arrdeposit = array();
			while($row=mysqli_fetch_array($resdep)){
				if($row['isactive'] == 1 && $row['isclear'] == 1)
					$totalclear += $row['jumlah'];
				else if($row['isactive'] == 0)	
					$totalreject += $row['jumlah'];
				
				array_push($arrdeposit, $row);
			}
		?>

		Banyak Deposit : <?php echo $totaldep ?> <br />
						
		Deposit Clear : 
		<?php echo $totalclear ?><br />
		Deposit Rejected :
		<?php echo $totalreject ?><br />

			<table class="table table-bordered">
			<thead>
			  <tr>
				<th>History Deposit</th>
				<th><text class="pull-right">Jumlah</text></th>
				<th><text class="text-center">BY</text></th>
			  </tr>
			</thead>
			<tbody>				
				<tr>
				<?php 
					for($i=0; $i<count($arrdeposit); $i++){
						echo '<tr>';
						echo '<td>'.$arrdeposit[$i]['new_datecreate'].'</td>';
						
						if($arrdeposit[$i]['isactive'] == 0)
							echo '<td style="background-color:red;"><strong><text class="pull-right">'.$arrdeposit[$i]['jumlah'].' IDR</strong></td>';
						else
							echo '<td style="background-color:green;"><strong><text class="pull-right">'.$arrdeposit[$i]['jumlah'].' IDR</strong></td>';
						
						echo '<td>'.$arrdeposit[$i]['nama_admin'].'</td>';
						echo '</tr>';
					}
				?>			  
				</tr>
			</tbody>
		  </table>
		</div>


		<div class="col-lg-4">
		<?php
			// $querywith = "SELECT wit.*, CONVERT_TZ(wit.datecreate,".$curtimezone.") AS new_datecreate , us.username as nama_admin FROM withdraw wit LEFT JOIN user us ON wit.iduser=us.id WHERE wit.idmember = $id ORDER BY idwithdraw DESC";
			
			$querywith = "SELECT wit.*, wit.clearby AS nama_admin 
								, from_unixtime(wit.tanggal,'%d-%m-%Y %H:%i:%s') as new_datecreate 
								 FROM withdraws wit 
								 WHERE wit.kdmember = $id ORDER BY kdwithdraw DESC";
			$reswith = mysqli_query($conn, $querywith);
			$totalwith = mysqli_num_rows($reswith);
			
			$totalwithclear = 0;
			$totalwithreject = 0;
			
			$arrwithdraw = array();
			while($row=mysqli_fetch_array($reswith)){
				if($row['isactive'] == 1 && $row['isclear'] == 1)
					$totalwithclear += $row['jumlah'];
				else if($row['isactive'] == 0)	
					$totalwithreject += $row['jumlah'];
				
				array_push($arrwithdraw, $row);
			}
		?>

		Banyak Withdraw : <?php echo $totalwith ?> <br />
						
		Withdraw Clear : 
		<?php echo $totalwithclear ?><br />
		Withdraw Rejected :
		<?php echo $totalwithreject ?><br />

			<table class="table table-bordered">
			<thead>
			  <tr>
				<th>History Withdraw</th>
				<th><text class="pull-right">Jumlah</text></th>
				<th><text class="text-center">BY</text></th>
			  </tr>
			</thead>
			<tbody>
			  <tr>
			  <?php 
					for($i=0; $i<count($arrwithdraw); $i++){
						echo '<tr>';
						echo '<td>'.$arrwithdraw[$i]['new_datecreate'].'</td>';
						
						if($arrwithdraw[$i]['isactive'] == 0)
							echo '<td style="background-color:red;"><strong><text class="pull-right">'.$arrwithdraw[$i]['jumlah'].' IDR</strong></td>';
						else
							echo '<td style="background-color:green;"><strong><text class="pull-right">'.$arrwithdraw[$i]['jumlah'].' IDR</strong></td>';
						
						echo '<td>'.$arrwithdraw[$i]['nama_admin'].'</td>';
						echo '</tr>';
					}
				?>	
			  </tr>
			</tbody>
		  </table>
		</div>
	</div>


</div>
</section>
		</div>
<?php  mysqli_close($conn); ?>
</body>

</html>