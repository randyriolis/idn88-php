<?php 
	date_default_timezone_set('Asia/Jakarta');
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST["menu"];
	
	$p= $_REQUEST['p'];
	
	if($p == 'post')
	{		
		// Insert Deposit
		$kdmember = $_REQUEST["kdmember"];
		$nama = $_REQUEST["nama"];
		$norek = $_REQUEST["norek"];
		$jumlah = $_REQUEST["jumlah"];
		$jumlah = str_replace(",", "", $jumlah);
		$tanggal = empty($_REQUEST['tanggal']) ? time() : strtotime($_REQUEST['tanggal']);
		// $tanggal = $_REQUEST['tanggal'];
				
		// Check
		$err = "";
		
		if($kdmember == "")	$err .= "<br/>Pilih satu Username.";
		if($jumlah == "")	$err .= "<br/>Jumlah tidak boleh kosong.";
		
		// search member
		$tmp = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM members WHERE kdmember='$kdmember' "));
		$ip = $_SERVER['REMOTE_ADDR'];
		$norek = $tmp['norek'];
		$kdproduct = $tmp['kdproduct'];
		$nama = $tmp['nama'];
		$kdbank = $tmp['kdbank'];
		$username = $tmp['username'];
				
		if($err != ""){
			echo '<span style="color:red"> Error'.$err.'</span><br/>';
			exit();
		}
		
		// new deposit
		$query = "INSERT INTO deposits (kdmember, username, jumlah, tanggal, dari_ip1, norek, isactive, isnew, kdproduct, namarek, kdbank)
					VALUES ($kdmember, '$username', '$jumlah', '$tanggal', '$ip', '$norek', 1, 1, $kdproduct, '$nama', '$kdbank')";					
		$res = mysqli_query($conn, $query);
				
		if($res)			echo '<span style="color:green"> Sukses.<br/>Deposit berhasil di-create</span><br/>';
		else 			echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
		
		exit();
	}
	
	// $tanggal = empty($trans['tanggal'])? date("d-m-Y H:i:s", time()+ 3600*(7+date("I")) ) : date("d-m-Y H:i:s", strtotime($trans['tanggal']) );	
	$tanggal = date("d-m-Y H:i:s", time());
?>
<div >
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>
		<span style="background-color: #454545; color: white; padding: 5px 20px 5px 20px ;">Deposit Manual</span>
	</h4>
	<br/>
</div>

<form id="form_createdeposit" class="form-vertical" action="dodeposit_create.php?p=post" method="post">
	<fieldset id="w2">
		<div class="row">
			<div class="col-sm-4">	
				<div class="form-group field-deposit-username required">
					<input type="hidden" name="kdmember" id="kdmember" />
					<label class="control-label" for="deposit-username">Username</label>
					
					<select id="accountsearch-username" class="form-control js-example-basic-single" name="username" onchange="changeForm();" style="width:100%">
						<option value="">-Pilih Username-</option>
						<?php 
							$tempquery = "SELECT mem.nama, mem.kdmember, mem.username, mem.norek, gm.nama AS nama_game  ".
												   "FROM members mem ".
												   "LEFT JOIN akuns acc ON acc.kdmember = mem.kdmember ".
												   "LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ".												   
												   "WHERE acc.isassigned = 1 AND mem.isactive = 1 ";
							$result = mysqli_query($conn, $tempquery);
							while($mem=mysqli_fetch_array($result)) {
								echo '<option value="'.$mem['kdmember'].'" data-nama="'.$mem['nama'].'" data-kodegame="'.$mem['nama_game'].'" data-norek="'.$mem['norek'].'">'.
												$mem['username'].' - '.$mem['nama'].' 
										 </option>';										 
							}							
						?>						
					</select>
					
					<div class="help-block"></div>
				</div>

			</div>

			<div class="col-sm-4">
				<div class="form-group field-deposit-jumlah required">
					<label class="control-label" for="deposit-jumlah">Jumlah</label>
					<input type="text" id="deposit-jumlah" class="form-control numb_format kv-align-right" name="jumlah" placeholder="Jumlah">
					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group field-transaksi-tanggal required">
					<label class="control-label" for="transaksi-tanggal">Tanggal</label>
					<input type="text" id="transaksi-tanggal" class="form-control" name="tanggal" placeholder="Tanggal" style="width:100%;" value="<?php echo $tanggal ?>" readonly>
					<label class="label_edit_tanggal"><input type="checkbox" id="click_edit_tanggal" value="Edit Tanggal" /> Edit Tanggal</label>
					
					<div class="help-block"></div>
				</div>							
			</div>
			
		</div>
	</fieldset>

	<fieldset id="w4">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group field-deposit-idpromo">
					<label class="control-label" for="deposit-nama">Nama Pemain</label>
					<input type="text" id="deposit-nama" class="form-control" name="nama" placeholder="Nama Pemain" readonly>
					
					<div class="help-block"></div>
				</div>
			</div>
						
			<div class="col-sm-4">
				<div class="form-group field-deposit-idgame required">
					<label class="control-label" for="deposit-kodegame">Kode Game</label>
					<input type="text" id="deposit-kodegame" class="form-control" name="kodegame" placeholder="Kode Game" readonly>
					
					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group field-deposit-norek">
					<label class="control-label" for="deposit-norek">Nomor Rekening</label>
					<div class="click_readonly" id="deposit-initial_bank" style="float:right; height: 34px; line-height:34px;  padding: 0px 12px; z-index:10; position:absolute; width:90%; text-align:right; color:#666 "> 
						&nbsp;
					</div>
					<input type="text" id="deposit-norek" class="form-control" name="norek" placeholder="Nomor Rekening" readonly >
					
					<div class="help-block"></div>
				</div>
				
			</div>
			
		</div>

	</fieldset>
	
	<fieldset id="w5">
		<div class="row">
			
			
			<div class="col-sm-12">
				<div style="text-align: right; margin-top: 20px">
					<div id="feedback_create"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
			
			
			
		</div>
	</fieldset>
	
	<br/>
	<fieldset id="w6">
		<div class="row" style="text-align: center; font-size: 19px;">
			MOHON SELALU PASTIKAN CREDIT PERMAINAN DAN SALDO BANK HARUS SAMA DENGAN YANG DI INTERNET
		</div>
	</fieldset>
		
</form>


<script type="text/javascript" src="js/select2.full.js"></script>
<script type="text/javascript" src="js/new_adminjs.js"></script>

<script type="text/javascript">
	
	$(document).ready(function()
	{
		// $(".js-example-basic-single").select2();
		$(".click_readonly").click(function(){
			$("#deposit-norek").select();
			copyToClipboard($("#deposit-norek").val());
		});
		
		$("#click_edit_tanggal").click(function(){		
			$("#transaksi-tanggal").prop("readonly", !$("#transaksi-tanggal").prop("readonly"));
		} );
	});
	
	
	function copyToClipboard(txt) 
	{
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(txt).select();
		document.execCommand("copy");
		$temp.remove();
		
		$("#copy_clipboard").css("display", "block");
		$("#copy_clipboard").html("'<b>"+txt + "</b>' is copied");
		
		$("#copy_clipboard").fadeTo(1000, 500).slideUp(500, function(){
			$("#copy_clipboard").slideUp(500);
		});  
	}
	
	
	function changeForm(){
		// var val = $("#accountsearch-username").val();
		var obj = $('#accountsearch-username :selected');
		
		$("#deposit-nama").val(obj.data("nama"));
		$("#deposit-norek").val(obj.data("norek"));
		$("#deposit-kodegame").val(obj.data("kodegame"));
		
		// $("#deposit-last_credit").val( getNumberFormat(obj.data("credit")) );
		$("#kdmember").val(obj.val());
		
	}
	
	var prevvalue = "";
	$(".numb_format").keyup(function(event)
	{		
			// skip for arrow keys
		  if(event.which >= 37 && event.which <= 40){
			  event.preventDefault();
		  }
		  var tmp = $(this);
		  var val = tmp.val();
		  console.log(val);
		  if(val == prevvalue)	return;
		  
		  val = val.replace(/\D/g,'');
		  prevvalue = val;
		  var num = val.replace(/,/gi, "").split("").reverse().join("");

		  var num2 = RemoveRougeChar(num.replace(/(.{3})/g,"$1,").split("").reverse().join(""));

		  // the following line has been simplified. Revision history contains original.
		  tmp.val(num2);
		  
	  });
	  
	  function RemoveRougeChar(convertString){
		if(convertString.substring(0,1) == ","){

			return convertString.substring(1, convertString.length)            

		}
		return convertString;	
	}
	
	function getNumberFormat(val){
		
		if(val == "0")	return "0";
		else if(val == "")	return "";		
		
		var str = val.toString();
		str = str.replace(/\D/g,'');
		var num = str.replace(/,/gi, "").split("").reverse().join("");

		var num2 = RemoveRougeChar(num.replace(/(.{3})/g,"$1,").split("").reverse().join(""));
		return num2;
	}
	
	
</script>
<?php 
	$formName = '"#form_createdeposit"';
	$feedback = '"#feedback_create"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
