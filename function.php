<?php 
	require __DIR__ . '/vendor/autoload.php';


	use Carbon\Carbon;
	use Twilio\Rest\Client;

	function convertPassword($pass)
	{
		$pass = md5($pass);
		
		// $pass = crypt($pass, "bijiontaliar25$");
		
		return $pass;
	}
	
	function SendSMS($msg, $no_hp)
	{		
		// jangan lupa catet log
		$url = "https://reguler.zsms.us/apps/smsapi.php?userkey=sea95v&passkey=qwe12388&nohp=$no_hp&pesan=$msg";
		$myXMLData = file_get_contents($url);		
		
		$xml = simplexml_load_string($myXMLData) or $err = "Error: Cannot create object";
		$res = (string) $xml->message->text;
	}

	function getSumber($id)
	{
		if ($id == 1) {
			$data = 'Google';
		}
		elseif ($id == 2) {
			$data = 'Facebook';
		}
		elseif ($id == 3) {
			$data = 'Instagram';
		}
		elseif ($id == 4) {
			$data = 'Twitter';
		}
		elseif ($id == 5) {
			$data = 'WhatsApp';
		}
		elseif ($id == 6) {
			$data = 'Refferal';
		}
		elseif ($id == 7) {
			$data = 'Lainnya';
		}
		else {
			$data = 'Empty';
		}

		return $data;
	}

	function getRupiah($angka){
	
		$hasil_rupiah = number_format($angka,0,',','.') . " IDR";
		return $hasil_rupiah;
	 
	}

	function twilioSendSms($no_telp, $pesan)
	{
		$twilio = new Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN); 
		 
		$message = $twilio->messages->create(
			$no_telp, // to 
		    array(
		    	"messagingServiceSid" => TWILIO_MESSAGE_SID,
		    	"body" => $pesan
		    ) 
		);

		$res = (string) $message->status;
		$res_text = ($res==="accepted") ? 'Success.. sms sent' : 'Error.. cannot send sms';

		echo '<span style="color:green"> '. $res_text . '</span><br/>';
	}

	function messagebirdSendSms($no_telp, $pesan)
	{
		$messagebird = new MessageBird\Client(MESSAGEBIRD_API_KEY);
		$message = new MessageBird\Objects\Message;
		$message->originator = 'TestMessage';
		$message->recipients = [$no_telp];
		$message->body = $pesan;
		$response = $messagebird->messages->create($message);

		$res = (string) $response->recipients->items[0]->status;
		$restes = (string) $response->recipients->items[0]->recipient;
		//var_dump($response);
		$res_text = ($res==="sent") ? 'Success.. sms sent' : 'Error.. cannot send sms';
		echo '<span style="color:green"> '. $res_text . '</span><br/>';
	}

	function messagebirdSendWhatsappTemplate($wasend, $user, $password, $link, $waofficial, $game)
	{
		$messageBird = new MessageBird\Client(MESSAGEBIRD_API_KEY);

		$hsmParam1 = new \MessageBird\Objects\Conversation\HSM\Params();
		$hsmParam1->default = $user;

		$hsmParam2 = new \MessageBird\Objects\Conversation\HSM\Params();
		$hsmParam2->default = $password;

		$hsmParam3 = new \MessageBird\Objects\Conversation\HSM\Params();
		$hsmParam3->default = $link;

		$hsmParam4 = new \MessageBird\Objects\Conversation\HSM\Params();
		$hsmParam4->default = $waofficial;

		$hsmParam5 = new \MessageBird\Objects\Conversation\HSM\Params();
		$hsmParam5->default = $game;

		$hsmLanguage = new \MessageBird\Objects\Conversation\HSM\Language();
		$hsmLanguage->policy = \MessageBird\Objects\Conversation\HSM\Language::DETERMINISTIC_POLICY;
		//$hsmLanguage->policy = \MessageBird\Objects\Conversation\HSM\Language::FALLBACK_POLICY;
		$hsmLanguage->code = 'id';

		$hsm = new \MessageBird\Objects\Conversation\HSM\Message();
		$hsm->templateName = MESSAGEBIRD_TEMPLATE_NAME;
		$hsm->namespace = MESSAGEBIRD_NAMESPACE;
		$hsm->params = [$hsmParam1,$hsmParam2,$hsmParam3,$hsmParam4,$hsmParam5];
		$hsm->language = $hsmLanguage;

		$content = new \MessageBird\Objects\Conversation\Content();
		$content->hsm = $hsm;

		$message = new \MessageBird\Objects\Conversation\Message();
		$message->channelId = MESSAGEBRID_CHANNEL_ID;
		$message->content = $content;
		$message->to = $wasend;
		$message->type = 'hsm';

		try {
		    $conversation = $messageBird->conversations->start($message);
		    echo '<span style="color:green">Success.. whatsapp sent</span>';
		    //var_dump($conversation);
		} catch (\Exception $e) {
		    echo sprintf("<span style='color:green'>%s: %s</span>", get_class($e), $e->getMessage());
		}
	}

	function messagebirdSendWhatsapp($wasend, $pesan)
	{
		$messageBird = new MessageBird\Client(MESSAGEBIRD_API_KEY);

		$content = new \MessageBird\Objects\Conversation\Content();
		$content->text = $pesan;

		$message = new \MessageBird\Objects\Conversation\Message();
		$message->channelId = MESSAGEBRID_CHANNEL_ID;
		$message->content = $content;
		$message->to = $wasend; // Channel-specific, e.g. MSISDN for SMS.
		$message->type = 'text';

		try {
		    $conversation = $messageBird->conversations->start($message);
		    //var_dump($conversation);
		    $res_text = 'Success.. whatsapp sent';
		    echo '<span style="color:green">'. $res_text . '</span><br/>';
		} catch (\Exception $e) {
		    echo sprintf("%s: %s", get_class($e), $e->getMessage());
		}
	}

	function twilioSendWhatsapp($wasend, $pesan)
	{
		$twilio = new Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN); 

		$message = $twilio->messages->create(
			"whatsapp:".$wasend, // to
			array(
				"from" => "whatsapp:".TWILIO_WHATSAPP_NUM,
				"body" => $pesan 
			) 
		); 

		$res = (string) $message->status;
		$res_text = ($res==='sent' || 'QUEUED') ? 'Success.. whatsapp sent' : 'Error.. cannot send whatsapp';

		echo '<span style="color:green">'. $res_text . '</span><br/>';

	}

	function testlibComposer($kata)
	{
		$sekarang = Carbon::now();

		$text = "Sekarang: $sekarang <br>";
		$text .= "Umur saya: " . Carbon::createFromDate(2000, 1, 1)->age . "<br>";
		$text .= "Besok: " . $sekarang->addDay() ."<br>";
		$text .= formatTelp($kata); 

		return $text;
	}

	function formatTelp($telp)
	{
		$code = substr($telp, 0, 2);
		$code = ($code=='08') ? str_replace("08", "+628", $code) : str_replace("62", "+62", $code);

		$telp = substr($telp, 2, strlen($telp)-2);
		$telp = $code . $telp;

		return $telp;
	}

	function formatWA($number)
	{
		$formatted_number = ereg_replace("[^0-9]", "", $number);
		$code = substr($formatted_number, 0, 2);
		$new_telp = substr($formatted_number, 2, strlen($formatted_number)-2);

		return $new_telp;	
	}

	function twilioSendgrid($toAddressEmail, $toEmailName, $contentEmail, $titleEmail)
	{
		$email = new \SendGrid\Mail\Mail(); 
		$email->setFrom("bandarsport.corporation@gmail.com", "Bandarsport Corp");
		$email->setSubject($titleEmail);
		$email->addTo($toAddressEmail, $toEmailName);
		$email->addContent("text/plain", $contentEmail);
		$sendgrid = new \SendGrid(SENDGRID_API_KEY);
		try {
		    $response = $sendgrid->send($email);
		    //print $response->statusCode() . "\n";
		    //print_r($response->headers());
		    //print $response->body() . "\n";
		    echo '<span style="color:green">Success.. email sent</span><br/>';
		} catch (Exception $e) {
		    echo 'Caught exception: '. $e->getMessage() ."\n";
		}
	}

	function getUrl(){
	    if(isset($_SERVER['HTTPS'])){
	        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
	    }
	    else{
	        $protocol = 'http';
	    }
	    return $protocol . "://" . $_SERVER['HTTP_HOST'];
	}
?>