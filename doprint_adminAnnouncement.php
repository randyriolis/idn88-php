<?php 
	include "config.php";
	include "inc_login.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM backend_announcement WHERE idannouncement = $delete ";
		$result = mysqli_query($conn, $query);
	}
	
	$wherequery = " ";
	
	$page = $_REQUEST['page'];
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$orderquery = "ORDER BY idannouncement ASC ";
	if($sort != "")	
	{
		if($sort == "txt_asc")		$orderquery = "ORDER BY txt ASC ";
		else if($sort == "txt_desc") $orderquery = "ORDER BY txt DESC ";
		
		else if($sort == "tanggal_mulai_asc")		$orderquery = "ORDER BY tanggal_mulai ASC ";
		else if($sort == "tanggal_mulai_desc") $orderquery = "ORDER BY tanggal_mulai DESC ";		
				
		else if($sort == "status_asc")		$orderquery = "ORDER BY status ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY status DESC ";		
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT ann.*, ln.namalink FROM backend_announcement ann
			  LEFT JOIN link ln ON ln.idlink = ann.idlink
			  WHERE 1=1  
			  ".$wherequery.$orderquery.$limitquery;
	echo $query;
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	

?>
	<tr data-key="<?php echo $row['idannouncement'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kv-align-left kv-align-middle" data-col-seq="1">
			SUBJECT: <b><?php echo $row['judul'] ?></b>
			<div style="padding-top: 5px"><?php echo $row['isi'] ?></div>
		</td>		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="6">
			<?php 
				echo "<span style='color:green'>".date("d-m-Y", strtotime($row['tanggal_mulai']))."</span>".
					"<br/>".
					"<span style='color:#990000'>".date("d-m-Y", strtotime($row['tanggal_akhir']))."</span>";
			?>	
		</td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1">
			<?php 
				if($row["idlink"] == 0) echo 'All Link';
				else echo $row['namalink'];
			?>
		</td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1">
			<b>
			<?php 
				$kepentingan = $row['kepentingan'];
				if($kepentingan == 1)	echo '<span style="color:#999900">Normal</span>';
				else if($kepentingan == 2)	echo '<span style="color:#000088">Penting</span>';
				else if($kepentingan == 3)	echo '<span style="color:#AA0000">Urgent</span>';
			?>
			</b>
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="doadminAnnouncement_menu.php?m=edit&id=<?php echo $row['idannouncement'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="8">
			<a class="btn btn-danger btn-xs" href="#" onClick="deletePromo(<?php echo $row['idannouncement'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(idannouncement) as num_rows FROM backend_announcement WHERE 1=1  ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deletePromo(idaccount)
	{
		var str = "Apakah Kamu yakin delete Announcement ini ?? ";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>