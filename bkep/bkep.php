<?php
 
/* 
 * This script only works on linux.
 * It keeps only 31 backups of past 31 days, and backups of each 1st day of past months.
 */


define('BACKUP_SAVE_TO', '/home/gilabet/public_html/kekuatanalam/bkep'); // without trailing slash
 
$time = time();
$date = date('Y_m_d', $time);

$backupFile = BACKUP_SAVE_TO . '/gilabet_' . $date . '.gz';
if (file_exists($backupFile)) {
    // do nothing, already backup
}
else 
{
	include_once "../config.php" ;

	define('DB_HOST', $servername);
	define('DB_NAME', $dbname);
	define('DB_USER', $username);
	define('DB_PASSWORD', $password);

	$command = 'mysqldump --opt -h ' . DB_HOST . ' -u ' . DB_USER . ' -p\'' . DB_PASSWORD . '\' ' . DB_NAME . ' | gzip > ' . $backupFile;
	$feedback = system($command);
	
	// close
	// mysqli_close($conn);
}


?>