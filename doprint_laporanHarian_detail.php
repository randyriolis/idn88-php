<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">

	<tr>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd" style="width:36px;" >#</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd" >Username</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd" >Akun</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd" >No.Rekening</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd">Bank</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd">Game</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd">DP/WD</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd">Nominal</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd">Tanggal dan Waktu</th>

		<th class="kartik-sheet-style kv-align-center kv-align-middle odd">Status | Diapprove oleh | Tipe</th>

	</tr>



<?php

	include_once "inc_login.php";
	include_once "config.php";
	
	

	$tanggal = $_REQUEST['tanggal'];

	$tanggal = date("Y-m-d", strtotime($tanggal));

	

	$query_select = "SELECT dp.jumlah, dp.tanggal, dp.kdmember, dp.cleartime AS dateassign, 'Deposit' AS nama_transaksi, dp.isactive, dp.clearby ";

	$linkquery = "";
	
	// Update, see only selected Link
	if($alpha_admb != 0)		$linkquery .= " AND mem.idlink=$alpha_admb  ";

	$queryDeposit = $query_select.

				   "FROM deposits dp ".
				   "INNER JOIN members mem ON mem.kdmember = dp.kdmember ".

				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".

				   "WHERE ln.status =1 $linkquery ".

				   " AND from_unixtime(dp.tanggal,'%Y-%m-%d') = date('$tanggal') AND dp.isactive = 1 AND dp.isclear = 1 ";

				   

	$query_select = str_replace("'Deposit'", "'Withdraw'", $query_select);

	$queryWithdraw = $query_select.

				   "FROM withdraws dp ".
				   "INNER JOIN members mem ON mem.kdmember = dp.kdmember ".

				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".

				   "WHERE ln.status =1 $linkquery ".

				   " AND from_unixtime(dp.tanggal,'%Y-%m-%d') = date('$tanggal') AND dp.isactive = 1 AND dp.isclear = 1 ";

		   

	$sql = "SELECT tr.*, mem.username, mem.norek, mem.nama, bk.inisialbank AS bank, gm.nama AS nama_game, clearby AS nama_admin    

					FROM (

						$queryDeposit 

						UNION ALL 

						$queryWithdraw 

					) AS tr 

				LEFT JOIN members mem ON tr.kdmember = mem.kdmember 

				LEFT JOIN banks bk ON mem.kdbank = bk.kdbank  				

				LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ";

					

	$result = mysqli_query($conn, $sql);

	

	$cnt = 0;

	$jumlah_approve = 0;		$count_approve = 0;

	$jumlah_reject = 0;			$count_reject = 0;

	while($row = mysqli_fetch_array($result)) 

	{

		$cnt ++;

		$bgcolor = $cnt %2 ==0? "odd" : "even";	

	

?>



	

		

		<tr>

			

		<tr data-key="<?php echo $row['idtransaksi'] ?>">

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >

				<?php echo $row["username"] ?>

			</td>

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >

				<?php echo $row["nama"] ?>

			</td>

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >

				<?php echo $row["norek"] ?>

			</td>

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >

				<?php echo $row["bank"] ?>

			</td>

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >

				<?php echo $row["nama_game"] ?>

			</td>

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >

				<?php 					

					echo $row["nama_transaksi"];					

				?>

			</td>

			

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>">			

				<?php echo number_format($row['jumlah'], 0) ?>				

			</td>

			

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>">

				<?php echo date("d-m-Y h:i:s", $row["tanggal"]) ?>

			</td>

			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:150px;" data-col-seq="5">

				<text class="label label-success"><strong>Diapprove</strong></text>

				<text class="label label-default"><strong><?php echo $row["nama_admin"] ?></strong></text>

							

			</td>

				

		</tr>



<?php

	}

	

?>



	</table>