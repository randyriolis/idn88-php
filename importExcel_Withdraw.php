<?php

//import.php
  
require_once __DIR__.'/vendor/autoload.php';
include_once "inc_login.php";
include_once "config.php";

date_default_timezone_set('Asia/Jakarta');

use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

if($_FILES["import_excel"]["name"] != '')
{
  $allowed_extension = array('xls', 'csv', 'xlsx');
  $file_array = explode(".", $_FILES["import_excel"]["name"]);
  $file_extension = end($file_array);

  if (strtolower($file_array[0])=='withdraw')
  {
	  if(in_array($file_extension, $allowed_extension))
	  {
	    $file_name = time() . '.' . $file_extension;
	    move_uploaded_file($_FILES['import_excel']['tmp_name'], $file_name);
	    $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
	    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);

	    $spreadsheet = $reader->load($file_name);

	    unlink($file_name);

	    $data = $spreadsheet->getActiveSheet()->toArray();

	    $no = 0;

	    foreach($data as $row)
	    {

	      $username = strtolower($row[0]);
	      $jumlah = str_replace(',','', $row[1]);
	      $jml_int = (int)$jumlah;

	      $timestamp = strtotime($row[2]);
	      $date = strtotime('+'.$no.' sec', $timestamp);
	      $date = empty($date) ? time() : $date;

	      if ($no > 0) {
	        if ($row[0] != NULL) {
	          $usermember = $row[0];
	          $querymember = mysqli_query($conn, "SELECT * FROM members where username='$usermember'");
	          $alert = ( ! mysqli_num_rows( $querymember ) ) ? 'gagal' : 'sukses';
	          //$tmp = mysqli_fetch_array( $querymember );

	          // search member
	          $tmp = mysqli_fetch_array($querymember);
	          $ip = $_SERVER['REMOTE_ADDR'];
	          $norek = $tmp['norek'];
	          $kdproduct = $tmp['kdproduct'];
	          $namarek = $tmp['namarek'];
	          $kdbank = $tmp['kdbank'];
	          $kdmember = $tmp['kdmember'];

	          if ($alert == 'sukses') {
	            $query = "INSERT INTO withdraws(username, jumlah, dari_ip1, isactive, isclear, ispending, tanggal, norek, namarek, kdbank, kdproduct, kdmember, clearby)
	              VALUES('$username', '$jml_int', '$ip', 1, 1, 0, '$date', '$norek', '$namarek', '$kdbank', '$kdproduct', '$kdmember', 'im')";

	            $result = mysqli_query($conn, $query);
	          }
	  
	          echo '['.$alert.']['.$no.'] => {'.$username.'},{'.$jumlah.'},{'.$date.'}<br>';
	        }
	      }

	      $no++;
	    }

	  
	    $message = '<br><div class="alert alert-success">Data Widthdraw Imported Successfully.</div>';

	  }
	  else
	  {
	    $message = '<div class="alert alert-danger">Only .xls .csv or .xlsx file allowed</div>';
	  }
  }
  else
  {
  	$message = '<div class="alert alert-danger">Filename must be WITHDRAW</div>';
  }
}
else
{
 $message = '<div class="alert alert-danger">Please Select File</div>';
}

echo $message;

?>