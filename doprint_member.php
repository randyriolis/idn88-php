<?php 
	include_once "inc_login.php";
	include_once "config.php";
	include_once "function.php";
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	$isheadadmin = $arrUser['isheadadmin'];

	// all request
	$bb = $_REQUEST['bb'];
	$page = $_REQUEST['page'];
	if($page < 1)	$page = 1;
	$username = $_REQUEST['account'];
	$namalink = $_REQUEST['namalink'];
	$kdproduct = $_REQUEST['kdproduct'];
	$idsumber = $_REQUEST['idsumber'];
	$findsumber = $_REQUEST['findsumber'];
	$bank = $_REQUEST['bank'];
	$nama = $_REQUEST['nama'];
	$norek = $_REQUEST['norek'];
	$email = $_REQUEST['email'];
	$dateassign = $_REQUEST['dateassign'];
	$ip = $_REQUEST['ip'];
	$status = $_REQUEST['status'];
	$url = $_REQUEST['url'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$delete = $_REQUEST['delete'];
	if($delete != "") 
	{
				
		$queryAcc = "DELETE FROM akuns WHERE kdmember = $delete ";
		mysqli_query($conn, $queryAcc);
		
		$queryAcc = "DELETE FROM members WHERE kdmember = $delete ";
		mysqli_query($conn, $queryAcc);
		
		$queryAcc = "DELETE FROM deposits WHERE kdmember = $delete ";
		mysqli_query($conn, $queryAcc);
		
		$queryAcc = "DELETE FROM withdraws WHERE kdmember = $delete ";
		mysqli_query($conn, $queryAcc);
	}
	
	$temp_function = $_REQUEST['temp_function'];
	
	if($temp_function == "adminHideContact")
	{
		$temp_id = $_REQUEST['temp_id'];
		$query = "UPDATE members SET isstatus=1, isactive =1  
				  WHERE kdmember = $temp_id ";
		$result = mysqli_query($conn, $query);	
	}
	
	else if($temp_function != ""){
		$val = $temp_function=="active"? 1 : 0;
			
		$berita = $temp_function=="active"? "Member di-active" : "Member di-nonactive";
		$temp_id = $_REQUEST['temp_id'];
		
		$nolstatus = '';
		if($val == 0)	$nolstatus = ", isstatus = 0 ";		
		
		// Set Account status to val				
		$query = "UPDATE members SET isactive = $val $nolstatus WHERE kdmember = $temp_id ";
		$result = mysqli_query($conn, $query);
		
		mysqli_query($conn, "INSERT INTO zlog_member (kdmember, kduser, keterangan, datecreate) VALUES ($delete, $iduser, '$berita', $current_timestamp)");	
	}
	
	$wherequery = " ";
	if($username != "")	$wherequery .= " AND (acc.username LIKE '%$username%' OR acc.pass LIKE '%$username%') ";
	if($kdproduct > 0)	$wherequery .= " AND mem.kdproduct = $kdproduct ";
	if($idsumber > 0)	$wherequery .= " AND mem.idsumber = $idsumber ";
	if($findsumber != "") $wherequery .= " AND (mem.sumber like '%$findsumber%') ";
	if($namalink != "")	$wherequery .= " AND ln.namalink like '%$namalink%' ";
	if($nama != "")	$wherequery .= " AND mem.nama like '%$nama%' ";
	if($norek != "")	$wherequery .= " AND (mem.norek like '%$norek%') ";
	if($email != "")	$wherequery .= " AND (mem.email like '%$email%' OR mem.tlp like '%$email%') ";
	if($dateassign != "")	$wherequery .= " AND DATE(CONVERT_TZ($modtime,".$curtimezone.")) like '%$dateassign%' ";
	if($ip != "")	$wherequery .= " AND mem.dari_ip1 like '%$ip%' ";
	if($status != "")	$wherequery .= " AND mem.isactive = $status ";
	if($url != "")	$wherequery .= " AND (mem.url like '%$url%') ";
	
	// Update, see only selected Link
	if($alpha_admb != 0)		$wherequery .= " AND mem.idlink=$alpha_admb  ";
	
	// Not Kepalacs
	else if($levelAdmin != 1) {
		// Can only see assigned link
		$access = $arrUser['link_assigned'];
		$access = explode(",",$access);
		$wherequery .= " AND ( ";
		
		$cnt = 0;
		foreach ($access as $value) {
			if(empty($value) == false) {
				if($cnt > 0) $wherequery .= ' OR ';
				$wherequery .= " mem.idlink=$value ";
				
				$cnt++;
			}
		}
		
		$wherequery .= " )";	
	
	}
	
	$orderquery = "ORDER BY mem.kdmember DESC ";
	if($sort != "")	
	{
		if($sort == "game_asc")		$orderquery = "ORDER BY gm.nama ASC ";
		else if($sort == "game_desc") $orderquery = "ORDER BY gm.nama DESC ";
		
		else if($sort == "namalink_asc")		$orderquery = "ORDER BY ln.namalink ASC ";
		else if($sort == "namalink_desc") $orderquery = "ORDER BY ln.namalink DESC ";
		
		else if($sort == "account_asc")		$orderquery = "ORDER BY acc.username ASC ";
		else if($sort == "account_desc") $orderquery = "ORDER BY acc.username DESC ";
		
		else if($sort == "bank_asc")		$orderquery = "ORDER BY mem.kdbank ASC ";
		else if($sort == "bank_desc") $orderquery = "ORDER BY mem.kdbank DESC ";
		
		else if($sort == "nama_asc")		$orderquery = "ORDER BY mem.nama ASC ";
		else if($sort == "nama_desc") $orderquery = "ORDER BY mem.nama DESC ";
		
		else if($sort == "norek_asc")		$orderquery = "ORDER BY mem.norek ASC ";
		else if($sort == "norek_desc") $orderquery = "ORDER BY mem.norek DESC ";
		
		else if($sort == "email_asc")		$orderquery = "ORDER BY mem.email ASC ";
		else if($sort == "email_desc") $orderquery = "ORDER BY mem.email DESC ";
		
		else if($sort == "dateassign_asc")		$orderquery = "ORDER BY mem.modtime ASC ";
		else if($sort == "dateassign_desc") $orderquery = "ORDER BY mem.modtime DESC ";
		
		else if($sort == "ip_asc")		$orderquery = "ORDER BY mem.dari_ip1 ASC ";
		else if($sort == "ip_desc") $orderquery = "ORDER BY mem.dari_ip1 DESC ";
		
		else if($sort == "status_asc")		$orderquery = "ORDER BY mem.isactive ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY mem.isactive DESC ";
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT mem.kdmember, mem.isstatus, acc.isassigned, mem.isactive, mem.tlp, mem.email, mem.norek, mem.nama AS namamember, 
					mem.namarek, bk.inisialbank AS namabank,  acc.kdakun, mem.username, mem.pass, gm.nama AS nama_game, 
					mem.kdproduct, mem.dari_ip1, mem.tanggal_daftar, mem.browser_name, mem.kenal, mem.ket, mem.kdbank, mem.idsumber, mem.sumber, mem.url,
					gm.kodeagen AS hex_account, acc.modby AS nama_admin, ln.namalink, ln.idlink, ln.ispoker  ".
					",CONVERT_TZ(from_unixtime(mem.tanggal_daftar,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00') as new_datecreate ".
					",CONVERT_TZ(from_unixtime(acc.modtime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00') as new_dateassign ".
					
				   "FROM members mem ".
				   "LEFT JOIN akuns acc ON acc.kdmember = mem.kdmember ".
				   "LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ".
				   "LEFT JOIN banks bk ON mem.kdbank = bk.kdbank ".
				   "LEFT JOIN link ln ON mem.idlink = ln.idlink ".
				   "WHERE 1=1 ".
				   $wherequery.
				   $orderquery.
				   $limitquery;				   
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	
		
		$new_datecreate = $row['new_datecreate'];
		
		$kdmember = $row['kdmember'];
		$nama = $row['namamember'];
		$namarek = $row['namarek'];
		$username = $row['username'];
		$pass = $row['pass'];
		$kdbank = $row['kdbank'];
		$email = $row['email'];
		$tlp = $row['tlp'];
		$kdproduct = $row['kdproduct'];
		$dari_ip1 = $row['dari_ip1'];
		$tanggal_daftar = $row['tanggal_daftar'];
		$browser_name = $row['browser_name'];
		$kenal = $row['kenal'];
		$isactive = $row['isactive'];		
		$norek = $row['norek'];
		$ket = $row['ket'];
		$idlink = $row['idlink'];
		$idsumber = $row['idsumber'];
		$sumber = $row['sumber'];
		
		$json = "{ -=data-=:-=member-=, -=kdmember-=:-=$kdmember-=, -=nama-=:-=$nama-=, -=namarek-=:-=$namarek-=, -=username-=:-=$username-=, -=pass-=:-=$pass-=, ".
				   "-=kdbank-=:-=$kdbank-=, -=email-=:-=$email-=, -=tlp-=:-=$tlp-=, ".
				   "-=kdproduct-=:-=$kdproduct-=, -=dari_ip1-=:-=$dari_ip1-=, -=tanggal_daftar-=:-=$tanggal_daftar-=, -=browser_name-=:-=$browser_name-=, ".
				   "-=kenal-=:-=$kenal-=, -=isactive-=:-=$isactive-=, -=norek-=:-=$norek-=, ".
				   "-=ket-=:-=$ket-=, -=idlink-=:-=$idlink-=, -=idsumber-=:-=$idsumber-=, -=sumber-=:-=$sumber-= }";		
		
		
		if($row["ispoker"] == 1)		
			$ispoker = true;
		else 						$ispoker = false;
		
?>
	<tr data-key="<?php echo $row['kdakun'] ?>" data-sample="anjay">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="max-width:100px;" data-col-seq="1">
			<?php 
				if($row['kdakun'] == null && ($row['username'] == "" || empty($row['username']) == true ) ){
					echo '<text class="text-danger"><em>belum assign</em></text>';
				}
				else {				
			?>
				<?php 
					echo $row['username']; 
					if($row["pass"] != "" && empty($row["pass"]) == false)	echo '<br/>'.$row["pass"];
				?>
				<br/>
				<a class="btn btn-default btn-xs" href="#" onClick="copyToClipboard('<?php echo $row['username']; ?>'); return false;">
					<span class="glyphicon glyphicon-plus"></span>
				</a>
				<a class="btn btn-default btn-xs" href="viewmember.php?id=<?php echo $row['kdmember'] ?>" target="_blank" data-pjax="0">
					<span class="glyphicon glyphicon-link"></span>					
				</a>
			<?php } ?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="min-width:100px;" data-col-seq="2">
			<?php
				echo $row['namalink'];
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="min-width:160px;" data-col-seq="3">			
			<?php 
				if($ispoker == false) {
					$memnm = $row['namamember'];
					echo htmlspecialchars($row['namamember'], ENT_QUOTES, 'utf-8'); 
				}
				else {
					$memnm = $row['namarek'];
					echo htmlspecialchars($row['namarek'], ENT_QUOTES, 'utf-8').'<br/>'; 
					echo '<i style="color:#777">'.htmlspecialchars($row['namamember'], ENT_QUOTES, 'utf-8').'</span>'; 
				}				
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="max-width:150px;" data-col-seq="4">			
			<?php echo htmlspecialchars($row['norek'], ENT_QUOTES, 'utf-8'); ?><br/>
			<?php echo $row['namabank'] ?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="5" style="max-width:200px; overflow:hidden">
			<?php
				$tlp = htmlspecialchars($row['tlp'], ENT_QUOTES, 'utf-8');
				$email = htmlspecialchars($row['email'], ENT_QUOTES, 'utf-8');
				
				$hidecontact = false;
				
				if($isheadadmin != 1 && empty($bb))					$hidecontact = true;
				else if($ispoker == true && $row['isstatus'] == 1 && $isheadadmin != 1)	$hidecontact = true;
				
				if($hidecontact == true) 
				{
					$tlp = substr($tlp, 0, -3) . 'xxx';
									
					// email replace
					$mail_parts = explode("@", $email);
					$length = strlen($mail_parts[0]);
					$show = $length - 3;
					$hide = $length - $show;
					$replace = str_repeat("x", $hide);
					$email = substr_replace ( $mail_parts[0] , $replace , $show, $hide ) . "@" . substr_replace($mail_parts[1], "xxx", 0, 3);						
				
				}
				
				echo htmlentities($tlp).'<br/>'.htmlentities($email);
			?>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="6" style="min-width:100px;">
			<?php echo explode(",", $row['hex_account'])[0] ?> <br/>
			<?php echo $row['nama_game'] ?>
		</td>
		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="6">			
			<?php echo getSumber($row["idsumber"]); ?>
			<?php echo $row['sumber'] !== '' ? '<br/>' . $row['sumber'] : ''; ?>
		</td>
		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="6">			
			<?php echo $row["url"]; ?>
		</td>
		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="7">
			<?php
				if($row['kdakun'] == null && ($row['username'] == "" || empty($row['username']) == true ) ){
					echo '<text class="text-danger"><em>belum assign</em></text>';
				}
				else {
					?>
					<a class="btn btn-default btn-xs" href="domember_printlog.php?id=<?php echo $row['kdmember'] ?>" data-toggle="modal" data-target="#myModallog" data-title="Detail Data">					
						<?php echo $row['new_dateassign']; ?>
						</a>
					<br /> 
					<?php 
						$styleAdmin = "";
						$to_time = strtotime($row['new_dateassign']);
						$from_time = strtotime($new_datecreate);
						$diff = round(abs($to_time - $from_time) / 60, 2);

						if($diff < 3)  $styleAdmin = 'class="label label-success" ';
						else if($diff > 5)  $styleAdmin = 'class="label label-danger" ';
						else $styleAdmin = 'class="label label-warning" ';
						
						echo "<span $styleAdmin style='font-size:11px;'>".$row['nama_admin'].'</span>'; 
					?>
					
					<?php
				}
			?>
			
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:110px;" data-col-seq="8">
			<?php echo $row['ip'] ?><br/>
			<a class="btn btn-default btn-xs" href="domember_printlog.php?id=<?php echo $row['kdmember'] ?>" data-toggle="modal" data-target="#myModallog" data-title="Detail Data">										
				<?php echo $new_datecreate; ?>
			</a>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="9">
			<?php
				if($row['isactive'] == 1)	echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>			
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="10">
			<?php
				if( ($row['isassigned'] == 1 || $ispoker == true) && $row['isactive'] == 1){
					?>
						<a class="btn btn-success btn-xs" href="domember_smsaccount.php?id=<?php echo $row['kdmember'] ?>" data-toggle="modal" data-target="#myModal" data-title="Detail Data">
						<i class="fa fa-inbox fa-lg" aria-hidden="true"></i>
						<br/>SMS
					<?php
				}
				else echo '&nbsp;';
			?>
			
			</a>
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="11">
			<?php 
				if( ($row['isassigned'] == 1 || $ispoker == true) && $row['isactive'] == 1){
					?>					
						<a class="btn btn-warning  btn-xs" href="domember_smsbank.php?id=<?php echo $row['kdmember'] ?>" data-toggle="modal" data-target="#myModalsms" data-title="Detail Data">
						<i class="fa fa-inbox fa-lg" aria-hidden="true"></i><br/>SMS Bank
						</a> 				
					<?php
				}
				else echo '&nbsp;';
			?>			
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="12">
			<?php 
				// Poker cannot be assigned
				if($ispoker == false) 
				{
					if($row['isassigned'] == 1) {
			?>
						<a class="btn btn-info btn-xs" href="domember_assign.php?menu=reassign&id=<?php echo $row['kdmember'] ?>" data-toggle="modal" data-target="#myModalreassign" data-title="Detail Data">						
						<i class="fa fa-user-o fa-lg" aria-hidden="true"></i><br/>Re-Assign</a> 
			<?php 
					} else {
			?>
						<a class="btn btn-info btn-xs" href="domember_assign.php?menu=assign&id=<?php echo $row['kdmember'] ?>" data-toggle="modal" data-target="#myModalassign" data-title="Detail Data">
						<i class="fa fa-user-o fa-lg" aria-hidden="true"></i><br/>Assign</a>
			<?php 	} 
				}
				
				else {
					if($row['isstatus'] == 0) {
			?>
					<a class="btn btn-info btn-xs" href="#" onClick="HideContact(<?php echo $row['kdmember'] ?>); return false;"><i class="fa fa-link  fa-lg" aria-hidden="true"></i><br/>OK</a>
				
			<?php 	}
				}
			?>
			
			
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" >	
			<a class="btn btn-success btn-xs" style="background-color:#008000" href="#" onClick="copyMember('<?php echo $json; ?>', '<?php echo $memnm; ?>', '<?php echo $row['kdmember']; ?>'); return false;"><i class="fa fa-files-o fa-lg" aria-hidden="true"></i><br/>COPY</a>
		</td>
		<?php if($levelAdmin == 1) { ?>
			<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="13">			
				<a class="btn btn-default btn-xs" href="domember_edit.php?id=<?php echo $row['kdmember'] ?>" data-toggle="modal" data-target="#myModalupdate" data-title="Detail Data">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i>
				<br/>Edit
				</a>
		</td>
			
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="14">			
			<a class="btn btn-danger btn-xs" style="background-color:#990000" href="#" onClick="deleteMember(<?php echo $row['kdmember'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 						
		</td>
		<?php } ?>
		
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="14">			
			<?php if($row['isactive'] == 1) { ?>
				<a class="btn btn-danger btn-xs" href="#" onClick="ToggleMember(<?php echo $row['kdmember'] ?>, true); return false;">
				<i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i><br/>Inactive</a>
			<?php } else { ?>
				<a class="btn btn-success btn-xs" href="#" onClick="ToggleMember(<?php echo $row['kdmember'] ?>, false); return false;">
				<i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i><br/>Active</a>
			<?php } ?>
		</td>					
		
	</tr>

<?php } ?>

<?php
	$query2 = "SELECT count(mem.kdmember) as num_rows FROM ".
				   "members mem ".
				   "LEFT JOIN akuns acc ON acc.kdmember = mem.kdmember ".
				   "LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ".
				   "LEFT JOIN _users us ON mem.modby = us.kduser ".
				   "LEFT JOIN banks bk ON mem.kdbank = bk.kdbank ".
				   "LEFT JOIN link ln ON mem.idlink = ln.idlink ".
				   "WHERE 1=1 ".
				   $wherequery.
				   $orderquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query2));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-= paging -=>
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function copyToClipboard(txt) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(txt).select();
		document.execCommand("copy");
		$temp.remove();
		
		$("#copy_clipboard").css("display", "block");
		$("#copy_clipboard").html("'<b>"+txt + "</b>' is copied");
		
		$("#copy_clipboard").fadeTo(1000, 500).slideUp(500, function(){
			$("#copy_clipboard").slideUp(500);
		});  
	}
	
	function copyMember(txt, nama, idmember) 
	{
		txt = txt.split("-=").join('"');
		
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val(txt).select();
		document.execCommand("copy");
		$temp.remove();
		
		$("#copy_clipboard").css("display", "block");
		$("#copy_clipboard").html("'<b>"+nama + "</b>' is copied");
		
		$("#copy_clipboard").fadeTo(1000, 500).slideUp(500, function(){
			$("#copy_clipboard").slideUp(500);
		});  
		
		$("#temp_function").val("inactive");
		$("#temp_id").val(idmember);
		refreshContent();
		
	}
	
	function deleteMember(kdakun)
	{
		var str = "Apakah Kamu yakin delete Member ini ?? Kehapus bener nih membernya!!";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(kdakun);
			refreshContent();
		}
	}
	
	function ToggleMember(kdakun, isActive)
	{
		if(isActive)			var str = "Apakah Kamu yakin Inactive Member ini ??.";
		else 					var str = "Apakah Kamu yakin Active Member ini ??.";
		
		var aa = confirm(str);
		if(aa)
		{
			$("#temp_id").val(kdakun);
			if(isActive)		$("#temp_function").val("inactive");
			else 				$("#temp_function").val("active");
			
			refreshContent();
		}
	}
	
	function HideContact(idmember){
		$("#temp_function").val("adminHideContact");
		$("#temp_id").val(idmember);
		refreshContent();	
	}
	
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}


function ajaxPasteMember2(data) {
    var rs = JSON.parse(data);
    var getNama = rs['nama'];
    var getNarek = (rs["namarek"]==undefined) ? rs["nama"] : rs["namarek"];
    var getEmail = rs["email"];
    var getTelp = (rs["tlp"]==undefined) ? rs["no_hp"] : rs["tlp"];
    var getKdp = (rs["kdproduct"]==undefined) ? '0' : rs["kdproduct"];
    var getNorek = (rs["norek"]==undefined) ? rs["no_rekening"] : rs["norek"];
    var getBron = (rs["browser_name"]==undefined) ? rs["browser"] : rs["browser_name"];
    var getKet = (rs["ket"]==undefined) ? rs["keterangan"] : rs["ket"];
    var getIdl = rs["idlink"];
    var getKdb = (rs["kdbank"]==undefined) ? rs["bank"] : rs["kdbank"];
    var getIsac = (rs["isactive"]==undefined) ? rs["status"] : rs["isactive"];
    var getWkt = (rs["tanggal_daftar"]==undefined) ? "1629817887" : rs["tanggal_daftar"];
    var getIp = (rs["dari_ip1"]==undefined) ? rs["ip"] : rs["dari_ip1"];
    var getKanal = (rs["kenal"]==undefined) ? rs["pendaftar"] : rs["kenal"];
    var getIds = rs["idsumber"];
    var getSum = rs["sumber"];
    var getKdg = (rs["kdgame"]==undefined) ? "" : rs["kdgame"];
    //var getBank = (rs["bank"]==undefined) ? "" : rs["bank"];
    var getUrl = (rs["url"]==undefined) ? "" : rs['url'];

    $.ajax({
        type: "POST",
        url: "domember_ajax.php",
        cache: false,
        data: {
            nama: getNama,
            narek: getNarek,
            email: getEmail,
            tlp: getTelp,
            kdp: getKdp,
            norek: getNorek,
            bron: getBron,
            ket: getKet,
            idl: getIdl,
            kdb: getKdb,
            isac: getIsac,
            wkt: getWkt,
            ip: getIp,
            kanal: getKanal,
            ids: getIds,
            sum: getSum,
            kdg: getKdg,
            //bank: getBank,
            url: getUrl,
        },
        //data: data,
        //dataType: 'json',
        success: function (data) {
            console.log(data);
            refreshContent();
        },
    });
    
    console.log('nama: '+getNama+'\nnamarek: '+getNarek+'\nemail: '+getEmail+'\ntelp: '+getTelp+'\nkdp: '+getKdp+'\nnorek: '+getNorek+'\nbrowser: '+getBron+'\nket: '+getKet+'\nidlink: '+getIdl+'\nkdbank: '+getKdb+'\nisactive: '+getIsac+'\ntgl_daftar: '+getWkt+'\nip: '+getIp+'\nkanal: '+getKanal+'\nidsumber: '+getIds+'\nsumber: '+getSum+'\nkdgame: '+getKdg+'\nurl: '+getUrl);
}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		
	?>
	
	<?php
		// Saved NEW data into OLD
		echo ' 
			if(localStorage.getItem("total_member_old") == null || '.$rowTotal.' > localStorage.getItem("total_member_old")){
				
				localStorage.setItem("total_member_old", '.$rowTotal.');
			}
		';		
	?>
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>