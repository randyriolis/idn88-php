<?php
	include "config.php";
	
	$kdwithdraw = $_REQUEST['id'];
	
?>
<div class="member-log">

<table class="table table-condensed">
    <thead class="thead-inverse">
      <tr >
        <th>User</th>
        <th>Date Create</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
		<?php
			$query = "SELECT us.username, zlog.datecreate, zlog.keterangan, zlog.idlogwd ".
						  "FROM zlog_withdraw zlog ".
						  "LEFT JOIN _users us ON zlog.kduser=us.kduser ".
						  " WHERE kdwithdraw = $kdwithdraw ORDER BY idlogwd DESC";						  
			$result = mysqli_query($conn, $query);
			
			$cnt = 0;
			while($row=mysqli_fetch_array($result))
			{
				$cnt++
		?>
			<tr>	
				<td><?php echo $row['username'] ?></td>
				<td><?php echo $row['datecreate'] ?></td>
				<td><?php echo $row['keterangan'] ?></td>
			</tr>
		<?php } ?>
		
		<?php
			if($cnt <= 0){
				echo '<tr><td>Tidak ditemukan Log untuk Withdraw ini </td></tr>';
			}
		?>
    </tbody>
  </table>
</div>

<?php mysqli_close($conn); ?>