<?php 

	include_once "inc_login.php";

	include "config.php";

	include "function.php";

		

	if (empty($_POST)){

		echo 'err';

		exit();

	}

	

	

	$wherequery = "  ";
	
		
	$linkquery = "";
	if($alpha_admb != 0)		$linkquery .= " AND mem.idlink=$alpha_admb ";
	
	$page = $_REQUEST['page'];

	$form_date_start = empty($_REQUEST['form_date_start']) ? "" : date("Y-m-d", strtotime($_REQUEST['form_date_start']));;

	$form_date_end = empty($_REQUEST['form_date_end']) ? "" : date("Y-m-d", strtotime($_REQUEST['form_date_end']));;

	

	

	$sort = $_REQUEST['sort'];

	$maxrow = $_REQUEST['maxrow'];	

	

	$orderquery = "ORDER BY tahun DESC, bulan DESC, hari ASC ";

	if($sort != "")	

	{

		

		if($sort == "tanggal_asc")		$orderquery = "ORDER BY hari ASC ";

		else if($sort == "tanggal_desc") $orderquery = "ORDER BY hari DESC ";

		

		else if($sort == "totalDeposit_asc")		$orderquery = "ORDER BY totalDeposit ASC ";
		else if($sort == "totalDeposit_desc") $orderquery = "ORDER BY totalDeposit DESC ";		

		else if($sort == "totalWithdraw_asc")		$orderquery = "ORDER BY totalWithdraw ASC ";
		else if($sort == "totalWithdraw_desc") $orderquery = "ORDER BY totalWithdraw DESC ";
		
		else if($sort == "totalTransaksiDeposit_asc")		$orderquery = "ORDER BY banyakDeposit ASC ";
		else if($sort == "totalTransaksiDeposit_desc") $orderquery = "ORDER BY banyakDeposit DESC ";
		
		else if($sort == "totalTransaksiWithdraw_asc")		$orderquery = "ORDER BY banyakWithdraw ASC ";
		else if($sort == "totalTransaksiWithdraw_desc") $orderquery = "ORDER BY banyakWithdraw DESC ";

		else if($sort == "totalTransaksiJumlah_asc")		$orderquery = "ORDER BY totalJumlah ASC ";
		else if($sort == "totalTransaksiJumlah_desc") $orderquery = "ORDER BY totalJumlah DESC ";
		
		else if($sort == "totalJumlah_asc")		$orderquery = "ORDER BY totalJumlah ASC ";
		else if($sort == "totalJumlah_desc") $orderquery = "ORDER BY totalJumlah DESC ";

		else if($sort == "totalBonus_asc")		$orderquery = "ORDER BY totalBonus ASC ";
		else if($sort == "totalBonus_desc") $orderquery = "ORDER BY totalBonus DESC ";

	}

	

	$start = ($page-1) * 20;

	

	$limitquery = "";

	if($maxrow >= 9999)	$limitquery = "";

	

	

	$form_filter_date = $_REQUEST['form_filter_date'];

	

	// DATE-selected

	if($form_date_start != "" && $form_date_end != "")	

		$datequery .= " AND ( CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00') between date('$form_date_start') AND date('$form_date_end') ) ";

	

	// filter by month

	else {

		$myArray = explode('_', $form_filter_date);

		$bulan = $myArray[0];

		$tahun = $myArray[1];

		

		$datequery .= " AND MONTH(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00')) = '$bulan' AND YEAR(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00'))  = '$tahun' ";

		

	}

	

	

	$queryDeposit = "SELECT SUM(dp.jumlah) as totalDeposit, 
					COUNT(dp.jumlah) as banyakDeposit ".
					
				   ", CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00') as new_datecreate ".
				   ", DAY(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00')) as hari ".
				   ", MONTH(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00')) as bulan ".
				   ", YEAR(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00')) as tahun ".

				   "FROM deposits dp ".				  
				   "INNER JOIN members mem ON mem.kdmember = dp.kdmember ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
				   "WHERE 1=1 AND ln.status =1 AND dp.isclear = 1 AND dp.isactive = 1 $linkquery ".
				   $datequery.
				   " GROUP BY YEAR(new_datecreate), MONTH(new_datecreate), DAY(new_datecreate), dp.cretime ".
				   " ORDER BY new_datecreate ASC ";		  

	$queryWithdraw = "SELECT SUM(dp.jumlah) as totalWithdraw, 
					COUNT(dp.jumlah) as banyakWithdraw ".
					
				   ", CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00') as new_datecreate ".
				   ", DAY(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00')) as hari ".
				   ", MONTH(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00')) as bulan ".
				   ", YEAR(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00')) as tahun ".

				   "FROM withdraws dp ".				   
				   "INNER JOIN members mem ON mem.kdmember = dp.kdmember ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
				   "WHERE 1=1 AND ln.status =1 AND dp.isclear = 1 AND dp.isactive = 1 $linkquery ".
				   $datequery.
				   " GROUP BY YEAR(new_datecreate), MONTH(new_datecreate), DAY(new_datecreate), dp.cretime ".
				   " ORDER BY new_datecreate ASC ";


	$query = "SELECT tahun, bulan, hari, SUM(totalDeposit) AS totalDeposit, SUM(banyakDeposit) AS banyakDeposit, SUM(totalWithdraw) AS totalWithdraw, SUM(banyakWithdraw) AS banyakWithdraw,  
			  (COALESCE( SUM(totalDeposit), 0) - COALESCE( SUM(totalWithdraw), 0)) AS totalJumlah,
			  (COALESCE( SUM(banyakDeposit), 0) + COALESCE( SUM(banyakWithdraw), 0)) AS banyakJumlah
					FROM (
						SELECT tahun, bulan, hari, totalDeposit, banyakDeposit, '0' AS totalWithdraw, '0' AS banyakWithdraw FROM ($queryDeposit) as tempDeposit 
						UNION ALL
						SELECT tahun, bulan, hari, '0' AS totalDeposit, '0' AS banyakDeposit, totalWithdraw, banyakWithdraw FROM ($queryWithdraw) as tempWithdraw 					
					) AS temporary 
					".					
					
					"WHERE 1=1 AND (totalDeposit > 0 OR totalWithdraw > 0) ".
					$wherequery.	
					'GROUP BY tahun, bulan, hari '.
					$orderquery.
					$limitquery;	
					

	$result = mysqli_query($conn, $query);

	$cnt = $start;

	$jumlah_semua_deposit = 0;

	$jumlah_semua_withdraw = 0;

	

	while($row = mysqli_fetch_assoc($result)) {

	

		$cnt ++;	

		

		$new_datecreate = $row['new_datecreate'];

		$bgcolor = $cnt %2 ==0? "odd" : "even";

		$accordionID = "collapse_".$cnt;

		

		$jumlah_semua_deposit += $row['totalDeposit'];

		$jumlah_semua_withdraw += $row['totalWithdraw'];

		

		$banyak_semua_deposit += $row['banyakDeposit'];

		$banyak_semua_withdraw += $row['banyakWithdraw'];

		

		$tanggal_full = $row["tahun"]."-".$row["bulan"]."-".$row["hari"];

		$tgl = date("d-m-Y", strtotime($tanggal_full));		

		$tgl_where = date("Y-m-d", strtotime($tanggal_full));

		

?>

	

	<tr data-key="<?php echo $row['iddeposit'] ?>">

		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:36px;" data-col-seq="0">

			<?php printf("%02d", $row['hari']);  ?>

		</td>

		<td class="kartik-sheet-style kv-align-center kv-align-right <?php echo $bgcolor ?>" style="width:140px;" data-col-seq="1">

			<?php echo number_format($row['totalDeposit'] , 0); ?>		

		</td>

		<td class="kartik-sheet-style kv-align-center kv-align-right <?php echo $bgcolor ?>" style="width:180px;" data-col-seq="2">

			<?php echo number_format($row['totalWithdraw'] , 0); ?>

		</td>

		<td class="kartik-sheet-style kv-align-center kv-align-right <?php echo $bgcolor ?>" data-col-seq="3" >

			<?php echo number_format($row['totalJumlah'] , 0); ?>

		</td>

		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" data-col-seq="4">			

			<?php echo $row['banyakDeposit'] ?>

		</td>

		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" style="width:220px;" data-col-seq="5">

			<?php echo $row['banyakWithdraw'] ?>

		</td>

		<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>"  data-col-seq="6">

			<?php echo $row['banyakJumlah'] ?>

		</td>

		

		<td class="kartik-sheet-style kv-align-right kv-align-middle <?php echo $bgcolor ?>">

			<a href="exportExcel_laporanHarian.php?tgl=<?php echo $tgl ?> " class="btn btn-dark btn-xs">Export</a>

		</td>

		<td class="kartik-sheet-style kv-align-right kv-align-middle <?php echo $bgcolor ?>">

			<a href="#<?php echo $accordionID ?>" type="button" class="btn btn-xs btn-info accordion-toggle accordion-lihat" data-toggle="collapse" data-parent="#accordion" >

				Lihat

			</a>	

		</td>

		

	</tr>

		

	<tr>

		<td colspan="99"  style="padding-top:0; padding-bottom:0; padding-left:36px">

			<div id="<?php echo $accordionID ?>" class="panel-collapse collapse" data-tanggal="<?php echo $tgl ?>">

				

			</div>

		</td>

	</tr>





<?php } ?>



	<!-- paging -->

	<tr>

		<td colspan="99">

			<br/>

			<div class="laporan_1">Total Deposit: </div>

			<div class="laporan_2"><?php echo number_format($jumlah_semua_deposit, 0) ?> </div>

			<br/>

			

			<div class="laporan_1">Total Withdraw: </div>

			<div class="laporan_2"><?php echo number_format($jumlah_semua_withdraw, 0) ?> </div>

			<br/>

			

			<div class="laporan_1">Total Jumlah: </div>

			<div class="laporan_2">

				<?php 

					$jumlah_semua_difference = $jumlah_semua_deposit - $jumlah_semua_withdraw;

					echo number_format($jumlah_semua_difference, 0);									

				?> 

			</div>

			

			<br/>	<br/>	

			<div class="laporan_1">Total Transaksi Deposit: </div>

			<div class="laporan_2"><?php echo number_format($banyak_semua_deposit, 0) ?> </div>

			<br/>

			

			<div class="laporan_1">Total Transaksi Withdraw: </div>

			<div class="laporan_2"><?php echo number_format($banyak_semua_withdraw, 0) ?> </div>

			<br/>

			

			<div class="laporan_1">Total Transaksi Jumlah: </div>

			<div class="laporan_2">

				<?php 

					$banyak_semua_difference = $banyak_semua_deposit + $banyak_semua_withdraw;

					echo number_format($banyak_semua_difference, 0);									

				?> 

			</div>

			

			<br/>		

			<br/>

			<div class="laporan_1">Total Bonus Pemain: </div>

			<div class="laporan_2">

				0

			</div>

		</td>

		

	</tr>

	



<script>

	

	function clickPage(whatPage){

		$("#page").val(whatPage);

		refreshContent();

	}

	

	$(document).ready(function(){

		$(".accordion-lihat").click(function(){

			

			// Active Div that used

			var tmpdiv = $($(this).attr("href"));

			console.log(tmpdiv.attr("id") );

			

			if($(tmpdiv).is(':hidden') ) {

				$(this).removeClass("btn-info");

				$(this).addClass("btn-danger");

				$(this).html("tutup");

			}

			else {

				$(this).removeClass("btn-danger");

				$(this).addClass("btn-info");

				$(this).html("lihat");

			}

			

			if( $(tmpdiv).html().isEmpty() ) 

			{

				// Show loading

				$("#loadingGif").css("display","");

				

				var tanggal = $(tmpdiv).data("tanggal");

				var url = "doprint_laporanHarian_detail.php?tanggal=" +tanggal

				

				$.get( url, function(data) {

					console.log("data = "+data);

					$(tmpdiv).html(data);

					

					// Hide Loading

					$("#loadingGif").css("display","none");

				})

			}

			

		});

	});

	

	

	String.prototype.isEmpty = function() {

		return (this.length === 0 || !this.trim());

	};

	

</script>



<?php 

	// SORT

	mysqli_close($conn);

?>