<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
	<tr>
		<th class="kartik-sheet-style kv-align-center kv-align-middle odd" >Nomor FU</th>
		<th class="kartik-sheet-style kv-align-center kv-align-middle odd"  >Tanggal FU</th>
		<th class="kartik-sheet-style kv-align-center kv-align-middle odd" >VIA</th>
		<th class="kartik-sheet-style kv-align-center kv-align-middle odd">Respon</th>
		<th class="kartik-sheet-style kv-align-center kv-align-middle odd">Edit</th>		
	</tr>

<?php
	include_once "config.php";
	
	$kdmember = $_REQUEST['kdmember'];
		
	$sql = "SELECT * FROM followup 
			WHERE kdmember = $kdmember 
			ORDER BY followup_number ASC ";
			
	$result = mysqli_query($conn, $sql);
	
	while($row = mysqli_fetch_array($result)) 
	{
		$cnt ++;
		$bgcolor = $cnt %2 ==0? "odd" : "even";	
	
		
?>

	
		
		<tr>
			
		<tr data-key="<?php echo $row['idfollowup'] ?>">
			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >
				<?php echo $row["followup_number"] ?>
			</td>
			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >
				<?php 
					$tgl = date("d-m-Y", strtotime($row["followup_date"]));
					echo $tgl;
				?>
			</td>
			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >
				<?php echo $row["followup_via"] ?>
			</td>
			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>">			
				<?php echo $row["followup_respon"] ?>
			</td>
			<td class="kartik-sheet-style kv-align-center kv-align-middle <?php echo $bgcolor ?>" >
				<a class="btn btn-default btn-xs" href="dofollowup_editnote.php?id=<?php echo $row['idfollowup'] ?>" data-toggle="modal" data-target="#myModalUpdate" data-title="Detail Data">
				<i class="fa fa-pencil fa-lg" aria-hidden="true" style="height: 4px"></i>
				<div style="font-size: 7px">Edit</div>
				</a>						
			</td>
			
		</tr>

<?php
	}
	
?>

	</table>