<?php 
	session_start();
	include "config.php";
	include "inc_login.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM contacts WHERE kdcontact = $delete ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$nama_contact_us = $_REQUEST['nama_contact_us'];
	$nomor_contact_us = $_REQUEST['nomor_contact_us'];
	$namalink = $_REQUEST['namalink'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = "";
	
	if($nama_contact_us != "")	$wherequery .= " AND nama_contact_us LIKE '%$nama_contact_us%' ";
	if($nomor_contact_us != "")	$wherequery .= " AND nomor_contact_us like '%$nomor_contact_us%' ";
	if($namalink != "")	$wherequery .= " AND ln.namalink like '%$namalink%' ";	
	
	
	$orderquery = "ORDER BY kdcontact ASC ";
	if($sort != "")	
	{
		if($sort == "nomor_contact_us_asc")		$orderquery = "ORDER BY nomor_contact_us ASC ";
		else if($sort == "nomor_contact_us_desc") $orderquery = "ORDER BY nomor_contact_us DESC ";
		
		else if($sort == "nama_contact_us_asc")		$orderquery = "ORDER BY nama_contact_us ASC ";
		else if($sort == "nama_contact_us_desc") $orderquery = "ORDER BY nama_contact_us DESC ";
				
		else if($sort == "namalink_asc")		$orderquery = "ORDER BY ln.namalink ASC ";
		else if($sort == "namalink_desc") $orderquery = "ORDER BY ln.namalink DESC ";
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT cont.info AS nomor_contact_us, cats.nama AS nama_contact_us, kdcontact, ln.namalink
					FROM contacts cont 
					INNER JOIN contact_cats cats ON cont.kdcontact_cat=cats.kdcontact_cat 
					INNER JOIN link ln ON cont.idlink=ln.idlink "
					.$wherequery.$orderquery.$limitquery;
	echo $query;
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	

?>
	<tr data-key="<?php echo $row['kdcontact'] ?>">
		<td class="kartik-sheet-style kv-align-center " style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kv-align-center " data-col-seq="1"><?php echo $row['nama_contact_us'] ?></td>
		<td class="kv-align-center" data-col-seq="3" style="width:24%;"><?php echo $row['nomor_contact_us'] ?></td>		
		<td class="kv-align-center" data-col-seq="3" style="width:24%;"><?php echo $row['namalink'] ?></td>		
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle"data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="docontactus_menu.php?menu=edit&id=<?php echo $row['kdcontact'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" data-col-seq="8">
			<a class="btn btn-danger btn-xs" href="#" onClick="deleteContact(<?php echo $row['kdcontact'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(kdcontact) as num_rows FROM contacts cont 
					LEFT JOIN link ln ON cont.idlink=ln.idlink 
					".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deleteContact(idaccount)
	{
		var str = "Apakah Kamu yakin delete Contact ini ?? Ilang loh Contactnya??";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>