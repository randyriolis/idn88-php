<?php 
	include_once "inc_login.php";
	include "config.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "UPDATE link SET status = 0 WHERE idlink = $delete ";
		$result = mysqli_query($conn, $query);
	}
	
	$drop = $_REQUEST['drop'];
	if($drop != ""){		
		// delete account
		$query = "DELETE FROM link WHERE idlink = $drop ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$namalink = $_REQUEST['namalink'];
	$url = $_REQUEST['url'];	
	$status = $_REQUEST['status'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = "";
	
	if($namalink != "")	$wherequery .= " AND namalink LIKE '%$namalink%' ";
	if($url != "")	$wherequery .= " AND url LIKE '%$url%' ";
	if($hex_password != "")	$wherequery .= " AND hex_password like '%$hex_password%' ";	
	if($status != "")	$wherequery .= " AND status = $status ";
	
	
	$orderquery = "ORDER BY idlink ASC ";
	if($sort != "")	
	{
		if($sort == "namalink_asc")		$orderquery = "ORDER BY namalink ASC ";
		else if($sort == "namalink_desc") $orderquery = "ORDER BY namalink DESC ";
		
		else if($sort == "url_asc")		$orderquery = "ORDER BY url ASC ";
		else if($sort == "url_desc") $orderquery = "ORDER BY url DESC ";
		
		else if($sort == "status_asc")		$orderquery = "ORDER BY status ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY status DESC ";		
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT * FROM link WHERE 1=1 ".$wherequery.$orderquery.$limitquery;	
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	
		$kodelink = (!empty($row['kodelink'])) ? ' ('.$row['kodelink'].')' : '';

?>
	<tr data-key="<?php echo $row['id'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $row['idlink'] ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1"><?php echo $row['namalink'] . $kodelink ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="2"><?php echo $row['url'] ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="2"><?php echo $row['waofficial'] ?></td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="5">
			<?php 
				if($row['status'] == 1) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>
		</td>		
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="dolink_menu.php?menu=edit&id=<?php echo $row['idlink'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="8">
			<?php 			
				if($iduser != $row['id'] && $row['status'] == 1) { 
			?>
				<a class="btn btn-danger btn-xs" href="#" onClick="deleteLink(<?php echo $row['idlink'] ?>); return false;">
				<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
			<?php } else { ?>
				<a class="btn btn-danger btn-xs" href="#" onClick="dropLink(<?php echo $row['idlink'] ?>); return false;">
				<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Drop</a> 	
			<?php } ?>	
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(idlink) as num_rows FROM link WHERE 1=1  ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deleteLink(idaccount)
	{
		var str = "Apakah Kamu yakin delete Link ini ?? ";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	function dropLink(idaccount)
	{
		var str = "Apakah Kamu yakin delete permanent Link ini ?? ";
		var aa = confirm(str);
		if(aa)
		{
			$("#drop").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>