<?php 
	include_once "inc_login.php";
	include_once "config.php";
	include_once "function.php";
	
	$id = $_REQUEST['id'];	
	
	// search member
	$sqlmem = "SELECT mem.*
			FROM members mem 
			WHERE mem.kdmember = $id ";				
	$datamember = mysqli_fetch_array(mysqli_query($conn, $sqlmem));
		
	$p= $_REQUEST['p'];
	if($p == 'post')
	{			
		$temp_id = $id;
		$followup_number = $_REQUEST["followup_number"];
		$followup_date = date("Y-m-d");
		$followup_via = $_REQUEST["followup_via"];
		$followup_respon = $_REQUEST["followup_respon"];
		//$reminder = $_REQUEST["reminder"];
		$status = $_REQUEST["status"];
		
		$err = "";
		
		if($followup_respon == "")		$err = "Isi respon dari member";
		
		if($err == "") 
		{
			$query = "INSERT INTO followup 
						(kdmember, followup_number, followup_date, followup_via, followup_respon, createdby) 
						VALUES
						($id, '$followup_number', '$followup_date', '$followup_via', '$followup_respon', $iduser)
					 ";
					 
			$res = mysqli_query($conn, $query);			
			$last_id = mysqli_insert_id($conn);
			
			// If status = 10 (Loss Member)
			// Bikin notification to kepalacs kalau mau jadiin lose member
			if($datamember["followup_status"] != 10 && $status == 10) 
			{
				$status = 99;
				
				$sqlnotif = "INSERT INTO notification 
							(type, dateend, status, idadmin, idmember, idfollowup) 
							VALUES 
							('AjukanLossMember', NULL, 1, $iduser, $id, $last_id) ";
				$resnotif = mysqli_query($conn, $sqlnotif);
			}
			
			// Kalau dari status loss member -> anything else
			else if($datamember["followup_status"] == 10 && $status != 10) 
			{
				$sqlnotif = "INSERT INTO notification 
							(type, dateend, status, idadmin, idmember, idfollowup, new_followup_status) 
							VALUES 
							('RevertLossMember', NULL, 1, $iduser, $id, $last_id, $status) ";
				$resnotif = mysqli_query($conn, $sqlnotif);
				
				$status = 99;
			}
			
			// Reset reminder
			/*
			if(empty($reminder) == true || $reminder == "")		
				$resetreminder = "UPDATE members SET reminder = NULL, followup_status = '$status' WHERE kdmember = $id ";
			else 
			{
				$strtotim = strtotime($reminder);
				$besok = strtotime("tomorrow");
				$sebulan = strtotime("+30 days");
				
				if($strtotim < $besok)		$strtotim = $besok;
				else if($strtotim > $sebulan)	$strtotim = $sebulan;
				
				$reminder = date("Y-m-d", $strtotim);
				$resetreminder = "UPDATE members SET reminder = '$reminder', followup_status = '$status' WHERE kdmember = $id ";
			}
			
			$res2 = mysqli_query($conn, $resetreminder);
			*/
			
			$resetreminder = "UPDATE members SET followup_status = '$status' WHERE kdmember = $id ";
			$res2 = mysqli_query($conn, $resetreminder);
			
			if($res)	echo '<span style="color:green"> Sukses.<br/>'.$sukses.'</span><br/>';
			else 				echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
		}
		
		else 
		{
			echo '<span style="color:red"> Error<br/>'.$err.'</span><br/>';	
		}
		
		exit();
	}
		
		
		
		
	$tipe = $tmp["kdmember"] != NULL? "member" : "game";
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>
		<span style="background-color: #333333; color: white; padding: 5px 20px 5px 20px ;">Laporan Follow Up</span>		
	</h4>
</div>

<form id="form_confirmdeposit" class="form-vertical" action="dofollowup_addnote.php?p=post" method="post">
	<fieldset id="w2">
		<div class="row">
		
				<div class="form-group field-dpconfirm_username required">
					<input type="hidden" name="id" id="id" value="<?php echo $id ?>" />		
					<input type="hidden" name="menu" id="menu" value="<?php echo $menu ?>" />
					<input type="hidden" name="tipe" id="tipe" value="<?php echo $tipe ?>" />
										
					<br/>
															
					<div class="col-sm-4">
						<div class="form-group field-member-nama required">
							<label class="control-label" for="member-asal">Follow Up Ke</label>
							<?php 
								$sql_numb = "SELECT fol.*, IFNULL(latest_dp.MaxDate, NULL) AS datedeposit 
											FROM followup fol
											
											LEFT JOIN (
												select t.kdmember, tm.MaxDate, t.jumlah 
												from deposits t 
												inner join ( 
													select kdmember, max(modtime) as MaxDate 
													from deposits aaa
													WHERE aaa.isactive = 1 AND aaa.isclear = 1 AND aaa.ispending = 0 
													group by kdmember 
												) tm on t.kdmember = tm.kdmember AND t.modtime = tm.MaxDate 				
												WHERE t.isactive = 1 AND t.isclear = 1 AND t.ispending = 0 
											) latest_dp ON fol.kdmember = latest_dp.kdmember 
											
											WHERE fol.kdmember = $id 
											ORDER BY idfollowup DESC 
											LIMIT 0,1 ";
											
								$res_numb = mysqli_fetch_array( mysqli_query($conn, $sql_numb) );
								
								if(empty($res_numb))		$tempnumb = 1;
								else $tempnumb = $res_numb["followup_number"] + 1;
							?>
							<input type="text" name="followup_number" id="followup_number" class="form-control"  value="<?php echo $tempnumb ?>" readonly>							
						</div>
					</div>			
					
					<div class="col-sm-4">
						<div class="form-group field-member-nama required">
							<label class="control-label" for="member-asal">Tanggal FU</label>							
							<?php 
								$skrg = date("d-m-Y");
							?>
							<input type="text" name="followup_date" id="followup_date" class="form-control"  value="<?php echo $skrg ?>" readonly>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group field-member-nama required">
							<label class="control-label" for="followup_via">VIA</label>							
							<br/>
							<select class="form-control" name="followup_via">
								<option value="wa">WA</option>
								<option value="telp">Telp</option>
								<option value="line">Line</option>
								<option value="facebook">Facebook</option>
								<option value="sms">SMS</option>
								<option value="email">Email</option>
							</select>
							
						</div>
					</div>
					<div class="clearfix"></div>
					
					
					<?php 
						$followup_status = $datamember["followup_status"];
					?>
					<div class="col-sm-12">
						<div class="form-group field-member-nama required" style="width: 100%;">
							<label class="control-label" for="member-asal">Status</label>
							<div class="clearfix"></div>
							
							<div class="col-sm-6 radio_status <?php if($followup_status==5) echo 'radio_status_selected' ?>">
								<label class="normal_label">
								<input type="radio" name="status" value="5" style="visibility: hidden" <?php if($followup_status==5) echo 'checked' ?>> 
								<img src="img/followup_blue.png" style="width: 20px" /> Sudah Depo
								</label>
							</div>
							
							<div class="col-sm-6 radio_status  <?php if($followup_status==2) echo 'radio_status_selected' ?>">
								<label class="normal_label">
								<input type="radio" name="status" value="2" style="visibility: hidden"  <?php if($followup_status==2) echo 'checked' ?>> 
								<img src="img/followup_red.png" style="width: 20px" /> Kemungkinan Kecil
								</label>
							</div>
							<div class="clearfix"></div>
							
							<div class="col-sm-6 radio_status <?php if($followup_status==1) echo 'radio_status_selected' ?>">
								<label class="normal_label">
								<input type="radio" name="status" value="1" style="visibility: hidden" <?php if($followup_status==1) echo 'checked' ?>> 
								<img src="img/followup_yellow.png" style="width: 20px" /> Kemungkinan Sedang
								</label>
							</div>
							
							<div class="col-sm-6 radio_status <?php if($followup_status==0) echo 'radio_status_selected' ?>">
								<label class="normal_label">
								<input type="radio" name="status" value="0" style="visibility: hidden" <?php if($followup_status==0) echo 'checked' ?>> 
								<img src="img/followup_green.png" style="width: 20px" /> Kemungkinan Besar
								</label>
							</div>
							<div class="clearfix"></div>
							
							<div class="col-sm-6 radio_status <?php if($followup_status==10) echo 'radio_status_selected' ?>">
								<label class="normal_label">
								<input type="radio" name="status" value="10" style="visibility: hidden" <?php if($followup_status==10) echo 'checked' ?>> 
								<img src="img/followup_black.png" style="width: 20px" /> Loss Member
								</label>
							</div>
							<div class="clearfix"></div>
							
							
						</div>
					</div>
					
					<div class="clearfix"></div>
					<div class="col-sm-12">
						<div class="form-group field-member-nama required">
							<label class="control-label" for="member-asal">Respon</label>							
							<textarea name="followup_respon" id="followup_respon" class="form-control" placeholder="Respon Member" rows="6"></textarea>
												
						</div>
					</div>
					
					<div class="clearfix"></div>
					<br/>
					
					<div class="help-block"></div>
					
					<div class="col-sm-12">
						<div style="text-align: right; margin-top: 20px">
							<div id="feedback_create"></div>
							<img class="thisLoadingGif" src="img/loading.gif" />
							<button type="reset" class="btn btn-default">Reset</button> 
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
					
				</div>

		</div>
		
	</fieldset>
	

	<fieldset id="w5">
		<div class="row">					
			<div class="col-sm-8 pull-right">
				<div style="text-align: right; ">
					<div id="feedback_confirm"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<?php 
						if($menu == "approve"){
							echo '<button type="submit" class="btn btn-success">Approve</button>';
						}
						else if($menu == "reject"){
							echo '<button type="submit" class="btn btn-danger">Reject</button>';
						}
					?>
					
				</div>
			</div>
			
		</div>
	</fieldset>
	
</form>


<style>	

.radio_status {
	height: 30px;
	line-height: 30px;
	border: 1px solid white;
	padding: 0;
	margin: 0;
}
.radio_status_selected {
	background-color: lightgreen;
	border: 1px solid green;
}
.normal_label {
	font-weight: normal;
	font-size: 12px;
	padding: 0;
	margin: 0;
}
</style>

<script type="text/javascript" src="js/select2.full.js"></script>

<script type="text/javascript">
	
	$(document).ready(function()
	{
		// date click
		$( ".form_date" ).datepicker({
			dateFormat: 'dd-mm-yy'
		});
		
		// $(".js-example-basic-single").select2();
		$(".thisLoadingGif").hide();
		
		$(".normal_label").click(function(){
			// Remove other styling
			$(".radio_status_selected").removeClass("radio_status_selected");
			
			$(this).parent().addClass("radio_status_selected");
		});
	});
	
	
</script>
<?php 
	$formName = '"#form_confirmdeposit"';
	$feedback = '"#feedback_confirm"';
	
	include "inc_doscript.php";
	
	mysqli_close($conn);
?>
