<?php
	include_once 'PHPExcel/Classes/PHPExcel.php';
	include_once "inc_login.php";
	include_once "config.php";
	
	$objPHPExcel = new PHPExcel();
	PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
	
	$menu = $_REQUEST['menu'];
	$sheet = array();	
	
	
	if($menu == "laporanHarian")
	{
		$namafile = "laporan_harian_dan_bulanan.xlsx";
		
		$form_date_start = empty($_REQUEST['form_date_start']) ? "" : date("Y-m-d", strtotime($_REQUEST['form_date_start']));;
		$form_date_end = empty($_REQUEST['form_date_end']) ? "" : date("Y-m-d", strtotime($_REQUEST['form_date_end']));;
		$form_filter_date = $_REQUEST['form_filter_date'];
		
		// DATE-selected
		if($form_date_start != "" && $form_date_end != "")	{
			$datequery .= "AND ( from_unixtime(dp.tanggal,'%Y-%m-%d') between date('$form_date_start') AND date('$form_date_end') ) ";
			$dateexcel = date("d-m-Y", strtotime($form_date_start)).' sampai '.date("d-m-Y", strtotime($form_date_end));
		}
		
		// filter by month
		else {
			$myArray = explode('_', $form_filter_date);
			$bulan = $myArray[0];
			$tahun = $myArray[1];
			
			// $datequery .= " AND ( MONTH(CONVERT_TZ(datecreate,".$curtimezone.")) = '$bulan' ) AND ( YEAR(CONVERT_TZ(datecreate,".$curtimezone.")) = '$tahun' ) ";
			$datequery .= " AND ( from_unixtime(dp.tanggal,'%m') = '$bulan' ) AND ( from_unixtime(dp.tanggal,'%Y') = '$tahun' ) ";
			
			$dateObj   = DateTime::createFromFormat('!m', $bulan);
			$monthName = $dateObj->format('F'); // March
			$dateexcel = $monthName.' '.$tahun;
		}
		
		
		$queryDeposit = "SELECT SUM(dp.jumlah) as totalDeposit, COUNT(dp.jumlah) as banyakDeposit ".
					   ", from_unixtime(dp.tanggal,'%Y-%m-%d %H:%i:%s') as new_datecreate ".
						", from_unixtime(dp.tanggal,'%d') as hari ".
						", from_unixtime(dp.tanggal,'%m') as bulan ".
						", from_unixtime(dp.tanggal,'%Y') as tahun ".
					   "FROM deposits dp ".
					   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
					   "WHERE 1=1 AND ln.status =1 AND dp.isclear = 1 AND dp.isactive = 1 ".
					   $datequery.
					   " GROUP BY YEAR(new_datecreate), MONTH(new_datecreate), DAY(new_datecreate) ".
					   " ORDER BY new_datecreate ASC ";
			   
		$queryWithdraw = "SELECT SUM(dp.jumlah) as totalWithdraw, COUNT(dp.jumlah) as banyakWithdraw ".
					   ", from_unixtime(dp.tanggal,'%Y-%m-%d %H:%i:%s') as new_datecreate ".
					   ", from_unixtime(dp.tanggal,'%d') as hari ".
					   ", from_unixtime(dp.tanggal,'%m') as bulan ".
					   ", from_unixtime(dp.tanggal,'%Y') as tahun ".
					   "FROM withdraws dp ".
					   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
					   "WHERE 1=1 AND ln.status =1 AND dp.isclear = 1 AND dp.isactive = 1 ".
					   $datequery.
					   " GROUP BY YEAR(new_datecreate), MONTH(new_datecreate), DAY(new_datecreate) ".
					   " ORDER BY new_datecreate ASC ";
		
		
		$query = "SELECT tahun, bulan, hari, SUM(totalDeposit) AS totalDeposit, SUM(banyakDeposit) AS banyakDeposit, SUM(totalWithdraw) AS totalWithdraw, SUM(banyakWithdraw) AS banyakWithdraw,  
						(COALESCE( SUM(totalDeposit), 0) - COALESCE( SUM(totalWithdraw), 0)) AS totalJumlah,
						(COALESCE( SUM(banyakDeposit), 0) + COALESCE( SUM(banyakWithdraw), 0)) AS banyakJumlah
						FROM (
							SELECT tahun, bulan, hari, totalDeposit, banyakDeposit, '0' AS totalWithdraw, '0' AS banyakWithdraw FROM ($queryDeposit) as tempDeposit 
							UNION ALL
							SELECT tahun, bulan, hari, '0' AS totalDeposit, '0' AS banyakDeposit, totalWithdraw, banyakWithdraw FROM ($queryWithdraw) as tempWithdraw 					
						) AS temporary 
						".					
						"WHERE 1=1 AND (totalDeposit > 0 OR totalWithdraw > 0) ".
						'GROUP BY tahun, bulan, hari '.
						" ORDER BY tahun DESC, bulan DESC, hari ASC ";
						
		$result = mysqli_query($conn, $query);
		$cnt = $start;
		$jumlah_semua_deposit = 0;
		$jumlah_semua_withdraw = 0;
				
		$exec1 = mysqli_query($conn, $query) or die ("Error in Query1".mysql_error());
		
		// Title
		$tmparray = array("Laporan Harian dan Bulanan", "", "", $dateexcel, "", "", "");
		array_push($sheet,$tmparray);
		
		//Set header with temp array
		$tmparray = array("TANGGAL", "TOTAL DEPOSIT", "TOTAL WITHDRAW", "TOTAL JUMLAH", "TOTAL TRANSAKSI DEPOSIT", "TOTAL TRANSAKSI WITHDRAW", "TOTAL TRANSAKSI JUMLAH");
		array_push($sheet,$tmparray);		
		
		$cnt = 0;
		$jumlah_semua_deposit = 0;
		$jumlah_semua_withdraw = 0;
		$banyak_semua_deposit = 0;
		$banyak_semua_withdraw = 0;
		
		while ($res1 = mysqli_fetch_array($exec1))
		{
			$tmparray = array();
			$cnt ++;
						
			$jumlah_semua_deposit += $res1['totalDeposit'];
			$jumlah_semua_withdraw += $res1['totalWithdraw'];
			
			$banyak_semua_deposit += $res1['banyakDeposit'];
			$banyak_semua_withdraw += $res1['banyakWithdraw'];
			
			
			array_push($tmparray, $res1['hari']);
			array_push($tmparray, $res1['totalDeposit']);
			array_push($tmparray, $res1['totalWithdraw']);
			array_push($tmparray, $res1['totalJumlah']);
			array_push($tmparray, $res1['banyakDeposit']);
			array_push($tmparray, $res1['banyakWithdraw']);
			array_push($tmparray, $res1['banyakJumlah']);
			// array_push($tmparray, "");
			
				
			array_push($sheet,$tmparray);
		}
				
		
		// add total
		$cnt += 2;
		array_push($sheet, array(""));
		
		array_push($sheet, array("", "TOTAL DEPOSIT:", "", $jumlah_semua_deposit));
		array_push($sheet, array("", "TOTAL WITHDRAW:", "", $jumlah_semua_withdraw));		
		
		$jumlah_semua_difference = $jumlah_semua_deposit - $jumlah_semua_withdraw;
		array_push($sheet, array("", "TOTAL JUMLAH:", "", $jumlah_semua_difference));		
		array_push($sheet, array(""));
		
		array_push($sheet, array("", "TOTAL TRANSAKSI DEPOSIT:", "", $banyak_semua_deposit));
		array_push($sheet, array("", "TOTAL TRANSAKSI WITHDRAW:", "", $banyak_semua_withdraw));
		$banyak_semua_difference = $banyak_semua_deposit + $banyak_semua_withdraw;
		array_push($sheet, array("", "TOTAL TRANSAKSI JUMLAH:", "", $banyak_semua_difference));
		array_push($sheet, array(""));
		
		// array_push($sheet, array("", "TOTAL BONUS PEMAIN:", "", "0"));
		
		// Numbering for Total
		$objPHPExcel->getActiveSheet()->getStyle('D'.$cnt.':D'.($cnt+10))->getNumberFormat()->setFormatCode('#,##0');
		
		// Merge for total
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+2).':C'.($cnt+2));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+3).':C'.($cnt+3));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+4).':C'.($cnt+4));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+5).':C'.($cnt+5));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+6).':C'.($cnt+6));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+7).':C'.($cnt+7));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+8).':C'.($cnt+8));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+9).':C'.($cnt+9));
		
				
		// Number Formatting
		$cnt += 6;		
		$objPHPExcel->getActiveSheet()->getStyle('A3:A'.$cnt)->getNumberFormat()->setFormatCode('@');
		$objPHPExcel->getActiveSheet()->getStyle('B3:B'.$cnt)->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('C3:C'.$cnt)->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('D3:D'.$cnt)->getNumberFormat()->setFormatCode('#,##0');
		
		$objPHPExcel->getActiveSheet()->getStyle('E3:E'.$cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('F3:F'.$cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('G3:G'.$cnt)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		// auto size
		foreach(range('A','G') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);			
		}
		
	}
	
	else if($menu == "laporanMemberWinLose"){		
		$namafile = "laporan_member_win_lose.xlsx";
		
		$form_date_start = empty($_REQUEST['form_date_start']) ? "" : date("Y-m-d", strtotime($_REQUEST['form_date_start']));;
		$form_date_end = empty($_REQUEST['form_date_end']) ? "" : date("Y-m-d", strtotime($_REQUEST['form_date_end']));;
		$form_filter_date = $_REQUEST['form_filter_date'];
		
		// DATE-selected
		if($form_date_start != "" && $form_date_end != "") {	
			$datequery .= " AND ( CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00') between date('$form_date_start') AND date('$form_date_end') ) ";
			$dateexcel = date("d-m-Y", strtotime($form_date_start)).' sampai '.date("d-m-Y", strtotime($form_date_end));
		}
		// YESTERDAY
		else if($form_filter_date == 2){
			$datequery .= " AND ( date(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d') , @@session.time_zone, '+7:00')) = SUBDATE(CURDATE(), 1)  ) ";
			$dateexcel = "Kemarin";
		}
		// seminggu
		else if($form_filter_date == 3){
			$datequery .= " AND ( date(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d') , @@session.time_zone, '+7:00')) between SUBDATE(CURDATE(), 7) AND CURDATE() ) ";
			$dateexcel = "Seminggu";
		}
		// sebulan
		else {
			$datequery .= " AND ( date(CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d') , @@session.time_zone, '+7:00')) between SUBDATE(CURDATE(), 30) AND CURDATE() ) ";		
			$dateexcel = "Sebulan";
		}
		
		
		$queryDeposit = "SELECT kdmember, SUM(IF(dp.isclear=1, dp.jumlah, 0)) as totalDeposit, COUNT(dp.jumlah) as banyakDeposit ".
				   "FROM deposits dp ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
				   "WHERE 1=1 ".
				   $datequery.
				   " GROUP BY kdmember ".
				   " ORDER BY kdmember ASC ";
	
		$queryWithdraw = "SELECT kdmember, SUM(IF(dp.isclear=1, dp.jumlah, 0)) as totalWithdraw, COUNT(dp.jumlah) as banyakWithdraw ".
				   "FROM withdraws dp ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".
				   "WHERE 1=1 ".
				   $datequery.
				   " GROUP BY kdmember ".
				   " ORDER BY kdmember ASC ";		
		
		$query = "SELECT mem.kdmember, mem.username, ln.namalink, mem.nama, mem.norek, bk.inisialbank, 
						gm.kodeagen AS hex_account, gm.nama AS nama_game, 
						totalDeposit, banyakDeposit, totalWithdraw, banyakWithdraw, (COALESCE(totalDeposit, 0) - COALESCE(totalWithdraw, 0)) AS totalJumlah
						
						FROM members mem  
						LEFT JOIN ($queryDeposit) as tempDeposit ON tempDeposit.kdmember=mem.kdmember 
						LEFT JOIN ($queryWithdraw) as tempWithdraw ON tempWithdraw.kdmember=mem.kdmember 
						LEFT JOIN link ln ON mem.idlink = ln.idlink 
						LEFT JOIN products gm ON gm.kdproduct=mem.kdproduct 
						LEFT JOIN banks bk ON bk.kdbank=mem.kdbank
						".					
						"WHERE 1=1 AND (totalDeposit > 0 OR totalWithdraw > 0) ".
						" AND ln.status=1 ".
						" GROUP BY mem.kdmember ".					
						" ORDER BY mem.kdmember DESC";
						 
						
		$exec1 = mysqli_query($conn, $query) or die ("Error in Query1".mysql_error());
		
		
		// Title
		$tmparray = array("Laporan Menang/Kalah Pemain", "", "", $dateexcel, "", "", "");
		array_push($sheet,$tmparray);
		
		//Set header with temp array
		$tmparray = array("#","USERNAME", "WEBSITE", "NAMA PEMAIN", "NO.REKENING", "KODE GAME", "TOTAL DEPOSIT", "TOTAL WITHDRAW", "TOTAL JUMLAH", "TOTAL BONUS", "CREDIT GAME", "DEPOSIT TERAKHIR", "WITHDRAW TERAKHIR");
		array_push($sheet,$tmparray);		
		
		$cnt = 0;
		$jumlah_semua_deposit = 0;
		$jumlah_semua_withdraw = 0;
		while ($res1 = mysqli_fetch_array($exec1))
		{
			$tmparray = array();
			$cnt ++;
			
			$res1['totalDeposit'] = $res1['totalDeposit']? $res1['totalDeposit'] : 0;
			$res1['totalWithdraw'] = $res1['totalWithdraw']? $res1['totalWithdraw'] : 0;
			
			$jumlah_semua_deposit += $res1['totalDeposit'];
			$jumlah_semua_withdraw += $res1['totalWithdraw'];
			
			array_push($tmparray, $cnt);
			array_push($tmparray, $res1['username']);
			array_push($tmparray, $res1['namalink']);
			array_push($tmparray, $res1['nama']);
			array_push($tmparray, $res1['norek']." - ".$res1['inisialbank']);
			array_push($tmparray, explode(",", $res1['hex_account'])[0]." - ".$res1['nama_game']);
			array_push($tmparray, $res1['totalDeposit']);
			array_push($tmparray, $res1['totalWithdraw']);
			array_push($tmparray, $res1['totalJumlah']);
			array_push($tmparray, "");
			array_push($tmparray, "");
			
			// Last deposit
			$kdmember = $res1['kdmember'];
			
			$queryLastDeposit = "SELECT jumlah , ".
								"CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00') as dateLastDeposit ".
								"FROM deposits dp ".
								"WHERE kdmember = $kdmember ".
								"ORDER BY dp.cretime DESC ".
								"LIMIT 0, 1";
			$resLastDeposit = mysqli_query($conn, $queryLastDeposit);
			$str = "";
			while($rowLastDepost=mysqli_fetch_array($resLastDeposit) ) {
				$str = number_format($rowLastDepost['jumlah'], 0). " - ". date( "d-m-Y H:i:s" , strtotime($rowLastDepost['dateLastDeposit']));				
			}
			array_push($tmparray, $str);
			
			// Last Withdraw
			$queryLastWithdraw = "SELECT jumlah , ".									
								"CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s'), @@session.time_zone, '+7:00') as dateLastWd ".
								"FROM withdraws dp ".
								"WHERE kdmember = $kdmember ".
								"ORDER BY dp.cretime DESC ".
								"LIMIT 0, 1";
			$resLastWithdraw = mysqli_query($conn, $queryLastWithdraw);
			$str = "";
			while($rowLastWd=mysqli_fetch_array($resLastWithdraw) ) {
				$str = number_format($rowLastWd['jumlah'], 0). " - ". date( "d-m-Y H:i:s" , strtotime($rowLastWd['dateLastWd']));	
			}
			array_push($tmparray, $str);
				
			array_push($sheet,$tmparray);
		}
				
		
		// add total
		$cnt += 2;
		array_push($sheet, array(""));
		
		array_push($sheet, array("", "TOTAL DEPOSIT:", "", $jumlah_semua_deposit));
		array_push($sheet, array("", "TOTAL WITHDRAW:", "", $jumlah_semua_withdraw));
		
		$jumlah_semua_difference = $jumlah_semua_deposit - $jumlah_semua_withdraw;
		if($jumlah_semua_difference > 0)	$kata = "(MENANG)";
		else $kata = "(KALAH)";
				
		array_push($sheet, array("", "TOTAL JUMLAH:", "", $jumlah_semua_difference, $kata));		
		array_push($sheet, array(""));
		array_push($sheet, array("", "TOTAL BONUS PEMAIN:", "", "0"));
		
		// Numbering for Total
		$objPHPExcel->getActiveSheet()->getStyle('D'.$cnt.':D'.($cnt+10))->getNumberFormat()->setFormatCode('#,##0');
		
		// Merge for total
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+2).':C'.($cnt+2));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+3).':C'.($cnt+3));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+4).':C'.($cnt+4));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+5).':C'.($cnt+5));
		$objPHPExcel->getActiveSheet()->mergeCells('B'.($cnt+6).':C'.($cnt+6));
		
		// center menang/kalah
		$objPHPExcel->getActiveSheet()->getStyle('E'.($cnt+4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
		// Number Formatting
		$cnt += 6;		
		$objPHPExcel->getActiveSheet()->getStyle('G3:G'.$cnt)->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('H3:H'.$cnt)->getNumberFormat()->setFormatCode('#,##0');
		$objPHPExcel->getActiveSheet()->getStyle('I3:I'.$cnt)->getNumberFormat()->setFormatCode('#,##0');
		
		
		// auto size
		foreach(range('A','M') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);			
		}
	}
	
	$worksheet = $objPHPExcel->getActiveSheet();
	foreach($sheet as $row => $columns) {
		foreach($columns as $column => $data) {
			// echo $column. ' '.$row.' = ' . $data.'<br/>';			
			$worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
			
		}
	}
	
	header('Content-type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename="'.$namafile.'"');
	
	//make first & second row bold
	$objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0);
	
	$objPHPExcel->getActiveSheet()->getStyle("A2:M2")->getFont()->setBold(true);
	$objPHPExcel->setActiveSheetIndex(0);
	
	// Merge title
	$objPHPExcel->getActiveSheet()->mergeCells('A1:C1');
	
	/*
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
	*/
	
	 // Save Excel file
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	
  
	mysqli_close($conn);
?>