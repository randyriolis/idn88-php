<?php 
	include_once "inc_login.php";
	include_once "function.php";
	include "config.php";
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['kdusercat'];
	
	$p= $_REQUEST['p'];
	if($p == 'post')
	{
		
		// process to send sms
		$nama = $_REQUEST["nama"];
		$no_rekening = $_REQUEST["no_rekening"];
		$bank = ($_REQUEST["bank"]==9999) ? $_REQUEST['ewallet'] : $_REQUEST['bank'];
		$idsumber = $_REQUEST["idsumber"];
		$sumber = $_REQUEST["sumber"];
		$no_hp = $_REQUEST["no_hp"];
		$email = $_REQUEST["email"];
		$kdproduct = $_REQUEST["kdproduct"];
		$idpromo = $_REQUEST["idpromo"];
		$keterangan = $_REQUEST["keterangan"];
		$idlink = $_REQUEST["idlink"];
		$iduser = $_SESSION['alpha_iduser'];
		$ip = $_SERVER['REMOTE_ADDR'];
		$url = getUrl();
				
		$agent = $_SERVER['HTTP_USER_AGENT'] ;	
			
		if(preg_match('/Linux/i',$agent)) $os = 'Linux';
		elseif(preg_match('/Mac/i',$agent)) $os = 'Mac'; 
		elseif(preg_match('/iPhone/i',$agent)) $os = 'iPhone'; 
		elseif(preg_match('/iPad/i',$agent)) $os = 'iPad'; 
		elseif(preg_match('/Droid/i',$agent)) $os = 'Droid'; 
		elseif(preg_match('/Unix/i',$agent)) $os = 'Unix'; 
		elseif(preg_match('/Windows/i',$agent)) $os = 'Windows';
		else $os = 'Unknown';
		 
		// Browser Detection	 
		if(preg_match('/Firefox/i',$agent)) $br = 'Firefox'; 
		elseif(preg_match('/Mac/i',$agent)) $br = 'Mac';
		elseif(preg_match('/Chrome/i',$agent)) $br = 'Chrome'; 
		elseif(preg_match('/Opera/i',$agent)) $br = 'Opera'; 
		elseif(preg_match('/MSIE/i',$agent)) $br = 'IE'; 
		else $bs = 'Unknown';
		  
		$browser_name = $br." - ".$os." - ".$agent;
		
		// Check
		$err = "";
		
		if($nama == "")	$err .= "<br/>Nama cannot be blank.";
		if($no_hp == "")	$err .= "<br/>No HP cannot be blank.";
		if($no_rekening == "")	$err .= "<br/>No Rekening cannot be blank.";
		if($email == "")	$err .= "<br/>Email cannot be blank.";
		if($bank == "")	$err .= "<br/>Bank cannot be blank.";
		
		// search nama game
		$tmp = mysqli_fetch_array(mysqli_query($conn, "SELECT nama AS nama_game FROM products WHERE kdproduct='$kdproduct' "));
		$nama_game = $tmp['nama_game'];
		
		if($err == "") {
			$sqlCheckEmail = "SELECT mem.*, gm.nama AS nama_game FROM members mem LEFT JOIN products gm ON mem.kdproduct=gm.kdproduct WHERE (mem.email = '$email' OR mem.tlp='$no_hp' OR mem.norek='$no_rekening' ) AND gm.nama = '$nama_game' ";
			// $result=mysqli_query($conn, "SELECT nama FROM member WHERE email = '$email' ");
			$result=mysqli_query($conn, $sqlCheckEmail);
			$rowcount=mysqli_num_rows($result);
			mysqli_free_result($result);
			
			if($rowcount > 0)
			{
				// Email sudah terpakai
				$err .= "<br/>E-mail, Nomor Rekening atau Nomor HP sudah terpakai untuk game ini";
			}
		}
		
		
		
		
		
		if($err != ""){
			echo '<span style="color:red"> Error'.$err.'</span><br/>';
			exit();
		}
		
		$wkt = time();
		$query = "INSERT INTO members (nama, namarek, username, kdbank, email, tlp, kdproduct, dari_ip1, tanggal_daftar, browser_name, kenal, isactive, norek, ket, idlink, idsumber, sumber, url)
				VALUES ('$nama', '$nama', '', '$bank', '$email', '$no_hp', '$kdproduct', '$ip', $wkt, '$browser_name', 'VISITOR', 1, '$no_rekening', '$keterangan', '$idlink', '$idsumber', '$sumber', '$url')";
		$res = mysqli_query($conn, $query);
		
		if($res)			echo '<span style="color:green"> Sukses.<br/>Member berhasil di-create</span><br/>';
		else 			echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
		
		exit();
	}
?>
<form id="form_createmember" class="form-vertical" action="domember_create.php?p=post" method="post">
	<fieldset id="w2">
		<div class="row">
			<div class="col-sm-4">	
				<div class="form-group field-member-nama required">
					<label class="control-label" for="member-nama">Nama</label>
					<input type="text" id="member-nama" class="form-control" name="nama" placeholder="Nama Rekening...">
					<div class="help-block"></div>
				</div>

			</div>

			<div class="col-sm-4">
				<div class="form-group field-member-no_rekening required">
					<label class="control-label" for="member-no_rekening">No Rekening</label>
					<input type="text" id="member-no_rekening" class="form-control" name="no_rekening" placeholder="Nomer Rekening...">

					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group field-member-bank required">
					<label class="control-label" for="member-bank">Bank</label>
					<select id="member-bank" class="form-control" name="bank">
						<?php
							$sqlbank = "SELECT * FROM banks WHERE isactive=1 AND istype=0 ORDER BY kdbank ASC";
							$querybank = mysqli_query($conn, $sqlbank);
							
							while($row=mysqli_fetch_array($querybank)){
								echo '<option value="'.$row['kdbank'].'">'.$row['inisialbank'].'</option>';
							}

							$result=mysqli_query($conn,"SELECT COUNT(*) AS jumlah FROM banks WHERE istype=1");
							$data=mysqli_fetch_assoc($result);
							if ($data['jumlah']>0) { echo '<option value="9999">E-WALLETS</option>'; }
						?>
					</select>
					<div id="bank_ewallet" style="margin-top: 5px; display: none;">
						<select id="ewallet" class="form-control" name="ewallet">
							<?php
								$sqlwallet = "SELECT * FROM banks WHERE isactive=1 AND istype=1 ORDER BY kdbank ASC";
								$querywallet = mysqli_query($conn, $sqlwallet);

								while($row=mysqli_fetch_array($querywallet)){
									echo '<option value="'.$row['kdbank'].'">'.$row['inisialbank'].'</option>';
								}
							?>
						</select>
					</div>
				
					<div class="help-block"></div>
				</div>
			</div>
		</div>
	</fieldset>
	
	<fieldset id="w3">
		<div class="row">
		<div class="col-sm-4">
			<div class="form-group field-member-no_hp required">
				<label class="control-label" for="member-no_hp">No Hp</label>
				<input type="text" id="member-no_hp" class="form-control" name="no_hp" placeholder="Nomer HP...">
				<div class="help-block"></div>
			</div>
		</div>

		<div class="col-sm-4">	
			<div class="form-group field-member-email required">
				<label class="control-label" for="member-email">Email</label>
				<input type="text" id="member-email" class="form-control" name="email" placeholder="Masukan E-mail...">
				<div class="help-block"></div>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="form-group field-member-kdproduct required">
				<label class="control-label" for="member-kdproduct">Game</label>
				<select id="member-kdproduct" class="form-control" name="kdproduct">
					<?php
						// QUERY
						$sqlgame = "SELECT gm.* FROM products gm WHERE gm.isactive = 1 AND kodegame IS NOT NULL ORDER BY kdproduct ASC  ";		
						$resultgame = mysqli_query($conn, $sqlgame);
						while($rowgame = mysqli_fetch_array($resultgame)) {
							echo '<option value="'.$rowgame['kdproduct'].'">'.$rowgame['nama'].'</option>';
						}										
					?>					
				</select>
				<div class="hint-block">Pilih Game</div>
				<div class="help-block"></div>
			</div>
		</div>

		</div>

	</fieldset>

	<fieldset id="w4">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group field-member-idpromo">
					<label class="control-label" for="member-idpromo">Promo</label>
					<select id="member-idpromo" class="form-control" name="idpromo">
						
						<option value="0">Tidak Pilih Promo</option>
						<option value="1">Depo 50%</option>
						<option value="5">Bonus Sportbook 50%</option>
					</select>
					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group field-member-keterangan">
					<label class="control-label" for="member-keterangan">Keterangan</label>
					<textarea id="member-keterangan" class="form-control" name="keterangan" placeholder="Keterangan bila perlu..."></textarea>
					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group field-member-keterangan">
					<label class="control-label" for="member-keterangan">Website</label>
					
					<select id="accountsearch-namalink" class="form-control" name="idlink">
						<?php
							$rs = mysqli_query($conn, "SELECT * FROM link WHERE status=1 AND kodelink IS NOT NULL ORDER BY idlink ASC ");
							
							while($rw=mysqli_fetch_array($rs)) {
								echo '<option value="'.$rw['idlink'].'" >'.$rw['namalink'].' ('.$rw['kodelink'].')</option>';
							}
							
							for($i=0; $i<count($arrgame); $i++) {
									
								}
						?>
					</select>
					
					<div class="help-block"></div>
				</div>
			</div>
		</div>

	</fieldset>

	<fieldset id="w4">
		<div class="row">			
			<div class="col-sm-4">
				<div class="form-group field-member-idsumber">
					<label class="control-label" for="member-idsumber">Sumber</label>
					
					<select id="sumber-info" class="form-control" name="idsumber">
						<?php
							$sumbers = array(
								"1" => "Google", 
								"2" => "Facebook", 
								"3" => "Instagram", 
								"4" => "Twitter", 
								"5" => "WhatsApp",
								"6" => "Refferal",
								"7" => "Lainnya"
							);

							foreach($sumbers as $code => $item){
								$sel = ($code == $row['idsumber'])? 'selected' : '';
								echo '<option '.$sel.' value="'.$code.'">'.$item.'</option>';
							}
						?>
					</select>
					<div id="sumber_text" style="margin-top: 5px; display: none;">
						<input type="text" id="member-sumber" class="form-control" name="sumber" value="<?php echo htmlentities($row['sumber']) ?>" placeholder="Sumber Info..." maxlength="20">
					</div>
					
					<div class="help-block"></div>
				</div>
			</div>
			

			<div class="col-sm-12">
				<div style="text-align: right; margin-top: 20px">
					<div id="feedback_create"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>

	</fieldset>
</form>

<script>
var select = document.getElementById("sumber-info");
select.onchange = function () {
    if (select.value >= "6") {
        document.getElementById("sumber_text").style.display = "inherit";
        if (select.value == "6") {
            $("#member-sumber").attr("placeholder", "Masukan id/user refferal");
        } else if (select.value == "7") {
            $("#member-sumber").attr("placeholder", "Masukan sumber info");
        }
    } else {
        document.getElementById("sumber_text").style.display = "none";
    }
};
var selectBank = document.getElementById("member-bank");
selectBank.onchange = function () {
	if (selectBank.value == "9999") {
        document.getElementById("bank_ewallet").style.display = "inherit";
	} else {
        document.getElementById("bank_ewallet").style.display = "none";
	}
};
</script>


<?php 
	$formName = '"#form_createmember"';
	$feedback = '"#feedback_create"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>

<script type="text/javascript">
//jQuery('#w0').yiiActiveForm([{"id":"member-nama","name":"nama","container":".field-member-nama","input":"#member-nama","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Nama cannot be blank."});yii.validation.string(value, messages, {"message":"Nama must be a string.","max":150,"tooLong":"Nama should contain at most 150 characters.","skipOnEmpty":1});}},{"id":"member-no_rekening","name":"no_rekening","container":".field-member-no_rekening","input":"#member-no_rekening","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"No Rekening cannot be blank."});yii.validation.string(value, messages, {"message":"No Rekening must be a string.","max":150,"tooLong":"No Rekening should contain at most 150 characters.","skipOnEmpty":1});}},{"id":"member-bank","name":"bank","container":".field-member-bank","input":"#member-bank","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Bank cannot be blank."});yii.validation.string(value, messages, {"message":"Bank must be a string.","max":150,"tooLong":"Bank should contain at most 150 characters.","skipOnEmpty":1});}},{"id":"member-no_hp","name":"no_hp","container":".field-member-no_hp","input":"#member-no_hp","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"No Hp cannot be blank."});yii.validation.string(value, messages, {"message":"No Hp must be a string.","max":20,"tooLong":"No Hp should contain at most 20 characters.","skipOnEmpty":1});}},{"id":"member-email","name":"email","container":".field-member-email","input":"#member-email","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Email cannot be blank."});yii.validation.string(value, messages, {"message":"Email must be a string.","max":100,"tooLong":"Email should contain at most 100 characters.","skipOnEmpty":1});}},{"id":"member-kdproduct","name":"kdproduct","container":".field-member-kdproduct","input":"#member-kdproduct","validate":function (attribute, value, messages, deferred, $form) {yii.validation.required(value, messages, {"message":"Game cannot be blank."});yii.validation.number(value, messages, {"pattern":/^\s*[+-]?\d+\s*$/,"message":"Game must be an integer.","skipOnEmpty":1});}},{"id":"member-idpromo","name":"idpromo","container":".field-member-idpromo","input":"#member-idpromo","validate":function (attribute, value, messages, deferred, $form) {yii.validation.number(value, messages, {"pattern":/^\s*[+-]?\d+\s*$/,"message":"Promo must be an integer.","skipOnEmpty":1});}},{"id":"member-keterangan","name":"keterangan","container":".field-member-keterangan","input":"#member-keterangan","validate":function (attribute, value, messages, deferred, $form) {yii.validation.string(value, messages, {"message":"Keterangan must be a string.","max":1500,"tooLong":"Keterangan should contain at most 1,500 characters.","skipOnEmpty":1});}}], []);
</script>