<?php

//import.php
  
require_once __DIR__.'/vendor/autoload.php';
include_once "inc_login.php";
include_once "config.php";

date_default_timezone_set('Asia/Jakarta');

use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

//$connect = new PDO("mysql:host=localhost;dbname=idn88", "root", "");

if($_FILES["import_excel"]["name"] != '')
{
  $allowed_extension = array('xls', 'csv', 'xlsx');
  $file_array = explode(".", $_FILES["import_excel"]["name"]);
  $file_extension = end($file_array);

  if (strtolower($file_array[0])=='deposit')
  {
  if(in_array($file_extension, $allowed_extension))
  {
    $file_name = time() . '.' . $file_extension;
    move_uploaded_file($_FILES['import_excel']['tmp_name'], $file_name);
    $file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_name);
    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($file_type);

    $spreadsheet = $reader->load($file_name);

    unlink($file_name);

    $data = $spreadsheet->getActiveSheet()->toArray();

    $no = 0;

    foreach($data as $row)
    {
      /*$insert_data = array(
        ':username'  => strtolower($row[0]),
        ':jumlah'  => $row[1],
        ':tanggal'  => $row[2]
      );*/

      $username = strtolower($row[0]);
      $jumlah = str_replace(',','', $row[1]);
      $jml_int = (int)$jumlah;

      //echo $no;
      
      //list($month, $day, $year) = explode('/', $row[2]);
      //$excelDate = $row[2]; // gives you a number like 44444, which is days since 1900
      //$stringDate = \PHPExcel_Style_NumberFormat::toFormattedString($excelDate, 'YYYY-MM-DD');
      //$UNIX_DATE = ($row[2] - 25569) * 86400;
      //$temp  = strtotime( PHPExcel_Style_NumberFormat::toFormattedString($row[2],'YYYY-MM-DD') );
      //$actualdate = date('Y-m-d',$temp) ;
      //$value = $row[2];
      //$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value, new DateTimeZone('Asia/Jakarta'));
      //$date_now = date('m/d/Y H:i:s', $date);

      //$UNIX_DATE = ($date - 25569) * 86400;
      //$date2 = gmdate("d-m-Y H:i:s", $UNIX_DATE);

      //$timezone = \PhpOffice\PhpSpreadsheet\Shared\Date::getDefaultOrLocalTimezone();

      //$date = DateTime::createFromFormat('m/d/Y', $row[2], new DateTimeZone('Asia/Jakarta'));
      //$convert = $date->format('d-m-Y');
      //$newdate = date('d-m-Y', mktime(0,0,0,$month,$day,$year));
      //$timestamp = strtotime('2017-02-28');
      
      $timestamp = strtotime($row[2]);
      $date = strtotime('+'.$no.' sec', $timestamp);
      $date = empty($date) ? time() : $date;

      //$d = DateTime::createFromFormat('d-m-Y H:i:s', $newdate, new DateTimeZone('Asia/Jakarta'));
      //$dd = $date->getTimestamp();

      // search member
      //$arrMember = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM members WHERE username='AEMPPAA534'") );
      /*$ip = $_SERVER['REMOTE_ADDR'];
      $norek = $tmp['norek'];
      $kdproduct = $tmp['kdproduct'];
      $nama = $tmp['nama'];
      $kdbank = $tmp['kdbank'];
      $username = $tmp['username'];*/

      //$alert = ( ! mysqli_num_rows($queryMember) ) ? 'gagal' : 'sukses';


      if ($no > 0) {
        if ($row[0] != NULL) {
          $usermember = $row[0];
          $querymember = mysqli_query($conn, "SELECT * FROM members where username='$usermember'");
          $alert = ( ! mysqli_num_rows( $querymember ) ) ? 'gagal' : 'sukses';
          //$tmp = mysqli_fetch_array( $querymember );

          // search member
          $tmp = mysqli_fetch_array($querymember);
          $ip = $_SERVER['REMOTE_ADDR'];
          $norek = $tmp['norek'];
          $kdproduct = $tmp['kdproduct'];
          $namarek = $tmp['namarek'];
          $kdbank = $tmp['kdbank'];
          $kdmember = $tmp['kdmember'];

          if ($alert == 'sukses') {
            $query = "INSERT INTO deposits(username, jumlah, dari_ip1, isactive, isnew, isclear, ispending, tanggal, norek, namarek, kdbank, kdproduct, kdmember)
              VALUES('$username', '$jml_int', '$ip', 1, 1, 1, 0, '$date', '$norek', '$namarek', '$kdbank', '$kdproduct', '$kdmember')";

            //$statement = $connect->prepare($query);
            //$statement->execute($insert_data);*/
            //echo 'Username: '.strtolower($row[0]).'<br>';

            $result = mysqli_query($conn, $query);
          }
  
          //if($result)   echo 'sukses '.$no.'<br>';
          echo '['.$alert.']['.$no.'] => {'.$username.'},{'.$jumlah.'},{'.$date.'}<br>';
        }
      }

      $no++;
      }

    
      
      //  if($res)  $message = '<div class="alert alert-success">Data Imported Successfully</div>';
      //  else      $message = '<div class="alert alert-alert"> Error<br/>Internetnya putus kali nih..</div>';
      $message = '<br><div class="alert alert-success">Data Deposit Imported Successfully</div>';

    }
    else
    {
      $message = '<div class="alert alert-danger">Only .xls .csv or .xlsx file allowed</div>';
    }
  }
  else
  {
    $message = '<div class="alert alert-danger">Filename must be DEPOSIT</div>';
  }
}
else
{
 $message = '<div class="alert alert-danger">Please Select File</div>';
}

echo $message;

?>