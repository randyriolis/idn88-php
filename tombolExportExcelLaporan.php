<?php 
	
?>

<form id="formexport" method="GET" action="exportExcelLaporan.php">
	<input type="hidden" name="menu" value="<?php echo $menu ?>" />
	<input type="hidden" name="filter_date" id="filter_date_export" />
	<input type="hidden" name="form_date_start" id="form_date_start_export" />
	<input type="hidden" name="form_date_end" id="form_date_end_export" />
	<input type="hidden" name="form_filter_date" id="form_filter_date_export" />
	

	<div class="btn-group" style="">						
		<a class="btn btn-warning" href="#" title="Export Excel" data-pjax="0" onclick="submitExport(); return false;">
			Export Excel
		</a>
	</div>

</form>


<script>
	function submitExport(){
		
		$("#filter_date_export").val( $("#filter_date").val() );
		$("#form_date_start_export").val( $("#form_date_start").val() );
		$("#form_date_end_export").val( $("#form_date_end").val() );
		$("#form_filter_date_export").val( $("#filter_date").val() );
		
		document.getElementById("formexport").submit();
	}
</script>