<?php	
	include_once "inc_login.php";	
	include_once "config.php";		
	$page = $_REQUEST['page'];	
	if(empty($page)) $page = 1;		
?>
    
	<!DOCTYPE html>
    <html lang="en-US">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
        <title>On Off Bank</title>
        <link href="css/bootstrap.css" rel="stylesheet">
        <link href="css/activeform.css" rel="stylesheet">
        <link href="css/select2.css" rel="stylesheet">
        <link href="css/select2-addl.css" rel="stylesheet">
        <link href="css/select2-krajee.css" rel="stylesheet">
        <link href="css/kv-widgets.css" rel="stylesheet">
        <link href="css/kv-grid.css" rel="stylesheet">
        <link href="css/bootstrap-dialog.css" rel="stylesheet">
        <link href="css/jquery.resizableColumns.css" rel="stylesheet">
        <link href="css/site.css" rel="stylesheet">
        <link href="css/disable.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/AdminLTE.min.css" rel="stylesheet">
        <link href="css/skin-blue.min.css" rel="stylesheet">
        <style>
            .BCA_tr {
                background-color: lightblue !important;
            }
            
            .MANDIRI_tr {
                background-color: lightgreen !important;
            }
            
            .BNI_tr {
                background-color: pink !important;
            }
            
            .BRI_tr {
                background-color: yellow !important;
            }
			
			.CIMB_tr {
                background-color: lightgray !important;
            }
            
            .hoverable_tr:hover {
                background-color: white !important;
                cursor: pointer;
            }
        </style>
    </head>

    <body class="hold-transition skin-blue sidebar-mini skin-blue">
        <?php		
			$act = "bankOnoff";		
			include_once "inc_header.php";	
		?>
		
            <div class="content-wrapper">
                <section class="content-header">
                    <h1></h1>
                    <ul class="breadcrumb">
                        <li><a href="site.php">Home</a></li>
                        <li class="active">onoff</li>
                    </ul>
                </section>
                <section class="content">
                    <div class="onoff-index">
                        <div class="account-form">
                            <p>
                                <form id="form_newonoff" class="form-inline" action="docreate_onoff.php" method="post">
                                    <div class="form-group"> &nbsp; </div>
                                </form>
                            </p>
                        </div>
                        <div id="w0" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
                            <div id="onoff-id-pjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
                                <div id="onoff-id" class="grid-view hide-resize" data-krajee-grid="kvGridInit_7e6ea3f8">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <div class="pull-right">
                                                <div id="page_number" class="summary"> </div>
                                            </div>
                                            <h3 class="panel-title">									On Off Bank								</h3>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="kv-panel-before">
                                            <div class="pull-right">
                                                <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
                                                    <div class="btn-group">
                                                        <a class="btn btn-default" href="member.php" title="Reset Grid" data-pjax="0" onClick="refreshContent(); return false;"> <i class="glyphicon glyphicon-repeat"></i> </a>
                                                    </div>
                                                    <div class="btn-group">
                                                        <a id="showall" class="btn btn-default" href="#" title="Show all data" onClick="tryShowAll(); return false;"> <i class='glyphicon glyphicon-resize-full'></i> All </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div id="onoff-id-container" class="table-responsive kv-grid-container">
                                            <table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
                                                <colgroup>
                                                    <col>
                                                        <col>
                                                            <col>
                                                                <col class="skip-export">
                                                                    <col class="skip-export">
                                                                        <col class="skip-export">
                                                </colgroup>
                                                <thead>
                                                    <tr>
                                                        <th class="kartik-sheet-style kv-align-center kv-align-middle kv-merged-header" style="width:36px;" rowspan="2" data-col-seq="0">&nbsp;</th>
                                                        <th class="kartik-sheet-style kv-align-middle" data-col-seq="1"> <a href="#" onClick="return trysort('nama_bank');" id="nama_bank" class="sorter">Nama Bank</a> </th>
                                                        <th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="6"> <a href="#" onClick="return trysort('status');" id="status" class="sorter">Hari</a> </th>
                                                        <th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="6"> <a href="#" onClick="return trysort('status');" id="status" class="sorter">Jam Buka</a> </th>
                                                        <th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="6"> <a href="#" onClick="return trysort('status');" id="status" class="sorter">Jam Tutup</a> </th>
                                                    </tr>
                                                    <tr id="onoff-id-filters" class="filters skip-export" style="display: none">
                                                        <form id="form_search" class="form-inline" action="doprint_onoff.php" method="post">
                                                            <input type="hidden" id="page" name="page" value="<?php echo $page ?>" />
                                                            <input type="hidden" id="sort" name="sort" value="" />
                                                            <input type="hidden" id="totalrow" name="totalrow" value="" />
                                                            <input type="hidden" id="maxrow" name="maxrow" value="20" />
                                                            <input type="hidden" id="edit" name="edit" value="" />
                                                            <input type="hidden" id="changeID" name="changeID" value="" />
                                                            <input type="hidden" id="changemode" name="changemode" value="" />
                                                            <td>
                                                                <input type="text" class="form-control" name="nama_bank">
                                                            </td>
                                                            <td>
                                                                <select class="form-control" name="status" onChange="refreshContent();">
                                                                    <option value=""></option>
                                                                    <option value="1">Active</option>
                                                                    <option value="0">Inactive</option>
                                                                </select>
                                                                <input type="submit" style="display:none" /> </td>
                                                        </form>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbody_content"> </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </section>
            </div>
            <?php 	include_once "inc_script.php";?>
                <script type="text/javascript" src="js/select2.full.js"></script>
                <div id="myModal" class="fade modal" role="dialog" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4>Edit Jam Buka Bank</h4></div>
                            <div class="modal-body"></div>
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function() {
                        refreshContent();
                        jQuery('#modalcreate').modal({
                            "show": false
                        });
                        $('.modalButton').each(function() {
                            $(this).click(function() {
                                $('#modalcreate').modal('show').find('#modalContent').load($(this).attr('value'));
                            });
                        });
                        jQuery('#myModal').modal({
                            "show": false
                        });
                        $('#myModal').on('show.bs.modal', function(event) {
                            var button = $(event.relatedTarget);
                            var modal = $(this);
                            var title = button.data('title');
                            var href = button.attr('href');
                            modal.find('.modal-title').html(title);
                            modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>');
                            $.post(href).done(function(data) {
                                modal.find('.modal-body').html(data);
                            });
                        });
                    });
					
					$("#form_search").submit(function( event ) 
					{		
						event.preventDefault();		
						refreshContent();	
					});
                </script>
                <?php 	
					// close	
					mysqli_close($conn);
				?>
    </body>

    </html>