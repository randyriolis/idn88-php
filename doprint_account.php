<?php 
	include_once "inc_login.php";
	include_once "config.php";
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['kdusercat'];

	// all request
	$page = $_REQUEST['page'];
	$kdproduct = $_REQUEST['kdproduct'];
	$username = $_REQUEST['username'];
	$member = $_REQUEST['member'];
	$isactive = $_REQUEST['isactive'];
	$assign = $_REQUEST['assign'];
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	$namalink = $_REQUEST['namalink'];
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){
		// delete account
		$query = "DELETE FROM akuns WHERE kdakun = $delete ";
		$result = mysqli_query($conn, $query);		
	}
	
	$wherequery = "  ";
	if($kdproduct != "")	$wherequery .= " AND gm.nama like '%$kdproduct%' ";
	if($username != "")	$wherequery .= " AND (acc.username LIKE '%$username%' OR acc.pass LIKE '%$username%') ";
	if($member != "")	$wherequery .= " AND (mem.nama LIKE '%$member%' ) ";
	if($isactive != "")	$wherequery .= " AND acc.isactive = $isactive ";
	if($assign != "")	$wherequery .= " AND acc.isassigned = $assign ";
	
	$orderquery = "ORDER BY acc.kdakun DESC ";
	if($sort != "")	
	{
		if($sort == "game_asc")		$orderquery = "ORDER BY acc.kdproduct ASC ";
		else if($sort == "game_desc") $orderquery = "ORDER BY acc.kdproduct DESC ";
		
		else if($sort == "username_asc") $orderquery = "ORDER BY acc.username ASC ";
		else if($sort == "username_desc") $orderquery = "ORDER BY acc.username DESC ";
		
		else if($sort == "member_asc") $orderquery = "ORDER BY mem.nama ASC ";
		else if($sort == "member_desc") $orderquery = "ORDER BY mem.nama DESC ";
		
		else if($sort == "isactive_asc") $orderquery = "ORDER BY acc.isactive ASC ";
		else if($sort == "isactive_desc") $orderquery = "ORDER BY acc.isactive DESC ";
		
		else if($sort == "assign_asc") $orderquery = "ORDER BY acc.isassigned ASC ";
		else if($sort == "assign_desc") $orderquery = "ORDER BY acc.isassigned DESC ";		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT acc.*, bk.inisialbank AS bank, mem.nama, mem.norek AS no_rekening, gm.nama AS nama_game FROM ".
				   "akuns acc ".
				   "LEFT JOIN members mem ON acc.kdmember = mem.kdmember ".
				   "LEFT JOIN products gm ON acc.kdproduct = gm.kdproduct ".
				   "LEFT JOIN banks bk ON mem.kdbank = bk.kdbank ".
				   "WHERE 1=1 ".
				   $wherequery.
				   $orderquery.
				   $limitquery;			
				   
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	

?>

	<tr data-key="<?php echo $row['kdakun'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kartik-sheet-style kv-align-middle" style="width:180px;" data-col-seq="1"><?php echo $row['nama_game'] ?></td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="2"><?php echo $row['username'] ?> | <?php echo $row['pass'] ?></td>		
			<?php
				if($row['kdmember'] == null || $row['kdmember'] == 0)
					echo '<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="3"><text class="text-danger"> <em>not set</em></text></td>';
				else
					echo '<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="3">'.$row['nama'].'<br />'.$row['no_rekening'].' ('.$row['bank'].')</td>';				
			?>			
		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="4">
			<?php
				if($row['isactive'] == 1)	echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>			
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="5">
			<?php
				if($row['isassigned'] == 1)	echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="6">
			<?php 
				if($row['kdmember'] == null || $row['kdmember'] == 0)
				{
					?>
					<a href="#" onClick="deleteAccount(<?php echo $row['kdakun'] ?>); return false;">
						<span class="glyphicon glyphicon-trash"></span>
					</a>
					<?php
				}
			?>
			
		</td>
	</tr>
	
<?php } ?>

<?php
	$query = "SELECT count(acc.kdakun) as num_rows FROM ".
				   "akuns acc ".
				   "LEFT JOIN members mem ON acc.kdmember = mem.kdmember ".
				   "LEFT JOIN products gm ON acc.kdproduct = gm.kdproduct ".
				   "LEFT JOIN banks bk ON mem.kdbank = bk.kdbank ".
				   "WHERE 1=1 ".
				   $wherequery.
				   $orderquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deleteAccount(kdakun)
	{
		var str = "Apakah Kamu yakin delete Account ini ??.";
		var aa = confirm(str);
		$("#delete").val(kdakun);
		
		if(aa)
		{			
			refreshContent();
		}
	}
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>