<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$kdannouncement = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post"){
		
		$isi = $_REQUEST['isi'];
		$isactive = $_REQUEST['isactive'];
		
		$err = "";
		$textSuccess = "";
		
		if($isi == "")		$err .= "<br/>Text Announcement harus diisi";
		
		if($err=="" && $menu == "edit"){
			// Current ID			
			$query = "UPDATE announcements SET isi='$isi', isactive=$isactive WHERE kdannouncement=$kdannouncement ";
			$textSuccess = "Announcement berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		else if($err=="" && $menu == "create"){			
			$query = "INSERT INTO announcements (isi, isactive) VALUES ('$isi', $isactive) ";
			$textSuccess = "Announcement berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
				
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';		
		
		mysqli_close($conn);
		exit();
	}
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM announcements WHERE kdannouncement = $kdannouncement ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Announcement: ".$kdannouncement;
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Announcement";
	}
?>

<div class="announcement-update">
	<h1><?php echo $title ?></h1>
	<form id="form_announcement" class="form-vertical" action="doannouncement_menu.php" method="post">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $kdannouncement ?>" >
		<input type="hidden" name="post" value="post" >
		<div class="row">
			
			<div class="col-sm-4">		
				<div class="form-group field-announcement-url required">
					<label class="control-label" for="announcement-url">Announcement Text</label>
					<textarea rows="10" cols="100" id="announcement-url" class="" name="isi" placeholder="Announcement Text..."><?php echo $row['isi'] ?></textarea>

					<div class="help-block"></div>
				</div>
			</div>
			
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group field-announcement-isactive required">
					<label class="control-label">Status</label>
					<input type="hidden" name="Announcement[isactive]" value="">
					<div id="announcement-isactive">
						<label class="radio-inline"><input type="radio" name="isactive" value="1" <?php if($menu=="create" || $row['isactive']==1) echo 'checked'; ?>> Active</label>
						<label class="radio-inline"><input type="radio" name="isactive" value="0" <?php if($menu=="edit" && $row['isactive']==0) echo 'checked'; ?>> Inactive</label></div>

					<div class="help-block"></div>
				</div>
			</div>
			
		</div>
	
		<div class="row">
			<div class="col-sm-4">
				<div style="text-align: left; margin-top: 20px">
					<div id="feedback_edit"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
	
	</form>
</div>

<?php 
	$formName = '"#form_announcement"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
