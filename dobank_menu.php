<?php 
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$idbank = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post"){
		
		$initial_bank = $_REQUEST['initial_bank'];
		$atas_nama = $_REQUEST['atas_nama'];
		$nomer_rekening = $_REQUEST['nomer_rekening'];
		$status = $_REQUEST['status'];
		$type_bank = $_REQUEST['type_bank'];
		
		$err = "";
		$textSuccess = "";
		
		if($initial_bank == "")		$err .= "<br/>Nama Bank harus diisi";
		if($atas_nama == "")		$err .= "<br/>Atas Nama harus diisi";
		if($nomer_rekening == "")		$err .= "<br/>Nomer Rekening harus diisi";		
		
		if($err=="" && $menu == "edit"){
			$query = "UPDATE banks SET inisialbank='$initial_bank', atas_nama='$atas_nama', nomer_rekening='$nomer_rekening', isactive=$status, istype=$type_bank WHERE kdbank=$idbank ";
			$textSuccess = "Bank berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		else if($err=="" && $menu == "create"){
			$query = "INSERT INTO banks (inisialbank, atas_nama, nomer_rekening, isactive, istype) VALUES ('$initial_bank', '$atas_nama', '$nomer_rekening', $status, $type_bank) ";
			$textSuccess = "Bank berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
				
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';		
		
		mysqli_close($conn);
		exit();
	}
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM banks WHERE kdbank = $idbank ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Bank: ".$idbank;
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Bank";
	}
?>

<div class="bank-update">
	<h1><?php echo $title ?></h1>
	<form id="form_bank" class="form-vertical" action="dobank_menu.php" method="post">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $idbank ?>" >
		<input type="hidden" name="post" value="post" >
		<div class="row">
			
			<div class="col-sm-4">		
				<div class="form-group field-bank-initial_bank required">
					<label class="control-label" for="bank-initial_bank">Nama Bank</label>
					<input type="text" id="bank-initial_bank" class="form-control" name="initial_bank" placeholder="Nama Bank contoh: BCA, BII, MANDIRI" value="<?php echo $row['inisialbank'] ?>">

					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group field-bank-atas_nama required">
					<label class="control-label" for="bank-atas_nama">Atas Nama</label>
					<input type="text" id="bank-atas_nama" class="form-control" name="atas_nama" placeholder="Atas Nama..." value="<?php echo $row['atas_nama'] ?>">
					
					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">	
				<div class="form-group field-bank-nomer_rekening required">
					<label class="control-label" for="bank-nomer_rekening">Nomer Rekening</label>
					<input type="text" id="bank-nomer_rekening" class="form-control" name="nomer_rekening" placeholder="Nomer Rekening..." value="<?php echo $row['nomer_rekening'] ?>">

					<div class="help-block"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-4">
				<div class="form-group field-bank-status required">
					<label class="control-label">Status</label>
					<input type="hidden" name="Bank[status]" value="">
					<div id="bank-status">
						<label class="radio-inline"><input type="radio" name="status" value="1" <?php if($menu=="create" || $row['isactive']==1) echo 'checked'; ?>> Active</label>
						<label class="radio-inline"><input type="radio" name="status" value="0" <?php if($menu=="edit" && $row['isactive']==0) echo 'checked'; ?>> Inactive</label></div>

					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group field-bank-type required">
					<label class="control-label">Type</label>
					<input type="hidden" name="Bank[type]" value="">
					<div id="bank-type">
						<label class="radio-inline"><input type="radio" name="type_bank" value="0" <?php if($menu=="create" || $row['istype']==0) echo 'checked'; ?>> Transfer</label>
						<label class="radio-inline"><input type="radio" name="type_bank" value="1" <?php if($menu=="edit" && $row['istype']==1) echo 'checked'; ?>> E-Wallets</label>
						<label class="radio-inline"><input type="radio" name="type_bank" value="2" <?php if($menu=="edit" && $row['istype']==2) echo 'checked'; ?> disabled> Digital</label>
					</div>
					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div style="text-align: right; margin-top: 20px">
					<div id="feedback_edit"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>

		</div>
	
	</form>
</div>

<?php 
	$formName = '"#form_bank"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
