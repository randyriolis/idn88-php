<?php 
	session_start();
	include "config.php";
	include "inc_login.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM promos WHERE kdpromo = $delete ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$judul = $_REQUEST['judul'];
	$isi = $_REQUEST['isi'];
	$kodeinpromo = $_REQUEST['kodeinpromo'];
	$isactive = $_REQUEST['isactive'];
	$gambarpromo = $_REQUEST['gambarpromo'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = "";
	
	if($judul != "")	$wherequery .= " AND judul LIKE '%$judul%' ";
	if($isi != "")	$wherequery .= " AND isi LIKE '%$isi%' ";
	if($kodeinpromo != "")	$wherequery .= " AND kodeinpromo like '%$kodeinpromo%' ";
	if($isactive != "")	$wherequery .= " AND isactive = $isactive ";
	if($gambarpromo != ""){
		if($gambarpromo == 1)	$wherequery .= " AND gambarpromo IS NOT NULL AND gambarpromo != '' ";
		else $wherequery .= "  AND (gambarpromo IS NULL OR gambarpromo = '') ";
	}	
	
	
	$orderquery = "ORDER BY kdpromo DESC ";
	if($sort != "")	
	{
		if($sort == "isi_asc")		$orderquery = "ORDER BY isi ASC ";
		else if($sort == "isi_desc") $orderquery = "ORDER BY isi DESC ";
		
		else if($sort == "kodeinpromo_asc")		$orderquery = "ORDER BY kodeinpromo ASC ";
		else if($sort == "kodeinpromo_desc") $orderquery = "ORDER BY kodeinpromo DESC ";
		
		else if($sort == "isactive_asc")		$orderquery = "ORDER BY isactive ASC ";
		else if($sort == "isactive_desc") $orderquery = "ORDER BY isactive DESC ";
		
		else if($sort == "judul_asc")		$orderquery = "ORDER BY judul ASC ";
		else if($sort == "judul_desc") $orderquery = "ORDER BY judul DESC ";
		
		else if($sort == "gambarpromo_asc")		$orderquery = "ORDER BY gambarpromo ASC ";
		else if($sort == "gambarpromo_desc") 	$orderquery = "ORDER BY gambarpromo DESC ";
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT * FROM promos WHERE kdpromo > 0  ".$wherequery.$orderquery.$limitquery;
	echo $query;
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	

?>
	<tr data-key="<?php echo $row['kdpromo'] ?>">
		<td class="kartik-sheet-style kv-align-center " style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kv-align-center " data-col-seq="1"><?php echo $row['judul'] ?></td>
		<td data-col-seq="2" >
			<div class="no_scroll" style="max-height:120px; overflow: scroll; border-bottom: 1px solid #ccc;">
			<?php echo $row['isi'] ?>
			</div>
		</td>
		<td data-col-seq="3"><?php echo $row['kodeinpromo'] ?></td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="4">
			<?php 
				if($row['deposit'] == 1) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>			
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="5">	
			<?php 
				if($row['withdraw'] == 1) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>	
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="6">
			<?php 
				if($row['isactive'] == 1) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>	
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="6">
			<?php 
				if($row['gambarpromo'] != NULL && $row['gambarpromo'] != "" ) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>	
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="dopromo_menu.php?menu=edit&id=<?php echo $row['kdpromo'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="8">
			<a class="btn btn-danger btn-xs" href="#" onClick="deletePromo(<?php echo $row['kdpromo'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(kdpromo) as num_rows FROM promo WHERE kdpromo > 0 ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deletePromo(idaccount)
	{
		var str = "Apakah Kamu yakin delete Promo ini ?? Ilang loh Promonya??";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>


<style>
	.no_scroll::-webkit-scrollbar {
        width: 0 !important;
    }
</style>