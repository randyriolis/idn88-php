<?php
	include_once "config.php";
	include_once "function.php";
	include_once "inc_login.php";

	$err = array();
	$arrstyle = array();	
	$arrstyle2 = array();
	
	$post = $_REQUEST['post'];
	if(!empty($post)){
		$date_start = $_REQUEST['date_start'];
		$date_start = strtotime($date_start);		
		
		// Success
		if(empty($date_start) == false) {
			
			$query2 = "UPDATE members SET tlp='000' WHERE tanggal_daftar <= '$date_start' ";
			$resQuery2 = mysqli_query($conn, $query2);
			
			if($resQuery2)
				$queryok = true;
			
		}
	}
	
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Hapus Nomor HP</title>    	
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "nonohp";
		include_once "inc_header.php";
	?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1></h1>        
			<ul class="breadcrumb"><li><a href="site.php">Home</a></li>
				<li class="active">Hapus No.HP</li>
			</ul>    
		</section>

		<section class="content">
			<div class="change-own-nohp">
				<h2 class="lte-hide-title">Hapus No.HP</h2>
				<div class="panel panel-default">
					<div class="panel-body">
		
						<div class="user-form">
							<form id="user" class="form-horizontal" action="" method="post" role="form">
								<input type="hidden" name="post" value="post">
								
								<?php 
									if( empty($queryok) == false) {
										echo '<div style="background-color:lightgreen; text-align: center; padding: 5px; margin-bottom: 15px;">Ganti nomor HP berhasil</div>';										
									}
								?>
								
								<div class="form-group required">
									<label class="control-label col-sm-3" for="changeownnohpform-current_nohp"  >Tanggal</label>
									<div class="col-sm-6">
										<input class="form_date" type="text" name="date_start" id="date_start" value="" autocomplete="off" />
										
										<div class="help-block help-block-error "></div>
									</div>
								</div>			
								
								<div class="clearfix"></div>
								
								<div class="form-group">
									<div class="col-sm-offset-3 col-sm-9">
										<button type="submit" class="btn btn-primary">Hapus</button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>

			</div>
		</section>
	</div>

<?php 
	include_once "inc_script.php";
?>
<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		refreshContent();
		
		jQuery('#modalcreate').modal({"show":false});
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
		$(".js-example-basic-single").select2();
		$("#loadingGif_deposit").css("display","none");
		
		$( ".form_date" ).datepicker({
			dateFormat: 'dd-mm-yy'
		});
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});	
	
	
</script>
<?php 
	// close
	mysqli_close($conn);
?>

</body>

</html>