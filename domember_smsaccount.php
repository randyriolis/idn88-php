<?php 
	include_once "inc_login.php";
	include "config.php";
	include "function.php";

	$id = $_REQUEST['id'];
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['kdusercat'];
	
	if(empty($id) )	$id = 1;
	
	$query = "SELECT mem.*, acc.username, acc.pass, ln.url AS url_link, ln.waofficial, gm.url AS url_game FROM ".
				   "akuns acc ".
				   "LEFT JOIN members mem ON acc.kdmember = mem.kdmember ".
				   "LEFT JOIN link ln ON mem.idlink = ln.idlink  ".
				   "LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ".
				   "WHERE mem.kdmember = $id ";
	$result = mysqli_query($conn, $query);	
	$row = mysqli_fetch_assoc($result);

	$sms_format = "UserID : ".$row['username'].". Password : ".$row['pass'].".\nSelamat bergabung bersama ".STRTOUPPER($row['url_link'])."\nWA Official : ".$row['waofficial']."\nLink login : ".$row['url_game'];
	
	$p= $_REQUEST['p'];
	if($p == 'post')
	{
		// process to send sms
		$msg = $_REQUEST["formatsms"];
		//$msg = str_replace(" ", "%20", $msg);
		//$msg = str_replace("\n", "%0A", $msg);
		$tlp = formatTelp($row['tlp']);

		$toAddressEmail = $row['email'];
		$toEmailName = $row['name'];
		$contentEmail = $_REQUEST["formatsms"];
		$titleEmail = $row['namalink']." Account Info";
		$user = $_REQUEST["username"];
		$pass = $_REQUEST['password'];
		$link = ($_REQUEST['link']=='') ? '-' : $_REQUEST['link'];
		$wao = ($_REQUEST['waofficial']=='') ? '-' : $_REQUEST['waofficial'];
		$game = ($_REQUEST['game']=='') ? '-' : $_REQUEST['game'];
		$wa = str_replace("+", "", $tlp);


		if(isset($_POST['send']) && in_array('sms', $_POST['send'])) {
			// proses kirim sms gateway twilio
			//twilioSendSms($tlp, $msg);			
			messagebirdSendSms($tlp, $msg);
		}

		if(isset($_POST['send']) && in_array('email', $_POST['send'])) {
			// proses kirim email gateway sendgrid
			twilioSendgrid($toAddressEmail, $toEmailName, $contentEmail, $titleEmail);
		} 	

		if(isset($_POST['send']) && in_array('whatsapp', $_POST['send'])) {
			// proses kirim whatsapp twilio
		 	//twilioSendWhatsapp($tlp, $msg);	
		 	messagebirdSendWhatsappTemplate($wa, $user, $pass, strtoupper($link), $wao, $game);
		}
		
		//mysqli_query($conn, "INSERT INTO zlog_member (idmember, iduser, keterangan, datecreate) VALUES ($id, $iduser, 'Melakukan SMS InfoAccount kepada member', $current_timestamp)");
		
		exit();
	}
?>


<div class="member-sms">    
	<div class="member-sms">

	<form id="form_smsaccount" class="form-vertical" action="domember_smsaccount.php?id=<?php echo $id ?>&p=post" method="post">
	<input type="hidden" name="_csrf" value="MmZmbG55V25.FxEKBx0aDHs0PlojTgAvYTMKFDQuJS17IQ0lLA4kGg==">    
	<input type="hidden" name="username" value="<?php echo $row['username'] ?>">
	<input type="hidden" name="password" value="<?php echo $row['pass'] ?>">
	<input type="hidden" name="link" value="<?php echo $row['url_link'] ?>">
	<input type="hidden" name="game" value="<?php echo $row['url_game'] ?>">
	<input type="hidden" name="waofficial" value="<?php echo $row['waofficial'] ?>">

	<p>
			SMS & WA ke : <?php echo $row['username'] ?> ( 
			<?php 
				$tlp = $row['tlp'];
				$tlp = substr($tlp, 0, strlen($tlp)-3);
				$tlp = $tlp . "***";
				echo $tlp;
			?>
			)
			<br/>
			E-mail ke 2: 
			<?php 
				// email replace
				$email = $row["email"];
				$mail_parts = explode("@", $email);
				$length = strlen($mail_parts[0]);
				$show = $length - 3;
				$hide = $length - $show;
				$replace = str_repeat("x", $hide);
				$email = substr_replace ( $mail_parts[0] , $replace , $show, $hide ) . "@" . substr_replace($mail_parts[1], "xxx", 0, 3);
				
				echo $email;
			?> 	
			
		</p>
		<div class="form-group field-member-formatsms">
			<label class="control-label" for="member-formatsms">Isi SMS</label>
			<textarea id="member-formatsms" class="form-control" name="formatsms" rows="4"><?php echo $sms_format ?></textarea>
			<input type="checkbox" name="send[]" value="sms" checked="checked"> Send SMS<br>
			<input type="checkbox" name="send[]" value="email"> Send Email<br>
			<input type="checkbox" name="send[]" value="whatsapp"> Send WhatsApp
			<div class="help-block"></div>
		</div>
		<div class="form-group">
			<div id="feedback_1"></div>
			<button type="submit" class="btn btn-primary">SEND SMS</button>  
				<img class="thisLoadingGif" src="img/loading.gif" />
		</div>

	</form>
	</div>
</div>

<?php 
	$formName = '"#form_smsaccount"';
	$feedback = '"#feedback_1"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>