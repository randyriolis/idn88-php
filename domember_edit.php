<?php 
	include_once "inc_login.php";
	include "config.php";
	$id = $_REQUEST['id'];	
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['kdusercat'];
	if( $levelAdmin == 0)	header("location: site.php");
	
	$query = "SELECT mem.*, acc.username, acc.pass, gm.nama AS nama_game, bk.istype AS type_bank FROM ".
				   "members mem ".
				   "LEFT JOIN akuns acc ON acc.kdmember = mem.kdmember ".
				   "LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ".
				   "LEFT JOIN banks bk ON bk.kdbank = mem.kdbank ".
				   "WHERE mem.kdmember = $id ";
	$result = mysqli_query($conn, $query);	
	$row = mysqli_fetch_assoc($result);
	
	$p= $_REQUEST['p'];
	if($p == 'post')
	{
		// process to create member
		$nama = $_REQUEST["nama"];
		$no_rekening = $_REQUEST["no_rekening"];
		$bank = ($_REQUEST["bank"]==9999) ? $_REQUEST['ewallet'] : $_REQUEST['bank'];
		$idsumber = $_REQUEST["idsumber"];
		$sumber = ($idsumber >= 6) ? $_REQUEST["sumber"] : '';
		$email = $_REQUEST["email"];
		$status = $_REQUEST["status"];
		$idpromo = $_REQUEST["idpromo"];
		$keterangan = $_REQUEST["keterangan"];
		$iduser = $_SESSION['alpha_iduser'];
		
		$tmp = "UPDATE members SET nama='$nama', norek='$no_rekening', kdbank='$bank', idsumber='$idsumber', sumber='$sumber', email='$email', isactive=$status, kdpromos=$idpromo, ket='$keterangan' WHERE kdmember = $id ";
		$res = mysqli_query($conn, $tmp);
		
		mysqli_query($conn, "INSERT INTO zlog_member (kdmember, kduser, keterangan, datecreate) VALUES ($id, $iduser, 'Update Information Member', current_timestamp)");
		
		if($res)			echo '<span style="color:green"> Sukses.<br/>Member berhasil di-edit</span><br/>';
		else 			echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
		
		exit();
	}
?>
<h1>Update Member: <?php echo htmlentities($row['nama']) ?></h1>
<h4><strong>Game :</strong> <?php echo htmlentities($row['nama_game']) ?>
</h4>
<h4><strong>Account :</strong> <?php echo htmlentities($row['username']) ?> </h4>

<form id="form_edit" class="form-vertical" action="domember_edit.php?id=<?php echo $id ?>&p=post" method="post">
	
	<fieldset id="w2">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group field-member-nama">
					<label class="control-label" for="member-nama">Nama</label>
					<input type="text" id="member-nama" class="form-control" name="nama" value="<?php echo htmlentities($row['nama']) ?>" placeholder="Nama Rekening...">
					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-4">				
				<div class="form-group field-member-no_rekening">
					<label class="control-label" for="member-no_rekening">No Rekening</label>
					<input type="text" id="member-no_rekening" class="form-control" name="no_rekening" value="<?php echo htmlentities($row['norek']) ?>" placeholder="Nomer Rekening...">
					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div class="form-group field-member-bank">
					<label class="control-label" for="member-bank">Bank</label>
					
					<select id="member-bank" class="form-control" name="bank">
						<?php							
							$sqlbank = "SELECT * FROM banks WHERE isactive=1 AND istype=0 ORDER BY kdbank ASC";
							$querybank = mysqli_query($conn, $sqlbank);
							
							while($abc=mysqli_fetch_array($querybank)){
								$sel = ($abc['kdbank'] == $row['kdbank'])? 'selected' : '';
								echo '<option '.$sel.' value="'.$abc['kdbank'].'">'.$abc['inisialbank'].'</option>';
							}

							$result=mysqli_query($conn,"SELECT COUNT(*) AS jumlah FROM banks WHERE istype=1");
							$data=mysqli_fetch_assoc($result);
							if ($data['jumlah']>0) {
								$sel = ($row['type_bank'] == 1)? 'selected' : '';
								echo '<option '.$sel.' value="9999">E-WALLETS</option>';
							}
						?>
					</select>
					<div id="bank_ewallet" style="margin-top: 5px; display: none">
						<select id="ewallet" class="form-control" name="ewallet">
							<?php
								$sqlwallet = "SELECT * FROM banks WHERE isactive=1 AND istype=1 ORDER BY kdbank ASC";
								$querywallet = mysqli_query($conn, $sqlwallet);

								while($cde=mysqli_fetch_array($querywallet)){
									$sel = ($cde['kdbank'] == $row['kdbank'])? 'selected' : '';
									echo '<option '.$sel.' value="'.$cde['kdbank'].'">'.$cde['inisialbank'].'</option>';
								}
							?>
						</select>
					</div> 
					
					<div class="help-block"></div>
				</div>
			</div>
		</div>
	</fieldset>
	
	<fieldset id="w3">
		<div class="row">
			
			<div class="col-sm-4">
				<div class="form-group field-member-email">
					<label class="control-label" for="member-email">Email</label>
					<input type="text" id="member-email" class="form-control" name="email" value="<?php echo htmlentities($row['email']) ?>" placeholder="Masukan E-mail...">
					<div class="help-block"></div>
				</div>
			</div>			

			<div class="col-sm-4">
				<div class="form-group field-member-idsumber">
					<label class="control-label" for="member-idsumber">Sumber</label>
					
					<select id="sumber-info" class="form-control" name="idsumber">
						<?php
							$sumbers = array(
								"1" => "Google", 
								"2" => "Facebook", 
								"3" => "Instagram", 
								"4" => "Twitter", 
								"5" => "WhatsApp",
								"6" => "Refferal",
								"7" => "Lainnya"
							);

							foreach($sumbers as $code => $item){
								$sel = ($code == $row['idsumber'])? 'selected' : '';
								echo '<option '.$sel.' value="'.$code.'">'.$item.'</option>';
							}
						?>
					</select>
					<div id="sumber_text" style="margin-top: 5px; display: none;">
						<input type="text" id="member-sumber" class="form-control" name="sumber" value="<?php echo htmlentities($row['sumber']) ?>" placeholder="Sumber Info..." maxlength="20">
					</div>
					
					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">		
				<div class="form-group field-member-status">
					<label class="control-label">Status</label>
					<input type="hidden" name="status" value="">
					<div id="member-status">
						<label class="radio-inline"> <input type="radio" name="status" value="1" <?php if($row['isactive'] == 1) echo 'checked' ?>> Active</label>
						<label class="radio-inline"> <input type="radio" name="status" value="0" <?php if($row['isactive'] == 0) echo 'checked' ?>> Inactive</label>
					</div>

					<div class="help-block"></div>
				</div>
			</div>
		</div>
	</fieldset>
	
	<fieldset id="w4">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group field-member-idpromo">
					<label class="control-label" for="member-idpromo">Promo</label>
					<select id="member-idpromo" class="form-control" name="idpromo">
						<option value="0" <?php if($row['kdpromos'] == 0 || $row['kdpromos'] == NULL) echo 'selected'; ?>>Tidak Pilih Promo</option>
						<option value="1" <?php if($row['kdpromos'] == 1) echo 'selected'; ?>>Depo 50%</option>
						<option value="5" <?php if($row['kdpromos'] == 5) echo 'selected'; ?>>Bonus Sportbook 50%</option>
					</select>
					
					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group field-member-keterangan">
					<label class="control-label" for="member-keterangan">Keterangan</label>
					<textarea id="member-keterangan" class="form-control" name="keterangan" placeholder="Keterangan bila perlu..."><?php if($row['ket'] != NULL) echo $row['ket'] ?></textarea>
					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-4">
				<div style="text-align: right; margin-top: 20px">
					<div id="feedback_edit"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>

	</fieldset>
</form>

<script>
$(document).ready(function () {
    var select = document.getElementById("sumber-info");
    select.onchange = function () {
        if (select.value >= "6") {
            document.getElementById("sumber_text").style.display = "inherit";
            if (select.value == "6") {
                $("#member-sumber").attr("placeholder", "Masukan id/user refferal");
            } else if (select.value == "7") {
                $("#member-sumber").attr("placeholder", "Masukan sumber info");
            }
        } else {
            document.getElementById("sumber_text").style.display = "none";
        }
    };
    if (select.value >= "6") {
        document.getElementById("sumber_text").style.display = "inherit";
    }

	var selectBank = document.getElementById("member-bank");
	selectBank.onchange = function () {
		if (selectBank.value == "9999") {
	        document.getElementById("bank_ewallet").style.display = "inherit";
		} else {
	        document.getElementById("bank_ewallet").style.display = "none";
		}
	};
    if (selectBank.value == "9999") {
        document.getElementById("bank_ewallet").style.display = "inherit";
    }
});
</script>

<?php 
	$formName = '"#form_edit"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>

