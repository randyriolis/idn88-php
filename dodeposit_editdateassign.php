<?php
	include "config.php";
	
	$kddeposit = $_REQUEST['id'];
	$p= $_REQUEST['p'];
	
	$sql_datecreate = "CONVERT_TZ(from_unixtime(dp.tanggal,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00')";
	$sql_dateassign = "CONVERT_TZ(from_unixtime(dp.cretime,'%Y-%m-%d %H:%i:%s') , @@session.time_zone, '+7:00')";
	
	$query = "SELECT dp.*, mem.kdmember, mem.nama, mem.tlp, mem.email, acc.kdakun, acc.username, dp.clearby AS nama_admin, dp.tanggal".
					", gm.nama AS nama_game, bk.inisialbank AS namabank, ln.namalink ".
				   ",$sql_datecreate as new_datecreate ".
					", $sql_dateassign as new_dateassign ".
				   "FROM deposits dp ".
				   "INNER JOIN members mem ON dp.kdmember=mem.kdmember ".
				   "INNER JOIN akuns acc ON mem.kdmember=acc.kdmember ".
				   "INNER JOIN products gm ON mem.kdproduct=gm.kdproduct ".
				   "LEFT JOIN link ln ON dp.idlink = ln.idlink ".			// Update terakhir, jadi semua link cm pake game dari gilabet || tapi data lama kan kagak..
				   "LEFT JOIN banks bk ON mem.kdbank = bk.kdbank ".
				   "WHERE kddeposit='$kddeposit' ";				   
				   //echo $query;
				   //exit;
	$tmp = mysqli_fetch_array(mysqli_query($conn, $query));		
	
	if($p == 'post')
	{	
		$original = strtotime($_REQUEST['original']);
		$tanggal = strtotime($_REQUEST['tanggal']);
		
		$diff = $tanggal - $original;
		
		$edits = "UPDATE deposits SET cretime = cretime + $diff WHERE kddeposit = '$kddeposit' ";
		$res = mysqli_query($conn, $edits);
		
		if($res)			echo '<span style="color:green"> Sukses.<br/>Deposit berhasil di-edit</span><br/>';
		else 			echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
		
		exit;
	}
?>

<form id="form_editdeposit" class="form-vertical" action="dodeposit_editdateassign.php?p=post" method="post">
	<fieldset id="w2">
		<div class="row">
			<div class="col-sm-12">	
				<div class="form-group field-dpconfirm_username required">
					<input type="hidden" name="id" id="id" value="<?php echo $kddeposit ?>" />
					<input type="hidden" name="menu" id="menu" value="<?php echo $menu ?>" />
					<input type="hidden" name="original" id="original" value="<?php echo date("d-m-Y H:i:s", strtotime($tmp['new_dateassign'])) ?>" />
					
					<b>
					<?php
						echo $infoTop;				
					?>
					</b>
					<br/>
					<div class="col-sm-5"><b>Username:</b></div>
					<div class="col-sm-7"><?php echo $tmp['username'] ?> - <?php echo $tmp['nama'] ?> </div>
					
					<div class="col-sm-5"><b>No.Rekening:</b></div>
					<div class="col-sm-7"><?php echo $tmp['norek'] ?></div><br/>
					
					<div class="col-sm-5"><b>Game:</b></div>
					<div class="col-sm-7"><?php echo $tmp['nama_game'] ?></div>
					
					<div class="col-sm-5"><b>Website:</b></div>
					<div class="col-sm-7"><?php echo $tmp['namalink'] ?></div><br/>
					
					<div class="col-sm-5">
						<b>
						<?php 
							echo 'Jumlah: ';							
						?>
						</b>
					</div>
					<div class="col-sm-7"><?php echo number_format($tmp['jumlah'], 0) ?></div>
					
					<div class="clearfix"></div>
					
					<div class="col-sm-5"><b>Tanggal Assign: </b></div>
					<div class="col-sm-7"><?php echo date("d-m-Y H:i:s", strtotime($tmp['new_dateassign'])) ?></div>
					
					<br/><br/>
					<div class="help-block"></div>
				</div>

			</div>		
			
		</div>
		
		<div class="row">
			
			<div class="col-sm-6">
				<div class="form-group field-transaksi-tanggal required">
					<label class="control-label" for="transaksi-tanggal">Tanggal</label>
					<input type="text" id="transaksi-tanggal" class="form-control" name="tanggal" placeholder="Tanggal" style="width:100%;" value="<?php echo date("d-m-Y H:i:s", strtotime($tmp['new_dateassign'])) ?>">
					<div style="color:#999"><i>Format tanggal: <br/>hari-bulan-tahun jam:menit:detik</i></div>
					<div class="help-block"></div>
				</div>							
			</div>
			
			<div class="col-sm-12">
				<div style="text-align: right; margin-top: 20px">
					<div id="feedback_create"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		
		</div>
		
	</fieldset>
	
</form>


<?php 
	$formName = '"#form_editdeposit"';
	$feedback = '"#feedback_create"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>

