<?php 
	session_start();
	include "config.php";
	include "inc_login.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM berita WHERE idberita = $delete ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$judul_berita = $_REQUEST['judul_berita'];
	$isi_berita = $_REQUEST['isi_berita'];
	$deposit = $_REQUEST['deposit'];
	$withdraw = $_REQUEST['withdraw'];
	$status = $_REQUEST['status'];
	$imagelink = $_REQUEST['imagelink'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = "";
	
	if($judul_berita != "")	$wherequery .= " AND judul_berita LIKE '%$judul_berita%' ";
	if($isi_berita != "")	$wherequery .= " AND isi_berita LIKE '%$isi_berita%' ";
	if($deposit != "")	$wherequery .= " AND deposit = $deposit ";
	if($withdraw != "")	$wherequery .= " AND withdraw = $withdraw ";
	if($status != "")	$wherequery .= " AND status = $status ";
	if($imagelink != ""){
		if($imagelink == 1)	$wherequery .= " AND imagelink IS NOT NULL AND imagelink != '' ";
		else $wherequery .= "  AND (imagelink IS NULL OR imagelink = '') ";
	}	
	
	
	$orderquery = "ORDER BY idberita DESC ";
	if($sort != "")	
	{
		if($sort == "isi_berita_asc")		$orderquery = "ORDER BY isi_berita ASC ";
		else if($sort == "isi_berita_desc") $orderquery = "ORDER BY isi_berita DESC ";
		
		else if($sort == "deposit_asc")		$orderquery = "ORDER BY deposit ASC ";
		else if($sort == "deposit_desc") $orderquery = "ORDER BY deposit DESC ";
		
		else if($sort == "withdraw_asc")		$orderquery = "ORDER BY withdraw ASC ";
		else if($sort == "withdraw_desc") $orderquery = "ORDER BY withdraw DESC ";
		
		else if($sort == "no_rekening_asc")		$orderquery = "ORDER BY mem.no_rekening ASC ";
		else if($sort == "no_rekening_desc") $orderquery = "ORDER BY mem.no_rekening DESC ";
		
		else if($sort == "status_asc")		$orderquery = "ORDER BY status ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY status DESC ";
		
		else if($sort == "judul_berita_asc")		$orderquery = "ORDER BY judul_berita ASC ";
		else if($sort == "judul_berita_desc") $orderquery = "ORDER BY judul_berita DESC ";
		
		else if($sort == "imagelink_asc")		$orderquery = "ORDER BY imagelink ASC ";
		else if($sort == "imagelink_desc") 	$orderquery = "ORDER BY imagelink DESC ";
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT * FROM berita WHERE idberita > 0 AND idlink=$idlink  ".$wherequery.$orderquery.$limitquery;
	echo $query;
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	

?>
	<tr data-key="<?php echo $row['idberita'] ?>">
		<td class="kartik-sheet-style kv-align-center " style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kv-align-center " data-col-seq="1"><?php echo $row['judul_berita'] ?></td>
		<td data-col-seq="2"><?php echo $row['isi_berita'] ?></td>		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="6">
			<?php 
				if($row['status'] == 1) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>	
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="6">
			<?php 
				if($row['imagelink'] != NULL && $row['imagelink'] != "" ) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>	
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="doberita_menu.php?menu=edit&id=<?php echo $row['idberita'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="8">
			<a class="btn btn-danger btn-xs" href="#" onClick="deleteberita(<?php echo $row['idberita'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(idberita) as num_rows FROM berita WHERE idberita > 0 AND idlink=$idlink ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deleteberita(idaccount)
	{
		var str = "Apakah Kamu yakin delete berita ini ?? Ilang loh beritanya??";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>