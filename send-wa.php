<?php

require_once __DIR__.'/vendor/autoload.php';

$messageBird = new MessageBird\Client('Rs9Nx7c7AhtCRlUqmJaa7X8mj');

//$messageBird = new MessageBird\Client('HSiWwHeG2zyltYIYiDgpcdDDf', null, [MessageBird\Client::ENABLE_CONVERSATIONSAPI_WHATSAPP_SANDBOX]);

// Enable the whatsapp sandbox feature
//$messageBird = new MessageBirdClient(
//    Rs9Nx7c7AhtCRlUqmJaa7X8mj',
//    null,
//    [MessageBirdClient::ENABLE_CONVERSATIONSAPI_WHATSAPP_SANDBOX]
//);

$hsmParam1 = new \MessageBird\Objects\Conversation\HSM\Params();
$hsmParam1->default = '558752334';

$hsmLanguage = new \MessageBird\Objects\Conversation\HSM\Language();
$hsmLanguage->policy = \MessageBird\Objects\Conversation\HSM\Language::DETERMINISTIC_POLICY;
//$hsmLanguage->policy = \MessageBird\Objects\Conversation\HSM\Language::FALLBACK_POLICY;
$hsmLanguage->code = 'en';

$hsm = new \MessageBird\Objects\Conversation\HSM\Message();
$hsm->templateName = 'kirim_pesan_member';
$hsm->namespace = '79ba2be1_a92c_45a9_a88e_87dc5e65cb91';
$hsm->params = [$hsmParam1];
$hsm->language = $hsmLanguage;

$content = new \MessageBird\Objects\Conversation\Content();
$content->hsm = $hsm;

$message = new \MessageBird\Objects\Conversation\Message();
$message->channelId = 'cf12d5f5-f909-4403-ab59-cef44cfc83b3';
$message->content = $content;
$message->to = '628983869801';
$message->type = 'hsm';

try {
    $conversation = $messageBird->conversations->start($message);

    var_dump($conversation);
} catch (\Exception $e) {
    echo sprintf("%s: %s", get_class($e), $e->getMessage());
}