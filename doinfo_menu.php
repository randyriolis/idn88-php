<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['menu'];
	$idinfo = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post"){
		
		$namainfo = $_REQUEST['namainfo'];
		$contentinfo = $_REQUEST['contentinfo'];
		$keterangan = "-";
		
		$err = "";
		$textSuccess = "";
		
		if($namainfo == "")		$err .= "<br/>Nama Info harus diisi";
		if($contentinfo == "")		$err .= "<br/>Content Info harus diisi";
		
		if($err=="" && $menu == "edit"){
			$query = "UPDATE info SET namainfo='$namainfo', contentinfo='$contentinfo', keterangan='$keterangan' WHERE idinfo=$idinfo ";		
			$textSuccess = "Info berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		else if($err=="" && $menu == "create"){
			$query = "INSERT INTO info (namainfo, contentinfo, keterangan) VALUES ('$namainfo', '$contentinfo', '') ";
			$textSuccess = "Info berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
				
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';		
		
		mysqli_close($conn);
		exit();
	}
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM info WHERE idinfo = $idinfo ");
		$row= mysqli_fetch_array($query);		
		
		$title = "Update Info: ".$idinfo;
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Info";
	}
?>

<div class="info-update">
	<h1><?php echo $title ?></h1>
	<form id="form_info" class="form-vertical" action="doinfo_menu.php" method="post">
		<input type="hidden" name="menu" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $idinfo ?>" >
		<input type="hidden" name="post" value="post" >
		<div class="row">
			
			<div class="col-sm-6">		
				<div class="form-group field-info-namainfo required">
					<label class="control-label" for="info-namainfo">Nama Info</label>
					<input type="text" id="info-namainfo" class="form-control" name="namainfo" placeholder="Nama Info => Keyword || Important" value="<?php echo $row['namainfo'] ?>">

					<div class="help-block"></div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group field-info-contentinfo required">
					<label class="control-label" for="info-contentinfo">Content Info</label>
					<textarea style="height:150px;" id="info-contentinfo" class="form-control" name="contentinfo" placeholder="Content Info => Text that will show up" ><?php echo $row['contentinfo'] ?></textarea>
					
					
					<div class="help-block"></div>
				</div>
			</div>
			
		</div>

		<div class="row">
			
			
			<div class="col-sm-4">
				<div style="text-align: left; margin-top: 20px">
					<div id="feedback_edit"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>

		</div>
	
	</form>
</div>

<?php 
	$formName = '"#form_info"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
