<?php 
	include "config.php";
	include "inc_login.php";
	
	if (empty($_POST)){
		echo 'err';
		exit();
	}
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM announcements WHERE kdannouncement = $delete ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$isi = $_REQUEST['isi'];
	$hex_account = $_REQUEST['hex_account'];
	$hex_password = $_REQUEST['hex_password'];
	$url = $_REQUEST['url'];
	$isactive = $_REQUEST['isactive'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = " ";
	
	if($isi != "")	$wherequery .= " AND isi LIKE '%$isi%' ";
	if($hex_account != "")	$wherequery .= " AND hex_account LIKE '%$hex_account%' ";
	if($hex_password != "")	$wherequery .= " AND hex_password like '%$hex_password%' ";
	if($url != "")	$wherequery .= " AND url like '%$url%' ";
	if($isactive != "")	$wherequery .= " AND isactive = $isactive ";
	
	
	$orderquery = "ORDER BY kdannouncement ASC ";
	if($sort != "")	
	{
		if($sort == "isi_asc")		$orderquery = "ORDER BY isi ASC ";
		else if($sort == "isi_desc") $orderquery = "ORDER BY isi DESC ";
		
		else if($sort == "order_display_asc")		$orderquery = "ORDER BY order_display ASC ";
		else if($sort == "order_display_desc") $orderquery = "ORDER BY order_display DESC ";		
				
		else if($sort == "isactive_asc")		$orderquery = "ORDER BY isactive ASC ";
		else if($sort == "isactive_desc") $orderquery = "ORDER BY isactive DESC ";		
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " LIMIT $start, 20 ";
	if($maxrow >= 9999)	$limitquery = "";	
		
	$query = "SELECT * FROM announcements WHERE 1=1  ".$wherequery.$orderquery.$limitquery;
	echo $query;
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		$cnt ++;	

?>
	<tr data-key="<?php echo $row['kdannouncement'] ?>">
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1"><?php echo $row['isi'] ?></td>		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:90px;" data-col-seq="6">
			<?php 
				if($row['isactive'] == 1) echo '<span class="glyphicon glyphicon-ok text-success"></span>';
				else echo '<span class="glyphicon glyphicon-remove text-danger"></span>';
			?>	
		</td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1"><?php echo $row['order_display'] ?></td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="7">
			<a class="btn btn-info btn-xs modalButton" type="button" id="modalButton" value="doannouncement_menu.php?menu=edit&id=<?php echo $row['kdannouncement'] ?>" href="#" onClick="return false;">
				<i class="fa fa-pencil fa-lg" aria-hidden="true"></i><br/>Edit
			</a> 
		</td>
		<td class="kartik-sheet-style skip-export kv-align-center kv-align-middle" style="width:80px;" data-col-seq="8">
			<a class="btn btn-danger btn-xs" href="#" onClick="deletePromo(<?php echo $row['kdannouncement'] ?>); return false;">
			<i class="fa fa-trash fa-lg" aria-hidden="true"></i><br/>Delete</a> 			
		</td>
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(kdannouncement) as num_rows FROM announcement WHERE 1=1  ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deletePromo(idaccount)
	{
		var str = "Apakah Kamu yakin delete Announcement ini ?? ";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>