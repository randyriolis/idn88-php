<?php 
	include_once "inc_login.php";
	include "config.php";
	include "function.php";
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	
	$id = $_REQUEST['id'];	
	$idnotification = $_REQUEST['ntf'];	
	if(empty($idnotification))	
	{
		$querynotif = "SELECT idnotification  
					   FROM notification ntf
					   WHERE idmember = $id 
					   AND (type = 'AjukanLossMember' OR type = 'RevertLossMember') 
					   ORDER BY idnotification DESC 
					   LIMIT 0,1 ";
		$resnotif = mysqli_fetch_array( mysqli_query($conn, $querynotif) );
		
		if(empty($resnotif))		$idnotification = -1;
		else 						$idnotification = $resnotif[0];		
	}
	
	// search member
	$sql = "SELECT mem.* , ln.ispoker, 
			ntf.type, ntf.idfollowup, ntf.new_followup_status   
			, from_unixtime(acc.modtime,'%d-%m-%Y') as new_dateassign 
			, from_unixtime(mem.tanggal_daftar,'%d-%m-%Y') as new_datecreate 
			FROM members mem 
			LEFT JOIN akuns acc ON mem.kdmember = acc.kdmember 
			LEFT JOIN notification ntf ON mem.kdmember = ntf.idmember AND ntf.idnotification = $idnotification 
			INNER JOIN link ln ON mem.idlink = ln.idlink
			WHERE mem.kdmember = $id 
			
			LIMIT 0, 1 ";
			
	$tmp = mysqli_fetch_array(mysqli_query($conn, $sql));		
		
	$p= $_REQUEST['p'];
	if($p == 'post')
	{			
		$temp_id = $id;
		$asal = $_REQUEST["asal"];
		$status = $_REQUEST["status"];
		$notes = $_REQUEST["notes"];
		$idnotification = $_REQUEST["idnotification"];		
		
		$query = "UPDATE members SET followup_asal = '$asal', followup_status = $status WHERE kdmember = $temp_id ";		
		$res = mysqli_query($conn, $query);
		
		// Non-active notification
		if($notes == "AjukanLossMember") 
		{
			$idquery = " AND idnotification = $idnotification ";
							
			// Rejected
			if($status == 2)	{
				$besok = date("Y-m-d", strtotime("tomorrow"));
				$querynotif = "UPDATE notification SET status = 1, type ='TolakLossMember', dateend = '$besok' WHERE idmember = $temp_id $idquery ";
			}
		
			// Approved
			else {				
				$besok = date("Y-m-d", strtotime("tomorrow"));
				$querynotif = "UPDATE notification SET status = 1, type ='SetujuLossMember', dateend = '$besok' WHERE idmember = $temp_id $idquery ";
			}
		
			mysqli_query($conn, $querynotif);			
		}
		
		// Wanted to Revert Member LOSS
		else if($notes == "RevertLossMember")
		{
			// Rejected
			if($status == 10) {
				$besok = date("Y-m-d", strtotime("tomorrow"));
				$querynotif = "UPDATE notification SET status = 1, type ='TolakRevertMember', dateend = '$besok' WHERE idnotification = $idnotification ";
			}
			
			else {
				$besok = date("Y-m-d", strtotime("tomorrow"));
				$querynotif = "UPDATE notification SET status = 1, type ='SetujuRevertMember', dateend = '$besok' WHERE idnotification = $idnotification ";
			}
			
			mysqli_query($conn, $querynotif);	
		}
		
		if($res)		echo '<span style="color:green"> Sukses.<br/>'.$sukses.'</span><br/>';
		else 			echo '<span style="color:red"> Error<br/>Internetnya putus kali nih..</span><br/>';
		
		exit();
	}
	
	
?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<h4>
		<span style="background-color: #333333; color: white; padding: 5px 20px 5px 20px ;">Edit Follow Up Member</span>		
	</h4>
</div>

<form id="form_confirmdeposit" class="form-vertical" action="dofollowup_edit.php?p=post" method="post">
	<fieldset id="w2">
		<div class="row">
			<div class="col-sm-12">	
				<div class="form-group field-dpconfirm_username required">
					<input type="hidden" name="id" id="id" value="<?php echo $id ?>" />		
					<input type="hidden" name="notes" id="notes" value="" />
					<input type="hidden" name="menu" id="menu" value="<?php echo $menu ?>" />
					<input type="hidden" name="tipe" id="tipe" value="<?php echo $tipe ?>" />
					<input type="hidden" name="idnotification" id="idnotification" value="<?php echo $idnotification ?>" />
					
					<br/>
					
					
					<div class="col-sm-4">Username:</div>
					<div class="col-sm-8">
						<?php 
							echo $tmp['username'];
							
						?>
					</div>
					
					<div class="col-sm-4">Nama:</div>
					<div class="col-sm-8">
						<?php 
							if($tmp['ispoker'] == 1)
								echo $tmp['namarek']; 
							else
								echo $tmp['nama']; 
						?>
					</div>
										
					<div class="col-sm-4">Tanggal Join: </div>
					<div class="col-sm-8">
						<?php 
							if($tmp['ispoker'] == 1)
								echo $tmp['new_datecreate']; 
							else
								echo $tmp['new_dateassign']; 
						?>
					</div>
					
					<br/><br/>
					<br/><br/>
					
					<div class="col-sm-6">	
						<div class="field-member-nama required">
							<label class="control-label" for="member-asal">Asal</label>
							<?php 
								$val = $tmp['followup_asal'];
								if($val == "")	$val = "web";
							?>
							<br/>
							<select class="form-control" name="asal">
								<option value="web" <?php if($val == "web") echo 'selected'; ?> >Web</option>
								<option value="livechat" <?php if($val == "livechat") echo 'selected'; ?> >Livechat</option>
								<option value="wa" <?php if($val == "wa") echo 'selected'; ?> >WA</option>
								<option value="facebook" <?php if($val == "facebook") echo 'selected'; ?> >Facebook</option>
								<option value="instagram" <?php if($val == "instagram") echo 'selected'; ?> >Instagram</option>
								<option value="line" <?php if($val == "line") echo 'selected'; ?> >Line</option>
								<option value="referral" <?php if($val == "referral") echo 'selected'; ?> >Referral</option>
							</select>
							
						</div>
					</div>
					
					<div class="clearfix"></div>
					<br/>
					
					<div class="col-sm-12">	
						<div class="field-member-nama required">
							<label class="control-label" for="member-status">Status</label>
							<div class="clearfix"></div>
														
							<?php 
								$followup_status = $tmp["followup_status"];
								$new_followup_status = $tmp["new_followup_status"];
								
								if($followup_status == 99)	$waiting = true;
								else 						$waiting = false;
								
								if(empty($new_followup_status)){
									if($new_followup_status == 0)	{}
									else 			$new_followup_status = -1;
								}
								else $followup_status = $new_followup_status;
								 
																
								if($tmp["type"] != "RevertLossMember" || empty($tmp["type"]))	
								{
									$new_followup_status = -1;									
								}
								
							?>
							
							
							<div class="col-sm-6 radio_status <?php if($followup_status==5) echo 'radio_status_selected' ?>" >
								<label class="normal_label">
								<input type="radio" name="status" value="5" style="display: none" <?php if($followup_status==5) echo 'checked' ?>> 
								<img src="img/followup_blue.png" style="width: 20px" /> Sudah Depo
								</label>
							</div>
							
							<?php if($new_followup_status == 5) { ?>
								<div class="col-sm-6">
									<button class="btn btn-default btn-lossmember" onclick="approveValue('5'); return false;">YES</button>
									<button class="btn btn-default btn-lossmember" onclick="approveRejectRevert(); return false;">NO</button>
								</div>
							<?php } ?>
							
							<div class="clearfix"></div>
							
							<div class="col-sm-6 radio_status  <?php if($followup_status==2) echo 'radio_status_selected' ?>">
								<label class="normal_label">
								<input type="radio" name="status" value="2" style="display: none"  <?php if($followup_status==2) echo 'checked' ?>> 
								<img src="img/followup_red.png" style="width: 20px" /> Kemungkinan Kecil
								</label>
							</div>
							
							<?php if($new_followup_status == 2) { ?>
								<div class="col-sm-6">
									<button class="btn btn-default btn-lossmember" onclick="approveValue('2'); return false;">YES</button>
									<button class="btn btn-default btn-lossmember" onclick="approveRejectRevert(); return false;">NO</button>
								</div>
							<?php } ?>
							
							<div class="clearfix"></div>
							
							<div class="col-sm-6 radio_status <?php if($followup_status==1) echo 'radio_status_selected' ?>">
								<label class="normal_label">
								<input type="radio" name="status" value="1" style="display: none" <?php if($followup_status==1) echo 'checked' ?>> 
								<img src="img/followup_yellow.png" style="width: 20px" /> Kemungkinan Sedang
								</label>
							</div>
							
							<?php if($new_followup_status == 1) { ?>
								<div class="col-sm-6">
									<button class="btn btn-default btn-lossmember" onclick="approveValue('1'); return false;">YES</button>
									<button class="btn btn-default btn-lossmember" onclick="approveRejectRevert(); return false;">NO</button>
								</div>
							<?php } ?>
							
							<div class="clearfix"></div>
							
							<div class="col-sm-6 radio_status <?php if($followup_status==0) echo 'radio_status_selected' ?>">
								<label class="normal_label">
								<input type="radio" name="status" value="0" style="display: none" <?php if($followup_status==0) echo 'checked' ?>> 
								<img src="img/followup_green.png" style="width: 20px" /> Kemungkinan Besar
								</label>
							</div>
							
							<?php if($new_followup_status == 0) { ?>
								<div class="col-sm-6">
									<button class="btn btn-default btn-lossmember" onclick="approveValue('0'); return false;">YES</button> 
									<button class="btn btn-default btn-lossmember" onclick="approveRejectRevert(); return false;">NO</button>									
								</div>
							<?php } ?>

							<div class="clearfix"></div>
							
							<input type="radio" name="status" value="10" style="display: none" <?php if($followup_status==10) echo 'checked' ?>> 
							<input type="radio" name="status" value="99" style="display: none" <?php if($followup_status==99) echo 'checked'; ?> > 
							
							<?php 
								if( ($levelAdmin == 1 || $levelAdmin == 11)) 
								{
							?>
								<div class="col-sm-6 radio_status <?php if($followup_status==10 || $followup_status == 99) echo 'radio_status_selected' ?>">
									<label class="normal_label">
									<img src="img/followup_black.png" style="width: 20px" /> LOSS MEMBER
									</label>
								</div>
								
								<?php 
									if($tmp["type"] == "AjukanLossMember") 
									{
								?>
									<div class="col-sm-6">
										<button class="btn btn-default btn-lossmember" onclick="approveYES(); return false;">YES</button> 
										<button class="btn btn-default btn-lossmember" onclick="approveNO(); return false;">NO</button> 
										
									</div>
								<?php } ?>
								
								<div class="clearfix"></div>
							<?php
								}
							?>
							
						</div>
					</div>
					
					<div class="help-block"></div>
					
					<div class="col-sm-12">
						<div style="text-align: right; margin-top: 20px">
							<div id="feedback_create"></div>
							<img class="thisLoadingGif" src="img/loading.gif" />
							<button type="reset" class="btn btn-default">Reset</button> 
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</div>
					
				</div>

			</div>		
			
		</div>
		
	</fieldset>
	

	<fieldset id="w5">
		<div class="row">					
			<div class="col-sm-8 pull-right">
				<div style="text-align: right; ">
					<div id="feedback_confirm"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<?php 
						if($menu == "approve"){
							echo '<button type="submit" class="btn btn-success">Approve</button>';
						}
						else if($menu == "reject"){
							echo '<button type="submit" class="btn btn-danger">Reject</button>';
						}
					?>
					
				</div>
			</div>
			
		</div>
	</fieldset>
	
</form>


<style>	

.radio_status {
	height: 30px;
	line-height: 30px;
	border: 1px solid white;
}
.radio_status_selected {
	background-color: lightgreen;
	border: 1px solid green;
}
.normal_label {
	font-weight: normal;
}
</style>

<script type="text/javascript" src="js/select2.full.js"></script>

<script type="text/javascript">
	
	$(document).ready(function()
	{
		// $(".js-example-basic-single").select2();
		$(".thisLoadingGif").hide();
		
		$(".normal_label").click(function(){
			// Remove other styling
			$(".radio_status_selected").removeClass("radio_status_selected");
			
			$(this).parent().addClass("radio_status_selected");
		});
	});
	
	
	function approveValue(val)
	{
		$("#notes").val("RevertLossMember");		
		$('input:radio[name=status]').filter('[value='+val+']').prop('checked', true);
		
		$("#form_confirmdeposit").submit();
	}
	function approveRejectRevert() 
	{
		$("#notes").val("RevertLossMember");
		$('input:radio[name=status]').filter('[value=10]').prop('checked', true);
		
		$("#form_confirmdeposit").submit();
	}
	
	function approveYES() 
	{
		$("#notes").val("AjukanLossMember");		
		$('input:radio[name=status]').filter('[value=10]').prop('checked', true);
		
		$("#form_confirmdeposit").submit();
	}
	function approveNO() 
	{		
		$("#notes").val("AjukanLossMember");
		// $("#status").val("2");
		$('input:radio[name=status]').filter('[value=2]').prop('checked', true);
		
		$("#form_confirmdeposit").submit();
	}
	
</script>
<?php 
	$formName = '"#form_confirmdeposit"';
	$feedback = '"#feedback_confirm"';
	
	include "inc_doscript.php";
	
	mysqli_close($conn);
?>
