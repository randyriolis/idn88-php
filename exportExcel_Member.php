<?php
	require_once __DIR__.'/vendor/autoload.php';
	include_once "inc_login.php";
	include_once "config.php";

	use PhpOffice\PhpSpreadsheet\RichText\RichText;
	use PhpOffice\PhpSpreadsheet\Shared\Date;
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Style\Color;
	use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
	
	
	$process = $_REQUEST['process'];
	
	if(empty($process) )
	{
		mysqli_close($conn);
		exit;
	}
	
	$username = $_REQUEST['account'];
	$namalink = $_REQUEST['namalink'];
	$idgame = $_REQUEST['idgame'];
	$kdgame = $_REQUEST['kdgame'];
	$credit = $_REQUEST['credit'];
	$bank = $_REQUEST['bank'];
	$nama = $_REQUEST['nama'];
	$no_rekening = $_REQUEST['no_rekening'];
	$email = $_REQUEST['email'];
	$dateassign = $_REQUEST['dateassign'];
	$ip = $_REQUEST['ip'];
	$status = $_REQUEST['status'];
	$filter_bulan = $_REQUEST['filter_bulan'];
	$filter_tahun = $_REQUEST['filter_tahun'];
	
	$sort = $_REQUEST['sort'];
	
	
	$wherequery = " AND ln.status = 1 AND MONTH(from_unixtime(mem.tanggal_daftar)) = '$filter_bulan' AND YEAR(from_unixtime(mem.tanggal_daftar)) = '$filter_tahun' ";
	
	if($username != "")	$wherequery .= " AND mem.username LIKE '%$username%' ";
	if($idgame > 0)	$wherequery .= " AND mem.kdproduct = $idgame ";
	if($namalink != "")	$wherequery .= " AND ln.namalink like '%$namalink%' ";	
	if($kdgame != "")	$wherequery .= " AND gm.nama like '%$kdgame%' ";	
	if($nama != "")	$wherequery .= " AND mem.nama like '%$nama%' ";
	if($no_rekening != "")	$wherequery .= " AND (mem.norek like '%$no_rekening%' OR bk.namabank like '%$no_rekening%' OR bk.inisialbank like '%$no_rekening%') ";
	if($email != "")	$wherequery .= " AND (mem.email like '%$email%' OR mem.tlp like '%$email%') ";
	if($dateassign != "")	$wherequery .= " AND DATE(CONVERT_TZ($modtime,".$curtimezone.")) like '%$dateassign%' ";
	if($ip != "")	$wherequery .= " AND mem.dari_ip1 like '%$ip%' ";
	if($status != "")	$wherequery .= " AND mem.isactive = $status ";
	
	
	$orderquery = "ORDER BY mem.kdmember DESC ";
	if($sort != "")	
	{
		if($sort == "game_asc")		$orderquery = "ORDER BY gm.nama ASC ";
		else if($sort == "game_desc") $orderquery = "ORDER BY gm.nama DESC ";
		
		else if($sort == "namalink_asc")		$orderquery = "ORDER BY ln.namalink ASC ";
		else if($sort == "namalink_desc") $orderquery = "ORDER BY ln.namalink DESC ";
		
		else if($sort == "account_asc")		$orderquery = "ORDER BY acc.username ASC ";
		else if($sort == "account_desc") $orderquery = "ORDER BY acc.username DESC ";
		
		else if($sort == "bank_asc")		$orderquery = "ORDER BY mem.kdbank ASC ";
		else if($sort == "bank_desc") $orderquery = "ORDER BY mem.kdbank DESC ";
		
		else if($sort == "nama_asc")		$orderquery = "ORDER BY mem.nama ASC ";
		else if($sort == "nama_desc") $orderquery = "ORDER BY mem.nama DESC ";
		
		else if($sort == "norek_asc")		$orderquery = "ORDER BY mem.norek ASC ";
		else if($sort == "norek_desc") $orderquery = "ORDER BY mem.norek DESC ";
		
		else if($sort == "email_asc")		$orderquery = "ORDER BY mem.email ASC ";
		else if($sort == "email_desc") $orderquery = "ORDER BY mem.email DESC ";
		
		else if($sort == "dateassign_asc")		$orderquery = "ORDER BY mem.modtime ASC ";
		else if($sort == "dateassign_desc") $orderquery = "ORDER BY mem.modtime DESC ";
		
		else if($sort == "ip_asc")		$orderquery = "ORDER BY mem.dari_ip1 ASC ";
		else if($sort == "ip_desc") $orderquery = "ORDER BY mem.dari_ip1 DESC ";
		
		else if($sort == "status_asc")		$orderquery = "ORDER BY mem.isactive ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY mem.isactive DESC ";
	}
	
	$query = "SELECT mem.kdmember, acc.isassigned, mem.isactive, mem.tlp, mem.email, mem.norek, mem.nama AS namamember, bk.inisialbank AS namabank,  acc.kdakun, acc.username, gm.nama AS nama_game, gm.kodeagen AS hex_account, acc.modby AS nama_admin, ln.namalink ".
		",from_unixtime(mem.tanggal_daftar,'%Y-%m-%d %H:%i:%s') as new_datecreate ".
		",from_unixtime(acc.modtime,'%Y-%m-%d %H:%i:%s') as new_dateassign ".
		"FROM members mem ".
		"LEFT JOIN akuns acc ON acc.kdmember = mem.kdmember ".
		"LEFT JOIN products gm ON mem.kdproduct = gm.kdproduct ".
		"LEFT JOIN banks bk ON mem.kdbank = bk.kdbank ".
		"LEFT JOIN link ln ON mem.idlink = ln.idlink ".
		"WHERE 1=1 ".
		$wherequery.
		$orderquery;

	//$query = "SELECT * FROM sample_datas";

	$result = mysqli_query($conn, $query);
			
	$spreadsheet = new Spreadsheet();

	// Set document properties
	$spreadsheet->getProperties()->setCreator('Dev IDN88')
	    ->setLastModifiedBy('Dev IDN88')
	    ->setTitle('Office 2007 XLSX Export Document Member')
	    ->setSubject('Office 2007 XLSX Export Document Member')
	    ->setDescription('Member document for Office 2007 XLSX, generated using PHP classes.')
	    ->setKeywords('office 2007 openxml php')
	    ->setCategory('Export data member');

	// Add some data
	/*$spreadsheet->setActiveSheetIndex(0)
	    ->setCellValue('A1', 'Hello')
	    ->setCellValue('B2', 'world!')
	    ->setCellValue('C1', 'Hello')
	    ->setCellValue('D2', 'world!');*/

  	$active_sheet = $spreadsheet->getActiveSheet();

  	$active_sheet->setCellValue('A1', '#');
  	$active_sheet->setCellValue('B1', 'USERNAME');
  	$active_sheet->setCellValue('C1', 'WEBSITE');
  	$active_sheet->setCellValue('D1', 'NAMA PEMAIN');
  	$active_sheet->setCellValue('E1', 'NO.REKENING');
  	$active_sheet->setCellValue('F1', 'KONTAK PEMAIN');
  	$active_sheet->setCellValue('G1', 'EMAIL PEMAIN');
  	$active_sheet->setCellValue('H1', 'GAME');
  	$active_sheet->setCellValue('I1', 'TANGGAL ASSIGN');
  	$active_sheet->setCellValue('J1', 'STATUS');

	// Rename worksheet
	$spreadsheet->getActiveSheet()->setTitle('Daftar Member '.$namabulan.' '.$filter_tahun);

  	$active_sheet->getStyle('A1:J1')->getFont()->setBold(true);
	$active_sheet->getStyle("A2:J2")->getFont()->setBold(true);

  	$count = 2;
  	$jumlahBaris = 1;

  	foreach($result as $row)
  	{
    	$active_sheet->setCellValue('A' . $count, $jumlahBaris);
    	$active_sheet->setCellValue('B' . $count, $row["username"]);
    	$active_sheet->setCellValue('C' . $count, $row["namalink"]);
    	$active_sheet->setCellValue('D' . $count, $row["namamember"]);
    	$active_sheet->setCellValue('E' . $count, $row["norek"]. " - ".$row["namabank"]);
    	$active_sheet->setCellValue('F' . $count, $row["tlp"]);
    	$active_sheet->setCellValue('G' . $count, $row["email"]);
    	$active_sheet->setCellValue('H' . $count, $row["nama_game"]);

		$active_sheet->setCellValue('I' . $count, ($row['kdakun'] == 0) ? "belum assign" : date("d-m-Y h:i:s", strtotime($row["new_dateassign"])));
		$active_sheet->setCellValue('J' . $count, ($row["isactive"] == 1) ? "Active" : "Inactive");

    	/*if($row['kdakun'] == 0){
    		$active_sheet->setCellValue('I' . $count, "belum assign");
		}
		else {
			$active_sheet->setCellValue('I' . $count, date("d-m-Y h:i:s", strtotime($row["new_dateassign"])));
		}

		if($row["isactive"] == 0)	$active_sheet->setCellValue('I' . $count, "Inactive");
		else if($row["isactive"] == 1)	$active_sheet->setCellValue('I' . $count, "Active");*/


    	$count = $count + 1;
    	$jumlahBaris ++;
  	}

  	$worksheet = $spreadsheet->getActiveSheet();
	foreach($sheet as $row => $columns) {
		foreach($columns as $column => $data) {
			// echo $column. ' '.$row.' = ' . $data.'<br/>';			
			$worksheet->setCellValueByColumnAndRow($column, $row + 1, $data);
			
		}
	}
	
	$jumlahBaris += 5;

	$active_sheet->getStyle('H3:H'.$jumlahBaris)->getNumberFormat()->setFormatCode('#,##0');
	
	// Merge Title
	//$active_sheet->mergeCells('A1:B1');

	// auto size
	foreach(range('A','J') as $columnID) {
		$active_sheet->getColumnDimension($columnID)->setAutoSize(true);	
	}

  	$arrbulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$namabulan = $arrbulan[$filter_bulan - 1];

  	$file_name = "Daftar_member_".$namabulan."_". $filter_tahun ."_". time() . '.' . strtolower($_REQUEST["file_type"]);

  	//header('Content-Type: application/x-www-form-urlencoded');
	//header('Content-Transfer-Encoding: Binary');
	//header("Content-disposition: attachment; filename=\"".$file_name."\"");

	// Redirect output to a client’s web browser (Xlsx)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$file_name.'"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// If you're serving to IE over SSL, then the following may be needed
	//header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	//header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

  	$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, $_REQUEST["file_type"]);
  	$writer->save('php://output');
  	//$writer->save($file_name);

  	//readfile($file_name);
	//unlink($file_name);
  
	mysqli_close($conn);
?>