<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$menu = $_REQUEST['m'];
	$idannouncement = $_REQUEST['id'];
	
	$post = $_REQUEST['post'];
	if($post == "post"){
		
		$judul = $_REQUEST['judul'];
		$isi = $_REQUEST['isi'];
		$tanggal_mulai = date("Y-m-d", strtotime($_REQUEST['tanggal_mulai']));		
		$tanggal_akhir = date("Y-m-d", strtotime($_REQUEST['tanggal_akhir']));	
		$kepentingan = $_REQUEST['kepentingan'];
		$idlink = $_REQUEST['idlink'];
		
		$err = "";
		$textSuccess = "";
		
		if($judul == "" || $isi == "")		$err .= "<br/>Text Announcement harus diisi";
		if($tanggal_mulai == "" || $tanggal_akhir == "")		$err .= "<br/>Tanggal Announcement harus diisi";
		
		
		if($err=="" && $menu == "edit"){					
			$query = "UPDATE backend_announcement SET 
						judul='$judul', isi='$isi', tanggal_mulai='$tanggal_mulai', tanggal_akhir='$tanggal_akhir', kepentingan='$kepentingan', idlink='$idlink'
						WHERE idannouncement=$idannouncement ";
			$textSuccess = "Announcement berhasil di-edit";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
		else if($err=="" && $menu == "create") {
			$query = "INSERT INTO backend_announcement (judul, isi, tanggal_mulai, tanggal_akhir, kepentingan, idlink) VALUES ('$judul', '$isi', '$tanggal_mulai', '$tanggal_akhir', '$kepentingan', '$idlink') ";
			$textSuccess = "Announcement berhasil di-create";
			
			if(mysqli_query($conn, $query))		$err .= "";
			else $err .= "<br/>Internetnya putus kali nih..";
		}
				
		if($err=="")	echo '<span style="color:green"> Sukses.<br/>'.$textSuccess.'</span><br/>';
		else 			echo '<span style="color:red"> Error'.$err.'</span><br/>';		
		
		mysqli_close($conn);
		exit();
	}
	
	if($menu == "edit"){
		$query = mysqli_query($conn, "SELECT * FROM backend_announcement WHERE idannouncement = $idannouncement ");
		$row= mysqli_fetch_array($query);		
		
		$title = "UPDATE Admin Announcement: ".$idannouncement;
		
		$tanggal_mulai = date("d-m-Y", strtotime($row["tanggal_mulai"]));
		$tanggal_akhir = date("d-m-Y", strtotime($row["tanggal_akhir"]));
	}
	else if($menu == "create")
	{
		$row = array();
		
		$title = "Create Announcement";
		
		$tanggal_mulai = date("d-m-Y");
		$tanggal_akhir = date("d-m-Y");
	}
		
	
?>

<div class="announcement-update">
	<h1><?php echo $title ?></h1>
	<form id="form_announcement" class="form-vertical" action="doadminAnnouncement_menu.php" method="post">
		<input type="hidden" name="m" value="<?php echo $menu ?>" >
		<input type="hidden" name="id" value="<?php echo $idannouncement ?>" >
		<input type="hidden" name="post" value="post" >
		
		<div class="row">			
			<div class="col-sm-4">		
				<div class="form-group field-announcement-url required">
					<label class="control-label" for="announcement-url">Subject</label>
					<input type="text" name="judul" value="<?php echo $row["judul"] ?>" class="form-control"  />

					<div class="help-block"></div>
				</div>
			</div>
		</div>
		
		<div class="row">
			
			<div class="col-sm-11">		
				<div class="form-group field-announcement-url required">
					<label class="control-label" for="announcement-url">Content</label>
					<textarea rows="5" cols="100" id="announcement-url" class="form-control" name="isi" placeholder="Announcement Content"><?php echo $row['isi'] ?></textarea>

					<div class="help-block"></div>
				</div>
			</div>
			
		</div>

		<div class="row" >
			<div class="col-sm-8" >
				<div class="form-group field-announcement-status required">
					<label class="control-label">Tanggal</label>
					<div id="announcement-status">
						<input type="text" name="tanggal_mulai" value="<?php echo $tanggal_mulai ?>" class="form-control form_date" style="width: 100px; display: initial;" /> -
						<input type="text" name="tanggal_akhir" value="<?php echo $tanggal_akhir ?>" class="form-control form_date" style="width: 100px; display: initial;" />
						
					<div class="help-block"></div>
					</div>
				</div>
			</div>			
		</div>
		
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group field-announcement-status required">
					<label class="control-label">Kepentingan</label>
					
					<select id="kepentingan" class="form-control" name="kepentingan">
						<option value="1" <?php if($row["kepentingan"] == 1) echo 'selected'; ?> >Normal</option>
						<option value="2" <?php if($row["kepentingan"] == 2) echo 'selected'; ?>>Penting</option>
						<option value="3" <?php if($row["kepentingan"] == 3) echo 'selected'; ?>>Urgent</option>
					</select>

					<div class="help-block"></div>
				</div>
			</div>
			
			<div class="col-sm-4">
				<div class="form-group field-announcement-status required">
					<label class="control-label">Website</label>
					
					<select id="idlink" class="form-control" name="idlink">
						<option value="0">All Link</option>
						<?php 
							$querylink = "SELECT * FROM link WHERE status = 1";
							$reslink = mysqli_query($conn, $querylink);
							
							while($tmplink = mysqli_fetch_array($reslink)) {
								$idl = $tmplink["idlink"]
						?>
							<option value="<?php echo $idl ?>" <?php if($row["idlink"] == $idl) echo 'selected'; ?> ><?php echo $tmplink["namalink"] ?></option>
						
						<?php
							}
						?>
					</select>

					<div class="help-block"></div>
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
	
		<div class="row">
			<div class="col-sm-4">
				<div style="text-align: left; margin-top: 20px">
					<div id="feedback_edit"></div>
					<img class="thisLoadingGif" src="img/loading.gif" />
					<button type="reset" class="btn btn-default">Reset</button> 
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</div>
	
	</form>
</div>

<script>

$(document).ready(function(){
	// date click
	$( ".form_date" ).datepicker({
		dateFormat: 'dd-mm-yy'
	});
});

</script>

<?php 
	$formName = '"#form_announcement"';
	$feedback = '"#feedback_edit"';
	
	include_once "inc_doscript.php";
	
	mysqli_close($conn);
?>
