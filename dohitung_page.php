<?php 
	$prevClass = $page ==1? 'class="prev disabled"' : 'class="prev enabled"';
	$nextClass = $page >=$maxPage? 'class="next disabled"' : 'class="next enabled"';
	
	$max = $page + 4;		
	$min = $page - 5;
	
	if($min <= 1 || $max <= 10)		$min = 1;
	if($max <= 10 || $page <= 5)	$max = 10;		
	if($max > $maxPage) $max = $maxPage;
	
	if($max <= 1)	$max = 1;
?>