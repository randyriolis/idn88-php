<?php
	include_once "config.php";
	include_once "function.php";
	include_once "inc_login.php";

	$err = array();
	$arrstyle = array();	
	$arrstyle2 = array();
	
	
	$address = "../../public_html/images";
		
	$post = $_REQUEST['post'];
	if(!empty($post))
	{
		$target_file = $address . "/popup-baru.jpg";
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		
		$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		if($check !== false) {
			$uploadOk = 1;
		} else {
			$msg = "<span style='color:red'>File is not an image.</span>";
			$uploadOk = 0;
		}
		
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
			$msg = "<span style='color:red'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</span>";
			$uploadOk = 0;
		}
		
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
			$msg = "<span style='color:red'>Sorry, your file was not uploaded.</span>";
		// if everything is ok, try to upload file
		} else {
			if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
				$msg = "<span style='color:green'>Upload gambar popup berhasil</span>";
			} else {
				$msg = "<span style='color:red'>Sorry, there was an error uploading your file.</span>";
			}
		}
		
	}
	
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Popup Bandarsport</title>    	
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "popup";
		include_once "inc_header.php";
		
	?>

	<div class="content-wrapper">
		<section class="content-header">
			<h1></h1>        
			<ul class="breadcrumb"><li><a href="site.php">Home</a></li>
				<li class="active">Popup</li>
			</ul>    
		</section>

		<section class="content">
			<div class="change-own-password">
				<h2 class="lte-hide-title">Popup</h2>
				<div class="panel panel-default">
					<div class="panel-body">
						
						<?php 
							if(empty($msg) == false)
							{
								echo '<div style="text-align: center;">'.$msg.'</div>';
							}
						?>
		
						<div class="user-form">
							<form id="user" class="form-horizontal" action="" method="post" role="form" enctype="multipart/form-data">
								<input type="hidden" name="post" value="post">
								
								<div class="form-group field-changeownpasswordform-current_password required">
									<label class="control-label" for="" style="padding-left: 10px;">Current Popup</label>
									<div class="col-sm-12">
										<?php 
											$new_address = str_replace("public_html/", "", $address);											
											$imgs = "http://112.140.187.81/~bandarsport/images/popup-baru.jpg?".time();
											
										?>
										<img src="<?php echo $imgs ?>" style="max-width: 300px;" />
									</div>
								</div>			
								
								<div class="form-group field-changeownpasswordform-password required">
									<label class="control-label" for="" style="padding-left: 10px;">Upload new popup</label>
									<div class="col-sm-12">
										<input type="file" name="fileToUpload" id="fileToUpload">
									</div>
								</div>
								
								<div class="form-group" style="padding-top: 30px;">
									<div class="col-sm-9">										
										<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Save</button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>

			</div>
		</section>
	</div>

<?php 
	include_once "inc_script.php";
?>
<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		refreshContent();
		
		jQuery('#modalcreate').modal({"show":false});
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});	
	
	
</script>
<?php 
	// close
	mysqli_close($conn);
?>

</body>

</html>