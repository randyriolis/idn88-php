<?php 
	include_once "inc_login.php";
	include "config.php";
	
	$delete = $_REQUEST['delete'];
	if($delete != ""){		
		// delete account
		$query = "DELETE FROM user WHERE id = $delete ";
		$result = mysqli_query($conn, $query);
	}

	// all request
	$page = $_REQUEST['page'];
	$username = $_REQUEST['username'];
	$password_hash = $_REQUEST['password_hash'];	
	$status = $_REQUEST['status'];
	$superadmin = $_REQUEST['superadmin'];
	$idlink = $_REQUEST['idlink'];
	
	$sort = $_REQUEST['sort'];
	$maxrow = $_REQUEST['maxrow'];
	
	$wherequery = "";
	
	if($username != "")	$wherequery .= " AND username LIKE '%$username%' ";
	if($password_hash != "")	$wherequery .= " AND password_hash LIKE '%$password_hash%' ";
	if($hex_password != "")	$wherequery .= " AND hex_password like '%$hex_password%' ";
	if($superadmin != "")	$wherequery .= " AND superadmin = $superadmin ";
	if($status != "")	$wherequery .= " AND status = $status ";
	if($idlink != "")	$wherequery .= " AND idlink = $idlink ";
	
	
	$orderquery = "ORDER BY us.id ASC ";
	if($sort != "")	
	{
		if($sort == "username_asc")		$orderquery = "ORDER BY username ASC ";
		else if($sort == "username_desc") $orderquery = "ORDER BY username DESC ";
		
		else if($sort == "password_hash_asc")		$orderquery = "ORDER BY password_hash ASC ";
		else if($sort == "password_hash_desc") $orderquery = "ORDER BY password_hash DESC ";
		
		else if($sort == "superadmin_asc")		$orderquery = "ORDER BY superadmin ASC ";
		else if($sort == "superadmin_desc") $orderquery = "ORDER BY superadmin DESC ";
				
		else if($sort == "status_asc")		$orderquery = "ORDER BY status ASC ";
		else if($sort == "status_desc") $orderquery = "ORDER BY status DESC ";
		
		else if($sort == "idlink_asc")		$orderquery = "ORDER BY idlink ASC ";
		else if($sort == "idlink_desc") $orderquery = "ORDER BY idlink DESC ";		
		
	}
	
	$start = ($page-1) * 20;
	
	$limitquery = " ";
	if($maxrow >= 9999)	$limitquery = "";	
	
	
	
	$query_ext = "SELECT us.id, us.username ".
				   ",CONVERT_TZ(dp.dateassign,".$curtimezone.") as new_dateassign ".
				   ",CONVERT_TZ(dp.datecreate,".$curtimezone.") as new_datecreate ".
				   
				   "FROM user us ".
				   "LEFT JOIN deposit dp ON us.id = dp.iduser ".
				   "WHERE 1=1 AND dp.dateassign IS NOT NULL  ";	
				   
    $query_ext_2 = "SELECT us.id, us.username ".
				   ",CONVERT_TZ(mem.dateassign,".$curtimezone.") as new_dateassign ".
				   ",CONVERT_TZ(mem.datecreate,".$curtimezone.") as new_datecreate ".
				   
				   "FROM user us ".
				   "LEFT OUTER JOIN member mem ON us.id = mem.iduser ".
				   "WHERE 1=1 AND mem.dateassign IS NOT NULL  ";	
				   
	$query_ext_3 = "SELECT us.id, us.username ".
				   ",CONVERT_TZ(wd.dateassign,".$curtimezone.") as new_dateassign ".
				   ",CONVERT_TZ(wd.datecreate,".$curtimezone.") as new_datecreate ".
				   
				   "FROM user us ".
				   "LEFT JOIN withdraw wd ON us.id = wd.iduser ".
				   "WHERE 1=1 AND wd.dateassign IS NOT NULL  ";	
	
	$minute_diff = "TIMESTAMPDIFF(MINUTE, new_datecreate, new_dateassign) ";
	$query_select = "SELECT id, username, Month(`new_dateassign`) as bulan, Year(`new_dateassign`) as tahun, new_dateassign, new_datecreate, 
							   $minute_diff as minute_diff, 
								IF($minute_diff < 3, 1, 0) as HIJAU, 
								IF($minute_diff > 3 AND $minute_diff< 5, 1, 0) as KUNING, 
								IF($minute_diff > 5, 1, 0) as MERAH ";
								
	
    $query = "SELECT id, username, bulan, tahun, 
					COUNT(username) as jumlah_transaksi,
					SUM(minute_diff) as minute_diff,
					SUM(HIJAU) as HIJAU,
					SUM(KUNING) as KUNING,
					SUM(MERAH) as MERAH
					FROM (
						$query_select FROM ($query_ext) AS alias_dp 
						UNION ALL $query_select FROM ($query_ext_2) AS alias_mem 
						UNION ALL $query_select FROM ($query_ext_3) AS alias_wd 
					) foo 
					WHERE bulan IS NOT NULL AND minute_diff > 0
					GROUP BY tahun, bulan, username 
					ORDER BY tahun DESC, bulan DESC, username ASC";
					
	$result = mysqli_query($conn, $query);
	$cnt = $start;
	$countMonth = 0;
	
	$prevmonth = 0;
	
	while($row = mysqli_fetch_assoc($result)) {
	
		if($row['bulan'] != $prevmonth) {
			$cnt = 0;
			$prevmonth = $row['bulan'];
			$countMonth ++;
			
			if($countMonth % 2== 0) 		$style_bgcolor = 'style="background-color:#ddd" ';
			else $style_bgcolor = 'style="background-color:#eee" ';
			?>
				<tr>
					<td colspan="6" style="height:10px;">
					
					</td>
				</tr>
				
				<tr <?php echo $style_bgcolor ?>>
					<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:160px;" data-col-seq="0">
						<b>
						<?php
							$dateObj   = DateTime::createFromFormat('!m', $row['bulan']);
							$monthName = $dateObj->format('F'); // March
							echo $monthName.' '.$row['tahun'];
						?>
						</b>
					</td>
					<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="1">											
						<b>Nama Admin</b>
					</td>
					
					<td class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="2">											
						<b>Transaksi</b>
					</td>
					
					<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:250px;" data-col-seq="4">
						<b>Durasi Approve</b>
					</td>
					<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:80px;" data-col-seq="6">											
						<span class="label label-success">Hijau</span>
					</td>
					<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:80px;" data-col-seq="6">											
						<span class="label label-warning">Kuning</span>
					</td>
					<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:80px;" data-col-seq="6">											
						<span class="label label-danger">Merah</span>
					</td>
				</tr>
			<?php
		}
		
		$cnt ++;
?>
	<tr data-key="<?php echo $row['id'] ?>" <?php echo $style_bgcolor ?>>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:36px;" data-col-seq="0"><?php echo $cnt ?></td>
		<td class="kv-align-center kv-align-middle" data-col-seq="1">
			<?php
				$class = "";
				
				if($row['superadmin'] == 0)	{
					$class = "default";
				}
				else if($row['superadmin'] == 2)	{
					$class = "warning";
				}
				else if($row['superadmin'] == 1)	{
					$class = "danger";
				}
				
				echo '<a class="btn btn-'.$class.' btn-sm modalButton" type="button" id="modalButton" value="doadmin_log.php?iduser='.$row['id'].'&rd='.rand(10,100).'" href="#" onClick="return false;" style="min-width:50px;">
								'.$row['username'].'
							</a>';
							
			?>
			
		</td>
		<td class="kv-align-center kv-align-middle" data-col-seq="2">
			<?php echo $row['jumlah_transaksi'] ?>
		</td>
		
		<td class="kv-align-center kv-align-middle" data-col-seq="2">
			<span class="ratarata">
				<?php 
					$tmp = $row['minute_diff'];
					$jam = floor($tmp / 60);
					$menit = $tmp % 60;
					
					if($jam > 0)	echo $jam.' jam ';
					echo $menit.' menit';
				?>
				<span class="tooltiptext">
					Rata-rata transaksi kira-kira: <br />
					<?php
						$hitung = round($row['minute_diff'] / $row['jumlah_transaksi']);
						$jam = floor($hitung / 60);
						$menit = $hitung % 60;
						
						if($jam > 0)	echo $jam.' jam ';
						echo $menit.' menit';
					?>
				</span>
			</span>
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:100px; background-color:lightgreen" data-col-seq="5">
			<?php 
				echo $row['HIJAU'];
			?>
		</td>		
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:100px;  background-color:yellow" data-col-seq="6">
			<?php 
				echo $row['KUNING'];
			?>	
		</td>
		<td class="kartik-sheet-style kv-align-center kv-align-middle" style="width:100px;  background-color:pink" data-col-seq="6">
			<?php 
				echo $row['MERAH'];
			?>	
		</td>		
	</tr>



<?php } ?>

<?php
	$query = "SELECT count(id) as num_rows FROM user WHERE id > 0  ".$wherequery.$orderquery.$limitquery;
	$result = mysqli_fetch_assoc(mysqli_query($conn, $query));
	$rowTotal = $result['num_rows'];
	$rowsPerPage = 20;	
	$maxPage = ceil($rowTotal / $rowsPerPage);
	
	//paging
	if($maxrow <= 20)
	{		
		include_once "dohitung_page.php";
	
?>

	<!-- paging -->
	<tr><td colspan="6">
	<div class="kv-panel-after"></div>
	
	<div class="panel-footer">    
		<div class="kv-panel-pager">
			<ul class="pagination">
				<li <?php echo $prevClass; ?> onClick="clickPage(<?php echo $page-1 ?>)"><span>&laquo;</span></li>
				
				<?php 
					for($aa=$min; $aa<=$max; $aa++){
						$act = $aa==$page? 'class="active"' : '';
						echo '<li '.$act.'><a href="#" onClick="clickPage('.$aa.'); return false;">'.$aa.'</a></li>';
					}
				?>							
				
				<li <?php echo $nextClass; ?> onClick="clickPage(<?php echo $page+1 ?>)"><span>&raquo;</span></li>
			</ul>
		</div>		
		<div class="clearfix"></div>
	</div>
	</td></tr>
	
<?php } ?>

<script>
	function deleteAdmin(idaccount)
	{
		var str = "Apakah Kamu yakin delete Admin ini ?? ";
		var aa = confirm(str);
		if(aa)
		{
			$("#delete").val(idaccount);
			refreshContent();
		}
	}
	
	$(document).ready(function()
	{	
		$('.modalButton').each(function (){
			$(this).click(function (){								
				$('#modalcreate').modal('show')
					.find('#modalContent')
					.load($(this).attr('value'));
			});
			
		});
		
	});
	
	function clickPage(whatPage){
		$("#page").val(whatPage);
		refreshContent();
	}
	
	<?php 
		if($maxrow <= 20)
		{
			$mx = $page * 20;
			if($mx > $rowTotal)	$mx = $rowTotal;
			$str = ((($page-1) * 20)+1)."-".$mx;
			
			echo '$("#page_number").html("Showing <b>'.$str.'</b> of <b>'.$rowTotal.'</b> items.");';
		}
		else
		{
			// show all
			echo '$("#page_number").html("Total <b>'.$rowTotal.'</b> items.");';
		}
		
		echo '$("#totalrow").val("'.$rowTotal.'")';
	?>
	
	
	<?php 
		// SORT
		mysqli_close($conn);
	?>
</script>

<style>
	
	/* Tooltip container */
	.ratarata {
		position: relative;
		display: inline-block;
		border-bottom: 1px dotted #999; /* If you want dots under the hoverable text */
		cursor:help;
	}

	/* Tooltip text */
	.ratarata .tooltiptext {
		visibility: hidden;
		width: 180px;
		background-color: black;
		color: #fff;
		text-align: center;
		padding: 5px 0;
		border-radius: 6px;
		opacity: 0.7;
	 
		/* Position the tooltip text - see examples below! */
		position: absolute;
		z-index: 1;
	}

	/* Show the tooltip text when you mouse over the tooltip container */
	.ratarata:hover .tooltiptext {
		visibility: visible;
	}
</style>