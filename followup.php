<?php
	include_once "inc_login.php";
	include_once "config.php";
	
	$page = $_REQUEST['page'];
	if(empty($page)) $page = 1;
	
	$idnotification = $_REQUEST["ntf"];
	
	$depo = $_REQUEST["d"];
	if(empty($depo))	$depo = 0;
	
	$idmember = $_REQUEST["id"];
	$popup = $_REQUEST["popup"];
	$acc = $_REQUEST["acc"];
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	
	// Remove the notification
	if(empty($acc) == false) {
		$querynot = "UPDATE notification SET status = 0 WHERE idnotification = $idnotification";
		mysqli_query($conn, $querynot);
	}
	
	if($levelAdmin == 5) {
		header("location:berita.php");
		exit();
	}
	
	/***********************************************/
	// followup_status
	// 0 -> Kemungkinan Bagus
	// 1 -> Kemungkinan Sedang
	// 2 -> Kemungkinan Kecil
	// 5 -> Sudah Depo
	// 10 -> Loss Member
	// 99 -> Waiting
	/***********************************************/
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Follow Up Member</title>
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "followup";
		
		if($depo == 0)			$subact = "followup_all";
		else if($depo == 1)		$subact = "followup_non";
		else if($depo == 2) 	$subact = "followup_depo";
		else if($depo == 3) 	$subact = "loss_member";
		
		include_once "inc_header.php";
		
		
	?>
	
	<div class="alert alert-warning" id="copy_clipboard" style="position: fixed; left: 50%;  transform: translate(-50%, -50%); top: 70px; display:none; z-index:100000 ">
		abc
	</div>

	<form id="form_search" class="form-inline" action="doprint_followup.php" method="post">
				
	<div class="content-wrapper">
	
		<div class="profile_pemain">
			
			<div class="pull-left" style="margin-left:20px;">
				<span style="font-size: 18px;">
					<?php
						if($depo == 0) 			echo 'Followup Member All';
						else if($depo == 1) 	echo 'Followup Member';
						else if($depo == 2)		echo 'Followup Member Depo';
						else if($depo == 3)		echo 'Loss Member';
					?>
				</span>	
			</div>
			
			<div class="pull-left" style="margin-left:30px;">				
				
				<?php if($depo == 2) { ?>
				<select class="" name="filter_hari" id="filter_hari"  style=" height: 30px; padding-left: 5px" onchange="refreshContent()">
					<option value="1">0 - 7 Hari</option>
					<option value="2">8 - 30 Hari</option>
					<option value="3">>30 Hari</option>
				</select>
				<?php } ?>
				
				<input type="hidden" id="page" name="page" value="<?php echo $page ?>" />
				<input type="hidden" id="sort" name="sort" value="" />
				<input type="hidden" id="totalrow" name="totalrow" value="" />
				<input type="hidden" id="maxrow" name="maxrow" value="20" />
				
				<input type="hidden" id="temp_function" name="temp_function" value="" />
				<input type="hidden" id="temp_idmember" name="temp_idmember" value="" />
				<input type="hidden" id="temp_value" name="temp_value" value="" />				
				<input type="hidden" id="depo" name="depo" value="<?php echo $depo ?>" />
				<input type="hidden" id="idmember" name="idmember" value="<?php echo $idmember ?>" />
				<input type="hidden" id="popup" name="popup" value="<?php echo $popup ?>" />
				
				<input type="hidden" id="idnotification" name="idnotification" value="<?php echo $idnotification ?>" />
					
				<div id="loadingGif" class="btn-group" style="margin-right:20px; ">
					<img src="img/loading.gif" style="width:30px" />
				</div>
				
				
			</div>

			<div class="pull-right" style="padding-right: 5px;">
				<div class="btn-toolbar kv-grid-toolbar" role="toolbar">
						
					<?php if($levelAdmin == 1) { ?>						
					<div class="btn-group">
						<?php include "tombolExportExcel_followup.php"; ?>
					</div>							
					<?php 
					}						
					?>
					
				</div>
			</div>					
		</div>

		<section class="content"  style="padding-top:0px;">
			<div class="withdraw-index">
				
				<div id="w0" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">    
				<div id="withdraw-id-pjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
					<div id="withdraw-id" class="grid-view hide-resize" data-krajee-grid="kvGridInit_7e6ea3f8">
						<div class="panel panel-primary">
							
							<div id="withdraw-id-container" class="table-responsive kv-grid-container">
							<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
								<colgroup>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
								</colgroup>
								<thead>

									<tr>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="max-width:150px;">
											#
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											<a href="#" onClick="return trysort('account');" id="account" class="sorter">Username</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											<a href="#" onClick="return trysort('nama');" id="nama" class="sorter">Nama</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											<a href="#" onClick="return trysort('asal');" id="asal" class="sorter">Asal</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											<a href="#" onClick="return trysort('telepon');" id="telepon" class="sorter">Telepon</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											<a href="#" onClick="return trysort('dateassign');" id="dateassign" class="sorter">Tanggal Join</a>											
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											<a href="#" onClick="return trysort('status');" id="status" class="sorter">Status</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">																						
											<a href="#" onClick="return trysort('latestdp');" id="latestdp" class="sorter">Latest Deposit</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">
											<a href="#" onClick="return trysort('latestdp_date');" id="latestdp_date" class="sorter">Tgl Latest DP</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											<a href="#" onClick="return trysort('latestfu');" id="latestfu" class="sorter">Latest FU</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">
											Reminder
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											Edit
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle">											
											Detail
										</th>
										
									</tr>
									
									<tr id="w1-filters" class="filters skip-export">											
										<td>&nbsp;</td>
										<td><input type="text" class="form-control" name="username" id="username_form" style="width:80px"></td>
										<td><input type="text" class="form-control" name="nama" id="nama_form" style="width:130px"></td>
										<td><input type="text" class="form-control" name="asal" id="asal_form" style="width:100%"></td>
										<td><input type="text" class="form-control" name="no_hp" id="no_hp_form" style="width:100%"></td>
										<td><input type="text" class="form-control" name="datecreate" id="datecreate_form" style="width:100%"></td>
										<td>
											<select class="form-control" name="status" id="status_form" onchange="refreshContent();" style="width:60px;">
												<option value=""></option>
												<option value="0">Kemungkinan Bagus</option>
												<option value="1">Kemungkinan Sedang</option>
												<option value="2">Kemungkinan Kecil</option>
												<option value="5">Sudah Depo</option>
												<option value="10">Loss Member</option>
											</select>
										</td>
										<td><input type="text" class="form-control" name="latestdp" id="latestdp_form" style="width:100%"></td>
										<td><input type="text" class="form-control" name="tgl_latestdp" id="tgl_latestdp_form" style="width:100%"></td>
										<td colspan="3"><input type="submit" style="display:none" />&nbsp;</td>
									</tr>
									
								</thead>
								
								<tbody id="tbody_content">
									
								</tbody>
							</table>
						</div>
						
						</div>
					</div>
				</div>
				</div>
			</div>

			<div id="myModalUpdate" class="fade modal" role="dialog" tabindex="-1">
			<div class="modal-dialog modal-md">
			<div class="modal-content">
			<div class="modal-body">

			</div>

			</div>
			</div>
			</div>
				


			</div>
		</section>
		
		</form>
	</div>

<?php 
	include_once "inc_script.php";
?>
<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		refreshContent();
		
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});	
	
	
	
	jQuery('#myModalUpdate').modal({"show":false});

    $('#myModalUpdate').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title') 
        var href = button.attr('href') 
        modal.find('.modal-title').html(title)
        modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data);
				$('#accountsearch-username').select2({
					dropdownParent: $('#myModalUpdate')
				});
            });
        })
	
	var prevvalue = "";
	$("#withdraw-jumlah").keyup(function(event)
	{		
			// skip for arrow keys
		  if(event.which >= 37 && event.which <= 40){
			  event.preventDefault();
		  }
		  var tmp = $(this);
		  var val = tmp.val();
		  console.log(val);
		  if(val == prevvalue)	return;
		  
		  val = val.replace(/\D/g,'');
		  prevvalue = val;
		  var num = val.replace(/,/gi, "").split("").reverse().join("");

		  var num2 = RemoveRougeChar(num.replace(/(.{3})/g,"$1,").split("").reverse().join(""));

		  // the following line has been simplified. Revision history contains original.
		  tmp.val(num2);
	  });
	  function RemoveRougeChar(convertString){
		if(convertString.substring(0,1) == ","){

			return convertString.substring(1, convertString.length)            

		}
		return convertString;	
	}
	
</script>
<?php 
	// close
	mysqli_close($conn);
?>

</body>

</html>