<?php 
	if($action == ""){
		$action = "exportExcel_Member.php";
	}
?>

<form id="formexport" method="GET" action="<?php echo $action ?>">	
	<input type="hidden" id="process" name="process" value="OKE" />
	<input type="hidden" id="maxrow_export" name="maxrow" value="20" />
	<input type="hidden" id="sort_export" name="sort" value="" />
	
	<input type="hidden" id="account_export" name="account" value="" />
	<input type="hidden" id="namalink_export" name="namalink" value="" />
	<input type="hidden" id="nama_export" name="nama" value="" />
	<input type="hidden" id="no_rekening_export" name="no_rekening" value="" />
	<input type="hidden" id="email_export" name="email" value="" />
	<input type="hidden" id="kdgame_export" name="kdgame" value="" />
	<input type="hidden" id="credit_export" name="credit" value="" />
	<input type="hidden" id="dateassign_export" name="dateassign" value="" />
	<input type="hidden" id="ip_export" name="ip" value="" />
	<input type="hidden" id="status_export" name="status" value="" />

	<div class="btn-group" style="margin-left:10px;">
		<p><?php // echo date('m/d/Y', 1629817887); ?>
		<select name="filter_bulan" id="filter_bulan" class="input-sm">
			<?php 
				$arrbulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

				for($a=1; $a<=12; $a++)
				{
					$print = $arrbulan[$a-1];
					echo '<option value="'.$a.'">'.$print."</option>";
				}
			
			?>
		</select>
		
		<select name="filter_tahun" id="filter_tahun" class="input-sm">
			<?php
				$a = "2018";
				
				for($a=date("Y"); $a>=2018; $a--) {
					$print = "Tahun ". $a;
					echo '<option value="'.$a.'">'.$print."</option>";
				}
				
			?>
		</select>

		<select name="file_type" id="file_type" class="input-sm">
            <option value="Xlsx">Xlsx</option>
            <option value="Xls">Xls</option>
            <option value="Csv">Csv</option>
        </select>
	</div>	
	
	<div class="btn-group" style="margin-left:10px;">
		<a class="btn btn-warning" href="#" title="Reset Grid" data-pjax="0" onclick="submitExport(); return false;">
			Export
		</a>
	</div>
 
</form>

<style>#formexport select.input-sm { height: 34px; line-height: 34px; font-size: 14px; }</style>
<script>
	async function submitExport()
	{
		$.confirm({
	        //theme: 'dark',
	        title: "Proses export data",
	        content: "Pastikan sebelum klik proceed filter bulan & tanggal dahulu!!!",
	        icon: "fa fa-question-circle",
	        animation: "scale",
	        closeAnimation: "scale",
	        opacity: 0.5,
	        buttons: {
	            confirm: {
	                text: "Proceed",
	                btnClass: "btn-blue",
	                action: function () {
	                    exportMember();
	                },
	            },
	            cancel: function () {
	                $.alert("Kamu membatalkan proses ini");
	            },
	            /*moreButtons: {
	                text: "something else",
	                action: function () {
	                    $.alert("you clicked on something else");
	                },
	            },*/
	        },
	    });
	}

	function exportMember(){
		// Get some values from elements on the page:
		var $form = $("#formexport");
		var sent = $($form).serialize();	
		var url = $form.attr("action");
		
		// Send the data using post
		var posting = $.post( url, sent );
		
		$("#loadingGif").css("display","");

		$("#maxrow_export").val( $("#maxrow_form").val() );
		$("#sort_export").val( $("#sort_form").val() );
		
		$("#account_export").val( $("#account_form").val() );
		$("#namalink_export").val( $("#namalink_form").val() );
		$("#nama_export").val( $("#nama_form").val() );
		$("#no_rekening_export").val( $("#no_rekening_form").val() );
		$("#email_export").val( $("#email_form").val() );
		$("#kdgame_export").val( $("#kdgame_form").val() );
		$("#credit_export").val( $("#credit_form").val() );
		$("#dateassign_export").val( $("#dateassign_form").val() );
		$("#ip_export").val( $("#ip_form").val() );
		$("#status_export").val( $("#status_form").val() );
		
		
		document.getElementById("formexport").submit();
		
		// Put the results in a div
		posting.done(function( data ) {				  					
			$("#loadingGif").css("display","none");
		});
	}
</script>