<?php
	include_once "inc_login.php";
	include_once "config.php";
	
	$page = $_REQUEST['page'];
	if(empty($page)) $page = 1;
	$tgl = $_REQUEST['tgl'];
	
	$arrUser = mysqli_fetch_array( mysqli_query($conn, "SELECT * FROM _users WHERE kduser='$iduser' ") );
	$levelAdmin = $arrUser['superadmin'];
	$linkAssigned = $arrUser['idlink'];
	
	if($levelAdmin == 5) {
		header("location:berita.php");
		exit();
	}
	
	// UPDATE level B dan udah milih link || Consider it is level 0
	if($alpha_admb == 0 && $levelAdmin==2)		$levelAdmin = 0;
	
	
	if($levelAdmin==0){
		$tm = " AND gm.idlink=$linkAssigned ";
	}
	
	// QUERY
	$sqlgame = "SELECT gm.* FROM products gm WHERE gm.isactive = 1 $tm ORDER BY kdproduct ASC  ";		
	$result = mysqli_query($conn, $sqlgame);
		
	$arrgame = array();
	while($row = mysqli_fetch_assoc($result)) {
		array_push($arrgame, $row);
	}
	
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="img/potion_healingPotion.png" />
    <title>Laporan Menang/Kalah Pemain</title>
	
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/activeform.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link href="css/select2-addl.css" rel="stylesheet">
	<link href="css/select2-krajee.css" rel="stylesheet">
	<link href="css/kv-widgets.css" rel="stylesheet">
	<link href="css/kv-grid.css" rel="stylesheet">
	<link href="css/bootstrap-dialog.css" rel="stylesheet">
	<link href="css/jquery.resizableColumns.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/disable.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/AdminLTE.min.css" rel="stylesheet">
	<link href="css/skin-blue.min.css" rel="stylesheet">
	
	
</head>
<body class="hold-transition skin-blue sidebar-mini skin-blue">

	<?php
		$act = "laporan";
		$subact = "memberWinLose";		
		include_once "inc_header.php";
	?>
	
	<div class="alert alert-warning" id="copy_clipboard" style="position: fixed; left: 50%;  transform: translate(-50%, -50%); top: 70px; display:none; z-index:100000 ">
		abc
	</div>

	<div class="content-wrapper">
	
		<div class="profile_pemain">
			<div class="pull-left">
				<span style="font-size: 18px;">Laporan Menang/Kalah Pemain</span>	
			</div>
			
			<div class="pull-left" style="margin-left:10px; ">
				<span style="font-size: 16px;">Dari:</span>
				<input class="form_date" type="text" name="date_start" id="date_start" value="<?php echo $tgl ?>" />
				
				<span style="font-size: 16px;">Sampai:</span>
				<input class="form_date" type="text" name="date_end" id="date_end" value="<?php echo $tgl ?>" />
				
			</div>
			
			
			<div class="pull-left" style="margin-left:10px; ">
				<select class="form_select_shortcut" name="filter_date" id="filter_date" />
					<option value="">Select Shortcut</option>
					<option value="1">Hari Ini</option>
					<option value="2">Kemarin</option>
					<option value="3">Seminggu</option>
					<option value="4">Sebulan</option>
				</select>
				
				<button onClick="refreshContent()" style="margin-bottom: 4px;" class="btn btn-xs btn-primary">Filter</button>				
			</div>

			<div class="pull-right" style="padding-right: 5px;">
				<?php 
					$menu = "laporanMemberWinLose";
					include "tombolExportExcelLaporan.php";
				?>
			</div>
			
			
			<div class="pull-right" style="padding-right: 5px;">
				<div class="btn-toolbar kv-grid-toolbar" role="toolbar">
					
					<span style="font-size: 14px;">Search:</span>
					<input class="form_search" type="text" name="search_form" id="search_form" value="" />
				
				
					
				</div>
			</div>				
			
			
		</div>

		<section class="content"  style="padding-top:0px;">
			<div class="deposit-index">
				<!--
				<div class="account-form">
					<p>
					<form id="form_newdeposit" class="form-inline" action="docreate_deposit.php" method="post">
						<div class="form-group field-deposit-idmember required">
							<label class="control-label sr-only" for="deposit-idmember">Idmember</label>
							
							<select id="deposit-idmember" class="form-control js-example-basic-single" name="idmember">
								<option value="">-- Search Member  Nama / Nomer Rekening / Username --</option>
								<?php 
									$addquery = $levelAdmin==0? " AND ln.idlink=$idlink " : " ";
								
									$tempquery = "SELECT mem.nama, mem.idmember, acc.username, mem.no_rekening FROM ".
												   "member mem ".
												   "LEFT JOIN account acc ON acc.idaccount = mem.idaccount ".
												   "LEFT JOIN game gm ON acc.idgame = gm.idgame ".
												   "LEFT JOIN link ln ON ln.idlink = gm.idlink ".
												   "WHERE mem.idaccount IS NOT NULL AND ln.status=1 AND mem.status = 1 AND mem.assign = 1 ".$addquery;
									$tempresult = mysqli_query($conn, $tempquery);
									while($row = mysqli_fetch_array($tempresult)){
										echo '<option value="'.$row['idmember'].'">'.$row['nama'].' | '.$row['no_rekening'].' | '.$row['username'].'</option>
										';
									}
								?>
								
							</select>
						</div>
							
						<div class="form-group field-deposit-jumlah required">
							<label class="control-label sr-only" for="deposit-jumlah">Jumlah</label>
							<input type="text" id="deposit-jumlah" class="form-control" name="jumlah" maxlength="50" placeholder="Jumlah" />
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-success">Create Deposit</button>    
						</div>
						<div id="loadingGif_deposit" class="btn-group" style="margin-right:20px">
							<img src="img/loading.gif" />
						</div>

					</form>    
					</p>
				</div>
				-->
				<br/>
				<div id="w0" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">    
				<div id="deposit-id-pjax" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">
					<div id="deposit-id" class="grid-view hide-resize" data-krajee-grid="kvGridInit_7e6ea3f8">
						<div class="panel panel-primary">
							
							<div id="deposit-id-container" class="table-responsive kv-grid-container">
							<table class="kv-grid-table table table-hover table-bordered table-striped kv-table-wrap">
								<colgroup>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col>
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
									<col class="skip-export">
								</colgroup>
								<thead>

									<tr>
										<th class="kartik-sheet-style kv-align-center kv-align-middle kv-merged-header" style="width:36px;" rowspan="2" data-col-seq="0">#</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:140px;" data-col-seq="1">											
											<a href="#" onClick="return trysort('account');" id="account" class="sorter">Username</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="2">
											<a href="#" onClick="return trysort('namalink');" id="namalink" class="sorter">Website</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="3">
											<a href="#" onClick="return trysort('nama');" id="nama" class="sorter">Nama Pemain</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:100px;" data-col-seq="4">				
											<a href="#" onClick="return trysort('no_rekening');" id="no_rekening" class="sorter">No.Rekening</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" data-col-seq="5">
											<a href="#" onClick="return trysort('game');" id="game" class="sorter">Kode Game</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" style="width:220px;" data-col-seq="7">
											<a href="#" onClick="return trysort('totalDeposit');" id="totalDeposit" class="sorter">Total Deposit</a>
										</th>
										
										<th class="kartik-sheet-style kv-align-center kv-align-middle" >
											<a href="#" onClick="return trysort('totalWithdraw');" id="totalWithdraw" class="sorter">Total Withdraw</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" >
											<a href="#" onClick="return trysort('totalJumlah');" id="totalJumlah" class="sorter">Total Jumlah</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" >
											<a href="#" onClick="return trysort('totalBonus');" id="totalBonus" class="sorter">Total Bonus</a>
										</th>
										
										<th class="kartik-sheet-style kv-align-center kv-align-middle" >
											<a href="#" onClick="return trysort('credit');" id="credit" class="sorter">Credit Game</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" >
											<a href="#"  id="lastdeposit" class="sorter">Deposit Terakhir</a>
										</th>
										<th class="kartik-sheet-style kv-align-center kv-align-middle" >
											<a href="#"  id="lastwithdraw" class="sorter">Withdraw Terakhir</a>
										</th>
									</tr>
									
									<tr id="deposit-id-filters" class="filters skip-export" style="display: none;">
										<form id="form_search" class="form-inline" action="doprint_laporanMemberWinLose.php" method="post">
										
										<input type="hidden" id="page" name="page" value="<?php echo $page ?>" />
										<input type="hidden" id="sort" name="sort" value="" />
										<input type="hidden" id="totalrow" name="totalrow" value="" />
										<input type="hidden" id="maxrow" name="maxrow" value="20" />
										<input type="hidden" id="temp_id" name="temp_id" value="" />
										<input type="hidden" id="temp_function" name="temp_function" value="" />
										
										<input type="hidden" id="form_date_start" name="form_date_start"  value="<?php echo $tgl ?>" />
										<input type="hidden" id="form_date_end" name="form_date_end" value="<?php echo $tgl ?>" />
										<input type="hidden" id="form_filter_date" name="form_filter_date" value="" />
										<input type="hidden" id="form_search_form" name="form_search_form" value="" />
																				
										<td>
											<input type="text" class="form-control search_username" name="account" value="" style="min-width:112px;" />
										</td>
										<td style="min-width: 110px">
											<select id="accountsearch-namalink" class="form-control" name="namalink" onchange="refreshContent();">
											<option value="">All Website</option>
											<?php
												$rs = mysqli_query($conn, "SELECT * FROM link WHERE status=1 ");
												
												while($rw=mysqli_fetch_array($rs)) {
													echo '<option value="'.$rw['namalink'].'" >'.$rw['namalink'].'</option>';
												}
												
												for($i=0; $i<count($arrgame); $i++) {
														
													}
											?>
											</select>																			
										</td>
										<td><input type="text" class="form-control search_namapemain" name="nama"></td>
										<td><input type="text" class="form-control" name="no_rekening"></td>
										<td>
											<select id="accountsearch-idgame" class="form-control js-example-basic-single" name="idgame" onchange="refreshContent();" style="width:110px;">
												<option value="">Any Game</option>
												<?php 
													for($i=0; $i<count($arrgame); $i++) {
														echo '<option value="'.$arrgame[$i]['nama_game'].'" data-nama="'.$arrgame[$i]['hex_account'].'" data-pass="'.$arrgame[$i]['hex_password'].'" >
															'.$arrgame[$i]['nama_game'].'
														</option>';
													}
												?>						
											</select>
										</td>
										<td><input type="text" class="form-control" name="jumlah"></td>
										<td><input type="text" class="form-control" name="lastcredit"></td>
										<td><input type="text" class="form-control" name="bonus"></td>
										<td><input type="text" class="form-control" name="creditgame"></td>
										<td><input type="text" class="form-control" name="bankdeposit"></td>
										<td><input type="text" class="form-control" name="datecreate"></td>
										<td><input type="text" class="form-control" name="dateassign"></td>
										
										</form>
									</tr>

								</thead>
								
								<tbody id="tbody_content">
									
								</tbody>
							</table>
						</div>
						
						</div>
					</div>
				</div>
				</div>
			</div>

			<div id="myModallog" class="fade modal" role="dialog" tabindex="-1">
			<div class="modal-dialog ">
			<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>Log Deposit</h4>
			</div>
			<div class="modal-body">

			</div>

			</div>
			</div>
			</div>


			</div>
		</section>
		
	</div>

<?php 
	include_once "inc_script.php";
	// include_once "autocomplete_username.php";
	// include_once "autocomplete_namapemain.php";
?>
<script type="text/javascript" src="js/select2.full.js"></script>

<script>	
	$(document).ready(function()
	{
		refreshContent();
		
		$(".js-example-basic-single").select2();
		$("#loadingGif_deposit").css("display","none");
		
		$( ".form_date" ).datepicker({
			dateFormat: 'dd-mm-yy'
		});
		
		
		$("#date_start").change(function() {
			$("#form_date_start").val($(this).val());
			console.log("form_date_start = "+$(this).val());
			
			if($("#date_end").val() != "" ){
				refreshContent();
			}
		});
		$("#date_end").change(function() {
			$("#form_date_end").val($(this).val());
			console.log("form_date_end = "+$(this).val());
			
			if($("#date_start").val() != "" ){
				refreshContent();
			}
		});
		
		$("#search_form").change(function() {
			$("#form_search_form").val($(this).val());			
			console.log("search_form = "+$(this).val());
		});
		$("#filter_date").change(function() {
			$("#form_filter_date").val($(this).val());
		});
		
		$('#search_form').keypress(function(e) {
			if(e.which == 13) {
				$("#form_search_form").val($(this).val());			
				refreshContent();
			}
		});
		
	});
	
	// Attach a submit handler to the form
	$("#form_search").submit(function( event ) {
		event.preventDefault();
		refreshContent();
	});	
	
	
	// Attach a submit handler to the form
	$("#form_newdeposit").submit(function( event ) {
		event.preventDefault();
		
		var cont = true;
		
		if($("#deposit-idmember").val() == 0){
			$("#deposit-idmember").css("border", "1px solid red");
			cont = false;
		}	
		else $("#deposit-idmember").css("border", "1px solid lightgray");
		
		if($("#deposit-jumlah").val() == ""){
			$("#deposit-jumlah").css("border", "1px solid red");
			cont = false;
		}	
		else $("#deposit-jumlah").css("border", "1px solid lightgray");		
		
		if(cont == true){			
			// Get some values from elements on the page:
			var $form = $("#form_newdeposit");
			var sent = $($form).serialize();
			var url = $form.attr("action");
			
			// Send the data using post
			var posting = $.post( url, sent );
			
			$("#loadingGif_deposit").css("display","");
			
			// Put the results in a div
			posting.done(function( data ) {				
				$("#loadingGif_deposit").css("display","none");				
				if(data == "sukses"){
					refreshContent();
					
					// reset delete variables
					// $("#deposit-idmember").val("");
					$("#deposit-jumlah").val("");
					
					$("#deposit-idmember").css("border", "1px solid lightgray");
					$("#deposit-jumlah").css("border", "1px solid lightgray");
				}
				else{
					$("#deposit-idmember").css("border", "1px solid red");
					$("#deposit-jumlah").css("border", "1px solid red");
				}
			});
			
			refreshContent();
		}
	});
	
	
	jQuery('#myModallog').modal({"show":false});

    $('#myModallog').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var modal = $(this)
        var title = button.data('title') 
        var href = button.attr('href') 
        modal.find('.modal-title').html(title)
        modal.find('.modal-body').html('<i class="fa fa-spinner fa-spin"></i>')
        $.post(href)
            .done(function( data ) {
                modal.find('.modal-body').html(data)
            });
        })
	
	var prevvalue = "";
	$("#deposit-jumlah").keyup(function(event)
	{		
			// skip for arrow keys
		  if(event.which >= 37 && event.which <= 40){
			  event.preventDefault();
		  }
		  var tmp = $(this);
		  var val = tmp.val();
		  console.log(val);
		  if(val == prevvalue)	return;
		  
		  val = val.replace(/\D/g,'');
		  prevvalue = val;
		  var num = val.replace(/,/gi, "").split("").reverse().join("");

		  var num2 = RemoveRougeChar(num.replace(/(.{3})/g,"$1,").split("").reverse().join(""));

		  // the following line has been simplified. Revision history contains original.
		  tmp.val(num2);
	  });
	  function RemoveRougeChar(convertString){
		if(convertString.substring(0,1) == ","){

			return convertString.substring(1, convertString.length)            

		}
		return convertString;	
	}
	
</script>
<?php 
	// close
	mysqli_close($conn);
?>

</body>

</html>